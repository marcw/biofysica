function [samples, stats] = jags_pearson(x, y, varargin)
% JAGS_PEARSON Bayesian estimation of Pearson correlation using JAGS.
%
% This function computes the Pearson correlation coefficient between two vectors,
% x and y, by using Just Another Gibbs Sampler (JAGS). It returns Markov Chain Monte Carlo (MCMC)
% samples and summary statistics.
%
% Syntax:
%   [samples, stats] = jags_pearson(x, y)
%   [samples, stats] = jags_pearson(x, y, Name, Value)
%
% Inputs:
%   x - Numeric array, a vector of numerical values.
%   y - Numeric array, a vector of numerical values, must be of the same length as x.
%
% Optional Name-Value Pair Arguments:
%   'burnInSteps'    - Number, total number of burn-in steps (default = 500).
%   'numSavedSteps'  - Number, total number of steps in chains to save (default = 1000).
%   'thinSteps'      - Number, number of steps to thin (default = 1).
%   'saveName'       - String, filename for saving MCMC samples (default = 'Hier-BinomialTest-Jags-').
%   'nChains'        - Number, number of MCMC chains to run (default = 4).
%   'runJagsMethod'  - String, method for running JAGS ('serial' or 'parallel') (default = 'serial').
%   'dic'            - Logical, flag to indicate if Deviance Information Criterion should be calculated (default = false).
%
% Outputs:
%   samples - Struct, contains MCMC samples of various parameters including Pearson's 'r', 'mu', and 'sigma'.
%   stats   - Struct, contains summary statistics and diagnostics.
%
% Example:
%   [samples, stats] = jags_pearson(randn(100, 1), randn(100, 1), 'burnInSteps', 1000, 'nChains', 2);
%
% Dependencies:
%   - JAGS (Just Another Gibbs Sampler): <http://mcmc-jags.sourceforge.net/>
%   - MATJAGS: <http://psiexp.ss.uci.edu/research/programs_data/jags/> or <https://github.com/msteyvers/matjags>
%
% See also:
%   matjags
%
% References:
% Bayesian Cognitive Modeling (2013) Lee & Wagenmakers.

% Validate inputs
[x,y] = validate_inputs(x,y);

%% MCMC parameters
mcmc_params = get_mcmc_parameters(varargin);

%% Create JAGS Model and obtain file path
modelFilePath = write_model;

%% Prepare the data for JAGS
dataStruct = prepare_data(x,y);

%% Initialize chains
initsStruct			= initializeChains(mcmc_params.nChains);

%% Run MCMC using JAGS
[samples, stats] = run_jags(mcmc_params, dataStruct, modelFilePath, initsStruct);
end

% The following functions (validate_inputs, get_mcmc_parameters, write_model,
% prepare_data, initialize_chains, run_jags) contain the corresponding extracted
% logic for each section.

function [x,y] = validate_inputs(x,y)

if isempty(x) || isempty(y)
	error('All input arrays must be non-empty.');
end

% Ensure the input vectors are column vectors
if isrow(x)
	x = x';
end
if isrow(y)
	y = y';
end

end

function mcmc_params = get_mcmc_parameters(varargin)
chain_globals;


% Unpack inner cell array if varargin is nested
if length(varargin) == 1 && iscell(varargin{1})
	varargin = varargin{:};
end

% Default settings
mcmc_params.nChainsDefault = 4;
mcmc_params.runjagsMethodDefault = 'serial';

% Extract parameters or use defaults
p = inputParser;
addParameter(p, 'numSavedSteps', 1000);
addParameter(p, 'thinSteps', 1);
addParameter(p, 'burnInSteps', 500);
addParameter(p, 'saveName', 'Hier-BinomialTest-Jags-');
addParameter(p, 'nChains', mcmc_params.nChainsDefault);
addParameter(p, 'runjagsMethod', mcmc_params.runjagsMethodDefault);
addParameter(p, 'dic', false);

parse(p, varargin{:});
mcmc_params = p.Results;


% Calculate the number of iterations needed per chain
mcmc_params.nIter = ceil((mcmc_params.numSavedSteps * mcmc_params.thinSteps) / mcmc_params.nChains);

% Check if parallelization should be used
if strcmp(mcmc_params.runjagsMethod, 'parallel')
	mcmc_params.doparallel = 1;
else
	mcmc_params.doparallel = 0;
end


% Parameters to be observed
mcmc_params.parameters = {'r', 'mu', 'sigma'};
end

function modelFilePath = write_model
modelname		= 'pearson.txt';
fullFilePath	= which(modelname);
% Get folder, name, and extension
[folder, ~, ~]	= fileparts(fullFilePath);
modelFilePath	= fullfile(folder,modelname);
end

function dataStruct = prepare_data(x,y)
% THE DATA.
% N.B.: This function expects the data to be a structure,
% with one field 'z' being a vector of integer successes,
% one field 'N' being a vector of integer attempts,
% one field 's' being a vector of subject identifiers,
% and one field 'c' being a vector of category identifiers, with
% subjects nested in categories.

% Combine x and y into a single matrix
combinedData = [x, y];

% Constants
[numDataPoints, ~] = size(combinedData);



dataStruct.x		= combinedData;
dataStruct.n		= numDataPoints;

end

function initsStruct = initializeChains(nChains)
    % Initial values of MCMC chains based on data

initsStruct = struct([]);
for ii = 1:nChains
	initsStruct(ii).r		= 0;    % because data are standardized
	initsStruct(ii).mu		= zeros(1,2);        % because data are standardized
	initsStruct(ii).lambda	= ones(1,2);  % because data are standardized
end

end

function [samples,stats] = run_jags(mcmc_params, dataStruct, modelFilePath, initsStruct)


%% MCMC
fprintf( 'Running JAGS...\n' );
[samples, stats] = matjags( ...
	dataStruct, ...                     % Observed data
	modelFilePath, ...    % File that contains model definition
	initsStruct, ...                          % Initial values for latent variables
	'doparallel' , mcmc_params.doparallel, ...      % Parallelization flag
	'nchains', mcmc_params.nChains,...              % Number of MCMC chains
	'nburnin', mcmc_params.burnInSteps,...              % Number of burnin steps
	'nsamples', mcmc_params.nIter, ...           % Number of samples to extract
	'thin', mcmc_params.thinSteps, ...                      % Thinning parameter
	'dic',mcmc_params.dic, ...                       % Do the DIC?
	'monitorparams', mcmc_params.parameters, ...     % List of latent variables to monitor
	'savejagsoutput',0, ...          % Save command line output produced by JAGS?
	'verbosity',0, ...               % 0=do not produce any output; 1=minimal text output; 2=maximum text output
	'cleanup',1);                    % clean up of temporary files?



%% Thin
if mcmc_params.thinSteps>1
	samples = thinchain(samples,thinSteps);
end


%% Extract chain values:
samples = extractchain(samples); % from multiple -dimension matrix to 1- or 2-D


end