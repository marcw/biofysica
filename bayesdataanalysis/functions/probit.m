function phi = probit(x)
    % PROBIT computes the probit function, which is the quantile function 
    % associated with the standard normal distribution.
    %
    % Usage:
    %   phi = PROBIT(x)
    %
    % Input:
    %   x - A numeric array where each element is in the interval [0, 1]
    %
    % Output:
    %   phi - The corresponding probit values
    %
    % Example:
    %   PROBIT(0.5) returns 0
    %
    % See also ERFINV

    % Validate input
    if any(x < 0 | x > 1)
        error('Input x must be in the interval [0, 1]');
    end

    % Adjust extreme values to avoid Inf or -Inf in output
    x(x == 0) = 0.0001;
    x(x == 1) = 0.9999;

    % Compute the probit function
    phi = sqrt(2) * erfinv(2*x - 1);
end
