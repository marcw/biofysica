function varargout = betameankappa2ab(mu,kappa)
% betaAB = BETAMODEKAPPA2AB(MU,SD)
%
% Obtain the beta shape parameters betaAB.A and betaAB.B from the mean MU
% and dispersion KAPPA
%
% See also BETAMUSD2AB, BETAMODEKAPPA2AB

% Original in R:	Kruschke, J. K. (2014). Doing Bayesian Data Analysis:
%					A Tutorial with R and BUGS. Academic Press / Elsevier.
% Modified to Matlab code: Marc M. van Wanrooij

  
if mu<=0 || mu>1
	msgid		= 'bayesian:betaadfrommeansd:meanOutOfRange';
	errstr		= 'Must have 0 < mean < 1';
	error(msgid,errstr);
end
if kappa<=2
	msgid		= 'bayesian:betaadfrommodekappa:kappaOutOfRange';
	errstr		= 'kappa must be > 2 for mode parameterization';
	error(msgid,errstr);
end


a = mu.*kappa;
b = (1-mu).*kappa;
switch nargout
	case 1
		varargout{1}	= [a b];
	case 2
		varargout{1}	= a;
		varargout{2}	= b;
end