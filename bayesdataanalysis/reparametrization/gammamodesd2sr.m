function [shape,rate] = gammamodesd2sr(mu,sd)
% [SHAPE,RATE] = GAMMAMODESD2SR(MU,SD)
%
% Get SHAPE and RATE parameters of gamma function from mode MU and standard deviation SD
%
%
% Note that RATE = 1/SCALE (for Matlab's gamma functions)
%
% See also GAMMASR2MUSD, GAMFIT, GAMPDF

if mu<=0
	error('MU must be larger than 0.')
end
if sd<=0
	error('SD must be >larger than 0.')
end

rate = ( mu + sqrt( mu.^2 + 4 * sd.^2 ) ) ./ ( 2 * sd.^2 );
shape = 1 + mu * rate;

