function varargout = betamodekappa2ab(mu,kappa)
% betaAB = BETAMODEKAPPA2AB(MODE,KAPPA)
%
% Obtain the beta shape parameters betaAB.A and betaAB.B from the mode MODE
% and dispersion KAPPA
%
% See also BETAMUSD2AB

% Original in R:	Kruschke, J. K. (2014). Doing Bayesian Data Analysis:
%					A Tutorial with R and BUGS. Academic Press / Elsevier.
% Modified to Matlab code: Marc M. van Wanrooij

  
if any(mu<=0) || any(mu>1)
	msgid		= 'bayesian:betaadfrommodekappa:modeOutOfRange';
	errstr		= 'Must have 0 < mean < 1';
	error(msgid,errstr);
end
if kappa<=2
	msgid		= 'bayesian:betaadfrommodekappa:kappaOutOfRange';
	errstr		= 'kappa must be > 2 for mode parameterization';
	error(msgid,errstr);
end


a = mu.*(kappa-2)+1;
b = (1-mu).*(kappa-2)+1;
switch nargout
	case 1
		varargout{1}	= [a b];
	case 2
		varargout{1}	= a;
		varargout{2}	= b;
end