function [samples,stats] = jags_binomial_subj_cat(successes, trials, subjectIDs, categories, varargin)
% jags_binomial_subj_cat Performs Bayesian binomial rate analysis using JAGS
%
%   [samples, stats] = jags_binomial_subj_cat(Z, N, S, C) uses the number of successes Z, the number of
%   trials N, subject IDs S, and categories C to return MCMC samples and summary statistics.
%
%   Inputs:
%   - successes    : Vector of observed success counts (integer).
%   - trials       : Vector of total trial counts corresponding to each element in successes (integer).
%   - subjectIDs   : Vector of identifiers for the subject or condition corresponding to each trial count.
%   - categories   : Vector of category identifiers that subjects are nested within.
%
%   Outputs:
%   - samples      : A struct containing MCMC samples of various model parameters.
%   - stats        : A struct containing summary statistics and diagnostics.
%
%   Optional Name-Value Pair Arguments:
%   - 'burnInSteps'    : Total number of burn-in steps (default = 500).
%   - 'numSavedSteps'  : Total number of steps in chains to save (default = 1000).
%   - 'thinSteps'      : Number of steps to thin (to prevent autocorrelation) (default = 1).
%   - 'saveName'       : String to specify the filename for saving MCMC samples (default = 'Hier-BinomialTest-Jags-').
%   - 'nChains'        : Number of MCMC chains to run (default = 4).
%   - 'runJagsMethod'  : String to specify how JAGS should run ('serial' or 'parallel') (default = 'serial').
%   - 'dic'            : Logical flag to indicate if DIC should be calculated (default = false).
%
%   External Dependencies:
%   - JAGS: http://mcmc-jags.sourceforge.net/
%   - MATJAGS: http://psiexp.ss.uci.edu/research/programs_data/jags/ or https://github.com/msteyvers/matjags
%
%   Reference:
%   Kruschke, J. K. (2011). Doing Bayesian Data Analysis: A Tutorial with R and BUGS. Academic Press / Elsevier.
%
%   See also: MATJAGS, PLOTPOST
%
%   Example:
%   [samples, stats] = jags_binomial_subj_cat([5 6], [10 10], [1 2], [1 1], 'burnInSteps', 1000);


% Validate inputs
[successes, trials, subjectIDs, categories] = validate_inputs(successes, trials, subjectIDs, categories);

%% MCMC parameters
mcmc_params = get_mcmc_parameters(varargin);

%% Create JAGS Model and obtain file path
modelFilePath = write_model;

%% Prepare the data for JAGS
dataStruct = prepare_data(successes, trials, subjectIDs, categories);

%% Initialize chains
initsStruct			= initializeChains(mcmc_params.nChains, dataStruct);

%% Run MCMC using JAGS
[samples, stats] = run_jags(mcmc_params, dataStruct, modelFilePath, initsStruct);
end

% The following functions (validate_inputs, get_mcmc_parameters, write_model,
% prepare_data, initialize_chains, run_jags) contain the corresponding extracted
% logic for each section. Create these functions with appropriate input/output parameters.


function [successes,trials,subjectIDs,categories] = validate_inputs(successes, trials, subjectIDs, categories)

if isempty(successes) || isempty(trials)
	error('All input arrays must be non-empty.');
end
if isempty(subjectIDs)
	subjectIDs = ones(size(successes,1),1);
end
if isempty(categories)
	categories = ones(size(categories,1),1);
end
if length(successes) ~= length(trials) || length(successes) ~= length(subjectIDs) || length(successes) ~= length(categories)
	error('All input arrays must have the same length.');
end

% Ensure the input vectors are column vectors
if isrow(successes)
	successes = successes';
end
if isrow(trials)
	trials = trials';
end
if isrow(subjectIDs)
	subjectIDs = subjectIDs';
end
if isrow(categories)
	categories = categories';
end

% Uniquify the subject IDs and categories
[~, ~, subjectIDs] = unique(subjectIDs);
[~, ~, categories] = unique(categories);
end

function mcmc_params = get_mcmc_parameters(varargin)
chain_globals;


% Unpack inner cell array if varargin is nested
if length(varargin) == 1 && iscell(varargin{1})
	varargin = varargin{:};
end

% Default settings
mcmc_params.nChainsDefault = 4;
mcmc_params.runjagsMethodDefault = 'serial';

% Extract parameters or use defaults
p = inputParser;
addParameter(p, 'numSavedSteps', 1000);
addParameter(p, 'thinSteps', 1);
addParameter(p, 'burnInSteps', 500);
addParameter(p, 'saveName', 'Hier-BinomialTest-Jags-');
addParameter(p, 'nChains', mcmc_params.nChainsDefault);
addParameter(p, 'runjagsMethod', mcmc_params.runjagsMethodDefault);
addParameter(p, 'dic', false);

parse(p, varargin{:});
mcmc_params = p.Results;


% Calculate the number of iterations needed per chain
mcmc_params.nIter = ceil((mcmc_params.numSavedSteps * mcmc_params.thinSteps) / mcmc_params.nChains);

% Check if parallelization should be used
if strcmp(mcmc_params.runjagsMethod, 'parallel')
	mcmc_params.doparallel = 1;
else
	mcmc_params.doparallel = 0;
end


% Parameters to be observed
mcmc_params.parameters = {'theta', 'omega', 'kappa', 'omegaO', 'kappaO'};
end

function modelFilePath = write_model
modelname		= 'binomial_subj_cat_model.txt';
fullFilePath	= which('jags_binomial_subj_cat.m');
% Get folder, name, and extension
[folder, ~, ~]	= fileparts(fullFilePath);
modelFilePath	= fullfile(folder,modelname);

% Define the model string
modelString = [...
	'model {' newline, ...
	'  for ( s in 1:Nsubj ) {' newline, ...
	'    z[s] ~ dbin( theta[s] , N[s] )' newline, ...
	'    theta[s] ~ dbeta( omega[c[s]]*(kappa[c[s]]-2)+1 ,', ...
	'                       (1-omega[c[s]])*(kappa[c[s]]-2)+1 )' newline, ...
	'  }' newline, ...
	'  for ( c in 1:Ncat ) {' newline, ...
	'    omega[c] ~ dbeta( omegaO*(kappaO-2)+1 ,', ...
	'                     (1-omegaO)*(kappaO-2)+1 )' newline, ...
	'    kappa[c] <- kappaMinusTwo[c] + 2' newline, ...
	'    kappaMinusTwo[c] ~ dgamma( 0.01 , 0.01 ) # mean=1 , sd=10 (generic vague)' newline, ...
	'  }' newline, ...
	'  omegaO ~ dbeta( 1.0 , 1.0 )' newline, ...
	'  kappaO <- kappaMinusTwoO + 2' newline, ...
	'  kappaMinusTwoO ~ dgamma( 0.01 , 0.01 )  # mean=1 , sd=10 (generic vague)' newline, ...
	'}'];

% Write the model string to a text file
fid = fopen(modelFilePath, 'wt');
% Check if the file was created successfully
if fid == -1
	error('Could not create JAGS model file');
end

fprintf(fid, '%s', modelString);
fclose(fid);

end

function dataStruct = prepare_data(z,N,s,c)



% THE DATA.
% N.B.: This function expects the data to be a structure,
% with one field 'z' being a vector of integer successes,
% one field 'N' being a vector of integer attempts,
% one field 's' being a vector of subject identifiers,
% and one field 'c' being a vector of category identifiers, with
% subjects nested in categories.

% Calculate number of subjects and categories
Nsubj	= length(unique(s));
Ncat	= length(unique(c));

% Add the observed successes, number of trials

dataStruct.z		= z;
dataStruct.N		= N;
dataStruct.s		= s;
dataStruct.c		= c;
dataStruct.Nsubj	= Nsubj;
dataStruct.Ncat		= Ncat;

end

function initsStruct = initializeChains(nChains,dataStruct)
%     % Initial values of MCMC chains based on data
%     thetaInit = NaN(1, Nsubj);
%
%     for sIdx = 1:Nsubj % for each subject
%         resampledZ = binornd(N(sIdx), z(sIdx) / N(sIdx));
%         thetaInit(sIdx) = resampledZ / N(sIdx);
%     end
%
%     % Keep away from 0,1
%     thetaInit = 0.001 + 0.998 * thetaInit;
%
%     % Lazy, start high and let burn-in find better value
%     kappaInit = 100;
%
% 	keyboard
%     % Create the initsList structure
%     omegaInit = accumarray(c(:), thetaInit(:), [], @mean);
%     initsStruct = struct(...
%         'theta', thetaInit, ...
%         'omega', omegaInit, ...
%         'omegaO', mean(thetaInit), ...
%         'kappaMinusTwo', repmat(kappaInit - 2, 1, Ncat), ...
%         'kappaMinusTwoO', kappaInit - 2 ...
%     );

initsStruct = struct();
    % Lazy, start high and let burn-in find better value
    kappaInit = 100;
for ii = 1:nChains
	thetaInit = NaN(dataStruct.Nsubj,1);
	for jj = 1:length(dataStruct.z)
		        resampledZ = binornd(dataStruct.N(jj), dataStruct.z(jj) / dataStruct.N(jj));
				thetaInit(jj) = resampledZ / dataStruct.N(jj);
	end
	% Keep away from 0,1
    thetaInit = 0.001 + 0.998 * thetaInit;

	cInit = dataStruct.c;
	omegaInit = assemble(thetaInit,cInit,'fun', @mean);
	sInit = dataStruct.s;

	thetaInit = assemble(thetaInit,sInit,'fun', @mean);
	
	initsStruct(ii).theta			= thetaInit;
	initsStruct(ii).omegaO			= mean(thetaInit);
	initsStruct(ii).omega			= omegaInit;
    initsStruct(ii).kappaMinusTwo	= repmat(kappaInit - 2, 1, dataStruct.Ncat);
    initsStruct(ii).kappaMinusTwoO	= kappaInit - 2;
end

end

function [samples,stats] = run_jags(mcmc_params, dataStruct, modelFilePath, initsStruct)


%% MCMC
fprintf( 'Running JAGS...\n' );
[samples, stats] = matjags( ...
	dataStruct, ...                     % Observed data
	modelFilePath, ...    % File that contains model definition
	initsStruct, ...                          % Initial values for latent variables
	'doparallel' , mcmc_params.doparallel, ...      % Parallelization flag
	'nchains', mcmc_params.nChains,...              % Number of MCMC chains
	'nburnin', mcmc_params.burnInSteps,...              % Number of burnin steps
	'nsamples', mcmc_params.nIter, ...           % Number of samples to extract
	'thin', mcmc_params.thinSteps, ...                      % Thinning parameter
	'dic',mcmc_params.dic, ...                       % Do the DIC?
	'monitorparams', mcmc_params.parameters, ...     % List of latent variables to monitor
	'savejagsoutput',0, ...          % Save command line output produced by JAGS?
	'verbosity',0, ...               % 0=do not produce any output; 1=minimal text output; 2=maximum text output
	'cleanup',1);                    % clean up of temporary files?



%% Thin
if mcmc_params.thinSteps>1
	samples = thinchain(samples,thinSteps);
end


%% MCMC diagnostics
% if diagFlag
% 	parameterNames	= fieldnames(samples); % get all parameter names
% 	for parIdx			= 1:numel(parameterNames)
% 		n = size(samples.(parameterNames{parIdx}),3);
% 		for ii = 1:n
% 			figure
% 			a		= squeeze(samples.(parameterNames{parIdx})(:,:,ii));
% 			samp	= samples;
% 			samp.(parameterNames{parIdx}) = a;
% 			diagmcmc(samp,'parName',parameterNames{parIdx});
% 		end
% 	end
% end


%% Extract chain values:
samples = extractchain(samples); % from multiple -dimension matrix to 1- or 2-D


end


% %%
% %
% % % graphic flags
% % diagFlag		= keyval('showDiag',varargin,false); % show MCMC diagnostics
% % postFlag		= keyval('showPost',varargin,false); % show posterior estimate distribution
% % predFlag		= keyval('showPred',varargin,true); % show posterior predictive
% % centroidFlag	= keyval('showCentroid',varargin,'mean'); % mode, median, mean
% %
%
%


