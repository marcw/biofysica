function [samples,stats] = jags_binomial_product(successes, trials, subjectIDs, categories, varargin)
% jags_binomial_subj_cat Performs Bayesian binomial rate analysis using JAGS
%
%   [samples, stats] = jags_binomial_subj_cat(Z, N, S, C) uses the number of successes Z, the number of
%   trials N, subject IDs S, and categories C to return MCMC samples and summary statistics.
%
%   Inputs:
%   - successes    : Vector of observed success counts (integer).
%   - trials       : Vector of total trial counts corresponding to each element in successes (integer).
%   - subjectIDs   : Vector of identifiers for the subject or condition corresponding to each trial count.
%   - categories   : Vector of category identifiers that subjects are nested within.
%
%   Outputs:
%   - samples      : A struct containing MCMC samples of various model parameters.
%   - stats        : A struct containing summary statistics and diagnostics.
%
%   Optional Name-Value Pair Arguments:
%   - 'burnInSteps'    : Total number of burn-in steps (default = 500).
%   - 'numSavedSteps'  : Total number of steps in chains to save (default = 1000).
%   - 'thinSteps'      : Number of steps to thin (to prevent autocorrelation) (default = 1).
%   - 'saveName'       : String to specify the filename for saving MCMC samples (default = 'Hier-BinomialTest-Jags-').
%   - 'nChains'        : Number of MCMC chains to run (default = 4).
%   - 'runJagsMethod'  : String to specify how JAGS should run ('serial' or 'parallel') (default = 'serial').
%   - 'dic'            : Logical flag to indicate if DIC should be calculated (default = false).
%
%   External Dependencies:
%   - JAGS: http://mcmc-jags.sourceforge.net/
%   - MATJAGS: http://psiexp.ss.uci.edu/research/programs_data/jags/ or https://github.com/msteyvers/matjags
%
%   Reference:
%   Kruschke, J. K. (2011). Doing Bayesian Data Analysis: A Tutorial with R and BUGS. Academic Press / Elsevier.
%
%   See also: MATJAGS, PLOTPOST
%
%   Example:
%   [samples, stats] = jags_binomial_subj_cat([5 6], [10 10], [1 2], [1 1], 'burnInSteps', 1000);


% Validate inputs
[successes, trials, subjectIDs, categories] = validate_inputs(successes, trials, subjectIDs, categories);

%% MCMC parameters
mcmc_params = get_mcmc_parameters(varargin);

%% Create JAGS Model and obtain file path
modelFilePath = write_model;

%% Prepare the data for JAGS
dataStruct = prepare_data(successes, trials, subjectIDs, categories);

%% Initialize chains
initsStruct			= initializeChains(mcmc_params.nChains, dataStruct);

%% Run MCMC using JAGS
[samples, stats] = run_jags(mcmc_params, dataStruct, modelFilePath, initsStruct);

%% 
end

% The following functions (validate_inputs, get_mcmc_parameters, write_model,
% prepare_data, initialize_chains, run_jags) contain the corresponding extracted
% logic for each section. 

function [successes,trials,subjectIDs,categories] = validate_inputs(successes, trials, subjectIDs, categories)

if isempty(successes) || isempty(trials)
	error('All input arrays must be non-empty.');
end
if isempty(subjectIDs)
	subjectIDs = ones(size(successes,1),1);
end
if isempty(categories)
	categories = ones(size(categories,1),1);
end
if length(successes) ~= length(trials) || length(successes) ~= length(subjectIDs) || length(successes) ~= length(categories)
	error('All input arrays must have the same length.');
end

% Ensure the input vectors are column vectors
if isrow(successes)
	successes = successes';
end
if isrow(trials)
	trials = trials';
end
if isrow(subjectIDs)
	subjectIDs = subjectIDs';
end
if isrow(categories)
	categories = categories';
end

% Uniquify the subject IDs and categories
[~, ~, subjectIDs] = unique(subjectIDs);
[~, ~, categories] = unique(categories);
end

function mcmc_params = get_mcmc_parameters(varargin)
chain_globals;


% Unpack inner cell array if varargin is nested
if length(varargin) == 1 && iscell(varargin{1})
	varargin = varargin{:};
end

% Default settings
mcmc_params.nChainsDefault = 4;
mcmc_params.runjagsMethodDefault = 'serial';

% Extract parameters or use defaults
p = inputParser;
addParameter(p, 'numSavedSteps', 1000);
addParameter(p, 'thinSteps', 1);
addParameter(p, 'burnInSteps', 500);
addParameter(p, 'saveName', 'Hier-BinomialTest-Jags-');
addParameter(p, 'nChains', mcmc_params.nChainsDefault);
addParameter(p, 'runjagsMethod', mcmc_params.runjagsMethodDefault);
addParameter(p, 'dic', false);
addParameter(p, 'showDiag', false);

parse(p, varargin{:});
mcmc_params = p.Results;


% Calculate the number of iterations needed per chain
mcmc_params.nIter = ceil((mcmc_params.numSavedSteps * mcmc_params.thinSteps) / mcmc_params.nChains);

% Check if parallelization should be used
if strcmp(mcmc_params.runjagsMethod, 'parallel')
	mcmc_params.doparallel = 1;
else
	mcmc_params.doparallel = 0;
end


% Parameters to be observed
mcmc_params.parameters = {'phi', 'omega', 'kappap', 'kappaw', 'p' , 'w'};
end

function modelFilePath = write_model
modelname		= 'binomial_product_model.txt';
fullFilePath	= which('jags_binomial_product.m');
% Get folder, name, and extension
[folder, ~, ~]	= fileparts(fullFilePath);
modelFilePath	= fullfile(folder,modelname);

modelString = [
'model{ ' newline,...
  '# Word performance Is Binomially Distributed ' newline,...
  'for (i in 1:Ndata){ ' newline,...
  '    z[i] ~ dbin(theta[i],N[i]) ' newline,...
  '# Probability Correct Is Product Of Word By Person Rates ' newline,...
  '    theta[i] <- p[s[i]]*w[c[i]] ' newline,...
  '} ' newline,...
  '# Priors For Subjects and Categories ' newline,...
  'for (i in 1:Nsubj){ ' newline,...
  '  p[i] ~ dbeta( phi*(kappap-2)+1 ,', ...
  '                (1-phi)*(kappap-2)+1 )' newline, ...
  '} ' newline,...
  'for (j in 1:Ncat){ ' newline,...
  '  w[j] ~ dbeta( omega*(kappaw-2)+1 ,', ...
  '                (1-omega)*(kappaw-2)+1 )' newline, ...
  '} ' newline,...
  '  omega ~ dbeta( 1.0 , 1.0 )' newline, ...
  '  kappaw <- kappawMinusTwoO + 2' newline, ...
  '  kappawMinusTwoO ~ dgamma( 0.01 , 0.01 )  # mean=1 , sd=10 (generic vague)' newline, ...
  '  phi ~ dbeta( 1.0 , 1.0 )' newline, ...
  '  kappap <- kappapMinusTwoO + 2' newline, ...
  '  kappapMinusTwoO ~ dgamma( 0.01 , 0.01 )  # mean=1 , sd=10 (generic vague)' newline, ...
'}',...
	];

% Write the model string to a text file
fid = fopen(modelFilePath, 'wt');
% Check if the file was created successfully
if fid == -1
	error('Could not create JAGS model file');
end

fprintf(fid, '%s', modelString);
fclose(fid);

end

function dataStruct = prepare_data(z,N,s,c)



% THE DATA.
% N.B.: This function expects the data to be a structure,
% with one field 'z' being a vector of integer successes,
% one field 'N' being a vector of integer attempts,
% one field 's' being a vector of subject identifiers,
% and one field 'c' being a vector of category identifiers, with
% subjects nested in categories.

% Calculate number of subjects and categories
Nsubj	= length(unique(s));
Ncat	= length(unique(c));
Ndata	= length(z);

% Add the observed successes, number of trials

dataStruct.z		= z;
dataStruct.N		= N;
dataStruct.s		= s;
dataStruct.c		= c;
dataStruct.Nsubj	= Nsubj;
dataStruct.Ncat		= Ncat;
dataStruct.Ndata	= Ndata;

end

function initsStruct = initializeChains(nChains,dataStruct)
%     % Initial values of MCMC chains based on data

initsStruct = struct();
    % Lazy, start high and let burn-in find better value
    kappaInit = 100;
for ii = 1:nChains
	vInit = NaN(dataStruct.Ndata,1);
	for jj = 1:length(dataStruct.z)
		        resampledZ = binornd(dataStruct.N(jj), dataStruct.z(jj) / dataStruct.N(jj));
				vInit(jj) = resampledZ / dataStruct.N(jj);
	end
	% Keep away from 0,1
    vInit = 0.001 + 0.998 * vInit;


	cInit = dataStruct.c;
	sInit = dataStruct.s;
	pInit = assemble(vInit,sInit,'fun', @mean);
	wInit = assemble(vInit,cInit,'fun', @mean);

	initsStruct(ii).p			= pInit;
		initsStruct(ii).w			= wInit;

% 	initsStruct(ii).omega			= omegaInit;
%     initsStruct(ii).kappaMinusTwo	= repmat(kappaInit - 2, 1, dataStruct.Ncat);
%     initsStruct(ii).kappaMinusTwoO	= kappaInit - 2;
end

end

function [samples,stats] = run_jags(mcmc_params, dataStruct, modelFilePath, initsStruct)


%% MCMC
fprintf( 'Running JAGS...\n' );
[samples, stats] = matjags( ...
	dataStruct, ...                     % Observed data
	modelFilePath, ...    % File that contains model definition
	initsStruct, ...                          % Initial values for latent variables
	'doparallel' , mcmc_params.doparallel, ...      % Parallelization flag
	'nchains', mcmc_params.nChains,...              % Number of MCMC chains
	'nburnin', mcmc_params.burnInSteps,...              % Number of burnin steps
	'nsamples', mcmc_params.nIter, ...           % Number of samples to extract
	'thin', mcmc_params.thinSteps, ...                      % Thinning parameter
	'dic',mcmc_params.dic, ...                       % Do the DIC?
	'monitorparams', mcmc_params.parameters, ...     % List of latent variables to monitor
	'savejagsoutput',0, ...          % Save command line output produced by JAGS?
	'verbosity',0, ...               % 0=do not produce any output; 1=minimal text output; 2=maximum text output
	'cleanup',1);                    % clean up of temporary files?


%% Diagnose MCMC
if mcmc_params.showDiag
	diagmcmc(samples);
end

%% Thin
if mcmc_params.thinSteps>1
	samples = thinchain(samples,thinSteps);
end


%% Extract chain values:
samples = extractchain(samples); % from multiple -dimension matrix to 1- or 2-D


end




