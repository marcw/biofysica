function [omega, l] = kdeMode(x)
% KDEMODE Compute the mode of a continuous sample distribution.
%
%   [OMEGA, L] = KDEMODE(X) returns the mode OMEGA and the maximum density L 
%   for a given vector X, which are samples from a continuous distribution.
%
%   OMEGA and L are returned as scalars for a vector input X, or as row
%   vectors for a matrix input X.
%
%   See also: KSDENSITY, MODE

% Determine the size of the input
[m, n] = size(x);

% Case: input is a vector
if any([m, n] == 1)
    % Use kernel density estimation to find mode
    [f, xi]		= ksdensity(x);
    % Find the maximum density and corresponding value
    [l, indx]	= max(f);
    omega		= xi(indx);

% Case: input is a matrix
elseif n > 1
    % Initialize output vectors
    omega	= NaN(1, n);
    l		= omega;
    % Iterate over each column and apply the function recursively
    for ii	= 1:n
        [omega(ii), l(ii)] = kdeMode(x(:, ii));
    end

% Case: unexpected input shape
else
    warning('Input has an unexpected size. Returning NaNs.');
    omega	= NaN;
    l		= NaN;
end

end
