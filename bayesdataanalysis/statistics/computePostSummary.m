function postSummary = computePostSummary(paramSampleVec, varargin)
% Obtain summary statistics in structure POSTSUMMARY of posterior samples.
% - mean
% - median
% - mode
% - density (estimated via ksdensity)
% - highest-density low and high interval
% - highest-density mass
% - comparison to value
% - ROPE

% Input parsing
p = inputParser;
addRequired(p, 'paramSampleVec', @isnumeric);
addOptional(p, 'credMass', 0.95, @(x) x > 0 && x < 1);
addOptional(p, 'ROPE', [], @isnumeric);
addOptional(p, 'compVal', [], @isnumeric);
parse(p, paramSampleVec, varargin{:});

credMass			= p.Results.credMass;
ROPE				= p.Results.ROPE;
compVal				= p.Results.compVal;

% Compute summary statistics for the posterior distribution
postSummary.mean	= nanmean(paramSampleVec);
postSummary.median	= nanmedian(paramSampleVec);
postSummary.mode	= kdeMode(paramSampleVec); % determine mode via kernel density estimation
[mcmcDensity.y,mcmcDensity.x] = ksdensity(paramSampleVec);
postSummary.mcmcDensity.x = mcmcDensity.x;
postSummary.mcmcDensity.y = mcmcDensity.y;

HDI					= hdimcmc(paramSampleVec, credMass); % determine highest density interval

postSummary.hdiMass = credMass;
postSummary.hdiLow	= HDI(1);
postSummary.hdiHigh = HDI(2);

% Determine the ROPE
if ~isempty(ROPE)
    postSummary		= computeROPE(postSummary, paramSampleVec, ROPE);
end

% Compare to the comparison value
if ~isempty(compVal)
    postSummary		= computeCompVal(postSummary, paramSampleVec, compVal);
end
end

% Helper function to compute ROPE related summary
function postSummary = computeROPE(postSummary, paramSampleVec, ROPE)
    pcInROPE				= sum(paramSampleVec > ROPE(1) & paramSampleVec < ROPE(2)) / length(paramSampleVec);
    postSummary.ROPElow		= ROPE(1);
    postSummary.ROPEhigh	= ROPE(2);
    postSummary.pcInROPE	= pcInROPE;
end

% Helper function to compute comparison value related summary
function postSummary = computeCompVal(postSummary, paramSampleVec, compVal)
    postSummary.compVal		= compVal;
    postSummary.pcGTcompVal = sum(paramSampleVec > compVal) / length(paramSampleVec);
end
