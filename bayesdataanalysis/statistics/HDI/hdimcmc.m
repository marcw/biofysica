function HDIlim = hdimcmc(sampleVec, credMass, varargin)
% HDIMCMC Compute Highest Density Interval (HDI) from a sample of representative values.
%
%   HDIlim = HDIMCMC(sampleVec) computes the HDI for a given
%   sampleVec with a default credMass of 0.95.
%
%   HDIlim = HDIMCMC(sampleVec, credMass) computes the HDI for a given
%   sampleVec with a specified credMass.
%
%   HDIlim = HDIMCMC(sampleVec, credMass, Name, Value) specifies additional
%   name-value pair arguments.
%       'method':    Method to compute HDI, either 'percentile' (default) or 'HDI'.
%
% Output:
%   - HDIlim: 1x2 vector specifying the lower and upper limits of the HDI.
%
% Example:
%   HDI = hdimcmc(randn(1, 1000));
%
% Reference:
%   Kruschke, J. K. (2011). Doing Bayesian Data Analysis: A Tutorial with R
%   and BUGS. Academic Press / Elsevier.
%
% Modified to Matlab by: Marc M. van Wanrooij

% Check number of input arguments to assign default value for credMass
if nargin < 2 || isempty(credMass)
	credMass = 0.95;
end

% Create an inputParser instance
p = inputParser;
addParameter(p, 'method', 'percentile');

% Parse optional name-value pair arguments
parse(p, varargin{:});
method = p.Results.method;


%% Compute HDI
switch method
	case 'percentile'
		p				= (100 - credMass * 100) / 2;
		HDIlim			= prctile(sampleVec, [p, 100 - p]);
		
	otherwise
		sortedSamples	= sort(sampleVec);
		nSamples		= length(sortedSamples);
		ciIdxInc		= floor(credMass * nSamples); % Number of samples included in CI
		nCIs			= nSamples - ciIdxInc;  % Number of samples NOT included in CI
		ciWidth			= diff(sortedSamples(1:ciIdxInc + nCIs)) - diff(sortedSamples(1:nCIs));
		
		[~, minIndx]	= min(ciWidth);
		HDIlim			= sortedSamples([minIndx, minIndx + ciIdxInc]);
end

