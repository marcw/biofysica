function [samples,stats] = jags_anova3xway(y,x1,x2,x3,varargin)
% SAMPLES = JAGS_ANOVA(Y,X1,X2,X3)
%
% Work in Progress
% From : ﻿Kruschke, J. K. (2015). Chapter 20 - Metric Predicted Variable
% with Multiple Nominal Predictors. In J. K. Kruschke (Ed.), Doing Bayesian
% Data Analysis (Second Edition) (Second Edi, pp. 583–620). Academic Press.
% https://doi.org/https://doi.org/10.1016/B978-0-12-405888-0.00020-9   



%% Model
modelname		= which('anova3xway_model.txt');

%% MCMC parameters
chain_globals;
numSavedSteps	= keyval('numSavedSteps',varargin,1000); % number of saved MCMC samples. How many you need depend on autocorrelation (effective sample size>10000), convergence (shrink factor<1.1), etc
thinSteps		= keyval('thinSteps',varargin,1); % 1/proportion MCMC samples thrown away
burnInSteps		= keyval('burnInSteps',varargin,500);
saveName		= keyval('saveName',varargin,'Hier-NormDist-Jags-');
nChains			= keyval('nChains',varargin,nChainsDefault);
runjagsMethod	= keyval('runjagsMethod',varargin,runjagsMethodDefault);
dic				= keyval('dic',varargin,false);
nIter			= ceil((numSavedSteps*thinSteps )/nChains); % Steps per chain.

diagFlag		= keyval('showDiag',varargin,false); % show MCMC diagnostics



%% THE DATA.
% Convert data file columns to generic x,y variable names for model:
[ux1,~,x1levels]	= unique(x1);
[ux2,~,x2levels]	= unique(x2);
[ux3,~,x3levels]	= unique(x3);
Ntotal				= length(y);
Nx1Lvl				= length(ux1);
Nx2Lvl				= length(ux2);
Nx3Lvl				= length(ux3);

% Compute scale properties of data, for passing into prior to make the prior
% vague on the scale of the data.
% For prior on baseline, etc.:
yMean				= mean(y);
ySD					= std(y);
% For prior on deflections:
[shape,rate]		= gammamodesd2sr(ySD/2,2*ySD);
aGammaShRa			= [shape rate];

% For prior on cell SDs:
X					= [x1 x2 x3];
cellSDs				= assemble(y,X,'fun',@nanstd);

medianCellSD		= nanmedian(cellSDs);
sdCellSD			= nanstd(cellSDs);

if ~medianCellSD
	warning('Uh-Oh')
	cellSDs = repmat(std(y),size(cellSDs));
	medianCellSD		= nanmedian(cellSDs);
	sdCellSD			= medianCellSD;
end

disp(['Median cell SD: ' num2str(medianCellSD)]);
disp([ 'StDev. cell SD: ', num2str(sdCellSD)]);

[shape,rate]		= gammamodesd2sr(medianCellSD,2*sdCellSD);
sGammaShRa			= [shape rate];


%% Specify the data in a structure for sending to JAGS:
dataStruct		= struct('y',y,...
'x1',x1levels,...
'x2',x2levels,...
'x3',x3levels,...
'Ntotal',Ntotal,...
'Nx1Lvl', Nx1Lvl,...
'Nx2Lvl', Nx2Lvl,...
'Nx3Lvl', Nx3Lvl,...
'yMean', yMean,...
'ySD', ySD,...
'medianCellSD', medianCellSD,...
'aGammaShRa', aGammaShRa,...
'sGammaShRa', sGammaShRa);

%% Data
parameters = {'b0' ,  'b1' ,  'b2' , 'b3',...
	'b1b2' ,'b1b3' ,'b2b3' ,...
	'b1b2b3',...
	'ySigma' , 'sigmaMode' , 'sigmaSD' ,'nu',...
	'a1SD','a2SD','a3SD','a1a2SD','a1a3SD','a2a3SD',...
	'a1a2a3SD',...
	};


% initsStruct		= initialize(Ngroups,nChains,gamma,lambda,fun); % initial parameter values
initsStruct = struct([]);
for ii = 1:nChains
	initsStruct(ii).nu		= 1; % no lapses
	
end


%% parallel?
if strcmp(runjagsMethod,'parallel')
	doparallel		= 1; % do use parallelization
else
	doparallel		= 0; % do not use parallelization
end

%% MCMC
fprintf( 'Running JAGS...\n' );
[samples, stats] = matjags( ...
	dataStruct, ...                     % Observed data
	modelname, ...    % File that contains model definition
	initsStruct, ...                          % Initial values for latent variables
	'doparallel' , doparallel, ...      % Parallelization flag
	'nchains', nChains,...              % Number of MCMC chains
	'nburnin', burnInSteps,...              % Number of burnin steps
	'nsamples', nIter, ...           % Number of samples to extract
	'thin', thinSteps, ...                      % Thinning parameter
	'dic',dic, ...                       % Do the DIC?
	'monitorparams', parameters, ...     % List of latent variables to monitor
	'savejagsoutput',0, ...          % Save command line output produced by JAGS?
	'verbosity',0, ...               % 0=do not produce any output; 1=minimal text output; 2=maximum text output
	'cleanup',1);                    % clean up of temporary files?


% samples				= unzscore(samples,mux,sdx);
%% MCMC diagnostics
if diagFlag
	diagmcmc(samples)
end

%%
% samples1 = samples;
samples = extractchain(samples);

% save('samples_app','samples');
% 
% 
% keyboard


%% Save samples
if ~isempty(saveName)
	save([saveName 'Mcmc'],'samples');
end






