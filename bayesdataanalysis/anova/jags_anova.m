function [samples,stats] = jags_anova(y,x1,x2,varargin)
% SAMPLES = JAGS_ANOVA(Y,X1,X2)
%
% Work in Progress
% From : ﻿Kruschke, J. K. (2015). Chapter 20 - Metric Predicted Variable
% with Multiple Nominal Predictors. In J. K. Kruschke (Ed.), Doing Bayesian
% Data Analysis (Second Edition) (Second Edi, pp. 583–620). Academic Press.
% https://doi.org/https://doi.org/10.1016/B978-0-12-405888-0.00020-9   

if nargin<1
	fname			= '/Users/marcw/Dropbox/Manuscript/Elisabeth Noordanus/Repository/DAC/tidy_app.csv';
	T				= readtable(fname);
	y				= T.reactiontime-240;
	x1				= [T.frequency T.velocity T.density];
	[uripple,~,x1]	= unique(x1,'rows');
	[~,~,x2]		= unique(T.participant);
	
	sel				= y>140 & y<2500;
	y				= 1000./y(sel);
	x1				= x1(sel);
	x2				= x2(sel);
end

%% Model
modelname		= which('anova_model.txt');

%% MCMC parameters
chain_globals;
numSavedSteps	= keyval('numSavedSteps',varargin,1000); % number of saved MCMC samples. How many you need depend on autocorrelation (effective sample size>10000), convergence (shrink factor<1.1), etc
thinSteps		= keyval('thinSteps',varargin,1); % 1/proportion MCMC samples thrown away
burnInSteps		= keyval('burnInSteps',varargin,500);
saveName		= keyval('saveName',varargin,'Hier-NormDist-Jags-');
nChains			= keyval('nChains',varargin,nChainsDefault);
runjagsMethod	= keyval('runjagsMethod',varargin,runjagsMethodDefault);
dic				= keyval('dic',varargin,false);
nIter			= ceil((numSavedSteps*thinSteps )/nChains); % Steps per chain.

diagFlag		= keyval('showDiag',varargin,false); % show MCMC diagnostics


% keyboard
%% THE DATA.
% Convert data file columns to generic x,y variable names for model:
[ux1,~,x1levels]	= unique(x1);
[ux2,~,x2levels]	= unique(x2);
Ntotal				= length(y);
Nx1Lvl				= length(unique(x1));
Nx2Lvl				= length(unique(x2));
% Compute scale properties of data, for passing into prior to make the prior
% vague on the scale of the data.
% For prior on baseline, etc.:
yMean				= mean(y);
ySD					= std(y);
% For prior on deflections:
[shape,rate]		= gammamodesd2sr(ySD/2,2*ySD);
aGammaShRa			= [shape rate];

% For prior on cell SDs:
X					= [x1 x2];
cellSDs				= assemble(y,X,'fun',@nanstd);
medianCellSD		= nanmedian(cellSDs);
sdCellSD			= nanstd(cellSDs);

if ~medianCellSD
	warning('Uh-Oh')
	cellSDs = repmat(std(y),size(cellSDs));
	medianCellSD		= nanmedian(cellSDs);
	sdCellSD			= medianCellSD;
end

warning(['Median cell SD: ' num2str(medianCellSD)]);
warning([ 'StDev. cell SD: ', num2str(sdCellSD)]);

[shape,rate]		= gammamodesd2sr(medianCellSD,2*sdCellSD);
sGammaShRa			= [shape rate];


%% Specify the data in a structure for sending to JAGS:
dataStruct		= struct('y',y,...
'x1',x1levels,...
'x2',x2levels,...
'Ntotal',Ntotal,...
'Nx1Lvl', Nx1Lvl,...
'Nx2Lvl', Nx2Lvl,...
'yMean', yMean,...
'ySD', ySD,...
'medianCellSD', medianCellSD,...
'aGammaShRa', aGammaShRa,...
'sGammaShRa', sGammaShRa);

%% Data
parameters = {'b0' ,  'b1' ,  'b2' ,  'b1b2' , 'm',...
	'a1SD' , 'a2SD' , 'a1a2SD',...
	'ySigma' , 'sigmaMode' , 'sigmaSD' ,'nu',...
	};




% initsStruct		= initialize(Ngroups,nChains,gamma,lambda,fun); % initial parameter values
initsStruct = struct([]);
for ii = 1:nChains
	initsStruct(ii).nu		= 1; % no lapses
end


%% parallel?
if strcmp(runjagsMethod,'parallel')
	doparallel		= 1; % do use parallelization
else
	doparallel		= 0; % do not use parallelization
end

%% MCMC
% fprintf( 'Running JAGS...\n' );
[samples, stats] = matjags( ...
	dataStruct, ...                     % Observed data
	modelname, ...    % File that contains model definition
	initsStruct, ...                          % Initial values for latent variables
	'doparallel' , doparallel, ...      % Parallelization flag
	'nchains', nChains,...              % Number of MCMC chains
	'nburnin', burnInSteps,...              % Number of burnin steps
	'nsamples', nIter, ...           % Number of samples to extract
	'thin', thinSteps, ...                      % Thinning parameter
	'dic',dic, ...                       % Do the DIC?
	'monitorparams', parameters, ...     % List of latent variables to monitor
	'savejagsoutput',0, ...          % Save command line output produced by JAGS?
	'verbosity',0, ...               % 0=do not produce any output; 1=minimal text output; 2=maximum text output
	'cleanup',1);                    % clean up of temporary files?


% samples				= unzscore(samples,mux,sdx);
%% MCMC diagnostics
if diagFlag
	diagmcmc(samples)
end

%%
% samples1 = samples;
samples = extractchain(samples);

% save('samples_app','samples');
% 
% 
% keyboard


%% Save samples
if ~isempty(saveName)
	save([saveName 'Mcmc'],'samples');
end






