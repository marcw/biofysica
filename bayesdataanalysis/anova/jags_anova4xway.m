function [samples, stats] = jags_anova4xway(y, x1, x2, x3, x4, varargin)
%JAGS_ANOVA4XWAY Run a Bayesian ANOVA using JAGS.
%
% INPUTS:
%   y - Dependent variable
%   x1, x2, x3, x4 - Independent variables
%   varargin - Additional parameters for the MCMC run
%
% OUTPUTS:
%   samples - Posterior samples
%   stats - Statistics from the MCMC run

% Load model
modelname = which('anova4xway_model.txt');

% Parse input arguments
[parsedArgs, nChains] = parseInputs(varargin{:});

% Convert data to model format
dataStruct = prepareData(y, x1, x2, x3, x4);

% Initializations
initsStruct = initializeInits(nChains,y, x1, x2, x3, x4);

% MCMC
[samples, stats] = runMCMC(dataStruct, modelname, initsStruct, parsedArgs, nChains);

% MCMC diagnostics
if parsedArgs.showDiag
	diagmcmc(samples);
end

% Extract samples
samples = extractchain(samples);

% Save samples
if ~isempty(parsedArgs.saveName)
	save([parsedArgs.saveName 'Mcmc'], 'samples');
end
end

function [parsedArgs, nChains] = parseInputs(varargin)
chain_globals;
% Set default values
defaults = struct('numSavedSteps', 1000, 'thinSteps', 1, 'burnInSteps', 500,...
'saveName', 'Hier-NormDist-Jags-', 'nChains', nChainsDefault,...
'runjagsMethod', runjagsMethodDefault, 'dic', false, 'showDiag', false);

% Initialize input parser
p = inputParser();
fields = fieldnames(defaults);
for f = 1:length(fields)
	addParameter(p, fields{f}, defaults.(fields{f}));
end
parse(p, varargin{:});

% Extract values
parsedArgs = p.Results;
nChains = parsedArgs.nChains;
parsedArgs.nIter = ceil((parsedArgs.numSavedSteps * parsedArgs.thinSteps) / nChains);
end

function dataStruct = prepareData(y, x1, x2, x3, x4)
% Convert data and perform computations
[ux1,~,x1levels]	= unique(x1);
[ux2,~,x2levels]	= unique(x2);
[ux3,~,x3levels]	= unique(x3);
[ux4,~,x4levels]	= unique(x4);

Ntotal				= length(y);
Nx1Lvl				= length(ux1);
Nx2Lvl				= length(ux2);
Nx3Lvl				= length(ux3);
Nx4Lvl				= length(ux4);

% Compute scale properties of data
yMean				= mean(y);
ySD					= std(y);
[shape,rate]		= gammamodesd2sr(ySD/2,2*ySD);
aGammaShRa			= [shape rate];

% For prior on cell SDs
X					= [x1 x2 x3 x4];
cellSDs				= assemble(y,X,'fun',@nanstd);

medianCellSD		= nanmedian(cellSDs);
sdCellSD			= nanstd(cellSDs);

if ~medianCellSD
	warning('Uh-Oh')
	cellSDs = repmat(std(y),size(cellSDs));
	medianCellSD		= nanmedian(cellSDs);
	sdCellSD			= medianCellSD;
end

[shape,rate]		= gammamodesd2sr(medianCellSD,2*sdCellSD);
sGammaShRa			= [shape rate];

    % Convert to structure
    dataStruct		= struct('y',y, 'x1',x1levels, 'x2',x2levels, 'x3',x3levels,... 
        'x4',x4levels, 'Ntotal',Ntotal, 'Nx1Lvl', Nx1Lvl, 'Nx2Lvl', Nx2Lvl,...
        'Nx3Lvl', Nx3Lvl, 'Nx4Lvl', Nx4Lvl, 'yMean', yMean, 'ySD', ySD,...
        'medianCellSD', medianCellSD, 'aGammaShRa', aGammaShRa, 'sGammaShRa', sGammaShRa);
end

function initsStruct = initializeInits(nChains,y, x1, x2, x3, x4)

%         'b0','b1', 'b2', 'b3', 'b4', ...
%         'b1b2', 'b1b3', 'b1b4', 'b2b3', 'b2b4', 'b3b4', ...
%         'b1b2b3', 'b1b2b4', 'b1b3b4', 'b2b3b4', ...
%         'b1b2b3b4'
% Initials from Data
b0 = mean(y);
b1 = assemble(y,x1,'fun',@mean)-b0;
b2 = assemble(y,x2,'fun',@mean)-b0;
b3 = assemble(y,x3,'fun',@mean)-b0;
b4 = assemble(y,x4,'fun',@mean)-b0;
% b1b2 = assemble(y,[x1 x2],'fun',@mean)-b0-b1-b2;

% Initialize chains
initsStruct = struct([]);
for ii = 1:nChains
	initsStruct(ii).nu = 100;
% 	initsStruct(ii).b0 = b0;
% 	initsStruct(ii).b1 = b1;
% 	initsStruct(ii).b2 = b2;
% 	initsStruct(ii).b3 = b3;
% 	initsStruct(ii).b4 = b4;
% 	initsStruct(ii).b1b2 = b1b2;
	
end
end

function [samples, stats] = runMCMC(dataStruct, modelname, initsStruct, parsedArgs, nChains)
    fprintf('Running JAGS...\n');
    parameters = {
        'b0','b1', 'b2', 'b3', 'b4', ...
        'b1b2', 'b1b3', 'b1b4', 'b2b3', 'b2b4', 'b3b4', ...
        'b1b2b3', 'b1b2b4', 'b1b3b4', 'b2b3b4', ...
        'b1b2b3b4'
    };
    [samples, stats] = matjags(dataStruct, modelname, initsStruct, ...
        'doparallel', strcmp(parsedArgs.runjagsMethod,'parallel'), 'nchains', nChains,...
        'nburnin', parsedArgs.burnInSteps, 'nsamples', parsedArgs.nIter, ...
        'thin', parsedArgs.thinSteps, 'dic', parsedArgs.dic, 'monitorparams', parameters,...
        'savejagsoutput', 0, 'verbosity', 0, 'cleanup', 1);
end
