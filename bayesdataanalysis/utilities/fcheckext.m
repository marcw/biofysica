function fname = fcheckext(fname, fext)
% FCHECKEXT checks or replaces the file extension of the given filename.
%
% Syntax: fname = fcheckext(fname, fext)
%
% Inputs:
%   - fname: Original filename
%   - fext: Desired file extension
%
% Outputs:
%   - fname: Filename with the desired extension
%

% Prepend a dot to the extension if not already there
if fext(1) ~= '.'
    fext = ['.' fext];
end

% Decompose the filename into path, name, and extension
[pathstr, name, ext] = fileparts(fname);

% Replace the extension if it's not the desired one
if ~strcmp(ext, fext)
    ext = fext;
end

% Reconstruct and return the updated filename
fname = fullfile(pathstr, [name ext]);

end
