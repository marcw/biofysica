function BF = bayesfactor(samplesPost, samplesPrior, varargin)
% BAYESFACTOR Calculates the Bayes Factor using the Savage-Dickey method.
%
% BF = BAYESFACTOR(SAMPLESPOST, SAMPLESPRIOR) determines the Bayes factor 
% for prior and posterior MCMC samples.
%
% BF = BAYESFACTOR(SAMPLESPOST, SAMPLESPRIOR, 'Crit', CRIT) determines if the
% posterior is significantly different from a critical value CRIT. By
% default, this tests the null hypothesis.
%
% Optional parameters:
% 'Eps' - bin size, default is 0.01.
% 'Bins' - bin range, default is -100:eps:100.
%
% See also KSDENSITY

% Parse input arguments
p = inputParser;
addRequired(p, 'samplesPost', @isnumeric);
addRequired(p, 'samplesPrior', @isnumeric);
addParameter(p, 'Crit', 0, @isnumeric); % Critical value
addParameter(p, 'Eps', 0.01, @isnumeric); % Bin size
addParameter(p, 'Bins', -100:0.01:100, @isnumeric); % Bin range
parse(p, samplesPost, samplesPrior, varargin{:});

crit		= p.Results.Crit;
eps			= p.Results.Eps;
binse		= p.Results.Bins;
binse		= min(binse):eps:max(binse); % bin range

% Compute densities for posterior and prior samples
[f, xi]		= computeDensity(samplesPost, binse);
[f2, x2]	= computeDensity(samplesPrior, binse);

% Find density values closest to critical value
[~, indk]	= min(abs(xi - crit));
[~, indk2]	= min(abs(x2 - crit));

% Compute Bayes Factor
v1			= f(indk);
v2			= f2(indk2);
BF			= v1 / v2;
end

% Helper function to compute density
function [f, x] = computeDensity(samples, bins)
    samples = samples(samples > bins(1) & samples < bins(end));
    [f, x] = ksdensity(samples, 'kernel', 'normal', 'support', [bins(1) bins(end)]);
end
