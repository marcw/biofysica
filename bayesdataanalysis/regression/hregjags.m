function [samples, stats] = hregjags(x, y, s, varargin)
%JAGS_ANOVA4XWAY Run a Bayesian ANOVA using JAGS.
%
% [samples, stats] = hregjags(x, y, s, varargin)
% INPUTS:
%   y - Dependent variable
%   x - Independent variable
%	s - subject
%   varargin - Additional parameters for the MCMC run
%
% OUTPUTS:
%   samples - Posterior samples
%   stats - Statistics from the MCMC run

% Load model
modelname = which('hreg_normal_model.txt');
% if ~exist(modelname,'file')
% writemodel;
% end
% Parse input arguments
[parsedArgs, nChains] = parseInputs(varargin{:});

% Convert data to model format
dataStruct = prepareData(x, y, s);

% Initializations
initsStruct = initializeInits(nChains,dataStruct);

% MCMC
[samples, stats] = runMCMC(dataStruct, modelname, initsStruct, parsedArgs, nChains);

% MCMC diagnostics
if parsedArgs.showDiag
	diagmcmc(samples);
end

% Extract samples
samples = extractchain(samples);

% Save samples
if ~isempty(parsedArgs.saveName)
	save([parsedArgs.saveName 'Mcmc'], 'samples');
end
end

function [parsedArgs, nChains] = parseInputs(varargin)
chain_globals;

% Set default values
defaults = struct('numSavedSteps', 1000, 'thinSteps', 1, 'burnInSteps', 500,...
'saveName', 'Hier-NormDist-Jags-', 'nChains', nChainsDefault,...
'runjagsMethod', runjagsMethodDefault, 'dic', false, 'showDiag', false);

% Initialize input parser
p		= inputParser();
fields	= fieldnames(defaults);
for f	= 1:length(fields)
	addParameter(p, fields{f}, defaults.(fields{f}));
end
parse(p, varargin{:});

% Extract values
parsedArgs			= p.Results;
nChains				= parsedArgs.nChains;
parsedArgs.nIter	= ceil((parsedArgs.numSavedSteps * parsedArgs.thinSteps) / nChains);
end

function dataStruct		= prepareData(x,y,s)

% Convert sName to consecutive integers:
[~,~,s] = unique(s);
ns		= max(s);

% Do some checking that data make sense:
if any(~isfinite(y))
	errror('All y values must be finite.');
end
if any(~isfinite(x))
	error('All x values must be finite.');
end


% For prior on cell SDs:
X					= s;
cellSDs				= assemble(y,X,'fun',@nanstd);

medianCellSD		= nanmedian(cellSDs);
sdCellSD			= nanstd(cellSDs);

[shape,rate]		= gammamodesd2sr(medianCellSD,2*sdCellSD);
sGammaShRa			= [shape rate];

    % Convert to structure
dataStruct = struct('x',x,'y',y,'s',s,'Nsubj',ns,'sGammaShRa',sGammaShRa,'medianCellSD',medianCellSD);

end

function initsStruct = initializeInits(nChains,dataStruct)
    % Initialize chains

initsStruct = struct([]);
for ii = 1:nChains
	initsStruct(ii).zbeta0			= zeros(dataStruct.Nsubj,1);    % because data are standardized
	initsStruct(ii).zbeta1			= zeros(dataStruct.Nsubj,1);        % because data are standardized
	initsStruct(ii).zbeta0mu		= 0;        % because data are standardized
	initsStruct(ii).zbeta1mu		= 0;        % because data are standardized
	initsStruct(ii).zbeta0sigma		= 1;        % because data are standardized
	initsStruct(ii).zbeta1sigma		= 1;        % because data are standardized
	initsStruct(ii).zsigma			= ones(dataStruct.Nsubj,1);        % because data are standardized
end
end

function [samples, stats] = runMCMC(dataStruct, modelname, initsStruct, parsedArgs, nChains)
    fprintf('Running JAGS...\n');

parameters = {'beta0','beta1','beta0mu','beta1mu','sigma'};


    [samples, stats] = matjags(dataStruct, modelname, initsStruct, ...
        'doparallel', strcmp(parsedArgs.runjagsMethod,'parallel'), 'nchains', nChains,...
        'nburnin', parsedArgs.burnInSteps, 'nsamples', parsedArgs.nIter, ...
        'thin', parsedArgs.thinSteps, 'dic', parsedArgs.dic, 'monitorparams', parameters,...
        'savejagsoutput', 0, 'verbosity', 0, 'cleanup', 1);
end



function writemodel
str = ['data {\r\n',...
	'# Standardize the data:\r\n',...
	'\t\tNtotal <- length(y)\r\n',...
	'\t\txm <- mean(x)\r\n',...
	'\t\tym <- mean(y)\r\n',...
	'\t\txsd <- sd(x)\r\n',...
	'\t\tysd <- sd(y)\r\n',...
	'\t\tfor ( i in 1:length(y) ) {\r\n',...
	'\t\t\tzx[i] <- ( x[i] - xm ) / xsd\r\n',...
	'\t\t\tzy[i] <- ( y[i] - ym ) / ysd\r\n',...
	'\t\t}\r\n',...
	'}\r\n',...
	'# Specify the model for standardized data:\r\n',...
	'model {\r\n',...
	'\tfor ( i in 1:Ntotal ) {\r\n',...
	'\t\tzy[i] ~ dt( zbeta0[s[i]] + zbeta1[s[i]] * zx[i] , 1/zsigma^2 , nu )\r\n',...
	'\t}\r\n',...
	'\tfor ( j in 1:Nsubj ) {\r\n',...
	'\t\tzbeta0[j] ~ dnorm( zbeta0mu , 1/(zbeta0sigma)^2 )\r\n',...
	'\t\tzbeta1[j] ~ dnorm( zbeta1mu , 1/(zbeta1sigma)^2 )\r\n',...
	'\t}\r\n',...
	'# Priors vague on standardized scale:\r\n',...
	'zbeta0mu ~ dnorm( 0 , 1/(10)^2 )\r\n',...
	'zbeta1mu ~ dnorm( 0 , 1/(10)^2 )\r\n',...
	'zsigma ~ dunif( 1.0E-3 , 1.0E+3 )\r\n',...
	'zbeta0sigma ~ dunif( 1.0E-3 , 1.0E+3 )\r\n',...
	'zbeta1sigma ~ dunif( 1.0E-3 , 1.0E+3 )\r\n',...
	'nu <- nuMinusOne+1\r\n',...
	'nuMinusOne ~ dexp(1/29.0)\r\n',...
	'# Transform to original scale:\r\n',...
	'for ( j in 1:Nsubj ) {\r\n',...
	'beta1[j] <- zbeta1[j] * ysd / xsd\r\n',...
	'beta0[j] <- zbeta0[j] * ysd  + ym - zbeta1[j] * xm * ysd / xsd\r\n',...
	'}\r\n',...
	'beta1mu <- zbeta1mu * ysd / xsd\r\n',...
	'beta0mu <- zbeta0mu * ysd  + ym - zbeta1mu * xm * ysd / xsd\r\n',...
	'sigma <- zsigma * ysd\r\n',...
	'}\r\n',...
	];

% Write the modelString to a file, using Matlab commands:
fid			= fopen('hreg_model.txt','w');
fprintf(fid,str);
fclose(fid);
end