% GETFILELIST Retrieves a list of files with a specific extension in a directory.
%
%   fileList = GETFILELIST(directory, extension) returns a structure array of files
%   that match the specified extension within the given directory.
%
%   Inputs:
%       directory - String specifying the directory path to search.
%       extension - String specifying the file extension pattern (e.g., '*.txt', '*.mat').
%
%   Outputs:
%       fileList  - Structure array containing information about the files found.
%                   This structure has fields like 'name', 'folder', 'date', etc.,
%                   similar to what is returned by MATLAB's 'dir' function.
%
%   Example:
%       % Get a list of all MATLAB files in the current directory
%       files = getFileList('.', '*.m');
%
%   Notes:
%       - The 'extension' input can include wildcards to match multiple files.
%       - Ensure that the directory path is correct and accessible.
%       - This function uses 'fullfile' to construct the full file pattern.
%
%   Author: Ruurd Lof
%   Date:   2016
%
%   See also: dir, fullfile

function fileList = getFileList(directory, extension)
    fullfn = fullfile(directory, extension);
    fileList = dir(fullfn); % default exp folder    
end