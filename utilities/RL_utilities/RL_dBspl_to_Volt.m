% RL_DBSPL_TO_VOLT Converts sound pressure level in decibels to RMS voltage.
%
%   rms = RWL_DBSPL_TO_VOLT(dBspl, sensedB) calculates the root mean square (RMS)
%   voltage corresponding to a given sound pressure level (SPL) in decibels (dB SPL),
%   using a specified sensitivity calibration factor in decibels.
%
%   Inputs:
%       dBspl   - Sound pressure level in decibels (dB SPL).
%       sensedB - Sensitivity calibration factor in decibels, representing the reference
%                 level for voltage conversion (e.g., microphone sensitivity).
%
%   Outputs:
%       rms     - Calculated RMS voltage corresponding to the input dB SPL.
%
%   Example:
%       % Convert a sound pressure level of 94 dB SPL to RMS voltage with a sensitivity
%       % of -26 dB.
%       rmsVoltage = rwl_dBspl_to_Volt(94, -26);
%
%   Notes:
%       - The function uses the formula:
%           rms = 10^((dBspl - sensedB) / 20);
%       - Ensure that the 'sensedB' value matches the calibration of your equipment.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: rwl_Volt_to_dBspl, RL_calc_dB

function rms = RL_dBspl_to_Volt(dBspl, sensedB)
    rms = 10^((dBspl-sensedB)/20);
end