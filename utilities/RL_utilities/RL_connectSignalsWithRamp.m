% RL_CONNECTSIGNALSWITHRAMP Connects two signals with overlapping Hanning ramps.
%
%   combinedSignal = RL_CONNECTSIGNALSWITHRAMP(signal1, signal2, sampleFrequency, overlap)
%   combines two input signals by applying Hanning window ramps to their overlapping regions,
%   padding them appropriately, and summing them to create a seamless transition between the
%   two signals.
%
%   Inputs:
%       signal1         - The first input signal (vector or matrix). Time should be along rows.
%       signal2         - The second input signal (vector or matrix). Must have the same length
%                         and number of channels as signal1.
%       sampleFrequency - Sampling frequency of the signals in Hertz (Hz).
%       overlap         - Duration of the overlap between the two signals in seconds.
%
%   Outputs:
%       combinedSignal  - The resulting signal after connecting signal1 and signal2 with ramps.
%
%   Example:
%       % Combine two audio signals with a 0.5-second overlap at 44.1 kHz sampling rate
%       combinedSignal = RL_connectSignalsWithRamp(audio1, audio2, 44100, 0.5);
%
%   Notes:
%       - Both input signals must have the same length and number of channels.
%       - The function applies a right Hanning ramp to the end of signal1 and a left Hanning ramp
%         to the beginning of signal2.
%       - The signals are padded to align properly before being summed.
%       - The overlap duration should be less than or equal to the length of the signals.
%       - If the signals are not oriented with time along rows, the function transposes them.
%
%   Dependencies:
%       - RL_applyHanningRamp: Applies a Hanning window ramp to a signal.
%       - RL_addPadding: Adds zero padding to a signal.
%
%   Author: Ruurd Lof
%   Date:   2024
%
%   See also: RL_applyHanningRamp, RL_addPadding

function combinedSignal = RL_connectSignalsWithRamp(signal1, signal2, sampleFrequency, overlap)
    
    % Ensure both signals have the same length and orientation
    if size(signal1, 1) < size(signal1, 2)
        signal1 = signal1';  
    end
    if size(signal2, 1) < size(signal2, 2)
        signal2 = signal2';  
    end
    
    % Check if signals are of the same length and channels
    if size(signal1, 1) ~= size(signal2, 1) || size(signal1, 2) ~= size(signal2, 2)
        error('Signals must have the same length and number of channels.');
    end
    
    % Calculate the length of the ramp in samples
    rampLengthInSamples = floor(overlap * sampleFrequency);
    signalLengthInSamples = size(signal1, 1);
    
    % Calculate padding length for ramped signals
    paddingLength =  signalLengthInSamples - rampLengthInSamples;
    
    % Apply right Hanning ramp to the end of signal1 and add padding
    rampedSignal1 = RL_applyHanningRamp(signal1, overlap, sampleFrequency, 'right');
    rampedSignal1 = RL_addPadding(rampedSignal1, paddingLength, 'right');
           
    % Apply left Hanning ramp to the beginning of signal2 and add padding
    rampedSignal2 = RL_applyHanningRamp(signal2, overlap, sampleFrequency, 'left');
    rampedSignal2 = RL_addPadding(rampedSignal2, paddingLength, 'left');
    
    % Create the combined signal
    combinedSignal = rampedSignal1 + rampedSignal2;

    if size(combinedSignal, 1) > size(combinedSignal, 2)
        combinedSignal = combinedSignal';  
    end    

end
