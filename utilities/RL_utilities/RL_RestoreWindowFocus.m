% RL_RESTOREWINDOWFOCUS Restores window focus on a specified monitor.
%
%   RL_RESTOREWINDOWFOCUS(MonitorNumber) restores the focus to a window on the specified monitor
%   by simulating mouse movements and clicks using Java's Robot class and functions from the
%   Psychtoolbox.
%
%   Inputs:
%       MonitorNumber - The identifier of the monitor where the window focus should be restored.
%
%   Example:
%       % Restore window focus on monitor 1
%       RL_RestoreWindowFocus(1);
%
%   Dependencies:
%       - Java's Robot class (java.awt.Robot)
%       - Psychtoolbox functions: Screen, GetMouse
%
%   Notes:
%       - The function simulates mouse movements and clicks on an empty area of the taskbar to
%         restore window focus, then moves the mouse back to its original position.
%       - Ensure that Java is properly configured in your MATLAB environment.
%       - The Psychtoolbox must be installed for this function to work.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: Screen, GetMouse, java.awt.Robot

function RL_RestoreWindowFocus(MonitorNumber)
    % uses Robot class from java
    % uses Screen function from Psych toolbox
    import java.awt.Robot;
    
    leftMouseButton       = 8;
    [width, height]       = Screen('WindowSize', MonitorNumber);
    emptyPlaceOnTaskbar   = [round(2*width/3), height];
    [x,y,~,~,~,~]         = GetMouse(MonitorNumber);
    originalMousePosition = [x, y];
    mouse                 = Robot;
    
    % mouse actions        
    mouse.mouseMove(emptyPlaceOnTaskbar(1), emptyPlaceOnTaskbar(2));
    mouse.mousePress(leftMouseButton);             
    mouse.mouseRelease(leftMouseButton); 
    mouse.mouseMove(originalMousePosition(1), originalMousePosition(2));
end