% RL_RANDOMIZEPHASE Randomizes the phase of a matrix while preserving magnitude.
%
%   result = RL_RANDOMIZEPHASE(X) multiplies each element of the input matrix X by a random
%   unit-length complex exponential, effectively randomizing the phase of each element without
%   altering its magnitude.
%
%   Inputs:
%       X      - Input matrix, which can be real or complex-valued.
%
%   Outputs:
%       result - Matrix of the same size as X, with randomized phases.
%
%   Example:
%       % Randomize the phase of a complex signal matrix
%       X = [1+1i, 2+2i; 3+3i, 4+4i];
%       Y = RL_randomizePhase(X);
%
%   Notes:
%       - The function generates random phases uniformly distributed between 0 and 2π.
%       - This operation is useful in signal processing applications where phase randomization is required.
%       - The magnitude (absolute value) of each element in X remains unchanged.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: rand, exp, angle, abs

function result = RL_randomizePhase(X)
    phase = rand(size(X))*2*pi;
    result = X .* exp(1i*phase);
end %randomizePhase