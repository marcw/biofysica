% RL_runningrmsPlot - Calculate and plot the RMS of an audio signal using a sliding window.
%
% Syntax:
%   RL_runningrmsPlot(sampleRate, audioSignal, window_size, unitName)
%
% Description:
%   This function computes the Root Mean Square (RMS) values of a given audio signal
%   over sliding windows of size 'window_size'. The calculated RMS values are plotted
%   against time, featuring a title that includes the 'unitName'.
%
% Input:
%   sampleRate  - Sampling frequency of the audio signal in Hz.
%   audioSignal - Vector containing the audio signal data.
%   window_size - Size of the window (in samples) used for RMS calculation.
%   unitName    - Name or identifier of the unit, included in the plot title.
%
% Output:
%   None explicitly returned; the function generates a plot of RMS values over time.
%
% Example:
%   RL_runningrmsPlot(44100, myAudio, 1024, 'Microphone 1');
%
% See also: rms, plot
%
% Author: [Ruurd Lof]
% Date:   [24/09/2024]


function RL_runningrmsPlot(sampleRate, audioSignal, window_size, unitName)

    % Bereken RMS voor audio1
    rms1 = arrayfun(@(i) rms(audioSignal(i:i+window_size-1)), 1:window_size:length(audioSignal)-window_size+1);    
    
    % Tijdvector voor de RMS-waarden
    time = (0:length(rms1)-1) * (window_size / sampleRate);
    
    % Plotten van de RMS-waarden
    plot(time, rms1, 'b', 'DisplayName', 'Audio');    
    xlabel('Time (s)');
    ylabel('RMS');
    ylim([0 inf]);
    legend;
    title(strcat('RMS of Audio Signal for ', unitName));
    grid on;
end

