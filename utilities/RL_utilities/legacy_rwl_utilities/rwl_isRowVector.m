function result = isRowVector(V)
    result = isrow(V);
end