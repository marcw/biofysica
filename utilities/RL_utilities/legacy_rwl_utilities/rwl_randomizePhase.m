function result = rwl_randomizePhase(X)
    phase = rand(size(X))*2*pi;
    result = X .* exp(1i*phase);
end %randomizePhase