function rwl_RestoreWindowFocus(MonitorNumber)
    % uses Robot class from java
    % uses Screen function from Psych toolbox
    import java.awt.Robot;
    
    leftMouseButton       = 8;
    [width, height]       = Screen('WindowSize', MonitorNumber);
    emptyPlaceOnTaskbar   = [round(2*width/3), height];
    [x,y,~,~,~,~]         = GetMouse(MonitorNumber);
    originalMousePosition = [x, y];
    mouse                 = Robot;
    
    % mouse actions        
    mouse.mouseMove(emptyPlaceOnTaskbar(1), emptyPlaceOnTaskbar(2));
    mouse.mousePress(leftMouseButton);             
    mouse.mouseRelease(leftMouseButton); 
    mouse.mouseMove(originalMousePosition(1), originalMousePosition(2));
end