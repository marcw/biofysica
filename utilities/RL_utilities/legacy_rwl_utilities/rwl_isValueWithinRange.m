% RWL_ISVALUEWITHINRANGE Checks if a value is within a specified range.
%
%   result = RWL_ISVALUEWITHINRANGE(value, minVal, maxVal) returns true if the input 'value'
%   is greater than or equal to 'minVal' and less than or equal to 'maxVal'. If the value is
%   outside this range, it returns false, displays an error dialog, and waits for user interaction.
%
%   Inputs:
%       value  - The numeric value to check.
%       minVal - The minimum allowable value.
%       maxVal - The maximum allowable value.
%
%   Outputs:
%       result - Logical true (1) if 'value' is within the range [minVal, maxVal], false (0) otherwise.
%
%   Example:
%       % Check if the number 5 is within the range 1 to 10
%       isValid = rwl_isValueWithinRange(5, 1, 10);
%
%   Notes:
%       - If 'value' is outside the specified range, an error dialog is displayed, and the function
%         waits for the user to close the dialog before proceeding.
%       - The function uses inclusive comparison, meaning 'value' equal to 'minVal' or 'maxVal' is
%         considered within the range.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: errordlg, uiwait


function result = rwl_isValueWithinRange(value, minVal, maxVal)
    result = (value >= minVal) && (value <= maxVal);
    if ~result
         errordlg(sprintf('Value outside of range. Value should be between %d and %d', minVal, maxVal),'Error', 'modal');
         uiwait;
    end
end