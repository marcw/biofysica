% RWL_ANGLEBETWEENVECTORS_DP Calculates the angle between vectors in double polar coordinates.
%
%   result = RWL_ANGLEBETWEENVECTORS_DP(V1_DP, V2_DP) computes the angle(s) between two vectors
%   provided in double polar (DP) coordinate format. The function converts these vectors to
%   Right-Anterior-Superior (RAS) coordinates, then calculates the angles between them.
%
%   Inputs:
%       V1_DP - Structure containing the first vector(s) in double polar coordinates.
%       V2_DP - Structure containing the second vector(s) in double polar coordinates.
%
%   Outputs:
%       result - Angle(s) between the input vectors, maintaining the original vector orientation
%                (row or column vector).
%
%   Example:
%       % Compute the angle between two vectors in double polar coordinates
%       angle = rwl_angleBetweenVectors_DP(V1_DP, V2_DP);
%
%   Dependencies:
%       - transform_DP2RAS: Converts DP coordinates to RAS coordinates.
%       - rwl_angleBetweenVectors_XYZ: Calculates angles between vectors in XYZ coordinates.
%       - rwl_isRowVector: Checks if a vector is a row vector.
%       - rwl_isColumnVector: Checks if a vector is a column vector.
%
%   Notes:
%       - The input structures V1_DP and V2_DP must have compatible fields for conversion.
%       - The function throws an error if the input vectors are neither row nor column vectors.
%
%   Author: Ruurd Lof
%   Date:   24/09/2024

function result = rwl_angleBetweenVectors_DP(V1_DP,V2_DP)
    % Convert double polar coordinates to RAS coordinates
    V1_RAS = transform_DP2RAS(V1_DP);
    V2_RAS = transform_DP2RAS(V2_DP);
    
    % Convert structs to arrays with rows of triples
    V1 = [V1_RAS.right(:), V1_RAS.anterior(:), V1_RAS.superior(:)];
    V2 = [V2_RAS.right(:), V2_RAS.anterior(:), V2_RAS.superior(:)];  

    angles = rwl_angleBetweenVectors_XYZ(V1, V2, 'row');

    % make output vector size as input vector size
    if rwl_isRowVector(V1_RAS.right)
       result = angles';
    elseif rwl_isColumnVector(V1_RAS.right)
       result = angles;
    else
        error('Error in rwl_angleBetweenVectors_DP: the fields of the input must be row or column vector')
    end 
end