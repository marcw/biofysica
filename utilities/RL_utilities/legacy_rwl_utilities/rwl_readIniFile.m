% RWL_READINIFILE Reads an INI file and returns its contents as a structured array.
%
%   ini_data = RWL_READINIFILE(fDir, fName) reads an INI file specified by the directory 'fDir'
%   and filename 'fName', parses its contents, and returns a structured array 'ini_data' where
%   each section is a field containing key-value pairs.
%
%   Inputs:
%       fDir  - String specifying the directory path of the INI file.
%       fName - String specifying the name of the INI file.
%
%   Outputs:
%       ini_data - Structure containing the parsed contents of the INI file. Sections are fields
%                  of the structure, and keys within sections are subfields.
%
%   Example:
%       % Read an INI file located in the current directory
%       iniData = rwl_readIniFile('.', 'config.ini');
%
%   Notes:
%       - The INI file format supported includes sections denoted by [SectionName], key-value pairs
%         separated by '=', and supports different data types including strings, arrays, booleans,
%         and numbers.
%       - Lines starting with '%' are treated as comments and are ignored.
%       - String values are enclosed in double quotes and may contain '%20' representing spaces.
%       - Arrays are enclosed in curly braces '{}'.
%       - Booleans are represented by 't', 'T', 'f', or 'F'.
%       - If a line doesn't match the expected format, it is ignored.
%
%   Dependencies:
%       - None
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: fopen, fclose, fgetl, strsplit, strtrim, str2num, isfield

function ini_data = rwl_readIniFile(fDir, fName)
    f = fullfile(fDir, fName);
    fid = fopen(f, 'r');
    ini_data = struct();
    current_section = '';
    while ~feof(fid)
        line = fgetl(fid);
        if isempty(line) || line(1) == '%'
            continue; % Ignore empty lines and comments
        end
        if line(1) == '[' && line(end) == ']' %find section
            current_section = line(2:end-1);
            continue; 
        end        
        parts = strtrim(strsplit(line, '='));
        if isempty(parts{1})
            continue; % Ignore lines that don't match the expected format
        end
        key = parts{1};
        valueString = parts{2};        
        switch valueString(1) 
            case '"' % read string(s)
               str = replace(valueString, '%20', ' ');
               str = replace(str, '"', '');
               if ~contains(str, ',')
                 value = str;
               else
                 value = strsplit(str, ',');
               end
            case '{' % read array
               value = str2num(valueString(2:end-1)); %#ok<ST2NM> 
            case {'t', 'T'}
                value = true;
            case {'f', 'F'}
                value = false;            
            otherwise %read number
               value = str2num(valueString); %#ok<ST2NM> 
        end
        
        if ~isfield(ini_data, current_section)
            ini_data.(current_section) = struct();                        
        end
        ini_data.(current_section).(key) = value;
    end
    fclose(fid);
end

