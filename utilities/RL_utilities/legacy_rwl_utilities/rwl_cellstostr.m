% RWL_CELLSTOSTR Concatenates a cell array of strings into a single string.
%
%   str = RWL_CELLSTOSTR(cellarray) takes a cell array of strings and concatenates
%   all its elements into a single string, separating each element with a space.
%
%   Inputs:
%       cellarray - Cell array containing strings to be concatenated.
%
%   Outputs:
%       str       - Single string containing all elements of cellarray separated by spaces.
%
%   Example:
%       % Convert a cell array of words into a sentence
%       words = {'Hello', 'world', 'from', 'MATLAB'};
%       sentence = rwl_cellstostr(words);
%       % sentence will be ' Hello world from MATLAB'
%
%   Notes:
%       - The function assumes that all elements in cellarray are strings.
%       - The resulting string starts with a leading space due to the formatting.
%         Use 'str = strtrim(str);' to remove leading and trailing spaces if desired.
%
%   Author: Ruurd Lof
%   Date:   2022

function str = rwl_cellstostr(cellarray)
    [~, Size] = size(cellarray);
    str = '';
    for i = 1:Size                
        str = sprintf('%s %s', str ,cellarray{i});
    end
end