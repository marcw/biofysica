% RWL_EXLSLRESOLVE Resolves and lists available Lab Streaming Layer (LSL) streams.
%
%   RWL_EXLSLRESOLVE searches for available LSL streams using the lsl_resolver function,
%   and prints the index, name, and type of each stream to the command window.
%
%   Inputs:
%       None
%
%   Outputs:
%       None
%
%   Example:
%       % Run the function to list all available LSL streams
%       rwl_exLslResolve;
%
%   Dependencies:
%       - lsl_resolver: Function to resolve LSL streams.
%
%   Notes:
%       - If no streams are found, the function throws an error with the message 'no streams found'.
%       - The function uses the 'infos' method to retrieve stream information.
%       - LSL stands for Lab Streaming Layer.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: lsl_resolver

function rwl_exLslResolve
    info=lsl_resolver();
    list=info.infos();
    if isempty(list)
        error('no streams found');
    end
    [~, nr] = size(list);
    for i=1:nr
        fprintf('%d: name: ''%s'' type: ''%s''\n',i,list{i}.name,list{i}.type);
    end
end