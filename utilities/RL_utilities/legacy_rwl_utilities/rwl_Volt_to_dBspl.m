% RWL_VOLT_TO_DBSPL Converts RMS voltage to sound pressure level in decibels.
%
%   dBspl = RWL_VOLT_TO_DBSPL(Volt, sensedB) calculates the sound pressure level (SPL)
%   in decibels (dB SPL) from the root mean square (RMS) voltage, using a specified
%   sensitivity calibration factor in decibels.
%
%   Inputs:
%       Volt    - RMS voltage value.
%       sensedB - Sensitivity calibration factor in decibels, representing the reference
%                 level for voltage conversion (e.g., microphone sensitivity).
%
%   Outputs:
%       dBspl   - Calculated sound pressure level in decibels (dB SPL).
%
%   Example:
%       % Convert an RMS voltage of 0.1 V to dB SPL with a sensitivity of -26 dB.
%       dBspl = rwl_Volt_to_dBspl(0.1, -26);
%
%   Notes:
%       - The function uses the formula:
%           dBspl = 20 * log10(Volt) + sensedB;
%       - Ensure that 'Volt' is a positive real number.
%       - 'sensedB' should match the calibration of your equipment.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: rwl_dBspl_to_Volt

function dBspl = rwl_Volt_to_dBspl(Volt, sensedB)
    dBspl = 20*log10(Volt) + sensedB;
end