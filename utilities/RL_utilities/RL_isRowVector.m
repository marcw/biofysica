function result = RL_isRowVector(V)
    result = isRow(V);
end