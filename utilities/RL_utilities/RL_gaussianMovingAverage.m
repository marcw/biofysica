% RL_gaussianMovingAverage - Applies a Gaussian moving average to the input data
%
% Syntax:  result = RL_gaussianMovingAverage(data, windowSize, sigma)
%
% Inputs:
%    data - A vector of data points to be smoothed
%    windowSize - The size of the Gaussian window (number of points)
%    sigma - The standard deviation of the Gaussian distribution
%
% Outputs:
%    result - The smoothed data after applying the Gaussian moving average
%
% Example: 
%    result = RL_gaussianMovingAverage([1, 2, 3, 4, 5], 3, 1.5)
%
% Author: [Ruurd Lof]
% Date: [20/09/2024]
    
function result = RL_gaussianMovingAverage(data, windowSize, sigma)

    dataSize = length(data);

    % Maak een Gaussiaans venster    
    gaussianWindow = gausswin(windowSize, sigma);
    
    % Normeer het venster zodat de som gelijk is aan 1
    gaussianWindow = gaussianWindow / sum(gaussianWindow);
    
    % add padding
    paddingSize = 2*windowSize;
    leftPadding  = ones(1,paddingSize) * data(1);
    rightPadding = ones(1,paddingSize) * data(end);
    paddedData = [leftPadding, data, rightPadding];

    % bereken het Gaussiaanse gemiddelde met convolutie
    paddedConvolution = conv(paddedData, gaussianWindow, 'same');
    
    % remove padding
    convolution = paddedConvolution(paddingSize+1:paddingSize+dataSize);

    % assign result
    result = convolution;
end