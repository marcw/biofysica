% RL_RUNNINGRMSPLOT_DB Plots the running RMS of an audio signal in decibels.
%
%   RL_RUNNINGRMSPLOT_DB(sampleRate, audioSignal, windowSize, unitName) calculates the running
%   Root Mean Square (RMS) of the input audio signal over a specified window size, converts
%   these RMS values to decibels (dB), and plots them over time.
%
%   Inputs:
%       sampleRate  - Sampling rate of the audio signal in Hertz (Hz).
%       audioSignal - Vector containing the audio signal data.
%       windowSize  - Number of samples over which to calculate the RMS.
%       unitName    - String specifying the name of the audio unit or signal, used in the plot title.
%
%   Outputs:
%       This function does not return any outputs; it generates a plot of the running RMS in dB.
%
%   Example:
%       % Calculate and plot the running RMS in dB for a given audio signal.
%       RL_runningrmsPlot_dB(44100, audioData, 1024, 'Microphone Input');
%
%   Notes:
%       - The sensitivity is set to 0 dB by default.
%       - The y-axis of the plot is limited from negative infinity to 0 dB.
%       - Ensure that the windowSize is appropriate for the length of the audioSignal.
%
%   Author: Ruurd Lof
%   Date:   24/09/2024
%
%   See also: RL_runningrmsPlot

function RL_runningrmsPlot_dB(sampleRate, audioSignal, windowSize, unitName)
    
    sensitivity = 0; %dB    
    
    % Bereken RMS voor audio1
    rms1 = arrayfun(@(i) rms(audioSignal(i:i+windowSize-1)), 1:windowSize:length(audioSignal)-windowSize+1);
    
    % Omzetten naar dB
    rms1_db = sensitivity + 20 * log10(rms1);
    
    % Tijdvector voor de RMS-waarden
    time = (0:length(rms1)-1) * (windowSize / sampleRate);
    
    % Plotten van de RMS-waarden in dB
    plot(time, rms1_db, 'b', 'DisplayName', 'Audio in');
    xlabel('Time (s)');
    ylabel('RMS (dB)');
    ylim([-inf 0]);
    legend;
    title(strcat('RMS of Audio Signals in dB for ', unitName));
    grid on;
end