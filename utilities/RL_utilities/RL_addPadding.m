% RL_ADDPADDING Adds zero padding to a signal at the specified position.
%
%   paddedSignal = RL_ADDPADDING(signal, N_padding, paddingPosition) adds zero padding
%   of length N_padding to the input signal either at the beginning or the end, as specified
%   by paddingPosition ('left' or 'right').
%
%   Inputs:
%       signal          - Input signal matrix. If the signal has fewer rows than columns,
%                         it is transposed to have time along the rows.
%       N_padding       - Number of zeros to add as padding.
%       paddingPosition - String specifying where to add the padding: 'left' or 'right'.
%
%   Outputs:
%       paddedSignal    - The signal with zero padding added at the specified position.
%
%   Example:
%       % Add 100 zeros to the beginning of a signal
%       paddedSignal = RL_addPadding(originalSignal, 100, 'left');
%
%   Notes:
%       - The function ensures the signal is oriented with time along the rows before padding.
%       - If 'paddingPosition' is neither 'left' nor 'right', the function throws an error.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: zeros, error

function paddedSignal = RL_addPadding(signal, N_padding, paddingPosition)

% Ensure signal is oriented with time along columns if necessary
    if size(signal, 1) < size(signal, 2)
        signal = signal';  
    end
    
    % Create the zero padding
    zeroPadding = zeros(N_padding, size(signal, 2));  % Matrix of zeros matching signal's channel count
    
    % Add padding to the specified position
    switch lower(paddingPosition)
        case 'left'
            % Add zeros to the left
            paddedSignal = [zeroPadding; signal];
        case 'right'
            % Add zeros to the right
            paddedSignal = [signal; zeroPadding];
        otherwise
            error('Invalid padding position. Use ''left'' or ''right''.');
    end

end
