% RL_ANGLEBETWEENVECTORS_RAS Calculates the angle between vectors in RAS coordinates.
%
%   result = RWL_ANGLEBETWEENVECTORS_RAS(V1_RAS, V2_RAS) computes the angle(s) between two vectors
%   provided in Right-Anterior-Superior (RAS) coordinate format. The function converts these
%   structures into arrays, calculates the angles between them, and returns the result while
%   maintaining the original vector orientation (row or column vector).
%
%   Inputs:
%       V1_RAS - Structure containing the first vector(s) with fields:
%                - right:     Right component(s) of the vector(s).
%                - anterior:  Anterior component(s) of the vector(s).
%                - superior:  Superior component(s) of the vector(s).
%       V2_RAS - Structure containing the second vector(s) with the same fields as V1_RAS.
%
%   Outputs:
%       result - Angle(s) between the input vectors in degrees, matching the orientation of the
%                input vectors (row or column vector).
%
%   Example:
%       % Compute the angle between two vectors in RAS coordinates
%       angle = rwl_angleBetweenVectors_RAS(V1_RAS, V2_RAS);
%
%   Dependencies:
%       - rwl_angleBetweenVectors_XYZ: Calculates angles between vectors in XYZ coordinates.
%       - isRowVector: Checks if a vector is a row vector.
%       - isColumnVector: Checks if a vector is a column vector.
%
%   Notes:
%       - The input structures V1_RAS and V2_RAS must have the fields 'right', 'anterior', and
%         'superior', and these fields must be either row or column vectors.
%       - The function throws an error if the input vectors are neither row nor column vectors.
%
%   Author: Ruurd Lof
%   Date:   24/09/2024



function result = RL_angleBetweenVectors_RAS(V1_RAS,V2_RAS)    
    % Convert structs to arrays with rows of triples
    V1 = [V1_RAS.right(:), V1_RAS.anterior(:), V1_RAS.superior(:)];
    V2 = [V2_RAS.right(:), V2_RAS.anterior(:), V2_RAS.superior(:)];   

    % calculate angles
    angles = RL_angleBetweenVectors_XYZ(V1, V2, 'row');

    % make output vector size as input vector size
    if RL_isRowVector(V1_RAS.right)
       result = angles';
    elseif RL_isColumnVector(V1_RAS.right)
       result = angles;
    else
        error('Error in rwl_angleBetweenVectors_RAS: the fields of the input must be row or column vector')
    end
end