% RL_CALC_DB Calculates the sound pressure level (SPL) in decibels of an audio signal.
%
%   dBspl = RWL_CALC_DB(wav, fs, Calibration_dB, frequencyWeighting) computes the SPL of an input
%   audio waveform by applying the specified frequency weighting, removing any DC bias, filtering
%   the signal if necessary, calculating the RMS voltage, and converting it to decibels using a
%   calibration factor.
%
%   Inputs:
%       wav                - Vector containing the audio waveform data.
%       fs                 - Sampling frequency of the audio signal in Hertz (Hz).
%       Calibration_dB     - Calibration factor in decibels to adjust the measured voltage to SPL.
%       frequencyWeighting - String specifying the frequency weighting filter to apply:
%                            'A' for A-weighting,
%                            'C' for C-weighting,
%                            'Z' for zero weighting (no weighting).
%
%   Outputs:
%       dBspl              - Calculated sound pressure level in decibels (dB SPL).
%
%   Example:
%       % Calculate the dB SPL of an audio signal with A-weighting
%       dBspl = rwl_calc_dB(audioSignal, 44100, -26, 'A');
%
%   Dependencies:
%       - filterA: Applies A-weighting filter to the signal.
%       - filterC: Applies C-weighting filter to the signal.
%       - rms: Computes the root mean square of the signal.
%       - rwl_Volt_to_dBspl: Converts RMS voltage to dB SPL using calibration.
%
%   Notes:
%       - The input audio signal 'wav' should be a single-channel vector.
%       - The function removes any DC offset from the audio signal before processing.
%       - The 'frequencyWeighting' parameter must be 'A', 'C', or 'Z'. An error is thrown otherwise.
%
%   Author: Ruurd Lof
%   Date:   2022
%
%   See also: filterA, filterC, rms, RL_Volt_to_dBspl, RL_dBspl_to_Volt

function dBspl = RL_calc_dB(wav, fs, Calibration_dB, frequencyWeighting)
    wav = wav - mean(wav); %remove bias
    switch frequencyWeighting
        case 'A'
            wavFiltered = filterA(wav, fs);
        case 'C'
            wavFiltered = filterC(wav, fs);
        case 'Z'
            wavFiltered = wav;
        otherwise
            ME = MException('rwl_calc_dB:inputError', 'frequencyWeighting must be A, C or Z');
            throw(ME);
    end
    rmsV = rms(wavFiltered);
    dBspl = rwl_Volt_to_dBspl(rmsV, Calibration_dB);
end