% RWL_ANGLEBETWEENVECTORS_XYZ Calculates the angle between vectors in XYZ coordinates.
%
%   result = RWL_ANGLEBETWEENVECTORS_XYZ(V1, V2, varargin) computes the angle(s) between two sets
%   of vectors provided in XYZ coordinate format. The vectors can be arranged either in rows or
%   columns, and this orientation can be specified using an optional argument.
%
%   Inputs:
%       V1        - Matrix containing the first set of vectors. Each vector is represented as a
%                   triple of XYZ components.
%       V2        - Matrix containing the second set of vectors. Must be the same size as V1.
%       varargin  - (Optional) Specifies the orientation of the vectors in V1 and V2.
%                   Accepts 'row' for vectors stored in rows or 'col' for vectors stored in columns.
%                   If not specified, the function will attempt to determine the orientation
%                   automatically, but will throw an error if ambiguous.
%
%   Outputs:
%       result    - Angle(s) between the input vectors in radians.
%
%   Example:
%       % Calculate the angle between two vectors stored in rows
%       V1 = [1, 0, 0];
%       V2 = [0, 1, 0];
%       angle = rwl_angleBetweenVectors_XYZ(V1, V2, 'row');
%
%       % Calculate the angle between sets of vectors stored in columns
%       V1 = [1, 0, 0; 0, 1, 0; 0, 0, 1]';
%       V2 = [0, 1, 0; 1, 0, 0; 0, -1, 0]';
%       angle = rwl_angleBetweenVectors_XYZ(V1, V2, 'col');
%
%   Dependencies:
%       - None
%
%   Notes:
%       - V1 and V2 must be of the same size.
%       - If the orientation of the vectors cannot be determined unambiguously, specify 'row' or
%         'col' as an additional argument.
%       - The function returns the angles in radians.
%
%   Author: Ruurd Lof
%   Date:   24/09/2024
%
%   See also: acos, dot, vecnorm

function result = RL_angleBetweenVectors_XYZ(V1, V2, varargin)
    % get the dimensions of the matrices V1 and V2    
    sizeV1 = size(V1);
    sizeV2 = size(V2);

    % determine the dimension of the vector triples
    if sizeV1(1) == 3
       vecDim = 1;
    end    
    if sizeV1(2) == 3
       vecDim = 2; 
    end

    if ~isempty(varargin) 
        inputString = lower(varargin{1});
        switch inputString
            case 'row'
                vecDim = 2;
            case 'col'
                vecDim = 1;
            otherwise
                error('Error in rwl_angleBetweenVectors_XYZ: varargin should be "row" or "col"');
        end
    end
    
    % check the dimensions and raise error if not correct 
    if isempty(varargin) && (sizeV1(1) == sizeV1(2))
        error('Error in rwl_angleBetweenVectors_XYZ: vectorDimension is ambiguous, please use extra parameter');
    end
    if ~(sizeV1 == sizeV2)
        error('Error in rwl_angleBetweenVectors_XYZ: size of V1 and V2 do not match');
    end
    if ~(sizeV1(vecDim) == 3)
        error('Error in rwl_angleBetweenVectors_XYZ: the value of vectorDimension does not result in triples for V1 or V2');
    end

    % determine vector norms and dot products
    normtype = 2; % 2 is euclidian norm
    norms1 = vecnorm(V1, normtype, vecDim);
    norms2 = vecnorm(V2, normtype, vecDim);
    dotProducts = dot(V1, V2, vecDim);

    % calculate angle in radians and assign to output
    result = acos(dotProducts ./ (norms1 .* norms2)); 
end