% RL_APPLYHANNINGRAMP Applies a Hanning ramp to the beginning, end, or both sides of a signal.
%
% PARAMETERS:
%   signal         - Input signal vector or matrix (time along columns).
%   rampDuration   - Duration of the ramp in seconds.
%   sampleFrequency- Sample frequency of the signal in Hz.
%   rampPosition   - Specifies the side to apply the ramp:
%                    1 = left side, 2 = right side, 3 = both sides.
%
% RETURNS:
%   rampedSignal - Signal with applied Hanning ramp.

function rampedSignal = RL_applyHanningRamp(signal, rampDuration, sampleFrequency, rampPosition)
    
    % Ensure time axis is along columns (transpose if time is along rows)
    if size(signal, 1) < size(signal, 2)
        signal = signal';  
    end
    
    signalLength = size(signal, 1);  % Length of the signal (time axis)
    numChannels  = size(signal, 2);  % Number of channels (column count)
    
    % Calculate the length of each Hanning ramp in samples based on rampDuration and sampleFrequency
    rampLengthInSamples = floor(rampDuration * sampleFrequency);  
    
    % Create left Hann ramp (cosine-squared window) from -pi/2 to 0
    rampValues = linspace(0, pi/2, rampLengthInSamples);  
    leftHannRamp = sin(rampValues);  % Squared sine ramp for the left side
    rightHannRamp = fliplr(leftHannRamp);  % Squared sine ramp for the right side
    
    % Initialize a vector of ones for signal multiplication
    multiplicationVector = ones(signalLength, 1);  
    
    % Apply the left and/or right Hanning ramps based on the specified position
    switch rampPosition
        case 'left'  % Left side ramp
            multiplicationVector(1:rampLengthInSamples)         = leftHannRamp;
        case 'right'  % Right side ramp
            multiplicationVector(end-rampLengthInSamples+1:end) = rightHannRamp;
        case 'both'  % Symmetrical ramps on both sides
            multiplicationVector(1:rampLengthInSamples)         = leftHannRamp;
            multiplicationVector(end-rampLengthInSamples+1:end) = rightHannRamp;
    end
    
    % Extend the multiplication vector to match all signal channels
    multiplicationMatrix = repmat(multiplicationVector, 1, numChannels);
    
    % Apply the ramp to the signal
    rampedSignal = signal .* multiplicationMatrix;

end
