function result = RL_isColumnVector(V)
   result = iscolumn(V);
end