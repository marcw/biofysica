function [a, u] = assemble(val, x, varargin)
% ASSEMBLE groups values by unique keys and applies a function.
%
% [A, U] = ASSEMBLE(Y, X)            groups Y values by each unique value U in X and
%                                    calculates their mean.
%
% [A, U] = ASSEMBLE(Y, X, 'fun', @FUNCTION)  applies @FUNCTION instead of
%                                            the default mean.
%
% Arguments:
% - val:         the values to be aggregated
% - x:           the keys by which to aggregate values
% - 'fun':       the function to apply to each group of values
%
% Returns:
% - a:           aggregated values
% - u:           unique keys
%
% See also: ACCUMARRAY, UNIQUE, INPUTPARSER

%% Aggregation functiob
% Initialize input parser
p				= inputParser;

% Add optional parameter and its default value
addOptional(p, 'fun', @mean, @(x) isa(x, 'function_handle'));

% Parse input arguments
parse(p, varargin{:});

% Retrieve aggregation function
fun				= p.Results.fun;

%% Align dimensions
% Transpose 'val' and 'x' if required to align dimensions
[rowsVal, ~]	= size(val);
[rowsX, colsX]	= size(x);

if colsX == rowsVal
	x			= transpose(x);
end

if rowsVal < rowsX
	val			= transpose(val);
end

% Find unique keys and their indices
[u, ~, subs]	= unique(x, 'rows');

%% Perform aggregation
a				= accumarray(subs, val, [], fun);

