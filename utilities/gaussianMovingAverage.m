function result = gaussianMovingAverage(data, windowSize, sigma)

    dataSize = length(data);

    % Maak het Gaussiaanse venster    
    gaussianWindow = gausswin(windowSize, sigma);
    
    % Normeer het venster zodat de som gelijk is aan 1
    gaussianWindow = gaussianWindow / sum(gaussianWindow);
    
    % add padding
    paddingSize = 2*windowSize;
    leftPadding  = ones(1,paddingSize) * data(1);
    rightPadding = ones(1,paddingSize) * data(end);
    paddedData = [leftPadding, data, rightPadding];

    % bereken het Gaussiaanse gemiddelde met convolutie
    paddedConvolution = conv(paddedData, gaussianWindow, 'same');
    
    % remove padding
    convolution = paddedConvolution(paddingSize+1:paddingSize+dataSize);

    % assign result
    result = convolution;
end