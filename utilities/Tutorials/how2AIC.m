function how2AIC
clear
clc
clf

% Computing and using Akaikes information criterion
% Ideally you'll see that the log-likelihood of model 4 is equal to model 1
% However model 1 should be the better model (lower AIC), having less 
% parameters.
%
% The AIC and relative likelihood formulas taken from:
% https://en.wikipedia.org/wiki/Akaike_information_criterion
%
% For the maximum likelihood estimate (MLE).
% StatQuest is great: https://www.youtube.com/watch?v=XepXtl9YKwc
% And a very simple entry, that for some reason helped me understand how 
% to code the estimation part:
% https://towardsdatascience.com/probability-concepts-explained-maximum-likelihood-estimation-c7b4342fdbb1
%
% I. Calderon (2023)

rng(18231); % To make this repeatable 

%% Dataset
% First we create some random data that is normally distributed with mean
% 0.5*exp(x*(1/2.3)) and sigma = 18. 
% These numbers just gave good results for this demo.Not special at all.
x       = linspace(0,15,20);
a       = 0.5;
b       = 1/2.3;
mu      = a*exp(x.*b);
sigma   = 18;

% Generate the data
y = normrnd(mu,sigma);

%% Models
% Next, we will fit three models using a maximum likelihood estimate
fun_mu_1  = @(b,x) b(1)*exp(x.*b(2)); % M1: Exponential
fun_mu_2  = @(b,x) b(1)+x.*b(2);      % M2: Linear
fun_mu_3  = @(b,x) b(1)+x.*b(2)+(x.^2).*b(3); % M3: Quadratic
fun_mu_4  = @(b,x) b(1)+x.*b(2)+(x.^2).*b(3) +(x.^3).*b(4); % M4: Cubic

% Define negative log-likelihood functions to minimize for each model
% In Matlab (and in general), algorithms search for minima, not for maxima.
% Then we find the minimum of the negative function yielding the max.
neg_log_likelihood{1} = @(p) -sum(log(eps+normpdf(y,fun_mu_1(p(1:2),x),p(3))));
neg_log_likelihood{2} = @(p) -sum(log(eps+normpdf(y,fun_mu_2(p(1:2),x),p(3))));
neg_log_likelihood{3} = @(p) -sum(log(eps+normpdf(y,fun_mu_3(p(1:3),x),p(4))));
neg_log_likelihood{4} = @(p) -sum(log(eps+normpdf(y,fun_mu_4(p(1:4),x),p(5))));

%% Fitting using MLE
% Now, for each model, find the best fit
maximized_LogLike = zeros(1,length(neg_log_likelihood));
optimized_params = cell(1,length(neg_log_likelihood));
for n = 1:length(neg_log_likelihood)
    % Set initial parameter values for each model
    if  n==3
        initial_params = [1,1,1,10];
    elseif n==4
        initial_params = [1,1,1,1,10];
    else
    	initial_params = [1,1,10];
    end
    
    % Find parameters that minimize the negative log-likelihood
    options	= optimset('MaxIter', 300000,...
                 'MaxFunEvals',10000,...
                 'TolFun',1e-16,...
                 'TolX',1e-16);   
              
    [optimized_params{n},n_ll] = fminsearch(neg_log_likelihood{n},...
                                            initial_params,...
                                            options);

    % Now store the maximized likelihood
    maximized_LogLike(n) = -n_ll; % take the negative, since we minimized
                                  % the negative Log-likelihood and we'll
                                  % use this value later.
    
end

%% Wrapping-up: AIC, Plots, etc.
% First we plot data and fits for visualization
subplot(2,3,1)
plot(x,y,'o','markerfacecolor','w','markeredgecolor','k','markersize',12)
hold on;
plot(x,fun_mu_1(optimized_params{1},x),'-','linew',3.5)
plot(x,fun_mu_2(optimized_params{2},x),'-.','linew',3.5)
plot(x,fun_mu_3(optimized_params{3},x),':','linew',3.5)
plot(x,fun_mu_4(optimized_params{4},x),'--','linew',3.5)
nicegraph;
set(gca,'Fontsize',12)
legend('Data','\mu_1=Ae^{Bx}','\mu_2=A+Bx','\mu_3=A+Bx+Cx^2',...
    '\mu_4=A+Bx+Cx^2+Dx^3','location','NorthWest')
xlabel('x')
ylabel('y')
title('Data and fits')

%% AIC -- Based on log-likelihood
% Akaikes information criteria rewards good fits (higher log-likelihood),
% while it penalizes number of parameters, using the following equation:
% AIC = 2*number of parameters - 2*maximized-log-likelihood
% From this you can see that the best parameter will be the one with the
% lowest AIC (i.e. less parameters, and highest likelihood)
AIC     = zeros(1,length(neg_log_likelihood));
nparams = cellfun(@numel,optimized_params);
for n = 1:length(neg_log_likelihood)
    AIC(n) = 2*nparams(n) - 2*maximized_LogLike(n); 
end
fprintf('--------------------------- \n')
fprintf('AIC Model 1: %.3f \n',AIC(1))
fprintf('AIC Model 2: %.3f \n',AIC(2))
fprintf('AIC Model 3: %.3f \n',AIC(3))
fprintf('AIC Model 4: %.3f \n',AIC(4))

% Plot delta-AIC: This is the difference relative to the lowest AIC value
subplot(2,3,2)
bar([1:4],AIC-min(AIC));
nicegraph;
xlim([0,5]);
set(gca,'Fontsize',12)
set(gca,'xtick',1:4,'xticklabel',{'Model 1','Model 2','Model 3','Model 4'})
ylim([0,max(AIC-min(AIC))+10])
ylabel('\Delta AIC')
title('Model 1 is the preferred (lowest AIC)')

% PLot the log-likelihood for each model
subplot(2,3,5)
bar([1:4],maximized_LogLike);
nicegraph;
xlim([0,5]);
set(gca,'Fontsize',12)
set(gca,'xtick',1:4,'xticklabel',{'Model 1','Model 2','Model 3','Model 4'})
ylabel('Log-likelihood')
title('Model 1 and 4 have equal likelihood...')
%% Now calculate and plot the relative likelihood
% The relative likelihood tells you how likely a model is to maximize the 
% likelihood compared to another. 
% Notice in this case model 4 is ~0.2 times as likely as model 1 to
% maximize the likelihood, again pointing towards model 1 being the best.
% While the other are effectively 0 times as likely.
subplot(2,3,3)
rel_likelihood = exp((min(AIC)-AIC)./2); % equation for rel.likelihood
bar([1:4],rel_likelihood);
nicegraph;
xlim([0,5]);
set(gca,'Fontsize',12)
set(gca,'xtick',1:4,'xticklabel',{'Model 1','Model 2','Model 3','Model 4'})
ylim([0,1.05])
ylabel('Relative likelihood')
title('...but model 1 has less parameters')

end