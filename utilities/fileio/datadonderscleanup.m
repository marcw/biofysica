% DATADONDERSCLEANUP
%
% Get rid of Mac ._ files (extended attribute [EA] files)
%
% - Set current directory to the Repository directory
% - Run/call this code

%% Clear & close
clearvars
close all

%% Determine which files are EA files
d		= dir('._*');
fnames	= {d.name};
disp(fnames);
nfiles	= numel(fnames);

%% And delete
for ii = 1:nfiles
	fname = fnames{ii};
	disp(fname);
	delete(fname);
end