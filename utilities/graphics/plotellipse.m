function h = plotellipse(Mu, SD, Phi, varargin)
% PLOTELLIPSE Plots an ellipse on the current figure.
% 
% USAGE:
%   h = PLOTELLIPSE(Mu, SD, Phi, Name, Value)
% 
% INPUTS:
%   Mu   - [x, y] coordinates of the ellipse center.
%   SD   - [Major, Minor] standard deviations (axes lengths).
%   Phi  - Orientation of the ellipse in degrees.
%   Name-Value pairs for additional properties (e.g., 'Color', 'r').
% 
% OUTPUTS:
%   h    - Handle to the ellipse patch object.

% Constants
DTR = pi/180; % Degrees to Radians

% Input parsing
p = inputParser;
addParameter(p, 'Color', 'k');
parse(p, varargin{:});
color = p.Results.Color;

% Ellipse parameters
Xo = Mu(1);
Yo = Mu(2);
MajorAxis = SD(1);
MinorAxis = SD(2);
Phi = Phi * DTR;

% Compute ellipse points
wt = linspace(0, 2*pi, 361);
X = Xo + MajorAxis * cos(Phi) * cos(wt) - MinorAxis * sin(Phi) * sin(wt);
Y = Yo + MajorAxis * sin(Phi) * cos(wt) + MinorAxis * cos(Phi) * sin(wt);

% Plotting
h = patch(X, Y, color, 'EdgeColor', color, 'LineWidth', 1);
hold on;
alpha(h, 0.4);
