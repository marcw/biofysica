function distplot(x,y,varargin)
% DISTPLOT(X,Y)
%
% Violin plot, plotting the distributions.
%
% 

p			= inputParser;
addRequired(p,'x')
[ux,~,ix]	= unique(x);
uix			= unique(ix);
nx			= numel(ux);
addRequired(p,'y')
if exist('cbrewer','file')
	col = cbrewer('qual','Pastel1',nx);
else
	col = jet(nx);
end
addOptional(p,'Color',col)
parse(p,x,y,varargin{:});

for ii		= 1:nx
	sel		= ix==uix(ii);
	[F,XI]	= ksdensity(y(sel));
	F		= 0.4*F./max(F);
	f		= [F; -F]+ii;
	xi		= [XI; XI];
	hpatch	= patch(f',xi',p.Results.Color(ii,:));
	hold on

	set(hpatch,'EdgeColor','none');
	alpha(hpatch,0.5);
	hdi = hdimcmc(y(sel));
	mu = mean(y(sel));
	plot([ii ii],hdi,'-','LineWidth',3,'Color',p.Results.Color(ii,:));
	plot(ii,mu,'o','MarkerFaceColor',p.Results.Color(ii,:),'MarkerEdgeColor',p.Results.Color(ii,:),'MarkerSize',15);
end
set(gca,'XTick',1:nx,'XTickLabel',ux);
xlabel('independent parameter');
ylabel('dependent parameter');
nicegraph;