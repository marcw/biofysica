function smoothimagesc(Y,x1,x2)
% SMOOTHIMAGESC(Y,x1,x2)


%%
cmap		= cbrewer('div','RdYlBu',64,'pchip');
cmap		= flipud(cmap);
nx1			= numel(x1);
nx2			= numel(x2);

%%
[X1,X2]		= meshgrid(x1,x2);
x1i			= linspace(1,nx1,200);
x2i			= linspace(1,nx2,200);
[X1i,X2i]	= meshgrid(x1i,x2i);
X1idx		= replacexy(X1,1:nx1,x1);
X2idx		= replacexy(X2,1:nx2,x2);
cidx = 1/3:1/3:3+2/3;
cidx = linspace(min(Y(:))+1+1/3,max(Y(:)),20);

colormap(cmap);


Mi			= interp2(X1idx,X2idx,Y,X1i,X2i,'makima');

imagesc(x1i,x2i,Mi);
hold on
contour(x1i,x2i,Mi,cidx,'k');
nicegraph;
caxis([1/3 max(Y(:))+1/3]);
set(gca,'XTick',1:nx1,'XTickLabel',x1,'YTick',1:nx2,'YTickLabel',x2);
grid off;
colorbar;
xlabel('parameter 1');
ylabel('parameter 2');

