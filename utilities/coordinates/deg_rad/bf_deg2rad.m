% Convert degrees to radians
%
% function RAD = DEG2RAD(DEG)
%
% Conversion function from Radians to Degrees.
%
function RAD = bf_deg2rad(DEG)

RAD = (pi/180)*DEG;