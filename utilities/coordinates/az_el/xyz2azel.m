% azel = xyz2azel(X,Y,Z)
%
%  coordinate trazelformation (x,y,z) -> (az, el). If x is a matrix with three
%  columns (and y and z are not given), they are taken to be [x y z].
%
% N.B. Orientation  of X, Y and Z are different in azel2cart.m

function azel=xyz2azel(x,y,z)

RTD = (180.0 / pi);

if (nargin==1 && size(x,2)==3)
  y=x(:,2); % y = left to right
  z=x(:,3); % z = front to back
  x=x(:,1); % x = bottom to top
end

azel		= zeros(length(x),2);
azel(:,1)	= RTD * atan2 (y, sqrt (x.^2 + z.^2)); % azimuth is horizontal angle with XZ plane
azel(:,2)	= RTD * atan2 (x, sqrt (y.^2 + z.^2)); % elevation is vertical angle with YZ plane
