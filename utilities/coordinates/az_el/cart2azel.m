% azel = xyz2azel(X,Y,Z)
%
%  coordinate trazelformation (x,y,z) -> (az, el). If x is a matrix with three
%  columns (and y and z are not given), they are taken to be [x y z].
%

function azel=cart2azel(x,y,z)
    azel = xyz2azel(x,y,z);
end