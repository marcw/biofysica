% by Ruurd Lof
% 12-03-2024

classdef coordinates_DP <handle

    properties
        azimuth
        azimuth_deg
        elevation
        elevation_deg
        radius
        hemisphere
        variableUnits
        asTable
    end

    properties(Access = protected)
        min_azimuth   = -pi/2; % -90 deg
        max_azimuth   =  pi/2; % +90 deg
        min_elevation = -pi/2; % -90 deg
        max_elevation =  pi/2; % +90 deg       
        myTable
    end


    methods
        function obj = coordinates_DP(azimuth, elevation, radius, hemisphere)
            
            % check input parameters            
            condition_azimuth = (azimuth >= obj.min_azimuth & azimuth <= obj.max_azimuth) | isnan(azimuth);            
            invalid_indices = find(~condition_azimuth);
            if ~isempty(invalid_indices)
                error('Azimuth is not within limits at indices: %s', mat2str(invalid_indices));
            end
            condition_elevation = (elevation >= obj.min_elevation & elevation <= obj.max_elevation) | isnan(elevation);            
            invalid_indices = find(~condition_elevation);
            if ~isempty(invalid_indices)
                error('Elevation is not within limits at indices: %s', mat2str(invalid_indices));
            end
            condition_radius = (radius > 0) | isnan(radius);            
            invalid_indices = find(~condition_radius);
            if ~isempty(invalid_indices)
                error('Radius is not within limits at indices: %s', mat2str(invalid_indices));
            end
            condition_hemisphere = (ismember(hemisphere, [-1, 0, 1])) | isnan(hemisphere);            
            invalid_indices = find(~condition_hemisphere);
            if ~isempty(invalid_indices)
                error('hemisphere is not in  [-1, 0, 1] at indices: %s', mat2str(invalid_indices));
            end            

            % create table
            obj.myTable = table(azimuth, elevation, radius, hemisphere);

            % set properties and userdata
            obj.myTable.Properties.VariableUnits = {'rad', 'rad', 'm', '{-1,+1}'};             
        end

        function add(obj, azimuth, elevation, radius, hemisphere)
            obj.myTable = vertcat(obj.myTable, table(azimuth, elevation, radius, hemisphere));                                
        end

        function result = get.asTable(obj)
           result = obj.myTable;
        end

        function result = selectPointByRow(obj, row)
            p = obj.myTable(row,:);
            result = coordinates_DP(p.azimuth, p.elevation, p.radius, p.hemisphere);
        end

        function result = get.azimuth(obj)
            result = obj.myTable.azimuth;
        end

        function result = get.azimuth_deg(obj)
            result = rad2deg(obj.azimuth);
        end

        function result = get.elevation(obj)
            result = obj.myTable.elevation;
        end

        function result = get.elevation_deg(obj)
            result = rad2deg(obj.elevation);
        end


        function result = get.radius(obj)
            result = obj.myTable.radius;
        end

        function result = get.hemisphere(obj)
            result = obj.myTable.hemisphere;
        end    

        function result = get.variableUnits(obj)
            result = obj.myTable.Properties.VariableUnits;
        end

    end
end