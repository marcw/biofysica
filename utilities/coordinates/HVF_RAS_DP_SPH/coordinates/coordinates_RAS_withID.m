% by Ruurd Lof
% 12-03-2024

classdef coordinates_RAS_withID < coordinates_RAS
    properties
        ID
    end
    
    methods
        function obj = coordinates_RAS_withID(ID, right, anterior, superior)
            % call parent
            obj@coordinates_RAS(right, anterior, superior);
            % add ID
            obj.ID = ID;
        end

        function obj = add(obj, ID, right, anterior, superior)
            obj.myTable = vertcat(obj.myTable, table(ID, right, anterior, superior));                     
        end 

        function result = selectPointByID(obj, ID)
            rowIndex = find(obj.myTable.ID == ID);
            result = obj.selectPointByRow(rowIndex);
        end        
    end
end