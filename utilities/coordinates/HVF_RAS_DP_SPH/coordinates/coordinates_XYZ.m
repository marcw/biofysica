% by Ruurd Lof
% 12-03-2024
% EyeSeeCamSci coordinates are defined by the EyeSeeCamSci headtracker

classdef coordinates_XYZ < handle   
    properties
        X
        Y
        Z
        variableUnits
        asTable
    end

    properties (Access = protected)
        myTable
    end

    methods
        function obj = coordinates_XYZ(varargin) 
            [X, Y, Z] = resolveTriples(varargin);
            obj.myTable = table(X, Y, Z);
            obj.myTable.Properties.VariableUnits = {'m', 'm', 'm'};            
        end

        function add(obj, varargin)
            [X, Y, Z] = resolveTriples(varargin); %#ok<PROPLC> 
            obj.myTable = vertcat(obj.myTable, table(X, Y, Z)); %#ok<PROPLC> 
        end     

        function result = get.asTable(obj)
           result = obj.myTable;
        end        
  
        function result = get.X(obj)
            result = obj.myTable.X;
        end

        function result = get.Y(obj)
            result = obj.myTable.Y;
        end

        function result = get.Z(obj)
            result = obj.myTable.Z;
        end

        function result = get.variableUnits(obj)
            result = obj.myTable.Properties.VariableUnits;
        end

    end
end