% by Ruurd Lof
% 12-03-2024

% RAS coordinates are medical system coordinates always defined relative to the body or the head
% R = horizontal axis positive to the right
% A = horizontal axis positive to the anterior direction
% S = vertical axis positive to the superior direction


classdef coordinates_RAS < handle   
    properties
        right
        anterior
        superior
        R
        A
        S
        variableUnits
        asTable        
    end

    properties (Access = protected)
        myTable
    end

    methods
        function obj = coordinates_RAS(varargin)
            switch length(varargin)
                case 1
                    point    = varargin{1};
                    right    = point(1);
                    anterior = point(2);
                    superior = point(3);                    
                case 3
                    right    = varargin{1};
                    anterior = varargin{2};
                    superior = varargin{3};
            end             
            obj.myTable = table(right, anterior, superior);
            obj.myTable.Properties.VariableUnits = {'m', 'm', 'm'};            
        end

        function add(obj, varargin)
            [right, anterior, superior] = resolveTriples(varargin); %#ok<PROPLC> 
            obj.myTable = vertcat(obj.myTable, table(right, anterior, superior));  %#ok<PROPLC> 
        end

        function result = selectRows(obj, rows)
            p = obj.myTable(rows);
            result = coordinates_DP(p.right, p.anterior, p.superior);
        end

        function result = asRowVector(obj, row)            
            p = obj.myTable(row,:);
            result = [p.right, p.anterior, p.superior];
        end

        function result = asColumnVector(obj, row)            
            result = transpose(obj.asRowVector(row));
        end        

        function result = get.asTable(obj)
           result = obj.myTable;
        end        
  
        function result = get.right(obj)
            result = obj.myTable.right;
        end

        function result = get.anterior(obj)
            result = obj.myTable.anterior;
        end

        function result = get.superior(obj)
            result = obj.myTable.superior;
        end

       function result = get.R(obj)
            result = obj.myTable.right;
        end

        function result = get.A(obj)
            result = obj.myTable.anterior;
        end

        function result = get.S(obj)
            result = obj.myTable.superior;
        end        

        function result = get.variableUnits(obj)
            result = obj.myTable.Properties.VariableUnits;
        end

    end
end