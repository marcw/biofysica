% by Ruurd Lof
% 12-03-2024

classdef coordinates_SPH_withID < coordinates_SPH
    properties
        ID
    end
    
    methods
        function obj = coordinates_SPH_withID(ID, azimuth, elevation, radius)
            % call parent
            obj@coordinates_SPH(azimuth, elevation, radius);
            % add ID
            obj.ID = ID;          
        end

        function obj = add(obj, ID, azimuth, elevation, radius)
            obj.myTable = vertcat(obj.myTable, table(ID, azimuth, elevation, radius));           
        end  

        function result = selectPointByID(obj, ID)
            rowIndex = find(obj.myTable.ID == ID);
            result = obj.selectPointByRow(rowIndex);
        end        

    end
end