% by Ruurd Lof
% 12-03-2024

% HVF coordinates are defined relative to the default orientation of a
% subject (test person) in the lab
% H = horizontal (positive is to the right of the person)
% V = vertical (positive = up)
% F = frontal  (positive = forward)

classdef coordinates_HVF < handle   
    properties
        right
        up
        forward
        H
        V
        F
        variableUnits
        asTable
    end

    properties (Access = protected)
        myTable
    end

    methods
        function obj = coordinates_HVF(right, up, forward)
            obj.myTable = table(right, up, forward);
            obj.myTable.Properties.VariableUnits = {'m', 'm', 'm'};            
        end

        function add(obj, right, up, forward)
            obj.myTable = vertcat(obj.myTable, table(right, up, forward));                     
        end

        function result = selectPointByRow(obj, row)
            p = obj.myTable(row);
            result = coordinates_DP(p.right, p.up, p.forward);
        end        

        function result = get.asTable(obj)
           result = obj.myTable;
        end        
  
        function result = get.right(obj)
            result = obj.myTable.right;
        end

        function result = get.forward(obj)
            result = obj.myTable.forward;
        end

        function result = get.up(obj)
            result = obj.myTable.up;
        end
  
        function result = get.H(obj)
            result = obj.myTable.right;
        end

        function result = get.F(obj)
            result = obj.myTable.forward;
        end

        function result = get.V(obj)
            result = obj.myTable.up;
        end        

        function result = get.variableUnits(obj)
            result = obj.myTable.Properties.VariableUnits;
        end

    end
end