% resolveTriples extracts or resolves three variables (X, Y, Z) from the input.
% The function supports two input formats: 
% 1. A single matrix or cell array containing three columns.
% 2. Three separate column vectors.
%
% INPUTS:
%   varargin - Either:
%              1. A single matrix or cell array with three columns, where each column
%                 represents X, Y, and Z.
%              2. Three separate column vectors (X, Y, Z).
%
% OUTPUTS:
%   X        - The first resolved vector or value
%   Y        - The second resolved vector or value
%   Z        - The third resolved vector or value
%
% The function automatically detects the input format and extracts the values for X, Y, and Z.
% It throws an error if the input format is not valid or if the inputs are not column vectors.

function [X, Y, Z] = resolveTriples(varargin)

    % Controleer het aantal invoerargumenten
    if nargin == 1
        input = varargin{1};
        if iscell(input) && length(input) == 1
            input = input{1};
            X = input(1);
            Y = input(2);
            Z = input(3);
        end
        if size(input, 2) == 3
            % Invoer is een matrix met drie kolommen
            X = input{1,1};
            Y = input{1,2};
            Z = input{1,3};
        end
    elseif nargin == 3
        % Controleer of alle invoeren kolomvectoren zijn
        if iscell(varargin) && all(cellfun(@iscolumn, varargin))
            X = varargin{1};
            Y = varargin{2};
            Z = varargin{3};
        else
            error('All inputs must be column vectors.');
        end
    else
        error('Function requires either one matrix, one column vector with length divisible by 3, or three column vectors as input.');
    end
end
