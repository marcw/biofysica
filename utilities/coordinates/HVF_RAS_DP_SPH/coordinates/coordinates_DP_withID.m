% by Ruurd Lof
% 12-03-2024

classdef coordinates_DP_withID < coordinates_DP
    properties
        ID
    end

    methods
        function obj = coordinates_DP_withID(ID, azimuth, elevation, radius, hemisphere)
            % call superclass method
            obj@coordinates_DP(azimuth, elevation, radius, hemisphere);
            % add ID
            obj.myTable.ID = ID;         
        end

        function obj = add(obj, ID, azimuth, elevation, radius, hemisphere)
            obj.myTable = vertcat(obj.myTable, table(ID, azimuth, elevation, radius, hemisphere));                                
        end

        function result = selectPointByID(obj, ID)
            rowIndex = find(obj.myTable.ID == ID);
            result = obj.selectPointByRow(rowIndex);
        end

        function result = get.ID(obj)
            result = obj.myTable.ID;
        end

    end

        
end