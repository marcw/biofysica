% by Ruurd Lof
% 12-03-2024

% For use of spherical coordinate system

classdef coordinates_SPH < handle

    properties
        azimuth
        azimuth_deg
        elevation
        elevation_deg
        radius
        variableUnits
        asTable
    end

    properties (Access = protected)
        min_azimuth   = -pi;
        max_azimuth   = pi;
        min_elevation = -pi/2;
        max_elevation = pi/2;
        myTable
    end

    methods
        function obj = coordinates_SPH(azimuth, elevation, radius)
            obj.myTable = table(azimuth, elevation, radius);

            % check input parameters
            condition_azimuth = (azimuth >= obj.min_azimuth) & (azimuth <= obj.max_azimuth);
            assert(all(condition_azimuth), 'Azimuth is not within limits');
            condition_elevation = elevation >= obj.min_elevation & elevation <= obj.max_elevation;
            assert(all(condition_elevation), 'Elevation is not within limits');
            condition_radius = radius > 0; 
            assert(all(condition_radius), 'Radius should be > 1');

            % set properties and userdata
            obj.myTable.Properties.VariableUnits = {'rad', 'rad', 'm'};
        end

        function add(obj, azimuth, elevation, radius)
            obj.myTable = vertcat(obj.myTable, table(azimuth, elevation, radius));           
        end

        function result = selectPointByRow(obj, row)
            p = obj.myTable(row);
            result = coordinates_DP(p.azimuth, p.elevation, p.radius);
        end         

        function result = get.asTable(obj)
           result = obj.myTable;
        end        

        function result = get.azimuth(obj)
            result = obj.myTable.azimuth;
        end

        function result = get.azimuth_deg(obj)
            result = rad2deg(obj.azimuth);
        end

        function result = get.elevation(obj)
            result = obj.myTable.elevation;
        end

        function result = get.elevation_deg(obj)
            result = rad2deg(obj.elevation);
        end        

        function result = get.radius(obj)
            result = obj.myTable.radius;
        end        

        function result = get.variableUnits(obj)
            result = obj.myTable.Properties.VariableUnits;
        end        
    end
end