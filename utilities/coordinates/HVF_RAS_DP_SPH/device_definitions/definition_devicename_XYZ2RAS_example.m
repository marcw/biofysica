% definition_devicename_XYZ2RAS_example provides an example for a transformation matrix for converting 
% XYZ coordinates to RAS (Right, Anterior, Superior) coordinates.
%
% OUTPUT:
%   result               - Struct containing the following fields:
%                          device      - name of the device for which the
%                                        transformation applies
%                          description - A textual representation of how the XYZ axes map
%                                        to the RAS axes.
%                          matrix      - A 3x3 transformation matrix used to convert
%                                        XYZ coordinates to RAS coordinates.
%
% This matrix is applied to XYZ coordinates to obtain the corresponding RAS coordinates
% according to the mapping defined above.
%
% This function can be used as a TEMPLATE for specific definitions
%

function result = definition_devicename_XYZ2RAS_example

    result.device      = 'example';
    result.description = 'X => Anterior, Y => Superior, Z => Right';

    result.matrix = ...
        [0, 1, 0;
         0, 0 ,1;
         1, 0, 0];
end

