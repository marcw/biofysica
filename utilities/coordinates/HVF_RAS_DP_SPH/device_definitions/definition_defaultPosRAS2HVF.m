% definition_defaultPosRAS2HVF provides the default transformation matrix for converting 
% RAS (Right, Anterior, Superior) coordinates to HVF (Horizontal, Vertical, Frontal) coordinates.
%
% OUTPUT:
%   result               - Struct containing the following fields:
%                          device      - name of the device for which the
%                                        transformation applies
%                          description - A textual representation of how the RAS axes map
%                                        to the HVF axes.
%                          matrix      - A 3x3 transformation matrix used to convert
%                                        RAS coordinates to HVF coordinates.
%
% This matrix is applied to RAS coordinates to obtain the corresponding HVF coordinates
% according to the mapping defined above.
%

function result = definition_defaultPosRAS2HVF  

    result.device      = 'Coil head movement detection (in labs)';
    result.description = 'horizontal => right; vertical => superior; frontal => anterior';
    
    result.matrix = ...
        [ 1,  0,  0;
          0,  0,  1;
          0,  1,  0 ];
end

