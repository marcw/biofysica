% by Ruurd Lof
% 12-03-2024

classdef HVF_headtracker_fieldValues
    properties
        horizontal
        vertical
        frontal
        variableUnits
        asTable
    end

    properties (Access = protected)
        myTable
    end

    methods
        function obj = HVF_headtracker_fieldValues(H, V, F)
            obj.myTable = table(H, V, F);
            obj.myTable.Properties.VariableUnits = {'V', 'V', 'V'};            
        end

        function obj = add(obj, H, V, F)            
            obj.myTable = vertcat(obj.myTable, table(H, V, F));
        end

        function result = selectPoint(obj, row)
            p = obj.myTable(row);
            result = coordinates_DP(p.H, p.V, p.F);
        end        

        function result = get.asTable(obj)
           result = obj.myTable;
        end         
  
        function result = get.horizontal(obj)
            result = obj.myTable.H;
        end

        function result = get.vertical(obj)
            result = obj.myTable.V;
        end

        function result = get.frontal(obj)
            result = obj.myTable.F;
        end

        function result = get.variableUnits(obj)
            result = obj.myTable.Properties.VariableUnits;
        end

    end
end