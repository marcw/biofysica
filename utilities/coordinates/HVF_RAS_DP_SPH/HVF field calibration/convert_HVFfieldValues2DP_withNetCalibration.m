% by Ruurd Lof
% 12-03-2024

function DPcoordinates = convert_HVFfieldValues2DP_withNetCalibration(fieldValues, netCalibrationFile)
    % extract coordinates (voltages between -10 and 10)
         H = fieldValues.H;
         V = fieldValues.V;
         F = fieldValues.F;
         
    % convert H, V, F to azimuth and elevation
        [azimuth_deg, elevation_deg] = pa_calib(H,V,F, netCalibrationFile);        

    % convert azimuth and elevation to radians
        azimuth   = deg2rad(azimuth_deg);
        elevation = deg2rad(elevation_deg);

    % check in which hemisphere (forward or backward)
        azimuthIsForward     = abs(azimuth) <= pi/2; 
        elevationIsForward   = abs(elevation) <= pi/2;
        hemisphereIsForward  = azimuthIsForward & elevationIsForward;
        hemisphereIsBackward = ~hemisphereIsForward;  

    % initialize hemisphere with zeros
        hemisphere = zeros(size(H));    

    % assign hemisphere values
        hemisphere(hemisphereIsForward) = 1; 
        hemisphere(hemisphereIsBackward) = -1; 

    % flip backward values for azimuth    
        azimuth(azimuth > pi/2) = pi - azimuth(azimuth > pi/2);
        azimuth(azimuth < -pi/2) = azimuth(azimuth < -pi/2) - pi;

    % flip backward values for elevation    
        elevation(elevation > pi/2) = pi - elevation(elevation > pi/2);
        elevation(elevation < -pi/2) = elevation(elevation < -pi/2) - pi;

    % check azimuth and elevation: when out of bounds set to zero
        index = azimuth > pi/2 | azimuth < -pi/2;
        azimuth(index) = 0;

        index = elevation > pi/2 | elevation < -pi/2;
        elevation(index) = 0;

        index = (azimuth + elevation) > pi/2;
        azimuth(index) = 0;
        elevation(index) = 0;        

    % assign dummy values to radius
        radius = ones(size(azimuth));

    % convert to table and set to output
        DPcoordinates = coordinates_DP(azimuth, elevation, radius, hemisphere);     
end