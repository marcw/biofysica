% transform_HVF2DP transforms coordinates from the HVF (Horizontal-Vertical-Frontal) system to
% double polar (DP) coordinates, which include azimuth, elevation, radius, and hemisphere.
%
% INPUTS:
%   HVFcoordinates - Struct containing the HVF coordinates with fields:
%                    right   - Rightward (X) component
%                    up      - Upward (Y) component
%                    forward - Forward (Z) component
%                    (Optional) ID - Identifier for the coordinates (if present)
%
% OUTPUT:
%   DPcoordinates  - Struct containing the double polar coordinates with fields:
%                    azimuth    - Horizontal angle in radians
%                    elevation  - Vertical angle in radians
%                    radius     - Euclidean distance from the origin
%                    hemisphere - Sign of the forward component (1 for forward, -1 for backward)
%                    (Optional) ID - If input contains an ID, it is included in the output
%
% The function computes the Euclidean distance (radius) and angles (azimuth, elevation) from the
% HVF coordinates, and determines the hemisphere based on the forward component.
% If an ID is provided in HVFcoordinates, it will be included in the output.


% by Ruurd Lof
% 12-03-2024

function DPcoordinates = transform_HVF2DP(HVFcoordinates) 
    % Extract components
    right   = HVFcoordinates.right;
    up      = HVFcoordinates.up;
    forward = HVFcoordinates.forward;

    % Compute double polar coordinates
    euclidian  = 2;
    radius     = vecnorm([right, up, forward], euclidian, 2);    
    azimuth    = asin(right./radius);    
    elevation  = asin(up./radius);   
    hemisphere = sign(forward);

    % convert to table and set output
    if isprop(HVFcoordinates, 'ID')
        ID = HVFcoordinates.ID;
        DPcoordinates  = coordinates_DP_withID(ID, azimuth, elevation, radius, hemisphere);
    else
        DPcoordinates  = coordinates_DP(azimuth, elevation, radius, hemisphere);
    end    
end