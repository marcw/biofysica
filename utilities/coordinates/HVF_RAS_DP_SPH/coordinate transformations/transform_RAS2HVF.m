% transform_RAS2HVF transforms coordinates from the RAS (Right, Anterior, Superior)
% system to the HVF (Horizontal-Vertical-Frontal) coordinate system.
%
% INPUTS:
%   RAScoordinates      - Struct containing the RAS coordinates with fields:
%                         R - Rightward (X-coordinate in RAS)
%                         A - Anterior (Y-coordinate in RAS)
%                         S - Superior (Z-coordinate in RAS)
%
% OUTPUT:
%   result              - Struct containing the transformed HVF coordinates with fields:
%                         H - Horizontal (X-coordinate in HVF)
%                         V - Vertical (Y-coordinate in HVF)
%                         F - Frontal (Z-coordinate in HVF)
%
% The function first converts the RAS coordinates to the XYZ coordinate system,
% then applies the default transformation from XYZ to HVF using the predefined
% transformation matrix `definition_defaultPosRAS2HVF`.

function result = transform_RAS2HVF(RAScoordinates)  
   XYZcoordinates = coordinates_XYZ(RAScoordinates.R, RAScoordinates.A, RAScoordinates.S);
   result = transform_XYZ2HVF(XYZcoordinates, definition_defaultPosRAS2HVF);
end