% transform_device2RAS transforms device coordinates (X, Y, Z) to RAS (Right, Anterior, Superior)
% space using a provided 3x3 transformation matrix.
%
% INPUTS:
%   deviceCoordinates     - Struct containing the device coordinates with fields:
%                           X - X-coordinates (e.g., Right direction)
%                           Y - Y-coordinates (e.g., Anterior direction)
%                           Z - Z-coordinates (e.g., Superior direction)
%   definition_XYZ2RAS    - Struct containing a 3x3 transformation matrix (definition_XYZ2RAS.matrix)
%                           that defines how to map device coordinates to RAS space.
%
% OUTPUT:
%   result                - Struct containing the transformed RAS coordinates with fields:
%                           R - Right (X-coordinate in RAS)
%                           A - Anterior (Y-coordinate in RAS)
%                           S - Superior (Z-coordinate in RAS)
%
% The function applies the inverse of the provided transformation matrix to the input
% device coordinates to calculate the corresponding RAS coordinates.
% An error is thrown if the transformation matrix is not 3x3.

function result = transform_device2RAS(deviceCoordinates, definition_XYZ2RAS)

    % Controleer of XYZ2XYZdefinition 4x4 is.
    if size(definition_XYZ2RAS.matrix, 1) ~= 3 || size(definition_XYZ2RAS.matrix, 2) ~= 3
        error('definition_XYZ2RAS moet een 3x3 matrix zijn.');
    end

    % Extract components XYZ
    X = deviceCoordinates.X;
    Y = deviceCoordinates.Y;
    Z = deviceCoordinates.Z;
        
    % The order is reversed and the inverse matrix is therefore used (for simplicity of code)
    RAS = [X, Y, Z] * inv(definition_XYZ2RAS.matrix); %#ok<MINV> 
        
    R = RAS(:,1);
    A = RAS(:,2);
    S = RAS(:,3);
    % set output
    result = coordinates_RAS(R, A, S);
end