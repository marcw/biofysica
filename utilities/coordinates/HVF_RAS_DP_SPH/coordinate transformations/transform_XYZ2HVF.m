% transform_XYZ2HVF transforms coordinates from the XYZ system to the HVF (Horizontal-Verticle-Frontal)
% coordinate system using a provided 3x3 transformation matrix.
%
% INPUTS:
%   XYZcoordinates       - Struct containing the XYZ coordinates with fields:
%                          X - X-coordinates
%                          Y - Y-coordinates
%                          Z - Z-coordinates
%   definition_XYZ2HVF   - Struct containing a 3x3 transformation matrix (definition_XYZ2HVF.matrix)
%                          that defines how to map XYZ coordinates to HVF space.
%
% OUTPUT:
%   result               - Struct containing the HVF coordinates with fields:
%                          H - Horizontal (X-coordinate in HVF)
%                          V - Vertical (Y-coordinate in HVF)
%                          F - Frontal (Z-coordinate in HVF)
%
% The function applies the inverse of the provided transformation matrix to the input
% XYZ coordinates to calculate the corresponding HVF coordinates.
% An error is thrown if the transformation matrix is not 3x3.


function result = transform_XYZ2HVF(XYZcoordinates, definition_XYZ2HVF)
    % Controleer of XYZ2XYZdefinition 4x4 is.
    if size(definition_XYZ2HVF.matrix, 1) ~= 3 || size(definition_XYZ2HVF.matrix, 2) ~= 3
        error('definition_XYZ2RAS moet een 3x3 matrix zijn.');
    end

    % Extract components XYZ
    X = XYZcoordinates.X;
    Y = XYZcoordinates.Y;
    Z = XYZcoordinates.Z;
        
    % The order is reversed and the inverse matrix is therefore used (for simplicity of code)
    HVF = [X, Y, Z] * inv(definition_XYZ2HVF.matrix); %#ok<MINV>         
    H = HVF(:,1);
    V = HVF(:,2);
    F = HVF(:,3);
    % set output
    result = coordinates_HVF(H, V, F);
end