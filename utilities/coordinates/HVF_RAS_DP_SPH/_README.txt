% by Ruurd Lof
% 12-03-2024

The directory transformation has functions for the following coordinate transformations:

1) Double Polar to RAS
2) RAS to Double Polar
3) Spherical to RAS
4) RAS to Spherical
5) Double Polar to Spherical
6) Spherical to Double Polar

All coordinate variable are structs of column arrays or tables. 

RAS coordinates is the standard for coordinates relative to body orientation
It has the following fields:
    - ID (optional)
    - right (in meters)
    - anterior (in meters)
    - superior (in meters)

Double Polar coordinates is our standard for auditory perception experiments
    - ID (optional)
    - azimuth (in radians)
    - elevation (in radians)
    - radius (in meters)
    - hemisphere (+1 or -1: stands for forward or backward hemisphere)

Spherical coordinates is following the Matlab definition
    - ID (optional)
    - azimuth (in radians: 0 to 2pi positive X-axis is 0)
    - elevation (in radians: -pi to pi positive Y-axis is 0)
    - radius (in meters)

