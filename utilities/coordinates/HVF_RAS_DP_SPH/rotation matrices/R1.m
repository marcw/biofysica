function R = R1(theta)
    % for rotations about the first axis of a Cartesian coordinate system
    R = [1 0 0;
         0 cos(theta) -sin(theta);
         0 sin(theta) cos(theta)];
end



