function R = R3(theta)
    % for rotations about the third axis of a Cartesian coordinate system
    R = [cos(theta) -sin(theta) 0;
         sin(theta) cos(theta) 0;
         0 0 1];
end




