function R = R2(theta)
    % for rotations about the second axis of a Cartesian coordinate system
    R = [cos(theta) 0 sin(theta);
         0 1 0;
         -sin(theta) 0 cos(theta)];
end




