classdef rotations
    properties
        asMatrix
        asQuaternion
    end

    methods
        function obj = rotations(angles_X, angles_Y, angles_Z, orders)
            n = height(angles_X);
            obj(n) = obj;
            for i = 1:n
                myRotation = rotation(angles_X(i), angles_Y(i), angles_Z(i), orders(i));
                obj(i).asMatrix = myRotation.asMatrix;
                obj(i).asQuaternion = myRotation.asMatrix;
            end
        end
    end
end