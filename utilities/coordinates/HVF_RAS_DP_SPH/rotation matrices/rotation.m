% by Ruurd Lof
% 22-05-2024

classdef rotation < handle   
    properties
        asMatrix        
        asQuaternion
    end

    properties (Access = private)
        angle_X
        angle_Y
        angle_Z
        order                
    end

    methods
        function obj = rotation(angle_X, angle_Y, angle_Z, order)            
            obj.angle_X = angle_X;
            obj.angle_Y = angle_Y;
            obj.angle_Z = angle_Z;
            obj.order = order;   
            obj.updateRotations;
        end

    end

    methods (Access = private)

        function updateRotations(obj)
            R_x = Rx(obj.angle_X);
            R_y = Ry(obj.angle_Y);
            R_z = Rz(obj.angle_Z);                 
            switch obj.order
                case 'XYZ'
                    Rtot = R_z * R_y * R_x;
                case 'XZY'
                    Rtot = R_y * R_z * R_x;
                case 'YXZ'
                    Rtot = R_z * R_x * R_y;
                case 'YZX'
                    Rtot = R_x * R_z * R_y;
                case 'ZXY'
                    Rtot = R_y * R_x * R_z;
                case 'ZYX'    
                    Rtot = R_x * R_y * R_z;
                otherwise
                    error('order should be in the form "XYZ"')
            end
            obj.asMatrix = Rtot;
            obj.asQuaternion = rotm2quat(Rtot);
        end
        
    end    
end
