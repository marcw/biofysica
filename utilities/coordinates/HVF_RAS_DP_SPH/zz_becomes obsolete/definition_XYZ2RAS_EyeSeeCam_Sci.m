function result = definition_XYZ2RAS_EyeSeeCam_Sci
    % X = anterior
    % Y = left    
    % Z = superior

    % The description gives a textual representation of the XYZ coordinates
    % in terms of the RAS coordinates    
    result.description = 'Y => -R, X => A, Z => S';

    % The 3x3 matrix is a mathematical tool for the conversion of a XYZ
    % coordinate triple to a RAS coordinate triple. It should perform the
    % conversion corresponding to the description above.
    %      X   Y   Z
    %    [ 0, -1,  0;     R
    %      1,  0,  0;     A
    %      0,  0,  1 ];   S
    
    result.matrix = ...
        [ 0, -1,  0;
          1,  0,  0;
          0,  0,  1 ];
end

