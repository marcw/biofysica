% by Ruurd Lof
% 12-03-2024

function DPcoordinates = transform_RAS2DP(RAScoordinates) 
    % Extract components
    right    = RAScoordinates.right;
    anterior = RAScoordinates.anterior;
    superior = RAScoordinates.superior;

    % Compute double polar coordinates
    euclidian  = 2;
    radius     = vecnorm([right, anterior, superior], euclidian, 2);    
    azimuth    = asin(right./radius);    
    elevation  = asin(superior./radius);   
    hemisphere = sign(anterior);

    % convert to table and set output
    if isprop(RAScoordinates, 'ID')
        ID = RAScoordinates.ID;
        DPcoordinates  = coordinates_DP_withID(ID, azimuth, elevation, radius, hemisphere);
    else
        DPcoordinates  = coordinates_DP(azimuth, elevation, radius, hemisphere);
    end    
end