% by Ruurd Lof
% 12-03-2024

function DPcoordinates = transform_SPH2DP(sphericalCoordinates)
   % convert spherical to RAS
   RAScoordinates = transform_SPH2RAS(sphericalCoordinates);
   %convert to DP and assigne output
   DPcoordinates = transform_RAS2DP(RAScoordinates);
end