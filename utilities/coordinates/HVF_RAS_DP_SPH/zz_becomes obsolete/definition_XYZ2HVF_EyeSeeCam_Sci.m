function result = definition_XYZ2HVF_EyeSeeCam_Sci
    % X =  F   (F = forward)
    % Y = -H   (H = right)  
    % Z =  V   (V = up)

% The description gives a textual representation of the XYZ coordinates
% in terms of the HVF coordinates   

    result.description = 'X => F, Y => -H, Z => V';

    % The 3x3 matrix is a mathematical tool for the conversion of a XYZ
    % coordinate triple to a RAS coordinate triple. It should perform the
    % conversion corresponding to the description above.
    %      X   Y   Z
    %    [ 0, -1,  0;     H
    %      0,  0,  1;     V
    %      1,  0,  0 ];   F
    
    result.matrix = ...
        [ 0, -1,  0;
          0,  0,  1;
          1,  0,  0 ];
end

