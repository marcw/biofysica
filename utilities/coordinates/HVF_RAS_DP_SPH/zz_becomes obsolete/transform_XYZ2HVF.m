function result = transform_XYZ2HVF(XYZcoordinates, definition_XYZ2HVF)
    % Controleer of XYZ2XYZdefinition 4x4 is.
    if size(definition_XYZ2HVF.matrix, 1) ~= 3 || size(definition_XYZ2HVF.matrix, 2) ~= 3
        error('definition_XYZ2HVF moet een 3x3 matrix zijn.');
    end

    % Extract components XYZ
    X = XYZcoordinates.X;
    Y = XYZcoordinates.Y;
    Z = XYZcoordinates.Z;
        
    % Perform transformation.
    % In order to keep the construction simple, the order is reversed and
    % the inverse matrix is therefor used
    HVF = [X, Y, Z] * inv(definition_XYZ2HVF.matrix); %#ok<MINV> 
        
    % Extract components of XYZ
    H = HVF(:,1);
    V = HVF(:,2);
    F = HVF(:,3);
    
    % set output
    result = coordinates_HVF(H, V, F);
end