% by Ruurd Lof
% 12-03-2024

function  sphericalCoordinates = transform_RAS2SPH(RAScoordinates)
   %extract components
   R  = RAScoordinates.right;
   A  = RAScoordinates.anterior;
   S  = RAScoordinates.superior;

   %convert cartesian to spherical
   [azimuth, elevation, radius]   = cart2sph(R, A, S);
   
   % set output values
    if isprop(RAScoordinates, 'ID')
        ID = RAScoordinates.ID;
        sphericalCoordinates  = coordinates_SPH_withID(ID, azimuth, elevation, radius);
    else
        sphericalCoordinates  = coordinates_SPH(azimuth, elevation, radius);
    end    
end