% by Ruurd Lof
% 12-03-2024

function RAScoordinates = transform_DP2RAS(DPcoordinates)
    % Extract components   
    azimuth    = DPcoordinates.azimuth;
    elevation  = DPcoordinates.elevation;   
    radius     = DPcoordinates.radius;
    hemisphere = DPcoordinates.hemisphere;
    
    % Check DP consistency
    ANGLE_TOLLERANCE = 0.001;
    if azimuth + elevation > pi/2 + ANGLE_TOLLERANCE
       error('Rings of azimuth and elevation do not intersect leading to imaginary solution for anterior');
    end 
    
    % Compute double polar coordinates
    right    = radius .* sin(azimuth);
    superior = radius .* sin(elevation); 
    anterior = hemisphere .* real(sqrt(radius.^2 - right.^2 - superior.^2));
    
    % convert to table and set output
    if isprop(DPcoordinates, 'ID')
        ID = DPcoordinates.ID;
        RAScoordinates  = coordinates_RAS_withID(ID, right, anterior, superior);
    else
        RAScoordinates  = coordinates_RAS(right, anterior, superior);
    end
end 