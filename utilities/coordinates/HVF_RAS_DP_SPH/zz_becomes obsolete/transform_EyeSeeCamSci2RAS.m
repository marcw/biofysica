function result = transform_EyeSeeCamSci2RAS(EyeSeeCamCoordinates)
    % Controleer of XYZ2XYZdefinition 4x4 is.    
    if size(definition_XYZ2XYZ.matrix, 1) ~= 3 || size(definition_XYZ2XYZ.matrix, 2) ~= 3
        error('definition_XYZ2XYZ moet een 3x3 matrix zijn.');
    end

    % Extract components XYZ
    X = EyeSeeCamCoordinates.X;
    Y = EyeSeeCamCoordinates.Y;
    Z = EyeSeeCamCoordinates.Z;
        
    % Perform transformation.
    % In order to keep the construction simple, the order is reversed and
    % the inverse matrix is therefor used
    RAS = [X, Y, Z] * inv(definition_EyeSeeCamSci2RAS.matrix); %#ok<MINV> 
        
    % Extract components of XYZ
    R = RAS(:,1);
    A = RAS(:,2);
    S = RAS(:,3);
    % set output
    result = coordinates_RAS(R, A, S);
end