% by Ruurd Lof
% 12-03-2024

function RAScoordinates = transform_SPH2RAS(sphericalCoordinates)
   %extract spherical components
   azimuth   = sphericalCoordinates.azimuth;
   elevation = sphericalCoordinates.elevation;
   radius    = sphericalCoordinates.radius;

   % convert spherical to cartesian   
   [right, anterior, superior] = sph2cart(azimuth, elevation, radius);

   % set output
   if isprop(sphericalCoordinates, 'ID')
       ID = sphericalCoordinates.ID;
       RAScoordinates  = coordinates_RAS_withID(ID, right, anterior, superior);
   else
       RAScoordinates  = coordinates_RAS(right, anterior, superior);
   end
end