function SphCoordinates = transform_DP2SPH(DPcoordinates)
   %convert DP to RAS
   RAScoordinates = transform_DP2RAS(DPcoordinates);
   %convert RAS to spherical and set output
   SphCoordinates = transform_RAS2SPH(RAScoordinates);
end