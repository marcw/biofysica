function result = transform_XYZ2XYZ(XYZcoordinates, definition_XYZ2XYZ)
    % Controleer of XYZ2XYZdefinition 4x4 is.
    if size(definition_XYZ2XYZ.matrix, 1) ~= 3 || size(definition_XYZ2XYZ.matrix, 2) ~= 3
        error('definition_XYZ2XYZ moet een 3x3 matrix zijn.');
    end

    % Extract components XYZ
    Xin = XYZcoordinates.X;
    Yin = XYZcoordinates.Y;
    Zin = XYZcoordinates.Z;
        
    % Perform transformation.
    % In order to keep the construction simple, the order is reversed and
    % the inverse matrix is therefor used
    XYZ = [Xin, Yin, Zin] * inv(definition_XYZ2XYZ.matrix); %#ok<MINV> 
        
    % Extract components of XYZ
    Xout = XYZ(:,1);
    Yout = XYZ(:,2);
    Zout = XYZ(:,3);
    % set output
    result = coordinates_XYZ(Xout, Yout, Zout);
end