%The quadrants are numbered 1 to 4 in the anti-clockwise direction 

function q = quadrantXY(x, y)    
    if x > 0 && y >= 0
        q = 1;
    elseif x <= 0 && y > 0
        q = 2;
    elseif x < 0 && y <= 0
        q = 3;
    elseif x >= 0 && y < 0
        q = 4;
    else
        q = 0; % The origin does not belong to any quadrant
    end
end