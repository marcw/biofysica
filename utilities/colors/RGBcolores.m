classdef RGBcolores
    properties (Constant)
        LIMA            = [0.00, 1.00, 0.00];
        ROJO            = [1.00, 0.00, 0.00];
        AZUL            = [0.00, 0.00, 1.00];
        
        CIAN            = [0.00, 1.00, 1.00];
        MAGENTA         = [1.00, 0.00, 1.00];
        AMARILLO        = [1.00, 1.00, 0.00];
                
        NEGRO           = [0.00, 0.00, 0.00];
        GRIS            = [0.50, 0.50, 0.50];
        BLANCO          = [1.00, 1.00, 1.00];
        
        GRANATE         = [0.50, 0.00, 0.00];
        VERDE           = [0.00, 0.50, 0.00];
        AZUL_MARINO     = [0.00, 0.00, 0.50];
        
        OLIVA           = [0.00, 0.50, 0.50];
        MORADO          = [0.50, 0.00, 0.50];
        VERDE_AZULADO   = [0.50, 0.50, 0.00];
        
        INDIGO          = [0.50, 0.00, 1.00];
        CHARTREUSE      = [0.50, 1.00, 0.00];
        AZUL_REAL       = [0.00, 0.50, 1.00]; 
        
        ROSA_BRILLIANTE = [1.00, 0.00, 0.50]; 
        NARANJA         = [1.00, 0.50, 0.00];         
        VERDE_PRIMAVERA = [0.00, 1.00, 0.50];                 
        
        AZUL_CLARO      = [0.50, 0.50, 1.00];        
        VERDE_CLARO     = [0.50, 1.00, 0.50]; 
        AGUAMARINA      = [0.50, 1.00, 1.00]; 
        
        SALMON          = [1.00, 0.50, 0.50];
        ORQUIDEA        = [1.00, 0.50, 1.00]; 
        AMARILLO_CLARO  = [1.00, 1.00, 0.50]; 
    end
end
