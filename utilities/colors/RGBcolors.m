classdef RGBcolors
    properties (Constant)
        LIME         = [0.00, 1.00, 0.00];
        RED          = [1.00, 0.00, 0.00];
        BLUE         = [0.00, 0.00, 1.00];
        
        CYAN         = [0.00, 1.00, 1.00];
        MAGENTA      = [1.00, 0.00, 1.00];
        YELLOW       = [1.00, 1.00, 0.00];
                
        BLACK        = [0.00, 0.00, 0.00];
        GREY         = [0.50, 0.50, 0.50];
        WHITE        = [1.00, 1.00, 1.00];
        
        MAROON       = [0.50, 0.00, 0.00];
        GREEN        = [0.00, 0.50, 0.00];
        NAVYBLUE     = [0.00, 0.00, 0.50];
        
        OLIVE        = [0.00, 0.50, 0.50];
        PURPLE       = [0.50, 0.00, 0.50];
        TEAL         = [0.50, 0.50, 0.00];
        
        INDIGO       = [0.50, 0.00, 1.00];
        CHARTREUSE   = [0.50, 1.00, 0.00];
        ROYALBLUE    = [0.00, 0.50, 1.00]; 
        
        HOTPINK      = [1.00, 0.00, 0.50]; 
        ORANGE       = [1.00, 0.50, 0.00];         
        SPRINGGREEN  = [0.00, 1.00, 0.50];                 
        
        LIGHTBLUE    = [0.50, 0.50, 1.00];        
        LIGHTGREEN   = [0.50, 1.00, 0.50]; 
        AQUAMARINE   = [0.50, 1.00, 1.00]; 
        
        SALMON       = [1.00, 0.50, 0.50];
        ORCHID       = [1.00, 0.50, 1.00]; 
        LIGHTYELLOW  = [1.00, 1.00, 0.50]; 
    end
end