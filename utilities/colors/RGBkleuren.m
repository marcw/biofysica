classdef RGBkleuren
    properties (Constant)
        LIMOENGROEN  = [0.00, 1.00, 0.00];
        ROOD         = [1.00, 0.00, 0.00];
        BLAUW        = [0.00, 0.00, 1.00];
        
        CYAAN        = [0.00, 1.00, 1.00];
        MAGENTA      = [1.00, 0.00, 1.00];
        GEEL         = [1.00, 1.00, 0.00];
                
        ZWART        = [0.00, 0.00, 0.00];
        GRIJS        = [0.50, 0.50, 0.50];
        WIT          = [1.00, 1.00, 1.00];
        
        KASTANJEBRUIN= [0.50, 0.00, 0.00];
        GROEN        = [0.00, 0.50, 0.00];
        MARINEBLAUW  = [0.00, 0.00, 0.50];
        
        OLIJFGROEN   = [0.00, 0.50, 0.50];
        PAARS        = [0.50, 0.00, 0.50];
        BLAUWGROEN   = [0.50, 0.50, 0.00];
        
        INDIGO       = [0.50, 0.00, 1.00];
        GROENGEEL    = [0.50, 1.00, 0.00];
        KONINGSBLAUW = [0.00, 0.50, 1.00]; 
        
        FELROZE      = [1.00, 0.00, 0.50]; 
        ORANJE       = [1.00, 0.50, 0.00];         
        LENTEGROEN   = [0.00, 1.00, 0.50];                 
        
        LICHTBLAUW   = [0.50, 0.50, 1.00];        
        LICHTGROEN   = [0.50, 1.00, 0.50]; 
        AQUAMARIJN   = [0.50, 1.00, 1.00]; 
        
        ZALM         = [1.00, 0.50, 0.50];
        ORCHIDEE     = [1.00, 0.50, 1.00]; 
        LICHTGEEL    = [1.00, 1.00, 0.50]; 
    end
end