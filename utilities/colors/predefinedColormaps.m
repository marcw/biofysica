classdef predefinedColormaps    
    properties (Constant)
        parula	      = parula;
        turbo	      = turbo;
        hsv	          = hsv;
        hot	          = hot;
        cool          = cool;
        spring        = spring;
        summer        = summer;        
        autumn        = autumn;
        winter        = winter;
        gray          = gray;
        bone          = bone;
        copper        = copper;
        pink          = pink;
        lines         = lines;
        jet           = jet;
        colorcube     = colorcube;
        prism         = prism;
        flag          = flag;
        blueshades    = blueshades;
        redshades     = redshades;
        greenshades   = greenshades;
        cyanshades    = cyanshades;
        magentashades = magentashades;
        yellowshades  = yellowshades;
    end

    methods 
        function show(obj, colorMap)
            % Generate a colorbar for the colormap
            
            % Create a figure
            figure;
            
            % Create a simple gradient
            x = linspace(0, 1, 256);
            y = ones(10, 1);
            gradient = y * x;
            
            % Display the gradient with the parula colormap
            imagesc(gradient);
            colormap(colorMap);
            axis off; % Remove the axes
            colorMapName = obj.getColormapName(colorMap);
            title(['''' colorMapName '''' ' color map']);
            
            % Add a colorbar for reference
            colorbar;

        end
    end    

    methods(Access=private)

        function result = getColormapName(obj, colormap)
            % Als geen match wordt gevonden
            result = 'Unknown colormap';
            
            % Haal class metadata op
            mc = metaclass(obj);
            
            % Loop door de properties
            for k = 1:length(mc.PropertyList)
                propName = mc.PropertyList(k).Name;
                
                % Controleer of de property overeenkomt met de colormap
                if isequal(obj.(propName), colormap)
                    result = propName;
                    break;
                end
            end                  
        end
    end
end 
