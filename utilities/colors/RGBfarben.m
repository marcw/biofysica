classdef RGBfarben
    properties (Constant)
        LIMETTENGRUEN  = [0.00, 1.00, 0.00];
        ROT            = [1.00, 0.00, 0.00];
        BLAU           = [0.00, 0.00, 1.00];
        
        CYAN           = [0.00, 1.00, 1.00];
        MAGENTA        = [1.00, 0.00, 1.00];
        GELB           = [1.00, 1.00, 0.00];
                
        SCHWARZ        = [0.00, 0.00, 0.00];
        GRAU           = [0.50, 0.50, 0.50];
        WEISS          = [1.00, 1.00, 1.00];
        
        KASTANIENBRAUN  = [0.50, 0.00, 0.00];
        GRUEN           = [0.00, 0.50, 0.00];
        MARINBLAU       = [0.00, 0.00, 0.50];
        
        OLIVE           = [0.00, 0.50, 0.50];
        LILA            = [0.50, 0.00, 0.50];
        TUERKIS         = [0.50, 0.50, 0.00]; 
        
        INDIGO          = [0.50, 0.00, 1.00];
        CHARTREUSE      = [0.50, 1.00, 0.00];
        KOENIGSBLAU     = [0.00, 0.50, 1.00]; 
        
        HEISSROSA       = [1.00, 0.00, 0.50]; 
        ORANGE          = [1.00, 0.50, 0.00];         
        FRUEHLINGSGRUEN = [0.00, 1.00, 0.50];                 
        
        HELLBLAU        = [0.50, 0.50, 1.00];        
        HELLGRUEN       = [0.50, 1.00, 0.50]; 
        AQUAMARIN       = [0.50, 1.00, 1.00]; 
        
        LACHS           = [1.00, 0.50, 0.50];
        ORCHIDEE        = [1.00, 0.50, 1.00]; 
        HELLGELB        = [1.00, 1.00, 0.50]; 
    end
end
