function murace = simulate_mean_race_model(mu, sigma, nsamples)
% Computes the mean reaction time using a Monte Carlo simulation
% for the race model.
%
% Inputs:
%   - mu: [n x 3] matrix of mean promptness (1000/RT)
%     (Columns: 1 = Auditory, 2 = AV, 3 = Visual)
%   - sigma: [nS x 3] matrix of standard deviation of promptness
%   - n_samples: Number of Monte Carlo samples
%
% Output:
%   - mean_RT: [n x 1] vector of mean RTs under the race model
%
% see also: COMPUTE_RACE_MODEL

if nargin<3
	nsamples = 1000;
end
n = size(mu, 1);  % Number of subjects/trials
murace = NaN(n, 1);  % Output vector for mean RTs

% Loop over subjects/trials
for ii = 1:n
	% Generate promptness samples from normal distributions
	A = normrnd(mu(ii,1), sigma(ii,1), [nsamples, 1]);
	V = normrnd(mu(ii,2), sigma(ii,2), [nsamples, 1]);

	% Apply race model: take the maximum promptness
	AV = max(A, V);

	% Convert promptness to reaction times
	RT_samples = 1000 ./ AV;

	% Compute the mean RT
	murace(ii) = median(RT_samples);
end
end