function murace = compute_race_model(mu, sigma, xrt, quantiles)
    % Computes the predicted reaction time under the race model of statistical facilitation
    % using the LATER model approach.
    %
    % Inputs:
    %   - mu: [n x 3] matrix of mean promptness (1 = Auditory, 2 = Visual,
	%   3 = AudioVisual, in [s^{-1}]), in which n are the number of
	%   conditions/blocks/locations/participants
    %   - sigma: [n x 3] matrix of standard deviation of promptness
    %   - xrt: vector of reaction times to evaluate probabilities (in [ms])
    %   - quantiles: vector of quantiles to compute (e.g., [0.25, 0.5, 0.75])
    %
    % Output:
    %   - murace: [n x length(quantiles)] matrix of predicted race model RTs
	%
    
    n		= size(mu, 1);  % Number of subjects/trials/conditions/locations
    murace	= NaN(n, length(quantiles));  % Output matrix for predicted RTs
    
    % Convert RTs to promptness (x = 1000/RT)
    x		= 1000./xrt;  

    % Loop over subjects/trials
    for ii	= 1:n
        % Compute cumulative probabilities in promptness space
        pA		= normcdf(-x, -mu(ii,1), sigma(ii,1));  % Auditory CDF
        pV		= normcdf(-x, -mu(ii,2), sigma(ii,2));  % Visual CDF
        race	= pA + pV - pA .* pV;  % Race model prediction

        % Ensure probabilities are within valid range [0,1]
        race	= max(0, min(race, 1));

        % Find RTs corresponding to given quantiles
        for q	= 1:length(quantiles)
            [~, idx] = min(abs(race - quantiles(q)));  % Find closest probability match
            murace(ii, q) = xrt(idx);  % Store predicted RT at this quantile
        end
    end
end