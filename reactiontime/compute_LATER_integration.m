function muLATER = compute_LATER_integration(mu, sigma, t_delay)
% Computes the expected reaction time under the LATER audiovisual integration model
% with a fixed baseline response delay.
%
% Inputs:
%   - mu: [n x 3] matrix of mean promptness (1/RT)
%     (Columns: 1 = Auditory, 2 = AV, 3 = Visual)
%   - sigma: [n x 3] matrix of standard deviation of promptness
%   - t_delay: Scalar fixed baseline response delay (e.g., motor delay)
%
% Output:
%   - E_RT_AV: [nS x 1] vector of expected RTs under the integration model

if nargin<2
	t_delay = 0;
end

n = size(mu, 1);  % Number of subjects/trials
muLATER = NaN(n, 1);  % Output vector for expected RTs

% Loop over subjects/trials
for ii = 1:n
	% Extract mean and standard deviation of promptness
	mu_A		= mu(ii,1);  % Auditory
	sigma_A		= sigma(ii,1);
	mu_V		= mu(ii,2);  % Visual
	sigma_V		= sigma(ii,2);

	% Convert expected promptness to adjusted reaction times (subtract delay)
	E_RT_A	= 1000 / mu_A - t_delay;
	E_RT_V	= 1000 / mu_V - t_delay;

	% Convert back to adjusted promptness
	X_A		= 1000 / (E_RT_A );
	X_V		= 1000 / (E_RT_V );

	% Compute expected promptness under integration model
	E_X_AV	= log(exp(X_A + sigma_A^2 / 2) + exp(X_V + sigma_V^2 / 2) - 1);

	% Convert to expected reaction time (E[RT] = 1 / E[X]) and add baseline delay
	muLATER(ii)		= t_delay + 1000 / E_X_AV;
end
end