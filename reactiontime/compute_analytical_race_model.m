function murace = compute_analytical_race_model(mu, sigma)
% Computes the expected reaction time under the race model analytically.
%
% Inputs:
%   - mu: [nS x 3] matrix of mean promptness (1000/RT, in [s^{-1}])
%     (Columns: 1 = Auditory, 2 = Visual, 3 = AudioVisual)
%   - sigma: [nS x 3] matrix of standard deviation of promptness (in
%   [s^{-1}])
%
% Output:
%   - murace: [nS x 1] vector of expected RTs under the race model (in
%   [ms])
%
% See also: https://en.wikipedia.org/wiki/Expected_value
% and https://math.stackexchange.com/questions/473229/expected-value-of-maximum-and-minimum-of-n-normal-random-variables

n				= size(mu, 1);  % Number of subjects/trials
murace			= NaN(n, 1);  % Output vector for expected RTs

% Loop over subjects/trials
for ii = 1:n
	% Extract mean and standard deviation of promptness for this subject
	mu_A		= mu(ii,1);  % Auditory
	sigma_A		= sigma(ii,1);
	mu_V		= mu(ii,2);  % Visual
	sigma_V		= sigma(ii,2);

	% Compute d, the normalized difference between means
	d			= (mu_A - mu_V) / sqrt(sigma_A^2 + sigma_V^2);

	% Compute the normal CDF and PDF values
	Phi_d		= normcdf(d, 0, 1);   % Standard normal CDF at d
	Phi_neg_d	= normcdf(-d, 0, 1); % Standard normal CDF at -d
	phi_d		= normpdf(d, 0, 1);   % Standard normal PDF at d

	% Expected promptness (E[X_AV]) under the race model
	E_X_AV		= mu_A * Phi_d + mu_V * Phi_neg_d + sqrt(sigma_A^2+sigma_V^2) * phi_d;
	% E_X_AV		= mu_A * Phi_d + mu_V * Phi_neg_d ;

	% Convert expected promptness back to reaction time (E[RT_AV] = 1 / E[X_AV])
	murace(ii) = 1000 / E_X_AV;
end
end