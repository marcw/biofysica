The program is developped in 'Control FPWIN PRO 7' from panasonic
This tool can be found in \\mbcogro-srv.science.ru.nl\install\Panasonic-DI SUNX Control
The program is specific for the LED Controller of the Sphere setup.
For uploading the program to the PLC CPU you need a special cable and a computer with a 9 pin RS232 serial interface
This program is written by Ruurd Lof (DCN, Radboud University)