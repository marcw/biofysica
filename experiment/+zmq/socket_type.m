% SOCKET_TYPE - A class representing ZeroMQ socket types in MATLAB.
%
% The socket_type class provides constants for various ZeroMQ socket types in
% MATLAB. These constants can be used when creating sockets to specify the
% desired socket type.
%
% Usage:
%   zmq.socket_type.pair    % Constant for the PAIR socket type
%   zmq.socket_type.pub     % Constant for the PUB socket type
%   zmq.socket_type.sub     % Constant for the SUB socket type
%   zmq.socket_type.req     % Constant for the REQ socket type
%   zmq.socket_type.rep     % Constant for the REP socket type
%   zmq.socket_type.dealer  % Constant for the DEALER socket type
%   zmq.socket_type.router  % Constant for the ROUTER socket type
%   zmq.socket_type.pull    % Constant for the PULL socket type
%   zmq.socket_type.push    % Constant for the PUSH socket type
%   zmq.socket_type.xpub    % Constant for the XPUB socket type
%   zmq.socket_type.xsub    % Constant for the XSUB socket type
%   zmq.socket_type.stream  % Constant for the STREAM socket type
%   zmq.socket_type.client  % Constant for the CLIENT socket type
%   zmq.socket_type.server  % Constant for the SERVER socket type
%
% Example:
%   % Create a PUB socket using the PUB socket type constant
%   socket = zmq.socket_t(context, zmq.socket_type.pub);
%
% See also zmq.context_t, zmq.socket_t
%
% References:
%   - ZeroMQ: http://zeromq.org/
%
% Author: Günter Windau

classdef socket_type
    
    properties (Constant)
        pair = org.zeromq.ZMQ.PAIR      % Constant for the PAIR socket type
        pub = org.zeromq.ZMQ.PUB        % Constant for the PUB socket type
        sub = org.zeromq.ZMQ.SUB        % Constant for the SUB socket type
        req = org.zeromq.ZMQ.REQ        % Constant for the REQ socket type
        rep = org.zeromq.ZMQ.REP        % Constant for the REP socket type
        dealer = org.zeromq.ZMQ.DEALER  % Constant for the DEALER socket type
        router = org.zeromq.ZMQ.ROUTER  % Constant for the ROUTER socket type
        pull = org.zeromq.ZMQ.PULL      % Constant for the PULL socket type
        push = org.zeromq.ZMQ.PUSH      % Constant for the PUSH socket type
        xpub = org.zeromq.ZMQ.XPUB      % Constant for the XPUB socket type
        xsub = org.zeromq.ZMQ.XSUB      % Constant for the XSUB socket type
        stream = org.zeromq.ZMQ.STREAM  % Constant for the STREAM socket type
        %client = org.zeromq.ZMQ.CLIENT  % Constant for the CLIENT socket type
        %server = org.zeromq.ZMQ.SERVER  % Constant for the SERVER socket type
    end
    
end
