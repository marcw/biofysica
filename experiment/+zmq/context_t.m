% CONTEXT_T - A class representing a ZeroMQ context in MATLAB.
%
% The context_t class provides a wrapper around the ZeroMQ context object in
% MATLAB. It allows you to create and manage ZeroMQ contexts for creating
% sockets and setting various context options.
%
% Usage:
%   context = zmq.context_t();         % Create a ZeroMQ context with default options
%   context = zmq.context_t(ioThreads) % Create a ZeroMQ context with a specific number of I/O threads
%   socket = zmq.socket_t(context, type); % Create a socket using the context
%
% Properties:
%   - ptr: Pointer to the ZeroMQ context object
%
% Methods:
%   - context_t: Constructor for the context_t class. Creates a new ZeroMQ context.
%     If no arguments are provided, it creates a context with default options.
%     If 'ioThreads' is provided, it creates a context with the specified number
%     of I/O threads.
%   - delete: Destructor for the context_t class. Deletes the context object
%     and frees associated resources.
%   - handle: Returns the handle to the ZeroMQ context object.
%
% Example:
%   % Create a ZeroMQ context with default options
%   context = zmq.context_t();
%
%   % Create a socket using the context
%   socket = zmq.socket_t(context, zmq.socket_type.req);
%
% See also zmq.socket_t, zmq.socket_type
%
% References:
%   - ZeroMQ: http://zeromq.org/
%
% Author: Günter Windau

classdef context_t < handle
    
    properties %(Access=private)
        ptr; % Pointer to the ZeroMQ context object
    end
    
    methods (Access=public)
        function this = context_t(ioThreads)
            % CONTEXT_T - Constructor for the context_t class. Creates a new ZeroMQ context.
            % If no arguments are provided, it creates a context with default options.
            
            
            % If 'ioThreads' is provided, it creates a context with the specified number
            % of I/O threads.
            %
            % Usage:
            %   context = zmq.context_t();         % Create a ZeroMQ context with default options
            %   context = zmq.context_t(ioThreads) % Create a ZeroMQ context with a specific number of I/O threads
            %
            % Inputs:
            %   - ioThreads: Number of I/O threads to use in the ZeroMQ context (optional)
            %
            % Output:
            %   - context: The created context_t object
            %
            import org.zeromq.*;
            if nargin == 0
                this.ptr = ZContext();
            else
                this.ptr = ZContext(ioThreads);
            end
        end
        
        function delete(this)
            % DELETE - Destructor for the context_t class. Deletes the context object
            % and frees associated resources.
            %
            % Usage:
            %   delete(context);
            %
            this.ptr.close();
            clear this.ptr; % Remove reference to Java object, so the JVM garbage collector can reclaim memory 
        end
        
        
        function h = handle(this)
            % HANDLE - Returns the handle to the ZeroMQ context object.
            %
            % Usage:
            %   handle = context.handle();
            %
            % Output:
            %   - handle: The handle to the ZeroMQ context object
            %
            h = this.ptr;
        end
    end
    
end