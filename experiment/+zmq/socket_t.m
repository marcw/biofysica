classdef socket_t < handle
    % SOCKET_T is a MATLAB class that represents a ZeroMQ socket.
    %
    % The socket_t class provides a MATLAB interface for interacting with a ZeroMQ socket.
    % It allows you to bind or connect to endpoints, send and receive messages, and perform
    % various socket operations.
    %
    % Properties:
    %   - context: The zmq.context_t associated with the socket
    %   - zsocket: The ZeroMQ socket object
    %
    % Methods:
    %   - socket_t: Constructor for the socket_t class
    %   - delete: Destructor for the socket_t class
    %   - bind: Binds the socket to the specified URI
    %   - connect: Connects the socket to the specified URI
    %   - send: Sends a message through the socket
    %   - recv: Receives a message from the socket
    %   - recv: Receives a message from the socket as a string
    %   - recv_multipart: Receives a multipart message from the socket
    %   - send_multipart: Sends a multipart message through the socket
    %   - jsend: Sends a JSON encoded matlab data to a ZeroMQ endpoint
    %   - jrecv: Receives JSON encoded data from a ZeroMQ endpoint
    %
    % Private Methods:
    %   - test_uri: Tests the validity of the URI
    %   - test_tcp: Tests if the hostname:port is available for TCP transport
    %   - split_uri: Splits the URI into transport, location, and optional port
    %
    % Example:
    %   % Create a ZeroMQ context
    %   ctx = zmq.context_t();
    %
    %   % Create a socket of type REQ
    %   socket = zmq.socket_t(ctx, zmq.socket_type.req);
    %
    %   % Connect the socket to a remote endpoint
    %   socket.connect('tcp://localhost:5555');
    %
    %   % Send a message
    %   socket.send('Hello, World!');
    %
    %   % Receive a response
    %   response = socket.recv();
    %
    %   % Clean up
    %   delete(socket);
    %   delete(ctx);
    %
    % See also zmq.context_t, zmq.socket_type
    
    properties % (Access = private)
        context;    % The zmq.context_t associated with the socket
        zsocket;    % The ZeroMQ socket object
        connect_timeout = 10; % The timeout for connection to a TCP endpoint
    end
    
    methods (Access = public)
        
        function this = socket_t(context, type)
            % SOCKET_T creates a socket_t object.
            %
            % Syntax:
            %   socket = socket_t(context, type)
            %
            % Parameters:
            %   - context: The ZeroMQ context associated with the socket (context_t object)
            %   - type: The type of the socket (socket_type enumeration)
                     
            this.context = context;
            h = this.context.handle();
            this.zsocket = h.createSocket(type);
        end
        
        function delete(this)
            % DELETE deletes the socket and releases associated resources.
            this.zsocket.close();
            clear this.zsocket; % Remove reference to Java object, so the JVM garbage collector can reclaim memory
        end
        
        function bind(this, uri)
            % BIND binds the socket to the specified URI.
            %
            % Parameters:
            %   - uri: The URI to bind
            this.zsocket.bind(uri);
        end
        
        function connect(this, uri)
            % CONNECT connects the socket to the specified URI.
            %
            % Parameters:
            %   - uri: The URI to connect
            if this.test_uri(uri)
                this.zsocket.connect(uri);
            else
                ME = MException('connect:connectError', 'connect failed at %s', uri);
                throw(ME);
            end
        end
        
        function subscribe(this, topic)
            % SUBSCRIBE subscribes the socket to the specified topic
            %
            % Parameters:
            % -topic: The TOPIC to subscribe to. An empty string subscribes
            %         to all topics
            this.zsocket.subscribe(topic);
        end
        
        function unsubscribe(this, topic)
            % UNSUBSCRIBE subscribes the socket to the specified topic
            %
            % Parameters:
            % -topic: The TOPIC to unsubscribe from
            this.zsocket.unsubscribe(topic);
        end
        
        function set_linger(this, millisecs)
            if isinf(millisecs)
                millisecs=-1;
            end            
            this.zsocket.setLinger(millisecs);
        end
        
        function millisecs = get_linger(this)
            millisecs = this.zsocket.getLinger();
            if millisecs < 0
                millisecs = Inf;
            end
        end
        
        function result = set_send_timeout(this, millisecs)
            result = this.zsocket.setSendTimeOut(millisecs);
        end
        
        function millisecs = get_send_timeout(this)
            millisecs = this.zsocket.getSendTimeOut();
        end
        
        function result = set_recv_timeout(this, millisecs)
            result = this.zsocket.setReceiveTimeOut(millisecs);
        end
        
        function millisecs = get_recv_timeout(this)
            millisecs = this.zsocket.getReceiveTimeOut();
        end

        function result = set_heartbeat_timeout(this, millisecs)
            result = this.zsocket.setHeartbeatTimeout(millisecs);
        end
        
        function millisecs = get_heartbeat_timeout(this)
            millisecs = this.zsocket.getHeartbeatTimeOut();
        end
        
        function result = set_connect_timeout(this, millisecs)
            if millisecs < 0
                result = false;
            else
                this.connect_timeout = millisecs/1000.0;
                result = true;
            end
            
        end
        
        function millisecs = get_connect_timeout(this)
            millisecs = this.connect_timeout*1000.0;
        end

        function send(this, msg, flags)
            % SEND sends a message through the socket.
            %
            % Parameters:
            %   - msg: The message to send
            %   - flags: Optional flags for message sending
            if nargin < 3
                this.zsocket.send(msg);
            else
                this.zsocket.send(msg, flags);
            end
        end
        
        function result = recv(this, flags)
            % RECV receives a message from the socket.
            %
            % Parameters:
            %   - flags: Optional flags for message receiving
            %
            % Returns:
            %   - result: The received message
            
            if nargin < 2
                result = this.zsocket.recv();
            else
                result = this.zsocket.recv(flags);
            end
        end
        
        function result = recvStr(this, flags)
            % RECVSTR receives a message from the socket as a string.
            %
            % Parameters:
            %   - flags: Optional flags for message receiving
            %
            % Returns:
            %   - result: The received message
            
            if nargin < 2
                result = this.zsocket.recvStr();
            else
                result = this.zsocket.recvStr(flags);
            end
        end
        
        function result = recv_multipart(this, flags)
            % RECV_MULTIPART receives a multipart message from the socket.
            %
            % Parameters:
            %   - flags: Optional flags for message receiving
            %
            % Returns:
            %   - result: A cell array containing the frames of the
            %   received multipart message. The data is in int8 format.
            import org.zeromq.*;          
            
            msg = ZMsg.recvMsg(this.zsocket);
            if isempty(msg)
                result = cell.empty;
            else
                nframes = msg.size();
                result = cell(1, nframes);
                for ii = 1:nframes
                    frame = msg.pollFirst();
                    result{ii} = frame.getData();
                end
            end
        end
        
        function result = recv_multipart_string(this, flags)
            % RECV_MULTIPART_STRING receives a multipart message of strings from the socket.
            %
            % Parameters:
            %   - flags: Optional flags for message receiving
            %
            % Returns:
            %   - result: A cell array containing the frames of the received multipart message
            %             or cell.empty when nothing was received.
            
            import org.zeromq.*;
            import java.nio.charset.Charset;
            
            charset = Charset.defaultCharset();
            
            msg = ZMsg.recvMsg(this.zsocket);
            if isempty(msg)
                result = cell.empty;
            else
                nframes = msg.size();
                result = cell(1, nframes);
                for ii = 1:nframes
                    frame = msg.pollFirst();
                    result{ii} = frame.getString(charset);
                end
            end
        end
        
        function send_multipart(this, varargin)
            % SEND_MULTIPART sends a multipart message through the socket.
            %
            % Parameters:
            %   - varargin: The frames of the multipart message to send
            import org.zeromq.*;

            msg = ZMsg;
            for ii = 1:numel(varargin)
                frame = this.create_frame(varargin{ii});
                msg.add(frame);
            end
            msg.send(this.zsocket);
        end
        
        function varargout = jrecv(this)
            % JRECV receives multiple data object from the socket. The data
            % is assumed to be JSON encoded and will be decoded to matlab
            % data objects.
            %
            % Returns:
            %    - varargout: The data. Excess data from the socket will be
            %    discarded, excess output arguments will be filled with
            %    empty values.
            r = this.recv_multipart_string(0);
            n = length(r);
            
            result = cell(1,n);
            for ii=1:n
                s=char(r{ii}.toString());
                s=strip(s,'right',char(0));  % remove trailing zeros 
                result{ii} = jsondecode(s);
            end
            m=min(n,nargout);
            varargout=cell(1,m);
            for ii=1:m
                varargout{ii}=result{ii};
            end
            for ii=m+1:nargout
                varargout{ii}=double.empty;
            end
        end
        
        function jsend(this, varargin)
            % JSEND sends Matlab data objects to an endpoint. The data
            % objects are transferred using JSON encoding.
            %
            % Parameters:
            %   - varargin: The data objects (text or numeric) to be sent
            %
            n = length(varargin);
            r = cell(1, n);
            for ii = 1:n
                r{ii} = jsonencode(varargin{ii});
            end
            this.send_multipart(r{:});               
        end
        
    end
    
    methods (Access = private)
        
        function result = test_uri(this, uri)
            % TEST_URI tests the validity of the URI.
            %
            % Parameters:
            %   - uri: The URI to test
            %
            % Returns:
            %   - result: A logical value indicating if the URI is valid or not
            
            [transport, location, port] = this.split_uri(uri);
            switch transport
                case 'tcp'
                    result = this.test_tcp(location, port);
                case {'ipc', 'inproc', 'pgm', 'epgm', 'vmci'}
                    result = true;
                otherwise
                    ME = MException('socket', 'unsupported transport %s', transport);
                    throw(ME);
            end
        end
        
        function result = test_tcp(this, location, port)
            % TEST_TCP tests if the hostname:port is available for TCP transport.
            %
            % Parameters:
            %   - location: The hostname or IP address
            %   - port: The port number
            %
            % Returns:
            %   - result: A logical value indicating if the hostname:port is available
            
            if isempty(port)
                ME = MException('socket: no port in tcp uri with host %s', location);
                throw(ME);
            end
            
            % zmq workaround to prevent infinite wait:
            % test if hostname:port is available
            try
               con=tcpclient(location, port, 'ConnectTimeout', this.connect_timeout);
            catch ME
               warning(ME.message);
               result = false; 
               return
            end
            result = true;
            delete(con);
            pause(1);  %GW TESTING Slow closing of tcp connection on windows...
        end
        
        function [transport, location, port] = split_uri(this, uri)
            % SPLIT_URI splits the URI into transport, location, and optional port.
            %
            % Parameters:
            %   - uri: The URI to split
            %
            % Returns:
            %   - transport: The transport part of the URI
            %   - location: The location part of the URI
            %   - port: The optional port number
            
            d = strsplit(uri, ':');
            if length(d) < 2
                ME = MException('socket', 'bad uri %s', uri);
                throw(ME);
            end
            
            transport = d{1};
            
            location = d{2};
            if ~startsWith(location, '//')
                ME = MException('socket', 'bad uri %s', uri);
                throw(ME);
            end
            location = location(3:end);
            
            if length(d) < 3
                port = [];
            else
                [port, ok] = str2num(d{3});
                if ~ok
                    ME = MException('socket', 'bad port %s', uri);
                    throw(ME);
                end
            end
        end
        
        function frame = create_frame(~, data)
            % CREATE_FRAME creates a ZeroMQ frame for the specified data.
            %
            % Parameters:
            %   - data: The data to be encapsulated in the frame
            %
            % Returns:
            %   - frame: The ZeroMQ frame
            
            import org.zeromq.*;
            
            if ischar(data) || isstring(data)
                frame = ZFrame(sprintf('%s\0', data));
                %frame = ZFrame(sprintf('%s', data));
            else
                assert(isnumeric(data), 'create_frame: expect numeric data');
                data = reshape(transpose(data), 1, []);
                bytes = typecast(data, 'uint8');
                frame = ZFrame(bytes);
            end
        end
        
    end
    
end
