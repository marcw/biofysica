classdef BIOX_triggerCodes < BIOX_types
    properties (Constant)
        template  = 'triggercode = %d';
        ZBusA     = 1;
        ZBusB     = 2;
        external  = 4;
        soft1     = 8;
        soft2     = 16;
        soft3     = 32;
    end
end

