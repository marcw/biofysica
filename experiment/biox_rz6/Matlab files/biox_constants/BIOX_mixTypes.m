classdef BIOX_mixTypes < BIOX_types
    properties (Constant)
        template = 'mixtype = %s';
        stop  = 'Stop';
        BtoA  = 'BtoA';
        AtoB  = 'AtoB';
        mixed = 'Mixed';
    end
end