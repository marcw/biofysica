classdef BIOX_DaqDivisor < BIOX_types
    properties (Constant)
        template = 'divisor = %d';
    end
end