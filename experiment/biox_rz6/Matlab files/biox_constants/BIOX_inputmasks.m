classdef BIOX_inputmasks < BIOX_types
    properties (Constant) 
        template  = 'inputmask = %s';
        userTrigger = 'A4';
    end
end