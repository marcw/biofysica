classdef BIOX_soundTypes < BIOX_types
    properties (Constant)
        stop      = 'Stop';
        tone      = 'Tone';
        sweep     = 'Sweep';
        noise     = 'Noise';
        ripple    = 'Ripple';
        WAV       = 'WAV';
        multitone = 'Multitone';
        b_is_a    = 'B=A';
    end
end