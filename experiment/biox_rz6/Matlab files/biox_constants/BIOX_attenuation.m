classdef BIOX_attenuation < BIOX_types
    properties (Constant)
        templatePar1 = 'attenuation1 = %d';
        templatePar2 = 'attenuation2 = %d';
        templatePar3 = 'scalefactor1 = %d';
        templatePar4 = 'scalefactor2 = %d';
    end
end