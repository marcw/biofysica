classdef BIOX_DaqActions < BIOX_types
    properties (Constant)
        template = 'action = %s';
        start = 'Start';
        stop  = 'Stop';
    end
end