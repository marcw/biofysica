classdef BIOX_wavActions < BIOX_types
    properties (Constant)  
        template = 'action = %s';
        Reset    = 'Reset';
        Continue = 'Continue';
        Loop     = 'Loop';
    end
end