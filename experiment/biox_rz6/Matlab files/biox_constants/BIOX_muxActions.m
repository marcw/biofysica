classdef BIOX_muxActions < BIOX_types
    properties (Constant)  
        template = 'muxaction = %s'
        set   = 'Set';
        reset = 'Reset';
    end
end