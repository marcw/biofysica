classdef BIOX_nChecks < BIOX_types
    properties (Constant)
        template  = 'nChecks = %d';
        default  = 200;
    end
end