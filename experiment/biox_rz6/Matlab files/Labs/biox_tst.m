% RL: This code is for RZ6 ID # (1DSP) in the Electronics Workshop

classdef biox_tst < biox_rz6_client

    methods
        function this = biox_tst        
            this@biox_rz6_client(1,'BIOX_1c_25khz.rcx');                     
        end
    end
end
