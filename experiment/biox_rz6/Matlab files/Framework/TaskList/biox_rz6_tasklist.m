classdef biox_rz6_tasklist < handle
    % todo ITD ??

    properties (Access = public)
        nr_of_tasks = 0;
        templateTable
        stringTypesTable
    end
    properties (Access=protected)
        taskListMatrix = []; % tasklist (array of integers representation)         
        debugflag = false;
        tasklist_definition_file = 'tasklist_definition.xlsx'; 

        stringTypes_sheet    = 'StringTypes';
        template_sheet       = 'Template';        
        parameterType_sheet  = 'ParameterType';
        parameterName_sheet  = 'ParameterName';         
        parameterTypeTable
        parameterNameTable

        % N.B. validTaskTypes and validSoundTypes must have the same order as
        % the task_ and soundtype_ constants defined below. validAction
        % When converting String to Number and vise versa, note that the
        % constants start at 0 while the index of the cellarray starts at 1.
        %----------------------------------------------------------------------------------------------
        validTaskTypes        = {'WaitForTrigger', 'SoundA', 'SoundB','Mux','HoldInput',...
                                 'SoundMov', 'Daq', 'SetDIO', 'TrigOut', 'Reset', 'Ready',...
                                 'MultiConfigA', 'MultiConfigB', 'Att', 'ITD', 'Mix', 'SoundAB'};
        validWFTtasks         = {'WaitForTrigger', 'WFT'};
        %----------------------------------------------------------------------------------------------
        validSoundTypes       = {'Stop', 'Tone', 'Sweep', 'Noise', 'Ripple', 'WAV', 'MultiTone', 'B=A'};
        %----------------------------------------------------------------------------------------------
        validSoundTasks       = {'SoundA', 'SoundB', 'SoundAB'};
        validTriggerTypes     = {'ZBusA', 'ZBusB', 'External', 'Soft1', 'Soft2', 'Soft3'};
        desc_singleTriggerTypes = [1, 2, 4, 8, 16, 32];
        validEdgeTypes        = {'Falling', 'Rising'};
        validWAVactions       = {'Continue', 'Reset', 'Loop'};
        validWAVRoutes        = {'1toA_2toB', '2toA_1toB', '1toA_1toB', '2toA_2toB'};
        validBisATypes        = {'NoMix','Fixed','Sine'}; % NoMix is added for technical reasons
        validMuxActions       = {'Set', 'Reset'};
        validActions           = {'Stop', 'Start'};
        validMixTypes         = {'Stop', 'BtoA', 'AtoB', 'Mixed'};        
        %----------------------------------------------------------------------------------------------
        taskListHeaders       = {'delay', 'task', 'soundtype', 'par1', 'par2', 'par3', 'par4'};
        parColumnNames        = {'par1','par2', 'par3', 'par4'};  

        dummyCharacter        = '-';  % x = dummy character in excel sheet
    end
           
    properties (Constant)
        desclen = 7;
        task_desc_index      = 1;
        soundtype_desc_index = 2;
        time_desc_index      = 3;
        par1_desc_index      = 4;
        par2_desc_index      = 5;
        par3_desc_index      = 6;
        par4_desc_index      = 7;        

        task_waitfortrigger = 0;
        task_sound_a = 1;
        task_sound_b = 2;
        task_mux = 3;
        task_holdinput = 4; 
        task_soundmov = 5;
        task_daq = 6;
        task_dioout = 7;
        task_trgout = 8;
        task_reset = 9;
        task_ready = 10;
        task_multiconfiga = 11;
        task_multiconfigb = 12;
        task_att = 13;
        task_itd = 14;
        task_mix  = 15;
        task_sound_ab = 16;
        
        soundtype_stop = 0;
        soundtype_tone = 1;
        soundtype_sweep = 2;
        soundtype_noise = 3;
        soundtype_ripple = 4;
        soundtype_wav = 5;
        soundtype_multitone = 6;
        soundtype_ba = 7;
    end

    methods
        function this = biox_rz6_tasklist(varargin) 
            filename = which(this.tasklist_definition_file);
            this.parameterTypeTable = readtable(filename, 'Sheet', this.parameterType_sheet);
            this.parameterNameTable = readtable(filename, 'Sheet', this.parameterName_sheet);
            this.templateTable      = readtable(filename, 'Sheet', this.template_sheet);
            this.stringTypesTable   = readtable(filename, 'Sheet', this.stringTypes_sheet);
            
            % Maak een inputParser-object
            p = inputParser;            
            % Voeg optionele inputargumenten toe
            addOptional(p, 'MyTable', table(), @(x) istable(x));            
            % Parse de inputargumenten
            parse(p, varargin{:});            
            % Toegang krijgen tot de optionele tabel
            myTable = p.Results.MyTable;
            if ~isempty(myTable)
                this.addTable(myTable);
            end
        end

        function clear(this)
            this.taskListMatrix = [];
            this.nr_of_tasks = 0;
        end
        
        function result = get.nr_of_tasks(this)
            result = this.nr_of_tasks;
        end       
               
        function result = get(this)
            result = this.taskListMatrix(1:this.nr_of_tasks,:); 
        end

        function result = asTable(this)
            result = this.getTableFromMatrix;
        end

        function result = asMatrix(this)
            result = this.get; 
        end

        function add_task(this, delaytime, task, varargin)
           desc = this.parse_task(delaytime, task, varargin{:});
           this.append_task(desc);           
        end

        function debug(this, value)
           if (value)
              this.debugflag = true;
           else
              this.debugflag = false;
           end
        end              

    end %public methods

    methods (Access=protected)
        function append_task(this, desc)
           this.nr_of_tasks = this.nr_of_tasks+1;           
           this.taskListMatrix(this.nr_of_tasks,:) = struct2array(desc);
           this.check_timing;
        end

        function result = getTableFromMatrix(this)
            taskListTable = table([],[],[],[],[],[],[], 'VariableNames', this.taskListHeaders);
            [nRows, ~] = size(this.taskListMatrix);
            for row = 1: nRows
                 newRow = this.desc2ListTableRow(row);
                 taskListTable = vertcat(taskListTable, newRow); %#ok<AGROW> 
            end
            result = taskListTable;
        end

        function result = desc2ListTableRow(this, matrixRow)
            desc = this.taskListMatrix(matrixRow, :);
            time = sprintf('%.3f', desc(3));
            task = this.validTaskTypes(desc(this.task_desc_index) + 1);            

            if ismember(task, this.validSoundTasks)
                soundtype = this.validSoundTypes(desc(this.soundtype_desc_index) + 1);
            else
                soundtype = this.dummyCharacter;
            end          

            searchTable        = this.parameterTypeTable;
            tasksCondition     = strcmpi(searchTable.task, task);
            soundTypeCondition = strcmpi(searchTable.soundtype,soundtype);
            numberOfRows = height(searchTable);
            trueCondition = true(1,numberOfRows)';
            par1Condition = trueCondition; % default
            par2Condition = trueCondition; % default
            par3Condition = trueCondition; % default
            par4Condition = trueCondition; % default

            task_id = desc(this.task_desc_index);

            % create parameter conditions for task with multiple parameter configurations
            % for unambiguous tasks: '% do nothing'
            switch task_id
                case this.task_waitfortrigger
                    triggerTypeNumber = desc(this.par1_desc_index); 
                    if ismember(triggerTypeNumber, this.desc_singleTriggerTypes)
                       par1Condition = strcmpi(searchTable.par1,'triggertype');
                    else
                       par1Condition = strcmpi(searchTable.par1,'triggercode'); 
                    end
                    triggerbyte = desc(this.par2_desc_index);
                    if triggerbyte ~= 0 
                        par2Condition = strcmpi(searchTable.par2,'bytestring');
                    else
                        par2Condition = strcmpi(searchTable.par2,this.dummyCharacter); 
                    end
                case this.task_sound_a
                    % do nothing                    
                case this.task_sound_b
                    if strcmpi(soundtype, 'B=A')
                        BisATypeIndex =  desc(this.par1_desc_index)+1;
                        BisAType = this.validBisATypes{BisATypeIndex};
                        switch lower(BisAType)
                            case 'sine'
                                par2Condition = strcmpi(searchTable.par2,'number');
                            case 'fixed'
                                par2Condition = strcmpi(searchTable.par2, this.dummyCharacter);                             
                        end
                    end
                case this.task_sound_ab                       
                    % do nothing
                case this.task_mux % Mux
                    muxByte = desc(this.par1_desc_index);
                    muxActionParID = 2;
                    muxAction = this.muxByte2muxParameter(muxByte, muxActionParID);
                    switch lower(muxAction)
                        case 'set'
                           par3Condition = strcmpi(searchTable.par3, 'number');
                        case 'reset'
                           par3Condition = strcmpi(searchTable.par3, this.dummyCharacter);                            
                    end
                case this.task_holdinput
                     % do nothing
                case this.task_soundmov % SoundMOV
                    actionIndex = desc(this.par1_desc_index)+1;
                    action = this.validActions{actionIndex};
                    switch lower(action)
                        case 'start'
                            par2Condition = strcmpi(searchTable.par2,'number');
                        case 'stop'
                            par2Condition = strcmpi(searchTable.par2,this.dummyCharacter); 
                    end
                case this.task_daq
                    divisor = desc(this.par3_desc_index);
                    if divisor == 1
                        par3Condition = strcmpi(searchTable.par3, this.dummyCharacter);
                    else
                        par3Condition =  strcmpi(searchTable.par3,'number');
                    end
                case this.task_dioout
                    % do nothing
                case this.task_trgout
                    doubledelay = desc(this.par2_desc_index);
                    if doubledelay == 0
                        par3Condition = strcmpi(searchTable.par2, this.dummyCharacter);
                    else
                        par3Condition =  strcmpi(searchTable.par2,'number');
                    end                    
                case this.task_multiconfiga
                    % do nothing
                case this.task_multiconfigb
                    % do nothing
                case this.task_att   
                    scalefactor1 = desc(this.par3_desc_index);
                    if scalefactor1 == 1
                        par3Condition = strcmpi(searchTable.par3, this.dummyCharacter);
                    else
                        par3Condition =  strcmpi(searchTable.par3,'number');
                    end
                case this.task_itd
                    % do nothing
                case this.task_mix
                    mixtypeIndex = desc(this.par1_desc_index)+1;
                    mixtype = this.validMixTypes{mixtypeIndex};
                    switch lower(mixtype)
                        case 'mixed'
                            par2Condition = strcmpi(searchTable.par2,'number');
                        case {'stop', 'btoa', 'atob'}
                            par2Condition = strcmpi(searchTable.par2,this.dummyCharacter);                             
                    end
                case this.task_ready
                    % do nothing
                otherwise
                    E = MException('BIOX:tasklist:desc2ListTableRow', 'BIOX:tasklist:desc2ListTableRow; cannot find task_id');
                    throw(E);
            end
                
            % create search condition for searching the taskrow described
            % by desc in the 
            searchCondition = tasksCondition & soundTypeCondition & par1Condition & par2Condition & par3Condition & par4Condition;           

            if (sum(searchCondition) == 0)
                E = MException('BIOX:tasklist:desc2ListTableRow', 'BIOX:tasklist:desc2ListTableRow; cannot find row in tasklist_definition.xlsx');
                throw(E);
            end

            if (sum(searchCondition) > 1)
                E = MException('BIOX:tasklist:desc2ListTableRow', 'BIOX:tasklist:desc2ListTableRow; find multiple rows in tasklist_definition.xlsx');
                throw(E);
            end   

            row = find(searchCondition);            

            parString = cell(1,4); % empty cells

            for parID = 1:4               
                value = desc(parID+3);
                col   = parID+2;

                parameterType = this.parameterTypeTable{row, col};
                type = lower(parameterType{1});
                %-----------------------------------------------------
                switch type
                    case 'number'
                        valueString = num2str(value);
                    case 'bytestring'
                        valueString = this.value2byteString(value);
                    case 'array'
                        valueString = this.value2arrayStr(value);
                    case {'triggertype', 'triggercode'}
                        valueString = this.value2triggerTypesStr(value);
                    case 'wavaction'
                        valueString = this.validWAVactions(value+1);
                    case 'wavroute'
                        valueString = this.validWAVRoutes(value+1);
                    case 'bisatype'
                        valueString = this.validBisATypes(value+1);
                    case 'action' 
                        valueString = this.validActions(value+1);
                    case 'mixtype'
                        valueString = this.validMixTypes(value+1);
                    case 'muxaction'
                        valueString = this.validMuxActions(value+1);
                    case 'edgetype'
                        valueString = this.validEdgeTypes(value+1); 
                    case this.dummyCharacter
                        valueString = this.dummyCharacter;
                    otherwise
                        error('Did not recognize parameter type.');
                end
                %-----------------------------------------------------
                if strcmpi(task, 'Mux')                    
                    valueString = this.muxByte2muxParameter(desc(this.par1_desc_index), parID);
                end
                %-----------------------------------------------------
                parameterName = this.parameterNameTable{row, col};
                if ~strcmp(parameterName, this.dummyCharacter)
                    parString{parID} = append(parameterName,' = ', valueString);
                else
                    parString{parID} = this.dummyCharacter;
                end
            end

            newRow = {time, task, soundtype, parString{1}, parString{2}, parString{3}, parString{4}};
            tableRow = cell2table(newRow, 'VariableNames', this.taskListHeaders);
            result = tableRow;
        end

        function addTable(this, listTable)
            %disp(listTable);
            nRows = height(listTable);            
            for row = 1: nRows
                delay_value    = listTable.delay(row);
                task_cell      = listTable.task(row);
                soundtype_cell = listTable.soundtype(row);
                parString{1}   = listTable.par1(row);
                parString{2}   = listTable.par2(row);
                parString{3}   = listTable.par3(row);
                parString{4}   = listTable.par4(row);
                %----------------------------------
                task           = task_cell{1};

                if iscell(delay_value)
                    delay = str2double(delay_value{1});
                end
                if isnumeric(delay_value)
                    delay = delay_value;
                end
                soundtype  = soundtype_cell{1};

                varNameStr = cell(1,4); % initialisation of parNameStr
                valueStr   = cell(1,4); % initialisation of valueStr                   

                for i = 1:4
                    equalSignPosCell = strfind(parString{i}, '=');                    
                    equalSignPos     = equalSignPosCell{1}; 
                    if ~isempty(equalSignPos)
                        varNameStr{i}    = cellfun(@(x) x(1:equalSignPos -1), parString{i}, 'UniformOutput', false); % format of parString: '<parameter name> = <value>'
                        varNameStr{i}    = strtrim(varNameStr{i});
                        varNameStr{i}    = strrep(varNameStr{i}, '''', '');
                        valueStr{i}      = cellfun(@(x) x(equalSignPos + 1:end), parString{i}, 'UniformOutput', false); % format of parString: '<parameter name> = <value>'
                        valueStr{i}      = strtrim(valueStr{i});
                        valueStr{i}      = strrep(valueStr{i}, '''', '');                          
                    else
                        varNameStr{i} = this.dummyCharacter;
                        valueStr{i}   = '';
                    end
                                       
                end

                searchTable        = this.parameterNameTable;
                tasksCondition     = strcmpi(searchTable.task, task);
                soundTypeCondition = strcmpi(searchTable.soundtype,soundtype);
                par1Condition      = strcmpi(searchTable.par1,varNameStr{1});
                par2Condition      = strcmpi(searchTable.par2,varNameStr{2});
                par3Condition      = strcmpi(searchTable.par3,varNameStr{3});
                par4Condition      = strcmpi(searchTable.par4,varNameStr{4});
                searchCondition    = tasksCondition & soundTypeCondition & ...
                                     par1Condition & par2Condition & ...
                                     par3Condition & par4Condition;                     
           
                rowMatch = find(searchCondition);  

                if isempty(rowMatch)
                    error('Matching row not found in ParameterNameTable');
                end

                par = cell(1,4); % initialisation of par
                index = 0;
                for i = 1:4
                    if isempty(parString{i})
                        continue;
                    end
                    if strcmp(parString{i}, this.dummyCharacter)
                        continue;
                    end
                    par{i} = this.setCorrectType( i, rowMatch, valueStr{i}); 
                    index = index + 1;
                 end
                %----------------------------------
                try
                    if ismember(task, this.validSoundTasks)
                        switch index
                            case 0
                                this.add_task(delay, task, soundtype);
                            case 1
                                this.add_task(delay, task, soundtype, par{1});
                            case 2
                                this.add_task(delay, task, soundtype, par{1}, par{2});
                            case 3
                                this.add_task(delay, task, soundtype, par{1},par{2}, par{3});
                            case 4
                                this.add_task(delay, task, soundtype, par{1},par{2}, par{3}, par{4});
                        end %switch
                    else
                        switch index
                            case 0
                                this.add_task(delay, task);
                            case 1
                                this.add_task(delay, task, par{1});
                            case 2
                                this.add_task(delay, task, par{1}, par{2});
                            case 3
                                this.add_task(delay, task, par{1},par{2}, par{3});
                            case 4
                                this.add_task(delay, task, par{1},par{2}, par{3}, par{4});                    
                        end %switch
                    end %else
                catch ME
                    rethrow(ME);
                end
            end %for
        end %table2TaskList

        function result = setCorrectType(this, i, row, valueStr)                        
            switch i
                case 1
                    typeCell = this.parameterTypeTable.par1(row);
                case 2
                    typeCell = this.parameterTypeTable.par2(row);
                case 3
                    typeCell = this.parameterTypeTable.par3(row);
                case 4
                    typeCell = this.parameterTypeTable.par4(row);
                otherwise
                    error('Error in parameter column name')
            end
            
            type     = typeCell{1};
            if iscell(valueStr)
                valueStr = valueStr{1};
            end
            
            % convert valueStr to value with the right type for the use of add_tasklist()
            switch lower(type)
                case {'number', 'triggercode'}
                    result = str2double(valueStr);
                case 'array'
                    result = str2num(valueStr); %#ok<ST2NM> 
                case 'bytestring'
                    result = this.valueStr2dec(valueStr);
                case {'wavaction', 'action', 'mixtype', 'wavroute', 'muxaction', 'edgetype', 'bisatype'}
                    result = valueStr;                
                case 'triggertype'
                    result = this.setTriggerType(valueStr);
                case this.dummyCharacter
                    result = []; % = dummy type
                otherwise
                    error('Did not recognize type name.');                    
            end
        end % setCorrectType       

        function result = value2arrayStr(this, value) %#ok<INUSL>
            bit_pos = [];
            for i = 0:7  % MATLAB integers zijn 32 bits
                if bitget(value, i + 1) == 1
                    bit_pos = [bit_pos, i]; %#ok<AGROW> 
                end
            end
            result =  ['[', strtrim(num2str(bit_pos)), ']'];
        end

        function result = valueStr2dec(this, valueStr) %#ok<INUSL> 
            bytestring = valueStr;
            bytestring = strrep(bytestring, '''', '');
            result = bin2dec(bytestring);            
        end

        function result = setTriggerType(this, valueStr)
            index = find(strcmpi(this.validTriggerTypes, valueStr), 1);
            result = sum(2.^(index-1));
        end

        function result = value2byteString(this, value) %#ok<INUSL> 
            bitString = dec2bin(value, 8); 
            result = [bitString(1:4), ' ', bitString(5:end)]; % add space in the middle
        end

        function result = value2triggerTypesStr(this, value_in)
            triggerList = cell(1,6);  %allocation for maximal 6 values
            exponent = 0; 
            count = 0; % count the number of triggerTypes
            value = value_in;
            while value > 0
                rest = mod(value, 2);                
                if rest == 1
                    count = count + 1;
                    triggerList{count} = this.validTriggerTypes(exponent + 1);
                end                
                value = floor(value / 2);
                exponent = exponent + 1;
            end
            if ~(count > 1)
                result = triggerList{1}; % give the value name
            else
                result = num2str(value_in); % give a number
            end
        end

        function check_timing(this)
            matrix = this.taskListMatrix;
            taskColumn = 1;
            delayColumn = 3;
            for row = 2:this.nr_of_tasks
                previousTaskIsNotWaitForTrigger = (matrix(row-1,taskColumn) ~= this.task_waitfortrigger);
                currentTaskIsBeforePreviousTask = matrix(row,delayColumn) < matrix(row-1,delayColumn);
                % when both are true something is wrong
                if previousTaskIsNotWaitForTrigger && currentTaskIsBeforePreviousTask                    
                    E = MException('BIOX:tasklist:timingError', 'BIOX:tasklist:timingError; A timing error is found in task number %d', row);
                    throw(E);
                end
            end
        end

        function desc = parse_task(this, varargin)
            funcName='biox_rz6_tasklist.m/add_task';

            acceptedTaskTypes  = horzcat(this.validTaskTypes, {'WFT'});

            validTask = @(x) any(validatestring(x,acceptedTaskTypes));
            validDelayTime = @(x) validateattributes(x,{'numeric'},{'scalar','nonnegative'});

            p = biox_inputParser;
            p.FunctionName = funcName;
            p.addRequired('DelayTime', validDelayTime);
            p.addRequired('Command', validTask);
            p.parse(varargin{1:2});      

            % Expand partially matched commands 
            command = lower(validatestring(p.Results.Command,acceptedTaskTypes));
            switch command
                case {'waitfortrigger', 'wft'}
                    desc = this.parse_waitfortrigger(p,varargin{:});

                case 'sounda'
                    desc = this.parse_sound(this.task_sound_a,p,varargin{:});

                case 'soundb'
                    desc = this.parse_sound(this.task_sound_b,p,varargin{:});
                    
                case 'soundab'
                    desc = this.parse_sound(this.task_sound_ab,p,varargin{:});

                case 'mux'
                    desc = this.parse_mux(p,varargin{:});

                case 'holdinput'
                    desc = this.parse_holdinput(p,varargin{:});

                case 'soundmov'
                    desc = this.parse_soundmov(p,varargin{:});

                case 'daq'
                    desc = this.parse_daq(p,varargin{:});
                                        
                case 'setdio'
                    desc = this.parse_setdio(p,varargin{:});

                case 'trigout'
                    desc = this.parse_trigout(p,varargin{:});

                case 'reset'
                    desc = this.parse_reset(p,varargin{:});

                case 'ready'
                    desc = this.parse_ready(p,varargin{:});

                case 'multiconfiga' %RL: naam veranderd
                    desc = this.parse_multiconfiga(p,varargin{:});

                case 'multiconfigb' %RL: naam veranderd
                    desc = this.parse_multiconfigb(p,varargin{:});

                case 'att'
                    desc = this.parse_att(p,varargin{:});

                case 'itd'
                    desc = this.parse_itd(p,varargin{:});
                    
                case 'mix'  %RL: toegevoegd   
                    desc = this.parse_mix(p,varargin{:});
                    
            otherwise
                error('unknown task: %s', p.Results.Command);
            end
                       
            desc.DelayTime = p.Results.DelayTime;

            if this.debugflag
               disp(['---',mfilename,' debug output---']);
               disp('INPUT:');
               disp(p.Results);
               disp('');
               disp('RESULT:');
               disp(desc);
               disp('---end---');
            end

            if any(isnan(struct2array(desc)))
               error('missing arguments');
            end
        end %parse_task

        function desc = newdesc(~)
            desc = struct( ...
              'TaskType', 0, ...
              'SoundType', 0, ...
              'DelayTime', 0, ...
              'Par1', 0, ...
              'Par2', 0, ...
              'Par3', 0, ...
              'Par4', 0 ...
            );
        end

        function desc = parse_waitfortrigger(this,p,varargin)
            desc = this.newdesc();
            
            desc.TaskType = this.task_waitfortrigger;

            validExtTrig = @(x) validateattributes(x, {'numeric'},{'scalar','nonnegative','<',256}); %aangpast RL 8-->256            
                    
            if ischar(varargin{3})
               validTriggerType = @(x) any(validatestring(x, this.validTriggerTypes));               
            else
               validTriggerType = @(x) validateattributes(x, {'numeric'},{'scalar','nonnegative','<',64}); %RL: 8 veranderd in 64.
            end                                                                                       
            
            validEdgeType = @(x) any(validatestring(x,this.validEdgeTypes));     
            
            validnChecks = @(x) validateattributes(x, {'numeric'},{'scalar','nonnegative','<',10000});   
                        
            p.addRequired('TriggerType', validTriggerType);            
            
            p.addOptional('ExternalTrigger', 0, validExtTrig);
            
            p.addOptional('EdgeType', 'Falling', validEdgeType); %default is 'Falling'
            
            p.addOptional('nChecks', 200, validnChecks);  %default is 200                          
            
            p.parse(varargin{:});
            
            desc.Par1 = TriggerType2num(p.Results.TriggerType);           
            
            desc.Par2 = p.Results.ExternalTrigger;            

            desc.Par3 = EdgeType2num(p.Results.EdgeType);
            
            desc.Par4 = max(2,p.Results.nChecks); %minimum value is 2          

            function n = TriggerType2num(x) 
               % validTriggerTypesInput = {  'ZBusA', 'ZBusB', 'External', 'Soft1', 'Soft2', 'Soft3' };
               if ischar(x)  
                  x = lower(x); 
                  if     x(5) == 'a'     %RL: extra opties toegevoegd
                     n = 1;
                  elseif x(5) == 'b'
                     n = 2;
                  elseif x(1) == 'e'
                     n = 4;
                  elseif x(5) == '1'
                     n = 8;                     
                  elseif x(5) == '2'
                     n = 16;                                          
                  elseif x(5) == '3'
                     n = 32;                                                               
                  end
               else % when x is a number
                  n = x;
               end
            end % TriggerType2num
            
            function n = EdgeType2num(x)
               if ischar(x)  
                  x = lower(x); 
                  if     x(1) == 'f' 
                     n = 0;
                  elseif x(1) == 'r'
                     n = 1;
                  end
               end
            end  %EdgeType2num

        end % parse_waitfortrigger

        function desc = parse_sound(this,tasktype,p,varargin)
            desc = this.newdesc();
            
            desc.TaskType = tasktype;           

            validSound      = @(x) any(validatestring(x, this.validSoundTypes));            
            validFreq       = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',1e+006});
%            validITD        = @(x) validateattributes(x,{'numeric'},{'scalar','nonnegative'});
            validPosNum     = @(x) validateattributes(x,{'numeric'},{'scalar','positive'});            
            valid01         = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',1});
            validStartPhase = @(x) validateattributes(x,{'numeric'},{'scalar','>=',-180,'<=',180});

            p.addRequired('SoundType', validSound);
            p.parse(varargin{1:3});

            % Expand partially matched SoundType strings, and convert to lowercase
            SoundType=lower(validatestring(p.Results.SoundType, this.validSoundTypes));
            
%{                        
            function n = input2bool(x)   %RL: for use in 'noise' case
               if ischar(x)  
                  x = lower(x); 
                  if     x(1) == 's'   
                     n = 0;
                  elseif x(1) == 'c'
                     n = 1;
                  end
               end
            end   
%}

            switch SoundType
            case 'stop'
               desc.SoundType = this.soundtype_stop;
               p.parse(varargin{1:3});

            case 'tone'
               desc.SoundType = this.soundtype_tone;
               p.addRequired('ToneFreq', validFreq);
               p.addOptional('ModFreq', 0, validFreq); %RL optional van gemaakt
               p.addOptional('ModBW', 0, validFreq);   %RL optional van gemaakt
               p.parse(varargin{:});
               desc.Par1 = p.Results.ToneFreq;
               desc.Par2 = p.Results.ModFreq;
               desc.Par3 = p.Results.ModBW;
               
            case 'multitone'
               desc.SoundType = this.soundtype_multitone;
               p.parse(varargin{:});               

            case 'sweep'
               desc.SoundType = this.soundtype_sweep;
               p.addRequired('StartFreq', validFreq);
               p.addRequired('NrOctaves', validPosNum);
               p.addRequired('Period', validPosNum);
               p.parse(varargin{:});
               desc.Par1 = p.Results.StartFreq;
               desc.Par2 = p.Results.NrOctaves;
               desc.Par3 = p.Results.Period; % Period in msec

            case 'noise'               
               desc.SoundType = this.soundtype_noise;
               p.addRequired('HpFreq1', validFreq);
               p.addRequired('LpFreq1', validFreq);
              
               p.parse(varargin{1:5});
               
               HpFreq1 = p.Results.HpFreq1;
               LpFreq1 = p.Results.LpFreq1;  
                             
               p.addOptional('HpFreq2', HpFreq1, validFreq);
               p.addOptional('LpFreq2', LpFreq1, validFreq);
               
               p.parse(varargin{:});
               
               HpFreq2 = p.Results.HpFreq2;
               LpFreq2 = p.Results.LpFreq2;                                 
                   
               if HpFreq1 >= LpFreq1
                 error('LpFreq must be greater than HpFreq');  
               end 
               
               if (HpFreq2 >= LpFreq2) && (HpFreq ~= 0)
                 error('LpFreq must be greater than HpFreq');  
               end 
               
               desc.Par1 = HpFreq1;
               desc.Par2 = LpFreq1;
               desc.Par3 = HpFreq2;
               desc.Par4 = LpFreq2;

            case 'ripple'
               desc.SoundType = this.soundtype_ripple;
               p.addRequired('StartFreq', validFreq);
               p.addRequired('ModInTime', validPosNum);
               p.addRequired('ModInFreq', validPosNum);
               p.addRequired('ModDepth' , valid01);
               p.parse(varargin{:});
               desc.Par1 = p.Results.StartFreq;
               desc.Par2 = p.Results.ModInTime; % Hz
               desc.Par3 = p.Results.ModInFreq; % Phase/Octave
               desc.Par4 = p.Results.ModDepth; % Modulation depth

            case 'wav'
               desc.SoundType = this.soundtype_wav;
               %RL mischien 'Reset' als input ipv 1?
               p.addOptional('Action', 'Reset', @(x) any(validatestring(x, this.validWAVactions))); %RL optional ipv Required
               if desc.TaskType == this.task_sound_ab                   
                   p.addOptional('Route', '1toA_2toB', @(x) any(validatestring(x, this.validWAVRoutes))); 
               end
               p.parse(varargin{:});
               switch lower(p.Results.Action)
               case 'continue'
                  desc.Par1 = 0;  
               case 'reset'
                  desc.Par1 = 1;
               case 'loop'   
                  desc.Par1 = 2; 
               otherwise
                 error('invalid parameter for WAV-sound, this is a bug');                      
               end 
               if desc.TaskType == this.task_sound_ab
                   switch p.Results.Route
                   case '1toA_2toB'
                       desc.Par2 = 0;
                   case '2toA_1toB' 
                       desc.Par2 = 1;
                   case '1toA_1toB'
                       desc.Par2 = 2;
                   case '2toA_2toB'
                       desc.Par2 = 3;
                   otherwise
    	               error('invalid parameter for WAV-sound, this is a bug');   
                   end
               end               

            case 'b=a'
               assert(desc.TaskType == this.task_sound_b,...
               'SoundType ''b=a'' is only valid for taskType ''SoundB''');
               desc.SoundType = this.soundtype_ba;

               validBisAType = @(x) any(validatestring(x, this.validBisATypes));

               p.addRequired('BisAType', validBisAType);
               p.parse(varargin{1:4});
               
               BisAType=lower(validatestring(p.Results.BisAType, this.validBisATypes));
               switch lower(BisAType)
               case 'fixed'
                  p.parse(varargin{:});
                  desc.Par1 = 1; 
               case 'sine'
                  p.addRequired('Period', validPosNum);
                  p.addRequired('Phase', validStartPhase);
                  p.parse(varargin{:});
                  desc.Par1 = 2;
                  desc.Par2 = p.Results.Period; % sec
                  desc.Par3 = p.Results.Phase; 
               otherwise
                  error('invalid mov type, this is a bug');
               end

            otherwise
               error('unknown sound type, this is a bug');
            end

        end %parse_sound

        function desc = parse_mux(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_mux;

           validChannel = @(x) validateattributes(x, {'numeric'}, {'scalar','nonnegative','<=',15});
           validDevice = @(x) validateattributes(x, {'numeric'}, {'scalar','nonnegative','<=',3});
           validMuxAction = @(x) any(validatestring(x, this.validMuxActions));
           p.addRequired('Device', validDevice);
           p.addRequired('Action', validMuxAction);
           p.addOptional('Channel', [], validChannel);

           p.parse(varargin{:});

           Channel = p.Results.Channel;
           Device  = p.Results.Device;
           Action  = p.Results.Action;

           if strcmpi(Action,'Set') && isempty(Channel)
               error('''Set'' requires a the value of ''Channel'' to be specified.');
           end

           desc.Par1 = this.muxParameters2muxByte(Device, Action, Channel);
        end % parse_mux       

        function desc = parse_holdinput(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_holdinput;

           validInputMask = @(x) validateattributes(x, {'numeric'}, {'scalar','nonnegative', '<',256});
                     
           validEdgeType = @(x) any(validatestring(x,this.validEdgeTypes));             
           validnChecks = @(x) validateattributes(x, {'numeric'},{'scalar','nonnegative','<',10000});      
           
           p.addRequired('InputMask', validInputMask);
           p.addOptional('EdgeType', 'Falling', validEdgeType); %default = 'Falling'                     
           p.addOptional('nChecks', 200, validnChecks);  %default is 200
           p.parse(varargin{:});
           
           desc.Par1 = p.Results.InputMask;
           desc.Par2 = EdgeType2num(p.Results.EdgeType);
           desc.Par3 = max(2, p.Results.nChecks); %minimum is 2
           
           function n = EdgeType2num(x) %RL: toegevoegd V3.28
               if ischar(x)  
                  x = lower(x); 
                  if     x(1) == 'f'     %RL: extra opties toegevoegd
                     n = 0;
                  elseif x(1) == 'r'
                     n = 1;
                  end
               end
           end % EdgeType2num

        end % parse_holdinput

        function desc = parse_soundmov(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_soundmov;

           validAction = @(x) any(validatestring(x, this.validActions));
           validNumSpeakers = @(x) validateattributes(x, ...
              {'numeric'}, {'odd','scalar','>=',3,'<=',21});
           validPeriod = @(x) validateattributes(x, ...
              {'numeric'}, {'scalar','nonnegative'});
           validStartPhase = @(x) validateattributes(x, ...
              {'numeric'}, {'scalar','>=',-180, '<=',180});

           p.addRequired('StartStop', validAction);
           p.parse(varargin{1:3});
           switch lower(p.Results.StartStop)
           case 'stop'
               p.parse(varargin{:});
               desc.Par1 = 0;
           case 'start'
               desc.Par1 = 1;
               p.addRequired('NumSpeakers', validNumSpeakers);
               p.addRequired('Period', validPeriod);
               p.addRequired('StartPhase', validStartPhase);
               p.parse(varargin{:});
               desc.Par2 = p.Results.NumSpeakers;
               desc.Par3 = p.Results.Period;
               desc.Par4 = p.Results.StartPhase;
           otherwise
               error('unexcpected error, this is a bug');
           end
        end % parse_soundmov

        function desc = parse_daq(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_daq;
           validChannelSelection = @(x) validateattributes(x, ...
               {'numeric'}, {'vector','nonnegative'});
           validDivisor = @(x) validateattributes(x, ...
              {'numeric'}, {'integer','positive'});
           validAction = @(x) any(validatestring(x, this.validActions));
         
           p.addRequired('StartStop', validAction);
           p.addRequired('ChannelSelection', validChannelSelection);           
           p.addOptional('Divisor', 1, validDivisor);

           p.parse(varargin{:});
           switch lower(p.Results.StartStop)
           case 'stop'
               desc.Par1 = 0;
           case 'start'
               desc.Par1 = 1;
           otherwise
               error('unexcpected error, this is a bug');
           end
          
           ChannelList = p.Results.ChannelSelection; % RL
           ChannelListInt = sum(2.^ChannelList); %RL: convert to integer 
           
           desc.Par2 = ChannelListInt;
           desc.Par3 = round(p.Results.Divisor); %must be an integer
        end % parse_daq

        function desc = parse_setdio(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_dioout;

           validByte = @(x) validateattributes(x, ...
              {'numeric'}, {'scalar','nonnegative','<',256});

           p.addRequired('OutputByte', validByte);
           p.parse(varargin{:});
           desc.Par1 = p.Results.OutputByte;
        end % parse_setdio

        function desc = parse_trigout(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_trgout;

           validByte = @(x) validateattributes(x, ...
              {'numeric'}, {'scalar','nonnegative','<',256});
           validDTInterval = @(x) validateattributes(x, ...
              {'numeric'}, {'scalar','>=',0.000040}); %RL: changed to >= 0.000040 = two clock tics

           p.addRequired('OutputByte', validByte);                     
           p.addOptional('DoubleDelay',0, validDTInterval);           
           p.parse(varargin{:});           
           desc.Par1 = p.Results.OutputByte;          
           desc.Par2 = p.Results.DoubleDelay;
        end % parse_trigout

        function desc = parse_reset(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_reset;
           p.parse(varargin{:});
        end % parse_reset

        function desc = parse_ready(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_ready;
           p.parse(varargin{:});
        end % parse_ready

        function desc = parse_multiconfig(this,tasktype,p,varargin)
           validIndex = @(x) validateattributes(x,{'numeric'},{'scalar','positive','<=',4});
           validFrequency = @(x) validateattributes(x,{'numeric'},{'scalar','nonnegative'});
           validPhase = @(x) validateattributes(x,{'numeric'},{'scalar','>=',-180,'<=',180});
           validAmplitude = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',1000}); %RL bereik veranderd 0...1000%

           desc = this.newdesc();
           desc.TaskType = tasktype;
           p.addRequired('Index', validIndex);
           p.addRequired('Frequency', validFrequency);
           p.addRequired('Phase', validPhase);
           p.addRequired('Amplitude', validAmplitude);
           p.parse(varargin{:});
           desc.Par1 = p.Results.Index;
           desc.Par2 = p.Results.Frequency;
           desc.Par3 = p.Results.Phase;
           desc.Par4 = p.Results.Amplitude;
        end % parse_multiconfig

        function desc = parse_multiconfiga(this,p,varargin)
            desc = this.parse_multiconfig(this.task_multiconfiga, p, varargin{:});
        end 

        function desc = parse_multiconfigb(this,p,varargin)
            desc = this.parse_multiconfig(this.task_multiconfigb, p, varargin{:});
        end

        function desc = parse_att(this,p,varargin)
           validAttenuation = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',80});
           validScaleFactor = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',1000});
           desc = this.newdesc();
           desc.TaskType = this.task_att;
           p.addRequired('Attenuation1', validAttenuation);
           p.addRequired('Attenuation2', validAttenuation);
           p.addOptional('ScaleFactor1', 1, validScaleFactor);
           p.addOptional('ScaleFactor2', 1, validScaleFactor);
           p.parse(varargin{:});
           desc.Par1 = p.Results.Attenuation1;
           desc.Par2 = p.Results.Attenuation2;
           desc.Par3 = p.Results.ScaleFactor1;
           desc.Par4 = p.Results.ScaleFactor2;
        end % parse_att

        function desc = parse_itd(this,p,varargin)
           validITD = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',0.01});           
           desc = this.newdesc();
           desc.TaskType = this.task_itd;
           p.addRequired('ITD1', validITD);
           p.addRequired('ITD2', validITD);
           p.parse(varargin{:});
           desc.Par1 = p.Results.ITD1;
           desc.Par2 = p.Results.ITD2;
        end % parse_itd        

         %RL functie parse_mix toegevoegd
        function desc = parse_mix(this,p,varargin)
           desc = this.newdesc();
           desc.TaskType = this.task_mix;

           validMixType =  @(x) any(validatestring(x, this.validMixTypes));
           
           validFactor = @(x) validateattributes(x,{'numeric'},{'scalar','>=',0,'<=',1});
           
           p.addRequired('Type', validMixType);                     
           p.addOptional('Factor',1,validFactor); 
           
           p.parse(varargin{:});           
           desc.Par1 = input2num(p.Results.Type);          
           desc.Par2 = p.Results.Factor;
           
           function n = input2num(x) 
           % expectedStringInputs = {'Stop','BtoA','AtoB','MixToBoth'};
               if ischar(x)  
                  x = lower(x); 
                  if     x(1) == 's'     %RL: extra opties toegevoegd
                     n = 0;
                  elseif x(1) == 'b'
                     n = 1;
                  elseif x(1) == 'a'
                     n = 2;
                  elseif x(1) == 'm'
                     n = 3;                                   
                  end
               else
                  n = 0;
               end % if..else
           end % function input2num
        end % function parse_mix   

        function result = muxParameters2muxByte(this, device, action, channel) %#ok<INUSL> 
            switch lower(action)
                case 'set'
                    setBit = 1; resetBit = 0;
                    channelValue  = bitshift(channel, 0);
                case 'reset'
                    setBit = 0; resetBit = 1;
                    channelValue = 0;
            end
            
            deviceValue   = bitshift(device,  4);
            setBitValue   = bitshift(setBit,  6);
            resetBitValue = bitshift(resetBit, 7);            
            result = channelValue + deviceValue + setBitValue + resetBitValue;
        end

        function result = muxByte2muxParameter(this, muxByte, parID) %#ok<INUSL> 
            byte = uint8(muxByte);             
            
            channelID    = bitshift(bitand(byte, bin2dec('00001111')),  0);
            deviceID     = bitshift(bitand(byte, bin2dec('00110000')), -4);
            setBit       = bitshift(bitand(byte, bin2dec('01000000')), -6);
            resetBit     = bitshift(bitand(byte, bin2dec('10000000')), -7);

            channelString = num2str(channelID);
            deviceString  = num2str(deviceID);

            if setBit
                 actionString = 'set';
            end
            if resetBit
                 actionString = 'reset';
            end

            switch parID
                case 1
                    result = deviceString;
                case 2
                    result = actionString;
                case 3
                    result = channelString;
                otherwise
                    result = '';
            end
        end

    end % methods (Access = protected)

    methods (Static)

        function desc=define_task(task_type, sound_type, delay_time, par1, par2, par3, par4)
           desc = [ task_type sound_type delay_time par1 par2 par3 par4 ];
        end        
        
    end % methods (Static)
end %classdef
