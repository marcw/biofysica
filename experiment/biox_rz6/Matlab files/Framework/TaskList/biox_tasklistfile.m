classdef biox_tasklistfile < handle

    properties (Constant)
        tasklistHeaders = {'delay', 'task', 'soundtype', 'par1', 'par2', 'par3', 'par4'}';
        txtDelimiter = '\t'; %= tab character
        csvDelimiter = ';';        
        csvFilter = {'*.csv', 'CSV Files (*.csv)'}
        txtFilter = {'*.txt', 'Text Files (*.txt)'}
        matFilter = {'*.mat', 'Matlab files (*.mat)'}
        xlsFilter = {'*.xls;*.xlsx', 'Excel Files (*.xls, *.xlsx)'}
        allFilter = {'*.xls;*.xlsx;*.txt;*.csv;*.mat', 'Excel/Text/CSV/Matlab Files  (*.xls, *.xlsx, *.txt, *.csv, *.mat)'}
    end

    properties 
        debug = true;
    end

    properties (Access = private)
        defaultDir
    end

    methods (Access = public)

        function obj = biox_tasklistfile(defaultDir)            
            obj.defaultDir = defaultDir;
        end

        function result = load(obj, filename)
            currentDir = pwd;
            cd(obj.defaultDir);
            try            
                [~, ~, extension] = fileparts(filename);
                ext = extension(2:end);
                switch ext                
                    case {'xls', 'xlsx'}
                        listTable = readtable(filename);
                    case 'txt'
                        listTable = readtable(filename, 'Delimiter', obj.txtDelimiter);
                    case 'csv'
                        listTable = readtable(filename, 'Delimiter', obj.csvDelimiter);                        
                    case 'mat'
                        loadedStruct = load(filename, 'listTable'); 
                        listTable = loadedStruct.listTable;
                    otherwise
                        listTable = table; %empty table                          
                end
            
            % Vervang NaN-waarden door {0x0 char} in de huidige kolom
            listTable = obj.cleanupNanValues(listTable); 
            result = biox_rz6_tasklist(listTable);                 
            cd(currentDir);
            catch ME
                cd(currentDir);
                rethrow(ME);
            end             
        end

        function result = loadDialog(obj, varargin)
            currentDir = pwd;
            cd(obj.defaultDir);
            try
                if ~isempty(varargin)
                    filetype = varargin{1};
                else
                    filetype = 'all';
                end
                switch filetype
                    case 'all'
                        filename = obj.allFileDialog;
                    case {'xls', 'xlsx'}
                        filename = obj.excelFileDialog;
                    case 'txt'
                        filename = obj.textFileDialog;
                    case 'csv'
                        filename = obj.csvFileDialog;                        
                    case 'mat'
                        filename = obj.matFileDialog;
                end
                result = obj.load(filename);
            cd(currentDir);
            catch ME
                cd(currentDir);
                rethrow(ME);
            end                 
        end

        function saveAs(obj, tasklist, varargin)            
            currentDir = pwd;            
            try
                if ~isempty(varargin)
                    filetype = varargin{1};
                else
                    filetype = 'all';
                end
                cd(obj.defaultDir);
                listTable = tasklist.asTable;
                templateTable = tasklist.templateTable;
                stringTypesTable = tasklist.stringTypesTable;
                switch filetype
                    case {'all'}
                        obj.saveFile(listTable);
                    case {'xls', 'xlsx'}
                        obj.saveExcelFile(listTable, templateTable, stringTypesTable);
                    case 'txt'
                        obj.saveTextFile(listTable);
                    case 'csv'
                        obj.saveCSVFile(listTable);
                    case 'mat'
                        obj.saveMatFile(listTable);
                end 
            catch ME
                cd(currentDir);
                rethrow(ME);
            end
        end
    end

    methods (Access = private)       
        

        function result = cleanupNanValues(~, listTable)
            for col = 1:size(listTable, 2)
                % Haal de kolomdata op als een cel-array
                colData = table2cell(listTable(:, col));
                modified = false;
                
                for row = 1:size(listTable, 1)
                    value = colData{row};
                    if ~iscell(value)
                        if isnan(value)
                            % Vervang NaN door een lege string
                            colData{row} = '';
                            modified = true;
                        else
                            % Converteer numerieke waarden naar cellen voor consistentie
                            colData{row} = value;
                        end
                    end
                end
                
                if modified
                    % Wijs de aangepaste cel-array toe aan de kolom in de tabel
                    listTable.(listTable.Properties.VariableNames{col}) = colData;
                end
            end
            result = listTable;
        end

        function result = allFileDialog(obj)
             [fileName, filePath] = uigetfile(obj.allFilter, 'Select a file');
            if fileName == 0
                result = '';
                return;
            end              
            result = fullfile(filePath, fileName);            
        end           

        function result = excelFileDialog(obj)
            [fileName, filePath] = uigetfile(obj.xlsFilter, 'Select an Excel file');
            if fileName == 0
                result = '';
                return;
            end              
            result = fullfile(filePath, fileName);            
        end

        function result = textFileDialog(obj)            
            [fileName, filePath] = uigetfile(obj.txtFilter, 'Select a text file');
            if fileName == 0
                result = '';
                return;
            end              
            result = fullfile(filePath, fileName);                   
        end

        function result = csvFileDialog(obj)            
            [fileName, filePath] = uigetfile(obj.csvFilter, 'Select a CSV file');
            if fileName == 0
                result = '';
                return;
            end              
            result = fullfile(filePath, fileName);                   
        end        

        function result = matFileDialog(obj) 
            [fileName, filePath] = uigetfile(obj.matFilter, 'Select a Matlab file');
            if fileName == 0
                result = '';
                return;
            end            
            result = fullfile(filePath, fileName);
        end       

        function saveFile(obj, listTable)
            [fileName, filePath] = uiputfile(obj.allFilter, 'Save as');
            if fileName == 0
                return;
            end
            selectedFilePath = fullfile(filePath, fileName);
            [~, ~, extension] = fileparts(selectedFilePath);
            ext = extension(2:end);
            switch ext
                case {'xls', 'xlsx'}
                    writetable(listTable, selectedFilePath, 'Sheet', 'taskList');            
                case 'txt'
                    writetable(listTable, selectedFilePath, 'Delimiter', obj.txtDelimiter);
                case 'csv'
                    writetable(listTable, selectedFilePath, 'Delimiter', obj.csvDelimiter);                    
                case 'mat'
                    save(selectedFilePath, 'listTable');
            end
        end

        function saveExcelFile(obj, listTable, templateTable, stringTypesTable)
            [fileName, filePath] = uiputfile(obj.xlsFilter, 'Save as');
            if fileName == 0
                return;
            end
            selectedFilePath = fullfile(filePath, fileName);
            writetable(listTable, selectedFilePath, 'Sheet', 'taskList');
            writetable(templateTable, selectedFilePath, 'Sheet', 'template');
            writetable(stringTypesTable, selectedFilePath, 'Sheet', 'stringtypes');
        end
        
        function saveTextFile(obj, listTable)
            [fileName, filePath] = uiputfile(obj.txtFilter, 'Save as');
            if fileName == 0
                return;
            end            
            selectedFilePath = fullfile(filePath, fileName);
            writetable(listTable, selectedFilePath, 'Delimiter', obj.txtDelimiter);
        end

        function saveCSVFile(obj, listTable)
            [fileName, filePath] = uiputfile(obj.csvFilter, 'Save as');
            if fileName == 0
                return;
            end            
            selectedFilePath = fullfile(filePath, fileName);
            writetable(listTable, selectedFilePath, 'Delimiter', obj.csvDelimiter);
        end        

        function saveMatFile(obj, listTable)
            [fileName, filePath] = uiputfile(obj.matFilter, 'Save as');
            if fileName == 0
                return;
            end            
            selectedFilePath = fullfile(filePath, fileName);            
            save(selectedFilePath, 'listTable');
        end

    end
end