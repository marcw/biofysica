classdef taskListConstants < handle
    properties (Constant)

        validTasks                   = { 'WaitForTrigger', 'SoundA','SoundB','Mux','HoldInput',...
                                         'SoundMov','Daq','SetDIO','TrigOut','Reset','Ready',...
                                         'MultiConfigA','MultiConfigB','Att','ITD','Mix','SoundAB'};

        valid_soundtypes             =  {'Stop','Tone','Sweep','Noise','Ripple','WAV','B=A','MultiTone'};

        task_sound_stop_parameters   = {};

        task_sound_tone_parameters   = {'ToneFreq', 'ModFreq', 'ModBW'};

        task_sound_sweep_parameters  = {'StartFreq', 'NrOctaves', 'Period'};

        task_sound_noise_parameters  = {'HpFreq1', 'LpFreq1', 'HpFreq2', 'LpFreq2'};

        task_sound_ripple_parameters = {'StartFreq', 'ModInTime', 'ModInFreq', 'ModDepth'};

        task_sound_wav_parameters    = {'Reset', 'Route'}
        valid_WAV_par1               = {'Reset', 'Continue', 'Loop'};
        valid_WAV_par2               = {'1toA_2toB', '2toA_1toB', '1toA_1toB', '2toA_2toB'};


        task_soundab_parameters      = {'movType', 'Period', 'Phase'};
        valid_soundab_par1           = {'Fixed', 'Linear', 'Sine'};        

        task_mux_parameters          = {'Device', 'Action', 'Channel'};      
        valid_mux_par2               = {'Set', 'Reset'};

        task_holdinput_parameters    = {'InputMask', 'EdgeType', 'nChecks'};

        task_soundmov_parameters     = {'StartStop', 'NumSpeakers', 'Period', 'StartPhase'};
        valid_SoundMov_par1          = {'Start','Stop'};        

        task_setdio_parameters       = {'OutputByte'};

        task_trgout_parameters       = {'OutputByte', 'DoubleDelay'};

        task_reset_parameters        = {};

        task_ready_parameters        = {}; 

        task_multiconfiga_parameters = {'Index', 'Frequency', 'Phase', 'Amplitude'};

        task_multiconfigb_parameters = {'Index', 'Frequency', 'Phase', 'Amplitude'};

        task_att                     = {'Attenuation1', 'Attenuation2', 'ScaleFactor1', 'ScaleFactor2'};

        task_ITD                     = {'ITD1', 'ITD1'};

        task_Mix                     = {'Input', 'Factor'};
        valid_Mix_par1               = {'Stop','BtoA','AtoB','Mixed'};       
    end

    methods (Access = public)       
        function result = getValue(obj, validStrings, searchString) %#ok<INUSL> 
            result = find(strcmp(validStrings, searchString)) - 1; %lowest value is 0
            if isempty(result)
                currentScript = mfilename;
                stack = dbstack;
                actueleFunctie = stack(1).name;
                message = ['"' searchString '" niet gevonden.'];
                error(['Error in ' currentScript ':' actueleFunctie ' Message = ' message]);
            end
        end

    end    

end