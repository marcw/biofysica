classdef BIOX_DIOTypes < BIOX_types      
    properties (Constant) 
        inputMaskTemplate  = 'inputmask = ''%s''';        
        outputByteTemplate = 'outputbyte = ''%s''';        
        A = {'00000001','00000010','00000100','00001000','00010000','00100000','01000000','10000000'};
        B = BIOX_DIOTypes.A;
        inputs = BIOX_DIOTypes.A;
        outputs = BIOX_DIOTypes.B;
    end
end