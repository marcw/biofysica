classdef BIOX < handle
    properties (Constant)
        DaqActions = BIOX_DaqActions;
        DaqChanList = BIOX_DaqChanList;
        DaqDivisor = BIOX_DaqDivisor;
        DIOchannels = BIOX_DIOchannels;
        DIOTypes = BIOX_DIOTypes;
        edgeTypes = BIOX_edgeTypes;
        inputmasks = BIOX_inputmasks;
        mixTypes = BIOX_mixTypes;
        muxActions = BIOX_muxActions;
        nChecks = BIOX_nChecks;
        parameterTemplates = BIOX_parameterTemplates;
        soundMOVActions = BIOX_soundMovActions;
        soundTypes = BIOX_soundTypes;
        taskTypes = BIOX_taskTypes;
        triggerCodes = BIOX_triggerCodes;
        triggerTypes = BIOX_triggerTypes;
        types = BIOX_types;
        wavActions = BIOX_wavActions;
        wavRoutes = BIOX_wavRoutes;
    end
end