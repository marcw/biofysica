classdef BIOX_triggerTypes < BIOX_types
    properties (Constant)
        template  = 'triggertype = %s';
        ZBusA     = 'ZBusA';
        ZBusB     = 'ZBusB';
        External  = 'External';
        Soft1     = 'Soft1';
        Soft2     = 'Soft2';
        Soft3     = 'Soft3';
    end
end

