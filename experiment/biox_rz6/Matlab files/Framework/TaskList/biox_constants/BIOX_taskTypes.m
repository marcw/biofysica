classdef BIOX_taskTypes < BIOX_types
    properties (Constant)        
        WaitForTrigger = 'WFT';
        SoundA         = 'SoundA';
        SoundB         = 'SoundB';
        SoundAB        = 'SoundAB';
        Multiplexer    = 'MUX';
        HoldInput      = 'HoldInput';
        SoundMOV       = 'SoundMOV';
        DataAcq        = 'Daq';
        SetDIO         = 'SetDIO';
        TrigOut        = 'TrigOut';
        Reset          = 'Reset';
        Ready          = 'Ready';
        MultiConfigA   = 'MultiConfigA';
        MultiConfigB   = 'MultiConfigB';
        Attenuation    = 'Att';
        ITD            = 'ITD';
        Mix            = 'Mix'; 
    end
end