classdef BIOX_edgeTypes < BIOX_types
    properties (Constant) 
        template  = 'edgetype = %s';
        falling   = 'Falling';
        rising    = 'Rising';
    end
end