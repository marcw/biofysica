classdef BIOX_wavRoutes < BIOX_types
    properties (Constant)    
        template      = 'route = %s';
        WAV_1toA_2toB = '1toA_2toB';
        WAV_2toA_1toB = '2toA_1toB';
        WAV_1toA_1toB = '1toA_1toB';
        WAV_2toA_2toB = '2toA_2toB';
    end
end