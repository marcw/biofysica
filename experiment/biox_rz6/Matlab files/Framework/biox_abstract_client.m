%  biox_abstract_client  

classdef  biox_abstract_client < handle

    properties (Access=protected)
        my_version = "4.00";           
    end

    methods (Abstract)
        write(this, tagname, value, offset)
        data = read(this, tagname, offset, nWords, datatype, nChannels)           
    end
    
end
