%BIOX course exercise 8 Playing a WAV file
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

%read data from a file
[source_data, source_freq] = audioread('c:\BIOXcourse\AnnekeWintDrieGroteBloemen.wav');       

%source_data has alreay two channels for stereo sound. For educational
%purpose we will only consider the first channel:

sound_data = source_data(:,1);

% resample data for rz6 sample rate
rz6_freq = round(rz6.read_samplerate); % this will be 24414 Hz for single core RZ6 or 48828 Hz for 3/4 core RZ6
soundData_48kHz = resample(sound_data, rz6_freq, source_freq);

%specify the channels; channel 1 corresponds to channel A and channel 2 to channel B  
chanlist  = [1 2];
soundlist = [soundData_48kHz soundData_48kHz]; %this specifies the same sound for both channels.

%write wav data to RZ6
rz6.write_wavdata(soundlist, chanlist);

tl = biox_rz6_tasklist;

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'Att',20,20);
tl.add_task(0.000,'WaitForTrigger', 'External', bin2dec('0001 0000'));

tl.add_task(0.000,'SoundAB', 'WAV');
tl.add_task(1.000,'SoundAB', 'Stop');
tl.add_task(1.000,'WaitForTrigger', 'External', bin2dec('0001 0000'));

tl.add_task(0.000,'SoundAB', 'WAV', 'Continue');
tl.add_task(2.700,'SoundAB', 'Stop');
%********************************************************************

%write tasklist to RZ6
rz6.write_tasklist(tl);

delete(tl);
delete(rz6);
