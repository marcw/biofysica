%BIOX course exercise 9
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

tl = biox_rz6_tasklist;

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000, 'SetDIO', bin2dec('0000 1000'));
tl.add_task(0.200, 'SetDIO', bin2dec('1000 0100'));
tl.add_task(0.400, 'SetDIO', bin2dec('0000 0010'));
tl.add_task(0.600, 'SetDIO', bin2dec('1000 0001'));
tl.add_task(0.800, 'SetDIO', bin2dec('0000 0010'));
tl.add_task(1.000, 'SetDIO', bin2dec('1000 0100'));
tl.add_task(1.200, 'SetDIO', bin2dec('0000 1000'));
tl.add_task(1.400, 'SetDIO', bin2dec('1000 0000'));
tl.add_task(1.600, 'SetDIO', bin2dec('0000 1111'));
tl.add_task(1.800, 'SetDIO', bin2dec('1000 0000'));
tl.add_task(2.000, 'SetDIO', bin2dec('0000 1111'));
tl.add_task(2.200, 'SetDIO', bin2dec('1000 0000'));
tl.add_task(2.400, 'SetDIO', bin2dec('0000 1111'));
tl.add_task(2.600, 'SetDIO', bin2dec('1000 0000'));
tl.add_task(2.800, 'SetDIO', bin2dec('0000 1111'));
tl.add_task(3.000, 'SetDIO', bin2dec('1000 0000'));
%********************************************************************

rz6.write_tasklist(tl);

delete(tl);
delete(rz6);
