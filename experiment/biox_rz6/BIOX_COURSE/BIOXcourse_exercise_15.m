%BIOX course exercise 15 Debugging
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

tl = biox_rz6_tasklist;

tl.debug(false); 

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.100,'Att',20,20);
tl.add_task(0.200,'WaitForTrigger', 'External', bin2dec('0001 0000'));

tl.add_task(0.200,'SoundAB', 'Tone', 1000);
tl.add_task(1.000,'SoundAB', 'Stop');
tl.add_task(1.100,'WaitForTrigger', 'External', bin2dec('0001 0000'));

tl.add_task(0.200,'Ready');
%********************************************************************
elapsed = 0;
rz6.write_tasklist(tl);
tic;
taskindex = rz6.read_taskindex;
tasktype = rz6.read_tasktype;
elapsed = elapsed + toc;  
str = sprintf("elapsed = %0.3f sec\n    taskindex = %d\n    tasktype = %d\n", elapsed, taskindex, tasktype);  
disp(str);    

for i = 1:tl.nr_of_tasks   
    taskindex = rz6.read_taskindex;
    tasktype = rz6.read_tasktype;
    elapsed = elapsed + toc;    
    tic
    str = sprintf("elapsed = %0.3f sec\n    taskindex = %d\n    tasktype = %d\n", elapsed, taskindex, tasktype);  
    disp(str);
    while true % wait for taskindex to change  or trial is ready                         
        if (rz6.read_taskindex ~= i) || rz6.read_trialready
            break; 
        end 
    end       
end 

disp(str);    
delete(tl);
delete(rz6);

