function CopyCourseExercises
    clc;
    clear;
    
    [sourceDir, ~, ~] = fileparts(mfilename('fullpath'));
    
    destinationDir = 'c:\BIOXcourse';
    
    [status,msg] = mkdir(destinationDir);
    
    if status
        
        copyfile(sourceDir, destinationDir)

        cd(destinationDir);
        msgbox('all done!')
    else
        msgbox(msg)
    end    
end