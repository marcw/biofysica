%BIOX course exercise 13 Multi trial tasklist
clear;
clc;

%********************************************************************
% prepare parameters for all trials
nTrials = 8;
tones = [0 2 4 5 7 9 11 12];
basefrequency = 400;
halfToneMultiplier = 2^(1/12);

%********************************************************************
%prepare trial specific parameters

frequency = zeros(nTrials,1);

for i=1:nTrials
   tone = tones(i); 
   frequency(i) = (basefrequency) * halfToneMultiplier^(tone);
end

%********************************************************************

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

tl = biox_rz6_tasklist;

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'Att',25,25); %channel A&B are 20 dB attenuated
tl.add_task(0.000,'WaitForTrigger', 'Soft1');        

% each trial plays a tone with its own frequency
for i = 1:nTrials      
    tl.add_task(0.000,'SoundAB','Tone', frequency(i)); 
    if ~(i == nTrials)        
        tl.add_task(0.200,'SoundAB','Stop'); %stop the sound
    else
        tl.add_task(0.600,'SoundAB','Stop'); %stop the sound
    end    
    tl.add_task(0.600,'WaitForTrigger', 'Soft1');        
end

%tl.add_task(0.400, 'Ready');

%********************************************************************

rz6.write_tasklist(tl);
pause(1); % wait for the tasklist to load and start
    
%********************************************************************
%                         Trial control
%********************************************************************

%start every second a new trial
for i = 1:nTrials
    playTrial(rz6);  
    pause(0.600); % wait 400 ms 
end

delete(tl);
delete(rz6);

%********************************************************************
%                      Function playTrial
%********************************************************************

function playTrial(rz6)
    rz6.trigger('Soft1');    
end

