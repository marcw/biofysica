%BIOX course exercise 6: timing of tasks
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

tl = biox_rz6_tasklist;

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'Att',25,25);
tl.add_task(0.000,'WaitForTrigger', 'Soft1');

tl.add_task(0.000,'SoundA','Tone', 500);
tl.add_task(1.000,'SoundA','Stop');
tl.add_task(1.000,'WaitForTrigger', 'Soft2');

tl.add_task(0.000,'SoundB','Tone', 550);
tl.add_task(1.000,'SoundB','Stop');

tl.add_task(1.000,'WaitForTrigger', 'Soft3');
tl.add_task(0.000,'SoundAB','Tone', 600);
tl.add_task(1.000,'SoundAB','Stop');
%********************************************************************

rz6.write_tasklist(tl);

WaitSecs(1);
rz6.trigger('Soft1');
WaitSecs(1); 
rz6.trigger('Soft2');
WaitSecs(1); 
rz6.trigger('Soft3');


delete(tl);
delete(rz6);