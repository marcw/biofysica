%BIOX course exercise 11 Data acquisition of analog I/O
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

acqChannelList = [3 4]; %these data-acquisition channels are looking at output-A and output-B

tl = biox_rz6_tasklist;

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000, 'Att',20,20);
tl.add_task(0.000, 'SoundA', 'Tone', 500);
tl.add_task(0.000, 'SoundB', 'Tone', 600);
tl.add_task(0.500, 'Daq', 'Start', acqChannelList, 10);
tl.add_task(0.800, 'Daq', 'Stop', [3]);
tl.add_task(1.000, 'Daq', 'Stop', [4]);
tl.add_task(2.000, 'SoundAB', 'Stop');
tl.add_task(2.000, 'Ready');
%********************************************************************

rz6.write_tasklist(tl);

acqReady = [0 0];

%wait for the tasklist to be ready
while rz6.read_trialready() == 0
  % do nothing  
end

data = rz6.read_acqdata(acqChannelList);

%data contains two channels of different length
data1 = data{1}; 
data2 = data{2}; 

plot(data1, data2(1:length(data1)));

delete(tl);
delete(rz6);
