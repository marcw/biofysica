%BIOX course exercise 12 Data-acquisition of digital I/O
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

%read data from a file
[source_data, source_freq] = audioread('c:\BIOXcourse\AnnekeWintDrieGroteBloemen.wav');       

%source_data has alreay two channels for stereo sound. For educational
%purpose we will only consider the first channel:

sound_data = source_data(:,1);

% resample data for rz6 sample rate
rz6_freq = round(rz6.read_samplerate); % this will be 48828 Hz
soundData_48kHz = resample(sound_data, rz6_freq, source_freq);

tl = biox_rz6_tasklist;     

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'Att',25,25);
tl.add_task(0.000,'DAQ', 'Start', 11, 10);
tl.add_task(0.000,'SoundAB', 'Tone', 500);
tl.add_task(0.200,'setDIO', bin2dec('0000 0001'));
tl.add_task(0.400,'setDIO', bin2dec('0000 0010'));
tl.add_task(0.600,'setDIO', bin2dec('0000 0011'));
tl.add_task(0.800,'setDIO', bin2dec('0000 0100'));
tl.add_task(1.000,'setDIO', bin2dec('0000 0101'));
tl.add_task(1.200,'setDIO', bin2dec('0000 0110'));
tl.add_task(1.400,'setDIO', bin2dec('0000 0111'));
tl.add_task(1.400,'WaitForTrigger', 'External', bin2dec('0001 0000'));

tl.add_task(0.000,'SoundAB', 'Stop');
tl.add_task(0.100,'setDIO', bin2dec('0000 0000'));
tl.add_task(0.200,'DAQ', 'Stop', 11);
tl.add_task(0.300,'Ready');
%********************************************************************

%write tasklist to RZ6
rz6.write_tasklist(tl);

while ~rz6.read_trialready()
    %do nothing
end

data1 = 256-rz6.read_ACQ11('A');  %data1 = array of bytes (digital inputs)
data2 = rz6.read_ACQ11('B');      %data2 = array of bytes (digital outputs)
data3 = rz6.read_ACQ11('STM');    %data4 = array of bits

x = 1:length(data1);

plot(x, data1, x, data2, x, data3);    
    
delete(tl);
delete(rz6);
