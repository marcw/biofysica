%BIOX course exercise 5: using 'WaitForTrigger' and relative timing
clear;
clc;

%specify the external input to use for the WaitForTrigger task
Input_A4 = bin2dec('0001 0000'); % '0001 0000' is input A4 because it is counted from the right.

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

%tasklist is often abbriviated as tl ('tee-el'), not as t1 ('tee-one')
tl = biox_rz6_tasklist();

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'Att',25,25);
tl.add_task(0.000,'WaitForTrigger', 'External', Input_A4); 

tl.add_task(0.000,'SoundA','Tone', 500, 5, 20);
tl.add_task(1.000,'SoundA','Stop');
tl.add_task(1.000,'WaitForTrigger', 'External', Input_A4); 

tl.add_task(0.000,'SoundB','Noise', 500, 1000); % note that after 'WaitForTrigger' the timing is with respect to the moment that the trigger is received. 
tl.add_task(1.000,'SoundB','Stop');
tl.add_task(0.000,'WaitForTrigger', 'External', Input_A4); 

tl.add_task(0.000,'SoundAB','Sweep', 250, 5, 0.2);
tl.add_task(1.000,'SoundAB','Stop');
tl.add_task(1.000,'WaitForTrigger', 'External', Input_A4); 
%********************************************************************

rz6.write_tasklist(tl);

delete(tl);
delete(rz6);