%BIOX course exercise 11 holdinput and responsetime 
clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

tl = biox_rz6_tasklist;

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'Att',25,25);
tl.add_task(0.000,'SoundAB','Tone', 500);
tl.add_task(0.000,'HoldInput', bin2dec('0001 0000'), 'Falling', 200);
tl.add_task(0.000,'WaitForTrigger', 'External', bin2dec('0001 0000'));

tl.add_task(0.000,'SoundA','Stop');
tl.add_task(0.000,'Ready');
%********************************************************************

rz6.write_tasklist(tl);

%wait for the tasklist to be ready
while rz6.read_trialready() == 0
  % do nothing  
end

inputbyte = dec2bin(rz6.read_inputholdbyte()); %read input byte from RZ6
responsetime = rz6.read_responsetime() %read response time from RZ6

delete(tl);
delete(rz6);

