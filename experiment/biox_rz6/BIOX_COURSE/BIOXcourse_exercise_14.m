%BIOX course exercise 14 Multplexers

%*******************************************************************
% N.B. This example does not work on a single core RZ6 !!!!
%*******************************************************************

clear;
clc;

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

tl = biox_rz6_tasklist;     

%********************************************************************
%                         Tasklist
%********************************************************************
tl.add_task(0.000,'WaitForTrigger', 'External', bin2dec('1111 1111'));
tl.add_task(0.000, 'MUX', 0, 'Set', 0);
tl.add_task(1.000, 'MUX', 0, 'Reset');
tl.add_task(1.400,'WaitForTrigger', 'External', bin2dec('1111 1111'));
tl.add_task(0.000, 'MUX', 0, 'Set', 0);
tl.add_task(0.500, 'MUX', 0, 'Set', 1);
tl.add_task(1.000, 'MUX', 0, 'Set', 2);
tl.add_task(1.500, 'MUX', 0, 'Set', 3);
tl.add_task(2.000, 'MUX', 0, 'Set', 4);
tl.add_task(2.500, 'MUX', 0, 'Set', 5);
tl.add_task(3.000, 'MUX', 0, 'Set', 6);
tl.add_task(3.500, 'MUX', 0, 'Set', 7);
tl.add_task(4.000, 'MUX', 0, 'Reset');
%********************************************************************

%write tasklist to RZ6
rz6.write_tasklist(tl);
    
delete(tl);
delete(rz6);
