%BIOX course exercise 3: Playing a sound

clear;
clc;

%create an rz6 object

rz6 = biox_tst;
%rz6 = biox_rz6_1c(1); %for single core RZ6
%rz6 = biox_rz6_3c(1); %for three core RZ6
%rz6 = biox_rz6_4c(1); %for four  core RZ6

disp(rz6.BIOXversion);
disp(rz6.RCXversion);

%create a tasklist object
myTasklist = biox_rz6_tasklist;

%add tasks to the tasklist

%********************************************************************
%                          Tasklist
%********************************************************************
myTasklist.add_task(0.000,'Att',25,25); %channel A&B are 20 dB attenuated
myTasklist.add_task(0.000,'SoundA','Tone', 500); %500Hz
myTasklist.add_task(2.000,'SoundA','Stop'); %stop the sound

%********************************************************************

%upload the tasklist to the rz6
rz6.write_tasklist(myTasklist); % put here the breakpoint

%destroy tasklist
delete(myTasklist);

%delete the rz6 object
delete(rz6);
