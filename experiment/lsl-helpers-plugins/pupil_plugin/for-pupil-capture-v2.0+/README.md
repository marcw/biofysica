# Pupil LSL Relay Plugin

## Description

These are the LSL plugins for sending data from the Pupil Capture program, versions 2.0 and up.
They are from https://github.com/labstreaminglayer/App-PupilLabs/ and are modified by Günter Windau by changing outlet name and type to be more meaningful in a network hosting multiple LSL outlets.

## Installation
To use the LSL Relay plugin in Pupil Capture, copy the complete contents of this directory to the Pupil Capture plugins directory. Typically this is $HOME/pupil_capture_settings/plugins on Linux/MacOS 

## Alternative method
There is a fork from this software that can be run as a separate process. 
See: https://pupil-labs-lsl-relay.readthedocs.io/en/stable/
