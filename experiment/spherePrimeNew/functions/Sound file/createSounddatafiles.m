function createSounddatafiles
    for i = 1:300
        f = 200+3*i;
        sf = 48828;
        t = (1:sf)/sf;        
        data = 0.03*sin(2*pi*f*t); 
        fn = ['C:\DATA\RL\SND\snd' num2str(i,'%03d')];
        save(fn, "data");
    end
end