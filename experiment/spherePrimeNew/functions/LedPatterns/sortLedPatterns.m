% SORTLEDPATTERNS Sorts and combines LED patterns based on event IDs and timings.
%
%   result = SORTLEDPATTERNS(nrOfBits, eventIDs, timings, eventType, patterns) processes
%   LED pattern data by sorting the events, identifying unique combinations of event IDs,
%   timings, and event types, and then combining the associated patterns. It assumes that
%   all input vectors are column vectors.
%
%   Inputs:
%       nrOfBits  - Number of bits representing each pattern.
%       eventIDs  - Column vector of event identifiers.
%       timings   - Column vector of timing information corresponding to events.
%       eventType - Column vector specifying the type of each event.
%       patterns  - Column vector of patterns (numeric representations of bit patterns).
%
%   Outputs:
%       result    - A table containing unique events with their combined patterns.
%                   The table includes:
%                       - eventIDs
%                       - timings
%                       - eventType
%                       - combinedPatterns
%
%   Example:
%       % Combine and sort LED patterns based on event data
%       resultTable = sortLedPatterns(8, eventIDs, timings, eventTypes, patterns);
%
%   Notes:
%       - The function sorts the input data by 'eventIDs', 'timings', and 'eventType',
%         in ascending, ascending, and descending order respectively.
%       - It identifies unique events based on 'eventIDs', 'timings', and 'eventType'.
%       - Patterns associated with the same unique event are combined using the 'sumPatterns' function.
%       - The 'sumPatterns' function must be defined separately and should handle bit-wise
%         summation of patterns considering 'nrOfBits'.
%       - The function currently contains a '!!todo' comment indicating incomplete implementation.
%
%   Dependencies:
%       - sumPatterns: Custom function to sum patterns considering the number of bits.
%
%   Author: Ruurd Lof
%   Date:   [Your Date Here]
%
%   See also: table, sortrows, unique, array2table

function result = sortLedPatterns(nrOfBits, eventIDs, timings, eventType, patterns)
    % assume column vectors    
    unsortedTable = table(eventIDs, timings, eventType, patterns);
    sortedTable   = sortrows(unsortedTable, {'eventIDs', 'timings', 'eventType'}, {'ascend', 'ascend', 'descend'});
    [uniqueEvents, ~, ~] = unique(sortedTable(:, 1:3), 'rows');
    [nUnique, ~] = size(uniqueEvents);
    combinedPatterns = zeros(nUnique, 1);
    for i = 1:nUnique       
       uniqueEvent = uniqueEvents(i, :);
       patternsToSum  = (sortedTable.eventIDs == uniqueEvent.eventIDs) & ...
                        (sortedTable.timings == uniqueEvent.timings) & ...
                        (sortedTable.eventType == uniqueEvent.eventType);
       combinedPatterns(i) = sumPatterns(sortedTable.patterns(patternsToSum), nrOfBits);
    end           
    sumTable = [uniqueEvents array2table(combinedPatterns, 'VariableNames', {'combinedPatterns'})];
    !!todo
    result = sumTable;
end

function result = sumPatterns(patterns, nrOfBits)  
    switch nrOfBits
        case {8, 16, 32}
            result = sum(patterns);
        case 128
            result = sumPatterns128(patterns);
        otherwise
            error('length of patterns must be 16 or 128 bits');
    end

end


