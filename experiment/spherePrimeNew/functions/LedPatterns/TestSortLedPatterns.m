clear; clc;
nrOfBits = 16;
eventIDs = randi([1, 2], 1, nrOfBits)';
timings =  randi([1, 2], 1, nrOfBits)';
eventType = randi([0,1], 1, nrOfBits)'; % 1 = on, 0 = off
patterns = 2.^randi([0, nrOfBits-1], 1, nrOfBits)';


sortedPatterns = sortLedPatterns(nrOfBits, eventIDs, timings, eventType, patterns);
