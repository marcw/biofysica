% GETHEADVELOCITY2 Computes distance and velocity traces from azimuth and elevation data.
%
%   [distanceTrace, velocityTrace] = GETHEADVELOCITY2(filter, rawTraceData_DP, sampleFrequency)
%   processes azimuth and elevation data from head movement traces, applies zero-phase filtering,
%   and computes the travel distance and velocity traces.
%
%   Inputs:
%       filter            - Digital filter object used to filter the azimuth and elevation data.
%       rawTraceData_DP   - Structure containing raw trace data in double polar coordinates with fields:
%                           - azimuth:   Azimuth angle data in degrees or radians.
%                           - elevation: Elevation angle data in degrees or radians.
%       sampleFrequency   - Sampling frequency of the data in Hertz (Hz).
%
%   Outputs:
%       distanceTrace     - Vector containing the cumulative distance traveled, computed from the filtered
%                           azimuth and elevation data.
%       velocityTrace     - Vector containing the velocity of movement, calculated as the gradient of the
%                           distance trace over time.
%
%   Example:
%       % Compute the distance and velocity traces from raw head movement data
%       [distanceTrace, velocityTrace] = getHeadVelocity2(myFilter, rawData, 1000);
%
%   Notes:
%       - The function uses zero-phase filtering via 'filtfilt' to prevent phase distortion.
%       - The 'hypot' function calculates the Euclidean norm of the azimuth and elevation data.
%       - The 'gradient' function computes the numerical gradient of the distance trace.
%
%   Dependencies:
%       - Signal Processing Toolbox (for 'filtfilt' function).
%
%   Author: Ruurd Lof
%   Date:   [Your Date Here]
%
%   See also: filtfilt, hypot, gradient


function [distanceTrace, velocityTrace] = getHeadVelocity2(rawTraceData_DP, sampleFrequency) 
    filter = load("HeadVelocityDigitalLowPassFilter.mat");            
    filtAzimuth   = filtfilt(filter.digitalLowPassFilter, rawTraceData_DP.azimuth);
    filtElevation = filtfilt(filter.digitalLowPassFilter, rawTraceData_DP.elevation);

    distanceTrace = hypot(filtAzimuth, filtElevation);    
    velocityTrace = gradient(distanceTrace, 1/sampleFrequency); 
end
