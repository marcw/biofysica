classdef systems
    properties (Constant)
        TEST   = 'test';
        SPHERE = 'sphere';
        RZ6    = 'rz6';
    end
end