% methods:
% run    runs state machine
% abort  aborts state machine
% start  starts experiment
% reset  resets experiment

classdef (Abstract) experimentStateMachine < handle  

    properties (Access = public)
        userInterface
        %------------------------------
        STATUS_START_UP            =  0
        STATUS_INFO_NOT_COMPLETE   =  1
        STATUS_WAITING_FOR_START   =  2
        STATUS_WAITING_FOR_TRIGGER =  3
        STATUS_WAITING_FOR_READY   =  4        
        STATUS_EXP_IS_FINISHED     =  5
        STATUS_IS_PAUSED           =  6 
        STATUS_NEED_RECONNECT      =  7       
    end



    properties (Access = protected)        
        status     = 0;
        isAborted  = false;
        isPaused   = false;   
        GUI_UPDATE_INTERVAL_SECONDS = 0.03;
        statusBeforePause
        statusBeforeReconnect
    end

    methods (Abstract)
        hasNextTrial(obj)        
        onUpdateTimer(obj)
        onStart(obj)
        onTrialReady(obj)
        onNextTrial(obj)        
        onFinished(obj)
        onReset(obj)
        onExitStateMachineLoop(obj)
        onStatusChanged(obj)
        onPlayAfterReconnect(obj);
        isTriggered(obj)
        isTrialReady(obj)
        guiInfoIsComplete(obj)
    end

    methods

        function obj = experimentStateMachine
            obj.status = obj.STATUS_START_UP;
        end       

        function run(obj, app)
            obj.status = obj.STATUS_INFO_NOT_COMPLETE;            
            tic;
            %state machine loop
            while isvalid(app) && ~obj.isAborted
                obj.checkGuiUpdateTimer;
                switch obj.status
                    case obj.STATUS_INFO_NOT_COMPLETE
                        obj.checkStatusGuiInfo
                    case obj.STATUS_WAITING_FOR_START
                        obj.checkStatusGuiInfo
                    case obj.STATUS_WAITING_FOR_TRIGGER                        
                        obj.checkIsTriggered;                        
                    case obj.STATUS_WAITING_FOR_READY
                        obj.checkIsTrialReady;                       
                    case obj.STATUS_EXP_IS_FINISHED
                        % wait for GUI response
                    case obj.STATUS_IS_PAUSED 
                        % wait for GUI response
                    case obj.STATUS_NEED_RECONNECT
                        % wait for GUI response
                end % switch
            end % while loop
            obj.onExitStateMachineLoop;
        end % run        

        function start(obj)
            if obj.status == obj.STATUS_WAITING_FOR_START
                obj.status = obj.STATUS_WAITING_FOR_TRIGGER;            
                obj.onStart; % abstract                
            end
        end

        function abort(obj)
            obj.isAborted = true;                      
        end      

        function pause(obj)
            if obj.status == obj.STATUS_IS_PAUSED
                return
            end
            obj.statusBeforePause = obj.status;
            obj.status = obj.STATUS_IS_PAUSED;
        end

        function endPause(obj)
            if obj.status ~= obj.STATUS_IS_PAUSED
                return
            end
            if obj.statusBeforePause == obj.STATUS_IS_PAUSED
                obj.reset;
                return
            end
            obj.status = obj.statusBeforePause;  
        end

        function togglePause(obj) 
            if obj.status ~= obj.STATUS_IS_PAUSED
                obj.pause;
            else
                obj.endPause;
            end
        end       

        function reset(obj)
            if ismember(obj.status, [obj.STATUS_WAITING_FOR_TRIGGER, ...
                                     obj.STATUS_WAITING_FOR_READY, ...
                                     obj.STATUS_EXP_IS_FINISHED...
                                     obj.STATUS_IS_PAUSED])
                obj.onReset; % abstract
                obj.status = obj.STATUS_WAITING_FOR_START; 
            end
        end

        function setNeedReconnect(obj)
            if obj.status ~= obj.STATUS_NEED_RECONNECT
                obj.statusBeforeReconnect = obj.status;                
            end
            obj.status = obj.STATUS_NEED_RECONNECT; 
        end

        function endNeedReconnect(obj)
            if obj.status ~= obj.STATUS_NEED_RECONNECT
                return;
            end
            if ismember(obj.statusBeforeReconnect, [obj.STATUS_WAITING_FOR_TRIGGER, ...
                                                    obj.STATUS_WAITING_FOR_READY] )
                
                obj.onPlayAfterReconnect;
                obj.status = obj.STATUS_WAITING_FOR_TRIGGER;
            else
                obj.status = obj.statusBeforeReconnect;
            end
        end         

%#######################################################################        

        function set.status(obj, value)
             if value ~= obj.status
                 obj.status = value;
                 obj.onStatusChanged;               
             end
        end

    end %public methods

%#######################################################################            

    methods (Access = private)      

        function checkIsTriggered(obj)
            if obj.isTriggered                
                obj.status = obj.STATUS_WAITING_FOR_READY;
            end            
        end

        function nextTrial(obj)
             if obj.hasNextTrial          
               obj.status = obj.STATUS_WAITING_FOR_TRIGGER;                  
               obj.onNextTrial; % abstract               
            else   
               obj.onFinished; % abstract
               obj.status = obj.STATUS_EXP_IS_FINISHED;
            end            
        end

        function checkIsTrialReady(obj)
            if obj.isTrialReady %abstract 
                obj.onTrialReady; %abstract 
                obj.nextTrial;
            end            
        end

        function checkGuiUpdateTimer(obj)
            if (toc > obj.GUI_UPDATE_INTERVAL_SECONDS)
                obj.onUpdateTimer;
                tic;
            end % if            
        end 

        function checkStatusGuiInfo(obj)            
            if obj.guiInfoIsComplete
                obj.status = obj.STATUS_WAITING_FOR_START;
            else
                obj.status = obj.STATUS_INFO_NOT_COMPLETE;
            end
        end       

    end % private methods
end