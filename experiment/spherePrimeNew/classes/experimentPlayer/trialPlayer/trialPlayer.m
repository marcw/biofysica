classdef trialPlayer < handle 

    events        
        acqDataAvailableEvent      
    end    

    properties (Access = private)
        system
        recordings
        stimList
    end

    methods 
        function obj = trialPlayer(system, recordings) 
            obj.system           = system;
            obj.recordings       = recordings;
        end
      
        function run(obj)
            obj.system.start;           
        end

        function stop(obj)
            obj.system.stop;           
        end

        function initialize(obj, stimList)
            obj.stimList = stimList;            
            obj.system.reset;            
            obj.system.prepare(stimList);             
        end         

        function finalize(obj)
            obj.system.acqSystem.downloadFieldData_HVF;
            rawFieldData_HVF = obj.system.acqSystem.fieldData_HVF;            
            rawTraceData_DP  = obj.system.acqSystem.traceData_DP;            
            eventDataTable   = obj.system.acqSystem.eventDataTable;
            triggerDataTable = obj.system.acqSystem.triggerDataTable;
            %---------------------------------------------------- 
            obj.recordings.stimlist             = obj.stimList;                
            obj.recordings.evtInfo              = obj.system.eventrecorder.dataTable;
            obj.recordings.acqData.traceTable   = rawTraceData_DP.asTable;
            obj.recordings.acqData.rawFieldData = rawFieldData_HVF.asTable;
            obj.recordings.acqData.eventTable   = eventDataTable;
            obj.recordings.acqData.triggerTable = triggerDataTable;
            obj.recordings.save;            
            %----------------------------------------------------            
            stimulusList = obj.system.soundSystem.stimulusList;
            if ~isempty(stimulusList)
                stim = stimulusList(1);
                obj.sendDataToGui(rawTraceData_DP, stim);
            end
        end        

    end

    methods (Access = private)
        function sendDataToGui(obj, rawTraceData_DP, stim)          
            %stimulus
            stimulusPos_DP = obj.system.soundSystem.locator.desired2realLocation_DP(stim.pos_DP);

            %response is maximum movement towards target 
            azimuth   = max(rawTraceData_DP.azimuth);
            elevation = max(rawTraceData_DP.elevation);
            radius = 1;
            hemisphere = 1;
            responsePos_DP = coordinates_DP(azimuth, elevation, radius, hemisphere);             

            % calculate acquisition times
            sampleFrequency = obj.system.acqSystem.sampleFrequency;
            nrOfSamples = length(rawTraceData_DP.azimuth);
            sampleIndexes = (1:nrOfSamples)';
            time = (sampleIndexes ./ sampleFrequency);
             
            %calculate distanceTrace and velocityTrace with lowPassFilter
            filter = obj.system.configuration.filters.digitalLowPassFilter;            
            [distanceTrace, velocityTrace] = getHeadVelocity2(filter, rawTraceData_DP, sampleFrequency);
            
            %create eventData
            eventData = acquisitionEventData(time, distanceTrace, velocityTrace, stimulusPos_DP, responsePos_DP);            
            %notify mainGui
            obj.notify('acqDataAvailableEvent', eventData);
        end
    end
    
end %classdef