classdef experimentPlayer < experimentStateMachine

    events
        expFinishedEvent
        expStatusChangedEvent
        infoStatusChangedEvent
        expInitialsChangedEvent
        indexChangedEvent
        hardwareStatusEvent                   
    end
    
    properties
        trial
        system  
        guiInfo
        autoTrialStart
        trialIndex     = 0
        startingIndex  = 1
        expInitials    = ''
        expFilename    = ''
        cfgFilename    = ''
        %------------------------------
        GUI_INFO_INITIALS_OK    =  1
        GUI_INFO_EXP_FILE_OK    =  2
        GUI_INFO_CFG_FILE_OK    =  3
        GUI_INFO_ALL_OK         = [1 2 3]
    end  

    properties (Access = private)
        environment        
        configuration
        recordings        
        trialList  
        trialListInfo
        expFileIsOK = false;
        cfgFileIsOK = false; 
        infoStatus
    end

    methods
        function obj = experimentPlayer(system, environment, configuration, recordings)
            obj@experimentStateMachine;
            obj.system        = system;
            obj.environment   = environment;
            obj.configuration = configuration;
            obj.recordings    = recordings;
            obj.trial         = trialPlayer(system, recordings);            
        end   

        function set.startingIndex(obj, value)
            if obj.startingIndex ~= value
                if obj.checkStartingIndex(value)                    
                    obj.startingIndex = value;                    
                else
                    obj.startingIndex = 1;
                    warning('starting index out of bounds');
                end
                obj.notifyTrialIndexChanged;
            end
        end

        function set.expFilename(obj, value)            
            if ~strcmp(obj.expFilename, value)
                obj.resetTrialList;
                obj.expFilename = fullfile(obj.getExpDir, value);   
                if exist(obj.expFilename, "file")                                
                    obj.checkExpFileIsOK;                    
                else
                    obj.infoStatus = setdiff(obj.infoStatus, [obj.GUI_INFO_EXP_FILE_OK]);                                                    %#ok<MCSUP>                     
                end
                obj.notifyTrialIndexChanged;
            end            
        end

        function set.cfgFilename(obj, value)
            if ~strcmp(obj.cfgFilename, value)                
                obj.cfgFilename = fullfile(obj.getCfgDir, value);
                if exist(obj.cfgFilename, "file")                                                   
                   obj.checkCfgFileIsOK;
                else
                   obj.infoStatus = setdiff(obj.infoStatus, [obj.GUI_INFO_CFG_FILE_OK]);                                                    %#ok<MCSUP> 
                end
            end
        end        

        function set.expInitials(obj, value)
            if ~strcmp(obj.expInitials, value) 
                obj.expInitials = value;
                OK = obj.checkInitialsAreOK;                
                obj.notifyInitialsChanged(OK);
            end
        end

        function set.trialIndex(obj, value)
            if obj.trialIndex ~= value
                obj.trialIndex = value;
                obj.onSetTrialIndex(value);
            end
        end          

        function set.trialList(obj, value)
            obj.trialList = value;
            obj.trialIndex = 0; %#ok<MCSUP> 
        end

        function set.infoStatus(obj, value)
            obj.infoStatus = value;
            obj.onInfoStatusChanged;               
        end        

        function result = get.autoTrialStart(obj)
            result = obj.configuration.autoTrialStart;
        end

        function reconnectHardware(obj)
            try
               obj.system.reconnectHardware;                              
            catch
               return;
            end               
            obj.endNeedReconnect;
        end

        % implementation of abstract method from parent class        
        function onSetTrialIndex(obj, trialIndex)  
            obj.recordings.trialIndex = trialIndex; 
            obj.notifyTrialIndexChanged;
        end
        

        % implementation of abstract method from parent class        
        function result = getnTrials(obj)
            result = length(obj.trialList);
        end

        % implementation of abstract method from parent class        
        function result = isCfgFilenameOK(obj)
            result = exist(obj.cfgFilename, "file");                           
        end


        % implementation of abstract method from parent class        
        function result = hasNextTrial(obj)
            result = obj.trialIndex < obj.getnTrials;
        end 

        % implementation of abstract method from parent class        
        function result = isTriggered(obj)            
            result = obj.system.isTriggered;                                
        end

        % implementation of abstract method from parent class        
        function result = isTrialReady(obj)
            result = obj.system.isReady;            
            if result
                obj.trial.finalize;                              
            end               
        end

        % implementation of abstract method from parent class        
        function onStart(obj)
            obj.prepareTrials;                  
            obj.trialIndex = obj.startingIndex;
            obj.playTrial;            
        end        

        % implementation of abstract method from parent class        
        function onReset(obj)
            obj.system.reset;
            obj.trialIndex = 0;            
        end
        
        % implementation of abstract method from parent class        
        function onExitStateMachineLoop(obj)
            obj.system.reset;
        end

        % implementation of abstract method from parent class        
        function onTrialReady(obj)
            obj.trial.stop;
        end

        % implementation of abstract methods from parentclass        
        function onFinished(obj)            
            % to do: play some tune or light show            
            obj.system.reset;
        end

        % implementation of abstract methods from parentclass                
        function onUpdateTimer(obj)
            if obj.system.needReconnect
                 obj.setNeedReconnect;                
            end
            obj.notifyHardwareStatus;            
        end

        % implementation of abstract method from parent class        
        function onStatusChanged(obj)
            obj.notifyExpStatusChanged;
        end

        % implementation of abstract method from parent class        
        function onInfoStatusChanged(obj)
            obj.notifyInfoStatusChanged;
        end      

        % implementation of abstract methods from parentclass
        function onNextTrial(obj)
            obj.trialIndex = obj.trialIndex + 1;
            obj.playTrial;
        end

        function onPlayAfterReconnect(obj)
            obj.playTrial;
        end

        % implementation of abstract methods from parentclass
        function result = guiInfoIsComplete(obj)
            result = isequal(obj.infoStatus,  obj.GUI_INFO_ALL_OK);
        end

    end          
    
    methods (Access = private)             
                
        %  notifycation functions

        function resetTrialList(obj)
            obj.trialList = [];
            obj.trialListInfo = [];
            obj.trialIndex = 0;            
        end

        function notifyInitialsChanged(obj, initialsAreOK)
            event = expInitialsChangedEventData(obj.expInitials, initialsAreOK);
            obj.notify('expInitialsChangedEvent', event); 
        end        

        function notifyExpStatusChanged(obj)
            statusEventData = expStatusChangedEventData(obj.status);             
            notify(obj,'expStatusChangedEvent', statusEventData);             
        end

        function notifyInfoStatusChanged(obj)        
            statusEventData = infoStatusChangedEventData(obj.infoStatus);             
            notify(obj,'infoStatusChangedEvent', statusEventData);             
        end            

        function notifyTrialIndexChanged(obj)
            eventData = indexChangedEventData(obj.trialIndex, obj.getnTrials, obj.startingIndex);                
            notify(obj, 'indexChangedEvent', eventData); %notifies maingui                                
        end        

        function notifyExpIsFinished(obj)
            notify(obj, 'expFinishedEvent'); %notifies maingui  
        end

        function notifyHardwareStatus(obj)
            hardwareStatus = obj.system.getHardwareStatus;
            hardwareType   = obj.system.getHardwareType;                
            eventData = hardwareStatusEventData(hardwareType, hardwareStatus);              
            notify(obj, 'hardwareStatusEvent', eventData);             
        end
                
 %---------------------------------------------------------------------------       

        function playTrial(obj)             
            stimList = obj.trialList(obj.trialIndex).stim;
            obj.trial.initialize(stimList);          
            obj.trial.run;
        end 

        function prepareTrials(obj)                                     
            %info collection for output file
            obj.recordings.expInfo      = obj.trialListInfo;
            obj.recordings.cfgInfo      = obj.configuration.info;
            obj.recordings.envInfo      = obj.environment.info;
            obj.recordings.sysInfo      = obj.system.info;            
            obj.recordings.guiInfo      = obj.guiInfo;             
        end

        function result = checkStartingIndex(obj, startingIndex)
            result = startingIndex >= 1 && startingIndex <= length(obj.trialList);
        end

        function [trialList, config] = readExpFromFile(obj)            
           [dir, fn, ext] = fileparts(obj.expFilename);
           [trialList, config] = new_readexpfile(dir, [fn ext]);           
        end

        function readCfgFromFile(obj)
            obj.configuration.readFromFile(obj.cfgFilename);
        end

        function result = checkInitialsAreOK(obj)
            datadir = [obj.environment.dataDirectory filesep obj.expInitials];
            datadirExists = exist(datadir, 'dir') == 7;
            if datadirExists                
                obj.infoStatus =union(obj.infoStatus, [obj.GUI_INFO_INITIALS_OK]);
                obj.environment.setPathBaseDir(obj.expInitials);
            else
                obj.infoStatus = setdiff(obj.infoStatus, [obj.GUI_INFO_INITIALS_OK]);
            end
            result = datadirExists;
        end
        

        function checkExpFileIsOK(obj)
            try
                [obj.trialList, obj.trialListInfo] = obj.readExpFromFile;                
                obj.infoStatus = union(obj.infoStatus, [obj.GUI_INFO_EXP_FILE_OK]);             
            catch  
                obj.infoStatus = setdiff(obj.infoStatus, [obj.GUI_INFO_EXP_FILE_OK]);                
            end               
        end

        function checkCfgFileIsOK(obj)
            try
                obj.readCfgFromFile;
                obj.infoStatus = union(obj.infoStatus, [obj.GUI_INFO_CFG_FILE_OK]);                
            catch
                obj.infoStatus = setdiff(obj.infoStatus, [obj.GUI_INFO_CFG_FILE_OK]);                                
            end
        end            

        function result = stimList(obj, index)            
            list = num2cell(obj.trialList(index).stim);
            result = list;
        end

        function result = getExpDir(obj)
            result = obj.environment.paths.expdir;
        end

        function result = getCfgDir(obj)
            result = obj.environment.paths.cfgdir;
        end       


    end %private methods        
    

end