classdef acquisitionSystem < systemPart

    properties (Access = public)                      
        fieldData_HVF
        traceData_DP
        triggerDataTable
        eventDataTable   
        sampleFrequency
    end

    properties (Access = private)                      
        fieldChannelIDs  = [1 2 3]
        triggerChannelID = 4
        calibrationFile = "defaultsphere.net"         
        acqDuration
    end    
    

    methods
        function obj = acquisitionSystem(system)            
            obj@systemPart(system);            
            obj.hardware = acquisitionHardware(system.hardware);
            obj.sampleFrequency = obj.hardware.SAMPLEFREQUENCY;
        end

        function prepare(obj, acqDuration)
            if ~obj.isActive
                return
            end            
            
            obj.acqDuration = acqDuration;
            obj.hardware.prepare(acqDuration);   
        end

        function result = newStimulus(obj, stim) %#ok<INUSL> 
            result = acquisitionStimulusClass(stim);            
        end        


        function onAddStimulus(obj, stim)
            index = length(obj.stimulusList);
            obj.system.triggerSystem.addAcqTrigger(stim, index);            
        end

        function startAcq(obj) %#ok<MANU> 
            % happens by acqTrigger in RCX file
        end

        function stopAcq(obj) %#ok<MANU> 
            % happens automatically in RCX file
        end

        function result = isReady(obj)
            result = false;
            if obj.hardware.isReady
                result = true;
            end
        end

        function downloadFieldData_HVF(obj) 
            nrOfSamples = obj.hardware.getAcqNrOfSamples;             
            for channel = obj.fieldChannelIDs                 
                Data{channel} = obj.hardware.getAcqData(channel, nrOfSamples);                 %#ok<AGROW> 
            end
            frontal    = Data{1};
            vertical   = Data{2};            
            horizontal = Data{3}; 
            obj.fieldData_HVF = coordinates_HVF(frontal, vertical, horizontal);
        end

        function result = get.fieldData_HVF(obj)
            result = obj.fieldData_HVF;
        end

        function result = get.traceData_DP(obj)             
            netCalibrationFile = load(obj.calibrationFile,'-mat');                        
            result = convert_HVFfieldValues2DP_withNetCalibration(obj.fieldData_HVF, netCalibrationFile);
        end

        function result = get.triggerDataTable(obj)
            nrOfSamples      = obj.hardware.getAcqNrOfSamples;
            time             = ((1:nrOfSamples)./obj.sampleFrequency)'; %time in seconds
            triggers         = obj.hardware.getAcqData(obj.triggerChannelID, nrOfSamples);      
            result = table(time, triggers);
            result.Properties.VariableUnits = {'sec', 'bin'};
            result.Properties.UserData.XLim = [0, obj.acqDuration]; %seconds
            result.Properties.UserData.YLim = [0, 1]; %units            
        end

        function result = get.eventDataTable(obj)            
            nrOfSamples       = obj.hardware.getEventDataNrOfSamples;
            time              = ((1:nrOfSamples)./obj.sampleFrequency)'; %time in seconds
            eventData         = obj.hardware.getEventData(nrOfSamples);
            result    = table(time, eventData);
            result.Properties.VariableUnits = {'sec', 'bin'};
            result.Properties.UserData.XLim = [0, ceil(max(time))]; %seconds
            result.Properties.UserData.YLim = [-120, 120]; %degrees
        end        

        function result =  fieldData2DP(obj, fieldData_HVF)
            cal  = load(obj.calibrationFile,'-mat');                      

            % calculate azimuth and elevation from field data with
            % calibration file                     
            result = transform_HVF2DP_withNetCalibration(fieldData_HVF, cal);                                 
        end
    end     
    
end