classdef triggerSystem < systemPart

% Trigger index numbers in exp file:     
%   zBusA trigger = reset    
%   zBusB trigger = 0
%   ext trigger   = 1..n  // normally the first button press has index 1


    properties (Access = private)       
        ledTriggerList{}
        soundTriggerList{}
        waitTriggerList{}
        acqTriggerList{}         
        autoTrigger
    end

    methods
        function obj = triggerSystem(system)
            obj@systemPart(system); 
            obj.hardware = triggerHardware(system.hardware);
        end

        function result = newStimulus(obj, stim) %#ok<INUSL> 
            result = triggerStimulusClass(stim); 
        end

        function onAddStimulus(obj, stim) %#ok<INUSD> 
            %do nothing
        end

        function prepare(obj, autoTrigger)
            obj.autoTrigger = autoTrigger;
            obj.prepareTriggerList(obj.ledTriggerList);
            obj.prepareTriggerList(obj.soundTriggerList);
            obj.prepareTriggerList(obj.waitTriggerList);
            obj.prepareTriggerList(obj.acqTriggerList);            
        end

        function start(obj)
            obj.hardware.resetTrigger;
            if obj.autoTrigger
                obj.hardware.eventTrigger;
            end
        end

        function addStimTrigger(obj, stim, index) %#ok<INUSD> 
            % sent trigger somewhere: not available in sphere_RA16.rcx
        end        

        function addWaitTrigger(obj, stim, index)
            if index > 1 
               error('to many wait triggers');
            end             
            obj.waitTriggerList{index} = waitTrigger(obj.hardware.RA16activexServer, stim.onevent, stim.ondelay);            
        end

        function addLedTrigger(obj, stim, index)
            if index > 8 
                error('to many LED triggers');
            end
            obj.ledTriggerList{index}  = ledTrigger(obj.hardware.RA16activexServer, index, stim.event , stim.delay); 
        end

        function addAcqTrigger(obj, stim, index)
            if index > 1 
               error('to many acquisition triggers');
            end            
            obj.acqTriggerList{index} = acqTrigger(obj.hardware.RA16activexServer, stim.onevent, stim.ondelay);
        end

        function addSoundTrigger(obj, stim, index) 
            if index > 2
                error('to many sound triggers');
            end                        
            obj.soundTriggerList{1} = soundTrigger(obj.hardware.RA16activexServer, 1, stim.onevent , stim.ondelay);            
            obj.soundTriggerList{2} = soundTrigger(obj.hardware.RA16activexServer, 2, stim.onevent , stim.ondelay);            
        end  

        function clear(obj)            
            clear@systemPart(obj);
            obj.ledTriggerList = obj.clearTriggerList(obj.ledTriggerList);
            obj.soundTriggerList = obj.clearTriggerList(obj.soundTriggerList);
            obj.waitTriggerList = obj.clearTriggerList(obj.waitTriggerList);
            obj.acqTriggerList = obj.clearTriggerList(obj.acqTriggerList);
        end

        function sentEventTrigger(obj)
            obj.hardware.eventTrigger;
        end

        function result = waitIsReady(obj)
            result = obj.hardware.waitIsReady;
        end 

        function result = isTriggered(obj, eventCount)
            result = obj.hardware.isTriggered(eventCount);
        end 

    end

    methods (Access = private)

        function result = clearTriggerList(obj, triggerList) %#ok<INUSL> 
            n = length(triggerList);
            for index = n:-1:1
                delete(triggerList{index}); %delete all objects in the list
            end
            result = {};            
        end

        function resetEventCount(obj)
            obj.hardware.resetTrigger;
        end

        function result = buttonIsPressed(obj)
            result = obj.hardware.isReady;
        end

        function prepareTriggerList(obj, list) %#ok<INUSL> 
            if isempty(list)
                return
            end
            for i = 1: length(list)
                list{i}.prepare;
            end
        end     

    end        

    
end