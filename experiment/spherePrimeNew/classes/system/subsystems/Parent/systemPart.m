classdef systemPart < handle
    properties          
        hardware        
        stimulusList = []
    end
   
    methods (Abstract)
        prepare(obj)
        onAddStimulus(obj, stim)
    end

    properties (Access = protected)
        system
    end

    methods
        function obj = systemPart(system)
            obj.system = system;
        end

        function addStimulus(obj, stim)
            if isempty(obj.stimulusList)
                obj.stimulusList = stim;
            else
                obj.stimulusList(end+1) = stim;                            
            end
            obj.onAddStimulus(stim);
        end  

        function clear(obj)
            obj.stimulusList = [];
        end

        function result = isActive(obj)
            result = ~isempty(obj.stimulusList);
        end
    end
end