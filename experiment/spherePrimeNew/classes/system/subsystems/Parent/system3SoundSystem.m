classdef system3SoundSystem < systemPart

   properties
       locator
       router
   end

   properties (Access = private)
       route
       calibration_dB_at_1Vrms
   end

   methods
       function obj = system3SoundSystem(system)             
            obj@systemPart(system);
        end

        function result = newStimulus(obj, stim) %#ok<INUSL> 
            result = soundStimulusClass(stim);
        end

        function onAddStimulus(obj, stim)
            index = length(obj.stimulusList);
            obj.system.triggerSystem.addSoundTrigger(stim, index);
        end         

        function prepare(obj, useTannoy)
            if ~obj.isActive
                return
            end            
            %-------------shorts--------------
            stimulus    = obj.stimulusList(1);
            maxSndLevel_dBspl = obj.system.configuration.maxSndLevel_dBspl;          
            obj.route   = obj.getRoute(stimulus.pos_DP, useTannoy);             
            matFile = fullfile(obj.system.environment.paths.snddir, stimulus.matfile);
            %-----------------------------------
            soundData = obj.getSoundDataFromFile(matFile);
            soundData = obj.makeRowVector(soundData); % some files contain column vectors            
            attenuation = max(maxSndLevel_dBspl - stimulus.intensity, 0); %in dB            
            %-----------------------------------
            obj.hardware.prepare(soundData, attenuation, obj.route);
        end

        function result = isReady(obj)
            result = obj.hardware.isReady(obj.route);
        end

        function clear(obj)
            clear@systemPart(obj);
            obj.hardware.clear;
        end
        
   end

   methods (Access = private)
       function result = getRoute(obj, desiredLocation_DP, useTannoy)
           obj.locator.useTannoy = useTannoy;           
           %---------------------------------            
           targetID = obj.locator.desiredLocation2ID(desiredLocation_DP);                
           result = obj.router.ID2Route(targetID);           
       end

       function result = getSoundDataFromFile(obj, matFile) %#ok<INUSL>
            sound = load(matFile);
            soundFieldNames = fieldnames(sound);
            %-----------------------------------            
            if ismember('snd', soundFieldNames)
               result = sound.snd;
               return;
            end
            if ismember('data', soundFieldNames)               
                result = sound.data;
                return;
            end           
       end

       function result = makeRowVector(obj, vector)           
           [size_row, size_col] = size(vector);
           if (size_row > 1) && (size_col == 1)
               result = vector';
           else
               result = vector;
           end
       end
   end  
   
end 