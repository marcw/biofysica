classdef testLedSystem < systemPart

    properties        
        locator
        router        
    end

    properties (Access = private)
        ledcolours = {'r', 'g'}
    end

    methods
        function obj = testLedSystem(system)
            obj@systemPart(system);
            ledDeviceName = system.environment.info.leds.device_name;
            obj.hardware  = ledBoxHardware(ledDeviceName);  
            obj.locator   = sphereLedLocator;
            obj.router    = ledPLCrouter;          
        end

        function prepare(obj)
            if ~obj.isActive
                return
            end            
            cfg.ledcolours = obj.ledcolours;
            [patterns, pattern_delays, pattern_events] = makeOrderedLedPatterns(obj.stimulusList, cfg);
            nrOfEvents = length(pattern_events);
            for index = 1:nrOfEvents
                stim.event = pattern_events(index);
                stim.delay = pattern_delays(index);
                obj.system.triggerSystem.addLedTrigger(stim, index);
            end
            obj.hardware.sendLedPattern(patterns);
        end        

        function onAddStimulus(obj, stim)
            % do nothing
        end 

    end %methods

end