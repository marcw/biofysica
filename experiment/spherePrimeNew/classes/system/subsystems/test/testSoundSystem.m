classdef testSoundSystem < system3SoundSystem

   methods
       function obj = testSoundSystem(system)             
            obj@system3SoundSystem(system);
            obj.hardware = soundHardware(system.hardware);
            obj.router   = testSoundRouter;
            obj.locator  = testSoundLocator;            
       end       

   end
end