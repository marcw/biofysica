classdef sphereSoundSystem < system3SoundSystem

   methods
       function obj = sphereSoundSystem(system)             
            obj@system3SoundSystem(system);
            obj.hardware = sphereSoundHardware(system.hardware);
            obj.router   = sphereSoundRouter;
            obj.locator  = sphereSoundLocator;
        end

   end
end