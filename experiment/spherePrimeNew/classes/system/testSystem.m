classdef testSystem < experimentSystem 

    methods
        function obj = testSystem(environment, configuration)            
            obj@experimentSystem(environment, configuration); 
            %-----------------------------------------                     
            obj.hardware      = testSystem3Hardware(environment);            
            %-----------------------------------------
            obj.ledSystem     = testLedSystem(obj);
            obj.soundSystem   = testSoundSystem(obj);
            obj.triggerSystem = triggerSystem(obj);            
            obj.acqSystem     = acquisitionSystem(obj);             
            %-----------------------------------------
        end

    end %methods (public)

end % classdef