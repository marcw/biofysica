classdef experimentSystem < handle

    properties (Access = public)
        hardware
        configuration
        environment 
        info      
        acqSystem
        triggerSystem 
        ledSystem
        soundSystem          
        eventrecorder      
    end

    properties (Access = private)
        HARDWARE_TYPE = 'TDT System 3';
    end    

    methods
        function obj = experimentSystem(environment, configuration)
            obj.configuration = configuration;
            obj.environment   = environment;            
            obj.createEventRecorder;
            %-------------            
            obj.info.hardware = obj.HARDWARE_TYPE;                        
        end

        function createEventRecorder(obj)
            eventTiming = obj.environment.info.hardware.event_timing;
            if ~strcmp(eventTiming, 'none')
                lslType      = obj.environment.info.event_timing.lsl_type;
                channelIDs   = obj.environment.info.event_timing.channel_IDs;
                channelNames = obj.environment.info.event_timing.channel_names;                
                obj.eventrecorder = eventRecorder(lslType, channelIDs, channelNames);
            else
                obj.eventrecorder = dummy_eventRecorder;
            end            
        end

        function reconnectHardware(obj)
            obj.hardware.reconnect;
        end

        function prepare(obj, stimList)
            obj.addStimListToSubSystems(stimList);            
            obj.prepareSubSystems;            
        end    

        function reset(obj)
            obj.clearSubSystems;            
        end        

        function start(obj)
           obj.eventrecorder.start; 
           obj.triggerSystem.start;           
        end

        function stop(obj)
            obj.eventrecorder.stop;            
        end

        function simulateButtonPress(obj)
            obj.triggerSystem.sentEventTrigger;
        end         

        function result = isReady(obj)
            result = true;
            if obj.soundSystem.isActive && obj.acqSystem.isActive 
                result = obj.soundSystem.isReady && obj.acqSystem.isReady; 
                return;
            end
            if obj.soundSystem.isActive
                result = obj.soundSystem.isReady;
                return;
            end
            if obj.acqSystem.isActive
                result = obj.acqSystem.isReady;
                return;
            end            
        end  

        function addStimListToSubSystems(obj, stimList)                                    
            for i = 1: length(stimList)                
                stim = stimList(i);                
                switch lower(stim.modality)
                    case {'led','sky'}
                        stim = addIDtoStim(obj.ledSystem.locator, stim);
                        obj.ledSystem.addStimulus(stim);
                    case 'sound' 
                        stim = addIDtoStim(obj.soundSystem.locator, stim);                        
                        obj.soundSystem.addStimulus(stim);
                    case 'data acquisition'
                        obj.acqSystem.addStimulus(stim);                        
                    case 'trigger'                        
                        obj.triggerSystem.addStimulus(stim);
                end
            end

            function stim = addIDtoStim(locator, stim)
               azimuth =  stim.azimuth;
               elevation = stim.elevation;
               radius = stim.radius;
               hemisphere = stim.hemisphere;
               desiredLocation_DP = coordinates_DP(azimuth, elevation, radius, hemisphere);
               ID = locator.desiredLocation2ID(desiredLocation_DP);                       
               % add new fields to stim
               stim.pos_DP = desiredLocation_DP;
               stim.Z      = ID;
            end
        end        

            

        function prepareSubSystems(obj)
            obj.ledSystem.prepare;  
            useTannoy = obj.configuration.useTannoySpeakers;
            obj.soundSystem.prepare(useTannoy);
            acqDuration = obj.configuration.acqDuration;
            obj.acqSystem.prepare(acqDuration);
            autoStart = obj.configuration.autoZbusTrigger;
            obj.triggerSystem.prepare(autoStart);                                      
        end    

        function clearSubSystems(obj)
            obj.triggerSystem.clear;
            obj.ledSystem.clear;
            obj.acqSystem.clear;
            obj.soundSystem.clear;            
        end   

        function result = isTriggered(obj)
            eventCount = obj.configuration.isTriggeredDetectionID;
            result = obj.triggerSystem.isTriggered(eventCount);
        end

        function result = getHardwareStatus(obj)
            result  = obj.hardware.checkStatusDevices;
        end

        function result = getHardwareType(obj)
            result = obj.HARDWARE_TYPE;
        end

        function result = needReconnect(obj)
            result = obj.hardware.needReconnect;
        end        

    end

end