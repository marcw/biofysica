classdef sphereSystem < experimentSystem 

    methods
        function obj = sphereSystem(environment, configuration)
            obj@experimentSystem(environment, configuration);
            %-----------------------------------------             
            obj.hardware      = sphereSystem3Hardware(environment);
            %-----------------------------------------                      
            obj.ledSystem     = sphereLedSystem(obj);
            obj.soundSystem   = sphereSoundSystem(obj); 
            obj.triggerSystem = triggerSystem(obj);            
            obj.acqSystem     = acquisitionSystem(obj);             
            %-----------------------------------------            
        end

    end %methods (public)

end % classdef