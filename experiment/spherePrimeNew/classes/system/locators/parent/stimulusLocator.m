classdef stimulusLocator < handle
    % STIMULUSLOCATOR Manages stimulus locations using double polar coordinates.
    %
    %   The stimulusLocator class reads and stores stimulus locations from an Excel file
    %   and provides methods to map desired locations to the nearest real locations based
    %   on double polar coordinates (azimuth and elevation in radians).
    %
    %   Properties (Access = private):
    %       stimulusLocations_DP - An object containing stimulus locations in double polar coordinates.
    %
    %   Methods:
    %       stimulusLocator(locationFile, locationSheet) - Constructor that initializes the stimulusLocator object.
    %       desired2realLocation_DP(desiredLocation_DP)  - Returns the closest real location to a desired location.
    %       desiredLocation2ID(desiredLocation_DP)       - Returns the ID of the closest real location.
    %
    %   Example:
    %       % Create a stimulusLocator object
    %       stimLocator = stimulusLocator('sphere_sound_locations.xlsx', 'locations_DP');
    %
    %       % Define a desired location in double polar coordinates
    %       desiredLocation = coordinates_DP(azimuth, elevation, radius, hemisphere);
    %
    %       % Get the real location closest to the desired location
    %       realLocation = stimLocator.desired2realLocation_DP(desiredLocation);
    %
    %   Dependencies:
    %       - coordinates_DP: Class for handling double polar coordinates.
    %       - coordinates_DP_withID: Class for double polar coordinates with IDs.
    %       - rwl_angleBetweenVectors_DP: Function to calculate angles between vectors in DP coordinates.
    %
    %   Author: Ruurd Lof
    %   Date:   [Your Date Here]
    %
    %   See also: coordinates_DP, coordinates_DP_withID, rwl_angleBetweenVectors_DP

    properties (Access = private)
        stimulusLocations_DP
    end

    methods
        function obj = stimulusLocator(locationFile, locationSheet)
            % STIMULUSLOCATOR Constructor that initializes the stimulusLocator object.
            %
            %   obj = STIMULUSLOCATOR(locationFile, locationSheet) creates an instance of the
            %   stimulusLocator class by reading stimulus locations from the specified Excel file
            %   and sheet.
            %
            %   Inputs:
            %       locationFile  - String specifying the path to the Excel file containing stimulus locations.
            %       locationSheet - String specifying the sheet name within the Excel file.
            %
            %   Outputs:
            %       obj - An instance of the stimulusLocator class.
            %
            %   Example:
            %       stimLocator = stimulusLocator('sphere_sound_locations.xlsx', 'locations_DP');

            obj.readLocationsFromFile(locationFile, locationSheet);                        
        end

        function result = desired2realLocation_DP(obj, desiredLocation_DP)
            % DESIRED2REALLOCATION_DP Maps a desired location to the closest real location.
            %
            %   result = DESIRED2REALLOCATION_DP(obj, desiredLocation_DP) returns the real location
            %   in double polar coordinates that is closest to the desired location.
            %
            %   Inputs:
            %       desiredLocation_DP - A coordinates_DP object representing the desired location.
            %
            %   Outputs:
            %       result - A coordinates_DP object representing the closest real location.
            %
            %   Example:
            %       realLocation = stimLocator.desired2realLocation_DP(desiredLocation);

            % Get the closest real location ID
            ID = obj.desiredLocation2ID(desiredLocation_DP); 
             
            % Assign output
            result = obj.stimulusLocations_DP.selectPointByID(ID);
        end

        function result = desiredLocation2ID(obj, desiredLocation_DP)
            % DESIREDLOCATION2ID Finds the ID of the closest real location to the desired location.
            %
            %   result = DESIREDLOCATION2ID(obj, desiredLocation_DP) computes the angles between the
            %   desired location and all available real locations, and returns the ID of the location
            %   with the minimal angle (i.e., the closest).
            %
            %   Inputs:
            %       desiredLocation_DP - A coordinates_DP object representing the desired location.
            %
            %   Outputs:
            %       result - The ID of the closest real location.
            %
            %   Example:
            %       locationID = stimLocator.desiredLocation2ID(desiredLocation);

            % Determine number of locations
            n = length(obj.stimulusLocations_DP.ID);  
            azimuth = repmat(desiredLocation_DP.azimuth, n, 1);
            elevation = repmat(desiredLocation_DP.elevation, n, 1);
            radius = repmat(desiredLocation_DP.radius, n, 1);
            hemisphere = repmat(desiredLocation_DP.hemisphere, n, 1);

            desiredLocation_DP = coordinates_DP(azimuth, elevation, radius, hemisphere);
            
            properLocations_DP = obj.stimulusLocations_DP;

            % Find the angles between all proper locations and the desired location                           
            angles = rwl_angleBetweenVectors_DP(properLocations_DP, desiredLocation_DP);            

            % Find the location where the angle is minimal
            row = angles == min(angles);

            % Assign output
            result = properLocations_DP.ID(row);
        end        
    end 

    methods (Access = private)

        function readLocationsFromFile(obj, excelFile, excelSheet)
            % READLOCATIONSFROMFILE Reads stimulus locations from an Excel file.
            %
            %   READLOCATIONSFROMFILE(obj, excelFile, excelSheet) reads the stimulus locations
            %   from the specified Excel file and sheet, and stores them in the object's properties.
            %
            %   Inputs:
            %       excelFile  - String specifying the path to the Excel file.
            %       excelSheet - String specifying the sheet name within the Excel file.
            %
            %   Outputs:
            %       None (updates the stimulusLocations_DP property of the object).
            %
            %   Example:
            %       obj.readLocationsFromFile('locations.xlsx', 'Sheet1');

             sheetTable = readtable(excelFile, 'Sheet', excelSheet);
             ID = sheetTable.ID;
             azimuth = sheetTable.azimuth;
             elevation = sheetTable.elevation;
             radius = sheetTable.radius;
             hemisphere = sheetTable.hemisphere;
             % Create double polar table
             obj.stimulusLocations_DP = coordinates_DP_withID(ID, azimuth, elevation, radius, hemisphere);
        end                      
    end
end



% classdef stimulusLocator < handle
%     %uses double polar coordinates
%     %azimuth and elevation are in radials    
%    
%     properties (Access = private)
%         stimulusLocations_DP
%     end
%     
%     methods
%         function obj = stimulusLocator(locationFile, locationSheet)
%             obj.readLocationsFromFile(locationFile, locationSheet);                        
%         end
% 
%         function result = desired2realLocation_DP(obj, desiredLocation_DP)
% 
%             % get the closest real location in double polar coordinates
%             ID = obj.desiredLocation2ID(desiredLocation_DP); 
%              
%             % assign output
%             result = obj.stimulusLocations_DP.selectPointByID(ID);
%         end
% 
%         function result = desiredLocation2ID(obj, desiredLocation_DP)
%             % determine number of locations
%             n = length(obj.stimulusLocations_DP.ID);  
%             azimuth = repmat(desiredLocation_DP.azimuth, n, 1);
%             elevation = repmat(desiredLocation_DP.elevation, n, 1);
%             radius = repmat(desiredLocation_DP.radius, n, 1);
%             hemisphere = repmat(desiredLocation_DP.hemisphere, n, 1);
% 
%             desiredLocation_DP = coordinates_DP(azimuth, elevation, radius, hemisphere);
%             
%             properLocations_DP = obj.stimulusLocations_DP;
% 
%             % find the angles between all proper locations with the desired locations                           
%             angles = rwl_angleBetweenVectors_DP(properLocations_DP,desiredLocation_DP);            
% 
%             % find the location where the angle is minimal
% 
%             row = angles == min(angles);
% 
%             % assign output
%             result = properLocations_DP.ID(row);
%         end        
%         
%     end 
% 
%     methods (Access = private)
% 
%         function readLocationsFromFile(obj, excelFile, excelSheet)
%              sheetTable = readtable(excelFile, 'sheet', excelSheet);
%              ID = sheetTable.ID;
%              azimuth = sheetTable.azimuth;
%              elevation = sheetTable.elevation;
%              radius = sheetTable.radius;
%              hemisphere = sheetTable.hemisphere;
%              % create double polar table
%              obj.stimulusLocations_DP = coordinates_DP_withID(ID, azimuth, elevation, radius, hemisphere);
%         end                      
%     end
% 
% 
% end
