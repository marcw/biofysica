classdef sphereSoundLocator < stimulusLocator
    properties        
       useTannoy = false
       excelFile
    end    

    properties (Access = private)
        locations % id, az_deg, el_deg
    end    

    methods
        function obj = sphereSoundLocator
           locationFile = "sphere_sound_locations.xlsx";
           %locationSheet   = "sphere_sound_locations_with_Tannoy.xlsx";
           locationSheet       = "locations_DP";
           obj@stimulusLocator(locationFile, locationSheet); 
        end
    end
         
    
end