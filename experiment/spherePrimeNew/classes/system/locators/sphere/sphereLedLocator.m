classdef sphereLedLocator < stimulusLocator
    properties
        excelFile
    end    

    methods
        function obj = sphereLedLocator           
            locationFile =  "sphere_led_locations.xlsx";
            locationSheet = "locations_DP";
            obj@stimulusLocator(locationFile, locationSheet);
        end
    end
end