classdef ledPLCrouter < stimulusRouter
   
    properties
        routes
        excelFile = 'sphere_led_locations.xlsx'
        excelSheet = 'routes'
    end
    
    methods
        function obj = ledPLCrouter            
            obj.routes = obj.readFromFile(obj.excelFile, obj.excelSheet);                        
        end        
    end

   methods
        function routes = readFromFile(obj, fname, sheet)
           values    = xlsread(fname, sheet); %#ok<XLSRD> 
           ID         = values(:,1); % id of the target
           Channel    = values(:,2);           
           routes    = table(ID, Channel);           
        end
    end
end

