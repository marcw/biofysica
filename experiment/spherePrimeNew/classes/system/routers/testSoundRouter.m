classdef testSoundRouter < stimulusRouter

    methods
        function result = ID2Route(obj, targetID)            
            result.RP2_ID = 1;        
            result.RP2out_ID = randi([1, 2]);
            result.PA5_ID = 1;
            result.RCX_MUX_ID = 0;
            result.RCX_Channel_ID = 0;
        end        
    end
end