classdef ledboxRouter < stimulusRouter
    properties
        routes
    end

    methods       
        function obj = ledboxRouter(routingFile, routingSheet)
            obj.routes = obj.readFromFile(routingFile, routingSheet);  
        end
    end

    methods (Access = private)
        function locations = readFromFile(obj, excelFile, excelSheet)           %#ok<INUSL> 
            values    = xlsread(excelFile,excelSheet); %#ok<XLSRD> 
            ledID        = values(:,1); % id of the target
            boxID     = values(:,2);
            channel   = values(:,3);
            locations = table(ledID, boxID, channel);           
        end        
     
    end
end