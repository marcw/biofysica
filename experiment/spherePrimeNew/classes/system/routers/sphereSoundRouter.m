classdef sphereSoundRouter < stimulusRouter
    properties
        routes
        excelFile = 'sphere_sound_locations.xlsx'
        excelSheet = 'routes'       
    end

    methods
        function obj = sphereSoundRouter
            obj.routes = obj.readFromFile(obj.excelFile, obj.excelSheet);
        end

        function result = ID2Route(obj, targetID)
            row = (obj.routes.targetID == targetID); 
            result.RP2_ID         = obj.routes.RP2_ID(row);
            result.RP2out_ID      = obj.routes.RP2out_ID(row);
            result.PA5_ID         = obj.routes.PA5_ID(row);
            result.RCX_MUX_ID     = obj.routes.MUX_ID(row)-1; % RCX_MUX_ID = 0..3
            result.RCX_Channel_ID = obj.routes.Channel_ID(row)-1; % RCX_Channel_ID = 0..15
        end
    end

    

    methods (Access = private)
        function routes = readFromFile(obj, fname, sheet) %#ok<INUSL> 
           values     = xlsread(fname, sheet); %#ok<XLSRD> 
           targetID   = values(:,1); % id of the target
           RP2_ID     = values(:,2);
           RP2out_ID  = values(:,3);
           PA5_ID     = values(:,4);
           MUX_ID     = values(:,5);
           Channel_ID = values(:,6);
           routes     = table(targetID, RP2_ID, RP2out_ID, PA5_ID, MUX_ID, Channel_ID);           
        end
    end
end
