classdef sphereSystem3Hardware < system3Hardware
    properties
       PA5activexServers
       RA16activexServer
       RP2activexServers       
    end

    methods
        function obj = sphereSystem3Hardware(environment)
            obj@system3Hardware(environment);
        end

        function connect(obj)
            [obj.ZBUSactivexServer,    err(1), errstr{1}] = ZBUS(1);                       % zBus, number of racks            
            [obj.RP2activexServers{1}, err(2), errstr{2}] = RP2(1, obj.RP2circuit);     % Real-time processor 1
            [obj.RP2activexServers{2}, err(3), errstr{3}] = RP2(2, obj.RP2circuit);    % Real-time processor 2            
            [obj.PA5activexServers{1}, err(4), errstr{4}] = PA5(1);                    % Programmable attenuator 1
            [obj.PA5activexServers{2}, err(5), errstr{5}] = PA5(2);                    % Programmable attenuator 2
            [obj.PA5activexServers{3}, err(6), errstr{6}] = PA5(3);                    % Programmable attenuator 3
            [obj.PA5activexServers{4}, err(7), errstr{7}] = PA5(4);                    % Programmable attenuator 4            
            [obj.RA16activexServer,    err(8), errstr{8}] = RA16(1, obj.RA16circuit);            
            obj.checkTDTerrors(err, errstr);
        end

        function reconnect(obj)
            [~, err(1), errstr{1}] = ZBUS(1);                    % zBus, number of racks            
            [~, err(2), errstr{2}] = RP2(1, obj.RP2circuit);     % Real-time processor 1
            [~, err(3), errstr{3}] = RP2(2, obj.RP2circuit);        % Real-time processor 2            
            [~, err(4), errstr{4}] = PA5(1);                    % Programmable attenuator 1
            [~, err(5), errstr{5}] = PA5(2);                    % Programmable attenuator 2
            [~, err(6), errstr{6}] = PA5(3);                    % Programmable attenuator 3
            [~, err(7), errstr{7}] = PA5(4);                    % Programmable attenuator 4            
            [~, err(8), errstr{8}] = RA16(1, obj.RA16circuit);            
            obj.checkTDTerrors(err, errstr);
        end
    end
end
     