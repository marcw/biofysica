classdef sphereLedHardware < handle
    
    properties
        ra16
        ledController
    end
    
    methods  
        function obj = sphereLedHardware(deviceName)  
            obj.ledController = ledcontroller(deviceName);
        end        

        function sendLedPattern(obj, ledPattern)             
            obj.ledController.write(ledPattern); 
        end

        function clearAll(obj)
            obj.ledController.clearAll;
        end
    end
end




