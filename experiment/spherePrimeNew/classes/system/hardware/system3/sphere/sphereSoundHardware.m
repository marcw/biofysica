classdef sphereSoundHardware < soundHardware
    properties
       PA5activexServers    
    end

    methods        
        function obj = sphereSoundHardware(hardware)
            obj@soundHardware(hardware)
            obj.PA5activexServers = hardware.PA5activexServers;
        end

        function clear(obj)
            clear@soundHardware(obj)
            obj.preparePA5(1, 61);
            obj.preparePA5(2, 62);
            obj.preparePA5(3, 63);
            obj.preparePA5(4, 64);
        end

    end %methods

    methods (Access = protected)

        function preparePA5(obj, PA5_ID, attenuation)
            PA5activexServer = obj.PA5activexServers{PA5_ID};
            PA5activexServer.SetAtten(attenuation);
        end    
    end
  
end


    