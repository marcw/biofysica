classdef soundHardware < handle
    properties
       RP2activexServers
       sampleFrequency = 48828 
    end

    properties (Access = private)
        SET = 1;
        RESET = 2;
        ENABLE = 1;
        DISABLE = 0;
    end

    methods        
        function obj = soundHardware(hardware)
            obj.RP2activexServers = hardware.RP2activexServers;
        end

        function result = isReady(obj, route)
            result = false;
            strOUT = int2str(route.RP2out_ID);
            if route.RP2_ID > length(obj.RP2activexServers)
               result = true;
               return
            end
            RP2activexServer = obj.RP2activexServers{route.RP2_ID};
            tag = ['Ready' strOUT];
            isReady = RP2activexServer.GetTagVal(tag);
            if isReady
                result = true;
            end            
        end

        function prepare(obj, soundData, attenuation, route)
            if route.RP2_ID > length(obj.RP2activexServers)
                return;
            end
            obj.prepareMUX(route.RP2_ID, route.RCX_MUX_ID, route.RCX_Channel_ID);
            obj.prepareRP2(soundData, route.RP2_ID, route.RP2out_ID);
            obj.preparePA5(route.PA5_ID, attenuation);
        end

        function clear(obj)
           for RP2_ID = 1:length(obj.RP2activexServers)
              RP2activexServer = obj.RP2activexServers{RP2_ID};
              for RP2out_ID = 1:2
                  intStr  = int2str(RP2out_ID);
 	              RP2activexServer.SetTagVal(['bufferSize' intStr], 0);
	              RP2activexServer.SetTagVal(['chanEnable' intStr], obj.DISABLE);                        
	              RP2activexServer.WriteTagV(['wavData'    intStr], 0, 0);
              end
              for MUX = 0:3                  
                  obj.setMUX(RP2_ID, MUX, 0, obj.RESET);
              end
           end 
        end

    end %methods

    methods (Access = protected)

        function prepareMUX(obj, RP2_ID, RCX_MUX_ID, RCX_channel_ID)
            obj.setMUX(RP2_ID, RCX_MUX_ID, RCX_channel_ID, obj.SET);            
        end

        function prepareRP2(obj, wavData, RP2_ID, RP2out_ID)
            RP2activexServer = obj.RP2activexServers{RP2_ID};           
            intStr  = int2str(RP2out_ID);
            bufferSize = length(wavData);
            %---------------------------- 
	        RP2activexServer.SetTagVal(['bufferSize' intStr], bufferSize);
	        RP2activexServer.SetTagVal(['chanEnable' intStr], obj.ENABLE);                        
	        RP2activexServer.WriteTagV(['wavData'    intStr],0, wavData);
        end

        function setMUX(obj, RP2_ID, RCX_device_ID, RCX_channel_ID, setReset)            
            RP2activexServer = obj.RP2activexServers{RP2_ID};                     
            RP2activexServer.SetTagVal('DeviceSelect', RCX_device_ID);
            RP2activexServer.SetTagVal('ChanSelect', RCX_channel_ID);            
            RP2activexServer.SetTagVal('SetReset', setReset);
            RP2activexServer.SoftTrg(1); % activates the MUX
        end

        function preparePA5(obj, PA5_ID, attenuation) %#ok<INUSD> 
            return; 
        end    
    end
  
end


    