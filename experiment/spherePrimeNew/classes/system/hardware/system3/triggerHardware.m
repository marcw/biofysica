classdef triggerHardware < handle
    properties
        ZBUSactivexServer
        RA16activexServer        
    end
    methods
        function obj = triggerHardware(hardware)
            obj.ZBUSactivexServer = hardware.ZBUSactivexServer;
            obj.RA16activexServer = hardware.RA16activexServer;
        end

        function result = isTriggered(obj, eventCount)
            %in the Matlab program trigger events are counted 0..7
            %in the RCX file of the RA16 trigger events are counted 1..8
            %therefore +1 is added
            result = false;
            eventCountRA16 = obj.RA16activexServer.GetTagVal('eventCount');
            if eventCountRA16 >= eventCount + 1
               result = true; 
            end
        end

        function result = waitIsReady(obj)
            result = obj.RA16activexServer.GetTagVal('Wait');
        end

        function resetTrigger(obj)
            obj.ZBUSactivexServer.zBusTrigA(0, 0, 2); 
        end

        function eventTrigger(obj)
            obj.ZBUSactivexServer.zBusTrigB(0, 0, 2); % Trigger event            
        end

    end

end



