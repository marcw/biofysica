classdef system3Hardware < handle

    properties(Access = public)
        ZBUSactivexServer 
    end

    properties (Access = protected)
        RP2circuit
        RA16circuit      
    end

    methods
        function obj = system3Hardware(environment)
            obj.RP2circuit  = which(environment.info.hardware.rcx_file_rp2);
            obj.RA16circuit = which(environment.info.hardware.rcx_file_ra16); 
            obj.connect;            
        end

        function result = checkStatusDevices(obj)
            for i = 1: length(obj.RP2activexServers)
               result.RP2{i} = obj.checkDeviceStatus(obj.RP2activexServers{i});
            end
            result.RA16  = obj.checkDeviceStatus(obj.RA16activexServer);
        end

        function result = needReconnect(obj)
            deviceStatus = obj.checkStatusDevices;
            rp2devicesAreRunning = true;
            for i = 1: length(obj.RP2activexServers)
                rp2devicesAreRunning = deviceStatus.RP2{i}.running && rp2devicesAreRunning;
            end
            allDevicesAreRunning = rp2devicesAreRunning && ...
                                   deviceStatus.RA16.running;
            result = ~allDevicesAreRunning;
        end      

    end

    methods (Abstract)
        connect(obj)
    end

    methods (Access = protected)
        function result = checkDeviceStatus(obj, device) %#ok<INUSL> 
            statusByte = device.GetStatus;
            result.connected = bitget(statusByte,1);
            result.loaded    = bitget(statusByte,2);
            result.running   = bitget(statusByte,3);
        end        

        function checkTDTerrors(obj, err, errstr) %#ok<INUSL> 
            for i = 1:length(err)
                if err(i)~= 0
                    error(errstr{i}{1});
                end
            end
        end
    end

end