classdef ledBoxHardware < handle
    
    properties
        ledController
    end
    
    methods  
        function obj = ledBoxHardware(id)  
            obj.ledController = ledcontroller_pi([id '.local']);
        end        

        function sendLedPattern(obj, ledPattern)            
            obj.ledController.write(ledPattern); 
        end
    end
end




