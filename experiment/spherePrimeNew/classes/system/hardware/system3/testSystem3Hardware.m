classdef testSystem3Hardware < system3Hardware

    properties
        RA16activexServer
        RP2activexServers        
    end

    methods   
        function obj = testSystem3Hardware(configuration)
            obj@system3Hardware(configuration);
        end

        function connect(obj)
            [obj.ZBUSactivexServer,    err(1), errstr{1}] = ZBUS(1);                    % zBus, number of racks            
            [obj.RP2activexServers{1}, err(2), errstr{2}] = RP2(1, obj.RP2circuit);     % Real-time processor 1
            [obj.RA16activexServer,    err(8), errstr{8}] = RA16(1, obj.RA16circuit);            
            obj.checkTDTerrors(err, errstr);
        end

        function reconnect(obj)
            [~, err(1), errstr{1}] = ZBUS(1);                    % zBus, number of racks            
            [~, err(2), errstr{2}] = RP2(1, obj.RP2circuit);     % Real-time processor 1
            [~,   err(8), errstr{8}] = RA16(1, obj.RA16circuit);            
            obj.checkTDTerrors(err, errstr);
        end
    end

end
     