classdef acquisitionHardware < handle   
    properties 
        SAMPLEFREQUENCY = 6103.5 % in Hz of the RA16        
    end

    properties (Access = private)        
        MAXSAMPLES      = 24000  % is restricted by RCX file        
        RA16activexServer
    end

    methods
        function obj = acquisitionHardware(hardware) 
            obj.RA16activexServer = hardware.RA16activexServer;
        end

        function prepare(obj, duration)
            acqSamples = obj.duration2samples(duration, obj.SAMPLEFREQUENCY);
            obj.checkAcqSamples(acqSamples);
            obj.RA16activexServer.SetTagVal('acqSamples', acqSamples);            
        end

        function checkAcqSamples(obj, value)            
            if value > obj.MAXSAMPLES
                error('sphereAcquisitionSystemClass.prepare: acqSamples > MAXSAMPLES');
            end
        end

        function result = isReady(obj)
            result = obj.RA16activexServer.GetTagVal('Ready');
        end

        function result = getAcqNrOfSamples(obj)
            result = obj.RA16activexServer.GetTagVal('acqSamples');
        end
        
        function result = getAcqData(obj, channel, nrOfSamples) %returns column of singles            
            result     = obj.RA16activexServer.ReadTagV(['Data_' int2str(channel)], 0, nrOfSamples)';
        end

        function result = getEventDataNrOfSamples(obj)
            result = obj.RA16activexServer.GetTagVal('eventSize');
        end
       

        function result = getEventData(obj, nrOfPoints) %return array of uint32            
            singleData = single(obj.RA16activexServer.ReadTagV('eventData',0,nrOfPoints))'; %reads 32 bits data as a singles
            result     = typecast(singleData, 'uint32'); %convert 32 bits data to integers
        end
    end
    
    methods (Access = private)
        function result = duration2samples(obj, duration, frequency)
           result = duration * frequency;
        end
    end

end