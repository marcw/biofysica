classdef lslRecorder < handle
    properties
        pupilmetadata
        session    
    end  

    methods (Abstract)
        downloadRecordedData(obj)
    end

    methods
%        function obj = lslRecorder
%             if isFireWallActive
%                error('Firewall is active. This interferes blocks the LSL. Ask Gunter to disable the firewall');
%             end
%        end

        function delete(obj)
            delete(obj.session);            
        end 

        function start(obj)
            obj.session.start;
        end

        function stop(obj)
            obj.session.stop;
            obj.downloadRecordedData;                                   
        end
    end    
end

