classdef eventRecorder < lslRecorder
    
    properties (Access = public)
        dataTable  
    end
    
    properties (Access = private)    
        lslType          
        genericLslName = 'Digital Events %d'
    end
   
    methods (Access = public)

        function obj = eventRecorder(lslType, channelIDs, channelTitles)
            obj@lslRecorder;
            obj.lslType = lslType;
            obj.createDataTable(channelIDs, channelTitles);
            obj.session = lsl_session();
            obj.labStreamingSession;        
        end

        function downloadRecordedData(obj)
            rows = 1:height(obj.dataTable);
            data = arrayfun(@obj.getData, rows, 'UniformOutput', false);
            obj.dataTable.data = data';
        end
        
    end    

    methods (Access = private)

        function labStreamingSession(obj)                        
            %****************eventrecorder channels***********************            
            nr_of_channels = height(obj.dataTable);
            for i = 1:nr_of_channels 
                lslString = sprintf('type=''%s'' and name=''%s''', obj.lslType, obj.dataTable.lslNames{i});  
                info=lsl_resolver(lslString);
                list=info.infos();                
                if isempty(list)
                    error('no streams found');
                end
                evstream=lsl_istream(info{1});        
                obj.session.add_stream(evstream);
                delete(info);
            end
        end  %labStreamingSession

        function createDataTable(obj, channelIDs, channelTitles)
            nr_of_channels = length(channelIDs);
            lslNames = arrayfun(@(x) sprintf(obj.genericLslName, x), channelIDs, 'UniformOutput', false);
            %-----------------------------------------------------------------------------
            % make columns of the rows
            channelIDs = channelIDs';
            channelTitles = channelTitles';
            lslNames = lslNames';
            data = cell(1, nr_of_channels)';
            %-----------------------------------------------------------------------------
            obj.dataTable = table(channelIDs, channelTitles, lslNames, data);            
        end

        function result = getData(obj, ID)
            result = obj.session.streams{ID}.read();            
        end  %getData  
    end    
end

