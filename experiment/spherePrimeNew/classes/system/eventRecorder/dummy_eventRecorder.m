classdef dummy_eventRecorder < handle
    
    properties (Access = public)
        dataTable  
    end
    
    properties (Access = private)    
        lslType          
        genericLslName = 'Digital Events %d'
    end
   
    methods (Access = public)

        function obj = dummy_eventRecorder      
        end

        function downloadRecordedData(obj)
        end

        function delete(obj)
        end 

        function start(obj)
        end

        function stop(obj)
        end        
        
    end    

    methods (Access = private)

        function result = getData(obj, ID)
            result = 0; 
        end  %getData  
    end    
end

