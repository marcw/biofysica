classdef waitTrigger < trigger
    methods
        function obj = waitTrigger(RA16activexServer, event, delay)
            obj@trigger(RA16activexServer, event, delay);
            obj.eventStr  = 'eventWait';
            obj.delayStr  = 'delayWait';
        end
    end
end