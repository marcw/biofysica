classdef trigger  < handle 
    properties
        RA16activexServer
        eventID
        eventStr 
        delayStr
        triggerDelay  % all delays in mSec      
    end

    methods
        function obj = trigger(RA16activexServer, eventID, triggerDelay)
            obj.RA16activexServer = RA16activexServer;
            obj.eventID = eventID;
            obj.triggerDelay = triggerDelay;
        end

        function prepare(obj)
            obj.RA16activexServer.SetTagVal(obj.eventStr, obj.eventID + 1); %in rcx: 1:8 ipv. 0:7
            obj.RA16activexServer.SetTagVal(obj.delayStr, obj.triggerDelay);
        end
    end
        
end