classdef soundTrigger < trigger
    properties
       enableStr
       enableVal = true;
    end

    methods
         function obj = soundTrigger(RA16activexServer, RP2_ID, eventID, triggerDelay)
             obj@trigger(RA16activexServer, eventID, triggerDelay);             
             intstr = int2str(RP2_ID);
             obj.eventStr  = ['eventRP2'  intstr];
             obj.delayStr  = ['delayRP2'  intstr];
             obj.enableStr = ['rp2Enable' intstr];              
         end                  

        function prepare(obj)
            prepare@trigger(obj);
            obj.RA16activexServer.SetTagVal(obj.enableStr, obj.enableVal);            
        end  

    end   
end