classdef acqTrigger < trigger
    methods
        function obj = acqTrigger(RA16activexServer, event, delay)
            obj@trigger(RA16activexServer, event, delay);
            obj.eventStr  = 'eventAcq';
            obj.delayStr  = 'delayAcq';
        end
    end
   
end