classdef ledTrigger < trigger

    methods
        function obj = ledTrigger(RA16activexServer, triggerIndex, eventIndex, triggerDelay)
            obj@trigger(RA16activexServer, eventIndex, triggerDelay);
            intstr = int2str(triggerIndex);
            obj.eventStr  = ['eventLED' intstr];
            obj.delayStr  = ['delayLED' intstr];
        end
    end

end
  