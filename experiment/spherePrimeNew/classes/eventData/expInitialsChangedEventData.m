classdef  (ConstructOnLoad) expInitialsChangedEventData < event.EventData

   properties
      isInitialsOK
      initials
   end
   
   methods
       function obj = expInitialsChangedEventData(initials, isOK)
           obj.initials = initials;
           obj.isInitialsOK = isOK;
      end
   end
end   