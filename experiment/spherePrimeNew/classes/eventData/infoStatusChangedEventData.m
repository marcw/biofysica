classdef  (ConstructOnLoad) infoStatusChangedEventData < event.EventData

   properties
      infoStatus
   end
   
   methods
       function obj = infoStatusChangedEventData(infoStatus)
           obj.infoStatus = infoStatus;
      end
   end
end   
