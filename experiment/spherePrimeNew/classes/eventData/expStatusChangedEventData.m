classdef  (ConstructOnLoad) expStatusChangedEventData < event.EventData

   properties
      status
   end
   
   methods
       function obj = expStatusChangedEventData(status)
           obj.status     = status;
      end
   end
end   
