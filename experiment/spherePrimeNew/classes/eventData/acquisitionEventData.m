classdef (ConstructOnLoad) acquisitionEventData < event.EventData
   properties
       time
       distanceTrace
       velocityTrace
       stimulusPos_DP
       responsePos_DP       
   end
   
   methods
       function obj = acquisitionEventData(time, distanceTrace, velocityTrace, stimulusPos_DP, responsePos_DP)
         obj.time = time;  
         obj.distanceTrace  = distanceTrace;
         obj.velocityTrace  = velocityTrace;
         obj.stimulusPos_DP = stimulusPos_DP;
         obj.responsePos_DP    = responsePos_DP;
      end
   end
end