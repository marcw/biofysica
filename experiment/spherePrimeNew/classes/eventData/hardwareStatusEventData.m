classdef (ConstructOnLoad) hardwareStatusEventData < event.EventData
   properties
      hardwareStatus
      hardwareType
   end
   
   methods
       function obj = hardwareStatusEventData(hardwareType, hardwareStatus)
           obj.hardwareType = hardwareType;
           obj.hardwareStatus = hardwareStatus;
       end
   end
end