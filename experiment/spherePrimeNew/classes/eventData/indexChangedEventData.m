classdef (ConstructOnLoad) indexChangedEventData < event.EventData
   properties
      index
      count
      startingIndex
   end
   
   methods
       function obj = indexChangedEventData(index, count, startingIndex)
         obj.index         = index;
         obj.count         = count;
         obj.startingIndex = startingIndex;
      end
   end
end