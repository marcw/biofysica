classdef environment < handle

    properties (Access = public)             
        paths        
        info 
        dataDirectory
    end

    properties (Access = private)
        DUMMY_LIST_ITEM = '??';         
    end
        
    methods

        function obj = environment(programName, version)
            fileName = obj.getEnvFilename;
            obj.info             = obj.readEnvDataFromFile(fileName);
            obj.info.envFilenam  = fileName;
            obj.info.programName = programName;
            obj.info.version     = version;            
            obj.info.title       = obj.getTitle;                  
        end %environmentClass            

        function result = getTitle(obj)
            programName = obj.info.programName;
            version     = obj.info.version;
            labName     = obj.info.lab.name;
            result      = programName + " " + version + " (" + labName + ")";
        end

        function result = getEnvFilename(obj)
            filterString = sprintf('%s (%s)', ENV_TYPE.TYPESTRING, ENV_TYPE.EXTENSION);
            envFilter = {ENV_TYPE.EXTENSION, filterString};        
            defaultfileName = 'test.env';         
            [fileName, filePath] = uigetfile(envFilter, ENV_TYPE.FILTERSTRING);
            if fileName
                result = fullfile(filePath, fileName);            
            else              
                result = defaultfileName;
            end
        end

        function result = get.dataDirectory(obj)
            result = obj.info.data.path;            
        end      

        function result = getExpFileList(obj, isOK)
            if isOK
                result = obj.getDropDownFileList(obj.paths.expdir, obj.EXP_FILE_TYPE);
            else
                result = {obj.DUMMY_LIST_ITEM};
            end
        end

        function result = getCfgFileList(obj, isOK)
            if isOK
                result = obj.getDropDownFileList(obj.paths.cfgdir, obj.CFG_FILE_TYPE);
            else
                result = {obj.DUMMY_LIST_ITEM};
            end
        end

        function result = getInitialsList(obj)
            % the list consists of the subdirectories of the data directory
            d = dir(obj.dataDirectory);
            subdirs = d([d.isdir] & ~strcmp({d.name}, '.') & ~strcmp({d.name}, '..'));
        
            % Initialiseren van de cel-array met de juiste grootte
            numSubdirs = length(subdirs);
            initialsList = cell(1, numSubdirs + 1); % +1 for the dummy item 
            initialsList{1} = obj.DUMMY_LIST_ITEM;
        
            % Vul de cel-array met directory namen
            for i = 1:numSubdirs
                initialsList{i + 1} = subdirs(i).name;
            end                    
            result = initialsList;
        end

        function setPathBaseDir(obj, expInitials)            
            basedir = [obj.dataDirectory filesep expInitials];             
            if exist('obj.paths', 'var') == 1 %paths is an object, do not make it twice
                obj.paths.basedir = basedir;
            else
                obj.paths = experimentDataPaths(basedir);
            end
        end         

    end %methods 

    methods (Access = private)       

        function result = readEnvDataFromFile(obj, filename) %#ok<INUSL> 
            fullFilename         = which(filename);
            [dir, fname, ext]    = fileparts(fullFilename);
            fn = [fname ext];
            result = RL_readIniFile(dir, fn);
        end

        function result = getDropDownFileList(obj, directory, filetype)
            fileList = getFileList(directory, filetype); 
            if isempty({fileList.name})
                result = {obj.DUMMY_LIST_ITEM};
            else
                result = [{obj.DUMMY_LIST_ITEM}, {fileList.name}];
            end
        end     
    end
 
end %classdef