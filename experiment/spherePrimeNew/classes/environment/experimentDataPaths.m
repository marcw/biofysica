classdef experimentDataPaths < handle
    properties
        cfgdir
        expdir
        snddir
        recdir
        basedir        
    end

    methods

        function obj = experimentDataPaths(basedir)
            obj.basedir = basedir;
        end

        function result = get.cfgdir(obj)            
            dir = [obj.basedir filesep 'CFG']; % cfg directory name            
            obj.checkDir(dir);
            result = dir;
        end

        function result = get.expdir(obj)
            dir = [obj.basedir filesep 'EXP']; % exp directory name            
            obj.checkDir(dir);
            result = dir;
        end

        function result = get.snddir(obj)
            dir = [obj.basedir filesep 'SND']; % snd directory name            
            obj.checkDir(dir);
            result = dir;        
        end

        function result = get.recdir(obj)
            dir = [obj.basedir filesep 'REC']; % rec directory name            
            obj.checkDir(dir);
            result = dir;
        end

    end %public methods

    methods (Access = private)
        function checkDir(~, dir)
            if (exist(dir, 'dir') ~= 7)
                mkdir(dir);
            end       
        end
    end %private methods
end

