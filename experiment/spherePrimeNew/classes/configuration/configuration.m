classdef configuration < handle
% N.B. The maxSndLevel_dBspl must be determined experimentally. This is the
% calibration of the system.

    properties        
        labName        
        acqDuration
        useTannoySpeakers
        filters       
        info
        cfgDir        
        maxSndLevel_dBspl       % the sound level measured at the chair position with 1V signal (no attenuation)
        autoZbusTrigger         % the program generates the first trigger (event = 0 according to experiment file)        
        isTriggeredDetectionID  % event numbering according to experiment file        
    end

    properties (Access = private)
        filename
    end


    methods

        function readFromFile(obj, fullFilename)
            [dir, fn, ext] = fileparts(fullFilename);
            obj.filename = [fn ext];
            obj.cfgDir = dir;            
            iniData = rwl_readIniFile(obj.cfgDir, obj.filename); 
            %------------------------------------------------------

            obj.filters                = load(iniData.acquisition.filterFilename); % load filter for function filtfilt
            obj.acqDuration            = iniData.acquisition.duration;
            obj.useTannoySpeakers      = iniData.hardware.useTannoySpeakers;
            obj.autoZbusTrigger        = iniData.hardware.autoZbusTrigger;
            obj.isTriggeredDetectionID = iniData.hardware.isTriggeredDetectionID;
            obj.maxSndLevel_dBspl      = iniData.hardware.maxSndLevel_dBspl;
        end

        function result = get.info(obj)
            result.filename           = obj.filename;
            result.dir                = obj.cfgDir;
            %result.labName            = obj.labName;
            result.acqDuration        = obj.acqDuration;
            result.useTannoySpeakers  = obj.useTannoySpeakers;            
            result.autoTrialStart     = obj.autoZbusTrigger;
            result.maxSndLevel_dBspl  = obj.maxSndLevel_dBspl;
        end
    end

end