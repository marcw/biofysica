classdef recordingsHandler < handle
    properties        
        trialIndex
        stimlist
        acqData
        evtInfo
        guiInfo
        cfgInfo
        envInfo
        expInfo
        sysInfo
    end

    properties (Access = private)
        environment
    end

    methods
        function obj = recordingsHandler(environment)
            obj.environment = environment;
        end

        function save(obj)
            [dirName, fileName] = obj.getOutputFilename;
            fn = fullfile(dirName, fileName);
            % build a struct
            recordings.thisFile = fn;
            recordings.trialIndex = obj.trialIndex;
            recordings.stimList   = obj.stimlist;
            recordings.guiInfo    = obj.guiInfo;
            recordings.cfgInfo    = obj.cfgInfo;
            recordings.envInfo    = obj.envInfo;
            recordings.expInfo    = obj.expInfo;
            recordings.evtInfo    = obj.evtInfo;
            recordings.acqData    = obj.acqData;
            recordings.sysInfo    = obj.sysInfo;
            if ~isfolder(dirName)
                mkdir(dirName);
            end
            save(fn, 'recordings');              
        end

        function [dirName, fileName] = getOutputFilename(obj)            
            strInitials = obj.guiInfo.expInitials;
            strSubjectID = sprintf('%04u',str2double(obj.guiInfo.subjectid));
            strData = obj.guiInfo.datestring;
            strBlock = sprintf('%04u',str2double(obj.guiInfo.block));
            strTrialIndex = sprintf('%04u',obj.trialIndex);
            recdir  = obj.environment.paths.recdir;
            fileName = [strInitials '-' strSubjectID '-' strData '-' strBlock '-' strTrialIndex '.mat']; % file name
            dirName	 = [recdir filesep strInitials '-' strSubjectID '-' strData]; % directory name    
        end
    end %methods    
    
end