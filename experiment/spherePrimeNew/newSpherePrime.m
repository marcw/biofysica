clear; clc;
%#########################################################################
%#                                                                       #
%#                           newSpherePrime                              #                          
%#                                                                       #
%#########################################################################
                        version = 'V1.17'; 
                    programName = 'newSpherePrime'; 
%#########################################################################
myEnvironment    = environment(programName, version);
myConfiguration  = configuration;
myRecordings     = recordingsHandler(myEnvironment);
%--------------------------------------------------------------                      
switch myEnvironment.info.system.ID
    case systems.TEST
        mySystem  = testSystem(myEnvironment, myConfiguration);
    case systems.SPHERE
        mySystem  = sphereSystem(myEnvironment, myConfiguration);
    case systems.RZ6
        % todo
end
%--------------------------------------------------------------
myPlayer         = experimentPlayer(mySystem, myEnvironment, myConfiguration, myRecordings); %runs the experiment
mainGui          = sphereExperimentGui(mySystem, myEnvironment, myPlayer, myRecordings);

%#############################END#########################################

% author: Ruurd Lof (Radboud University, DCN)
 
% change history

%07-03-24 version 1.16 New coordinate systems 
%15-02-24 version 1.15 Redo of stimulus positions
%15-01-24 version 1.14 
%26-10-23 version 1.13 
%16-10-23 version 1.12 Added Digital Event Recorder
%12-10-23 version 1.11 Fixed readInifile for arrays and cell arrays, added lslRecorder to version  
%29-09-23 version 1.10 Made test and sphere version, changed naming,
%21-09-23 version 1.09 Added STATUS_NEED_RECONNECT status
%21-09-23 version 1.08 Added reconnect button
%20-09-23 version 1.07 Pause button added
%18-09-23 version 1.07 State Machine placed in abstract superClass 
%14-09-23 version 1.06 New state machine for experimentPlayer/new naming 
%13-09-23 version 1.05 
%07-09-23 version 1.04 Major change in trialPlayer and experimentPlayer
%05-09-23 version 1.03
%21-08-23 version 1.02
%01-08-23 version 1.01
%27-06-23 version 1.00

