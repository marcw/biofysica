% RL_INTERPOLANTFUNCSPHEREPRIME Interpolates desired spherical coordinates to the nearest available location.
%
%   [azimuth_out, elevation_out, hemisphere_out] = RL_INTERPOLANTFUNCSPHEREPRIME(azimuth_deg_in, elevation_deg_in, hemisphere_in)
%   maps the desired azimuth and elevation angles (in degrees) to the nearest available real location
%   on a sphere based on predefined sphere sound locations. It outputs the adjusted azimuth,
%   elevation, and hemisphere values corresponding to the nearest real location.
%
%   Inputs:
%       azimuth_deg_in    - Desired azimuth angle in degrees.
%       elevation_deg_in  - Desired elevation angle in degrees.
%       hemisphere_in     - Hemisphere specification ('left', 'right', or other valid identifiers).
%
%   Outputs:
%       azimuth_out       - Adjusted azimuth angle in degrees corresponding to the nearest real location.
%       elevation_out     - Adjusted elevation angle in degrees corresponding to the nearest real location.
%       hemisphere_out    - Adjusted hemisphere corresponding to the nearest real location.
%
%   Example:
%       % Interpolate to the nearest available sphere location
%       [az_out, el_out, hem_out] = RL_interpolantfuncSpherePrime(45, 30, 'left');
%
%   Notes:
%       - The function uses a 'stimulusLocator' object initialized with 'sphere_sound_locations.xlsx'.
%       - Input angles are converted from degrees to radians internally for processing.
%       - The function utilizes 'coordinates_DP' for handling double polar coordinates.
%
%   Dependencies:
%       - stimulusLocator class
%       - coordinates_DP class
%
%   Author: Ruurd Lof
%   Date:   [Your Date Here]
%
%   See also: stimulusLocator, coordinates_DP

function [azimuth_out, elevation_out, hemisphere_out] = RL_interpolantfuncSpherePrime(azimuth_deg_in, elevation_deg_in, hemisphere_in)

    % make stimulus locator object for sphere locations
    stimLocator = stimulusLocator('sphere_sound_locations.xlsx', 'locations_DP');

    % convert double polar coordinates to radians
    azimuth_in = pi * azimuth_deg_in / 180;
    elevation_in = pi * elevation_deg_in / 180;
    
    % make coordinate object for desiredLocation
    desiredLocation_DP = coordinates_DP(azimuth_in, elevation_in, 1, hemisphere_in);
    
    % get real location for stimulus 
    realLocation_DP = stimLocator.desired2realLocation_DP(desiredLocation_DP);

    % set output parameters
    azimuth_out = realLocation_DP.azimuth_deg;
    elevation_out = realLocation_DP.elevation_deg;
    hemisphere_out = realLocation_DP.hemisphere;
end