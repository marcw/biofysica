 % RL_generateRippleFFTtype generates a rippled stimulus by applying both time-domain
% and frequency-domain modulations to an input signal using FFT and IFFT.
%
% INPUTS:
%   signal          - Input broadband signal in the time domain
%   sampleRate      - Sampling rate in Hz
%   density         - Ripple density (ripples per octave)
%   velocity        - Ripple velocity (ripples per second)
%   rippleType      - Type of ripple ('ascending' or 'descending')
%   modulationDepth - Depth of modulation (scaling factor for the ripple amplitude)
%   phi             - Initial phase offset for modulation in radians
%
% OUTPUT:
%   result          - Generated rippled stimulus as a time-domain signal
%
% The function modulates the input signal first in the time domain and then in the
% frequency domain using FFT and IFFT. The modulation depth, ripple density, and
% ripple velocity are applied to create either an ascending or descending ripple
% effect, depending on the specified rippleType. The result is a rippled stimulus 
% combining the original signal with the modulated components.

function result = RL_generateRippleFFTtype(signal, sampleRate, density, velocity, rippleType, modulationDepth, phi)

    ripples_per_octave = density;
    ripples_per_sec    = velocity;    

    % Calculate the number of samples and time vector    
    n             = length(signal);  
    half_n        = floor(n / 2);    
    t             = (1:n)/sampleRate;
    f             = sampleRate * (1:half_n) / n + eps; % eps is a necessary infinitesimal addition
    octaves       = log2(f/100); % octave above 100 Hz   

    broadBandSignal_td = signal;

    % create modulation functions for time domain
    sin_modulation_td = sin(2 * pi * ripples_per_sec * t + phi);
    cos_modulation_td = cos(2 * pi * ripples_per_sec * t + phi);        
    
    % apply time modulations to broadband signal
    sin_modulatedSignal_td = sin_modulation_td .* broadBandSignal_td;
    cos_modulatedSignal_td = cos_modulation_td .* broadBandSignal_td;

    % perform fft 
    sin_modulatedSignal_fd = fft(sin_modulatedSignal_td);
    cos_modulatedSignal_fd = fft(cos_modulatedSignal_td);

    % create modulation functions for frequency domain    
    sin_modulation_fd = sin(2 * pi * ripples_per_octave * octaves);
    cos_modulation_fd = cos(2 * pi * ripples_per_octave * octaves);
    
    % extend the modulation functions for ifft compatibility by adding the mirrored (fliplr) signal
    extended_sin_modulation_fd = [sin_modulation_fd, fliplr(sin_modulation_fd)]; 
    extended_cos_modulation_fd = [cos_modulation_fd, fliplr(cos_modulation_fd)]; 

    % apply extended frequency modulations to time modulated signals in frequency domain
    sin_rippledSignal_fd = extended_sin_modulation_fd .* sin_modulatedSignal_fd;
    cos_rippledSignal_fd = extended_cos_modulation_fd .* cos_modulatedSignal_fd;
      
    % perform inverse fft
    sin_rippledSignal_td =  ifft(sin_rippledSignal_fd, 'symmetric');
    cos_rippledSignal_td =  ifft(cos_rippledSignal_fd, 'symmetric');

    % calculate the rippled signal depending on the ripple type    
    switch rippleType
        case 'ascending'
            rippledSignal_td =   sin_rippledSignal_td + cos_rippledSignal_td;  
        case 'descending'
            rippledSignal_td = - sin_rippledSignal_td + cos_rippledSignal_td;  
    end    

    % calculate the rippled stimulus
    rippledStimulus = broadBandSignal_td + modulationDepth * rippledSignal_td;

    % set result    
    result = rippledStimulus;

end
