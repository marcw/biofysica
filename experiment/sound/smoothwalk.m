function y = smoothwalk(points, dur, sr)
% smoothwalk returns a smooth interpolation (a "walk") over the given points.
%
% Inputs:
%   points - A vector of control points.
%   dur    - Duration over which to interpolate (seconds).
%   sr     - Sample rate (Hz).
%
% Output:
%   y - A vector of interpolated values, one for each sample.
%
% See also RIPPLESOUND

tupoints = linspace(0, dur, numel(points));
tunew = linspace(0, dur, round(dur * sr));
% Use cubic (spline) interpolation to generate a smooth walk.
y = interp1(tupoints, points, tunew, 'spline');
end
