function result = createRippleSpectrogram(par)
    % createRippleSpectrogram - Create the spectogram of ripple stimuli.
    %
    % [RIPPLESPECTROGRAM] = createRippleSpectrogram();
    %   Create the spectogram of a ripple stimuli RIPPLESPECTROGRAM
    %   with time axis T (across columns) and frequency axis F (across
    %   rows). A complete description of such spectrogram (and its stimuli)
    %   is given in:
    %
    %       Chi, Taishih, et al. "Spectro-temporal modulation transfer
    %           functions and speech intelligibility." The Journal of the
    %           Acoustical Society of America 106.5 (1999): 2719-2732.
    %
    %       Elhilali, Mounya, Taishih Chi, and Shihab A. Shamma. "A
    %           spectro-temporal modulation index (STMI) for assessment of 
    %           speech intelligibility." Speech communication 41.2 (2003):
    %           331-348.
    %
    %   Full credit goes to the authors of the papers.
    %
    %   Input
    %
    %       PAR (struct or object) with fields:
    %           par.sampleRate;
    %           par.duration;
    %           par.t_offset;
    %           par.rippleDepth;
    %           par.nBands;
    %           par.fLow;
    %           par.fHigh;
    %           par.phi;
    %           par.rippleVelocity;
    %           par.rippleDensity;
    %           par.rippleType;
    %
    %   Output
    %
    %       RIPPLESPECTROGRAM   Matrix. Ripple spectrogram. Rows is
    %       time-axis, Columns is frequency-axis
    %
    %
    % Created March 2, 2016.
    % Arturo Moncada-Torres
    %   arturo.moncadatorres@med.kuleuven.be
    %   http://www.arturomoncadatorres.com
    %
    %  Modified Juni 2024 by Ruurd Lof
    %  ruurd.lof@donders.ru.nl

    ascending   = 1; % ascending ripples
    descending  = 2; % descending ripples  
        
    sampleRate     = par.sampleRate;
    duration       = par.duration;
    t_offset       = par.t_offset;
    rippleDepth    = par.rippleDepth;
    nBands         = par.nBands;
    fLow           = par.fLow;
    fHigh          = par.fHigh;
    phi            = par.phi;
    rippleVelocity = par.rippleVelocity;
    rippleDensity  = par.rippleDensity;
    rippleType     = par.rippleType; % ascending or descending
    
    % Number of samples.
    nSamples      = duration * sampleRate;    

    % create frequency vector f [Hz].
    f = logspace(log10(fLow), log10(fHigh), nBands);

    % create time vector t [s].
    t = (0:nSamples-1)/sampleRate - t_offset;
    f = log2(f./fLow); % octaves above fLow

    % rippleDensity is positive when descending and negative when ascending
    switch rippleType
        case ascending 
            rippleSign = -1;
        case descending
            rippleSign = 1;
    end
    
    % initialize rippleSpectrogram
    ripples = zeros(nBands,nSamples);
    
    % calculate rippleSpectrogram
    for band = 1:nBands
        for sample = 1:nSamples       
            ripples(band,sample) = cos(2.*pi.*(rippleVelocity .* t(sample) + rippleSign * rippleDensity .* f(band)) + phi);
        end
    end

    % apply ripplesDepth 
    ripples = 0.5 * (1 - rippleDepth * ripples);
    
    % assign result
    result = ripples;
end

