function [y, a] = ripplesound(dur, n, omega, w, delta, phi, f0, fmx, l, a0, sr)
% RIPPLESOUND synthesizes a ripple sound.
% can be used to 
%
% Inputs:
%   dur   - Duration of sound in seconds.
%   n     - Number of sinusoids.
%   omega - Ripple density (cycles/oct); can be scalar or a time series.
%   w     - Ripple velocity (Hz); if scalar, applied uniformly; if a vector,
%           a cumulative sum is taken and normalized by sr.
%   delta - Normalized ripple depth (0 to 1).
%   phi   - Ripple starting phase (radians).
%   f0    - Frequency of the lowest sinusoid (Hz).
%   fmx   - Frequency of the highest sinusoid (Hz).
%   l     - Level in dB.
%   a0    - Reference amplitude.
%   sr    - Sample rate.
%
% Outputs:
%   y - The synthesized waveform.
%   a - The envelope matrix (useful for plotting).
%
% See also: GENRIPPLE
% 
% Based on:
% Escabı́, M. A., & Schreiner, C. E. (2002). Nonlinear Spectrotemporal Sound Analysis by Neurons in the Auditory Midbrain. Journal of Neuroscience, 22(10), 4114–4131. https://doi.org/10.1523/JNEUROSCI.22-10-04114.2002
%
% Modified from Python code by: https://crackedbassoon.com/writing/ripple-sounds

if nargin<1
dur   = 10;             % duration in seconds
n     = 1000;          % number of sinusoids (use a lower value for faster figures)
omega = 1;             % ripple density (cyc/oct)
w     = 8;             % ripple velocity (Hz)
delta = 0.9;           % normalized ripple depth (0 to 1)
f0    = 250;           % lowest frequency (Hz)
fmx   = 8000;          % highest frequency (Hz)
phi   = 0.0;           % ripple starting phase (radians)
l     = 70;            % level in dB (relative to reference amplitude)
a0    = 1e-5;          % reference amplitude
sr    = 44100;         % sample rate (Hz)
% n = 128;
% fmx = oct2bw(250,128/20);
end
nsamples	= round(dur * sr);           % total number of samples
t			= linspace(0, dur, nsamples);         % time vector (1 x m)
idx			= (0:(n-1))';                % sinusoid index (n x 1)

%% Generate logarithmically spaced frequencies between f0 and fm1:
f = f0 * (fmx / f0) .^ (idx / (n - 1));   % (n x 1)

%% Random phases for each sinusoid (n x 1)
sphi = 2 * pi * rand(n, 1);

%% Create sinusoids
% Each row corresponds to one frequency.
% (f * t) yields an (n x m) matrix. The addition of sphi uses implicit
% expansion (each row gets its phase).
s = sin(2 * pi * (f * t) + sphi);

%% Compute the frequency-based coordinate for envelope modulation.
x = log2(f / f0);    % (n x 1)


% nFreq	= 128;
% FreqNr	= 0:1:nFreq-1;
% F0		= oct2bw(250,0);
% df		= 1/20;
% Freq	= F0 * 2.^(FreqNr*df);
% Oct		= FreqNr/20;                   % octaves above the ground frequency
% nTime   = round( (durrip/1000)*Fs ); % # Samples for Rippled Noise
% time	= ((1:nTime)-1)/Fs; % Time (sec)
% T			= 2*pi*vel*time;
% F			= 2*pi*dens*Oct;
% snd	= 0; % 'initialization'
% for ii		= 1:nFreq
% 	Phistat		= 2*pi*Freq(ii).*durstat/1000 + Phi(ii); % phase after stationary period needs to be included
% 	a			= 1+md*sin(T+F(ii));
% 	rip			= a.*sin(2*pi*Freq(ii).* time + Phistat);
% 	snd			= snd+rip;
% end
% 
%% Compute wprime:
if isscalar(w)
	wprime = w * t;  % (1 x m)
else
	% If w is provided as a vector, take its cumulative sum divided by sr.
	wprime = cumsum(w) / sr;
	if iscolumn(wprime)
		wprime = wprime';
	end
end

%% The Envelope
% Create the envelope using explicit expansion. Implicit expansion does not
% seem to work.

% Ensure wprime is replicated to match the size of omega .* x (n×m)
wprime_expanded = repmat(wprime, size(x,1), 1);

% Similarly, if delta is 1×m, replicate it to n×m:
delta_expanded = repmat(delta, size(x,1), 1);

% Then compute the envelope 'a' using element-wise operations:
a = 1 + delta_expanded .* sin(2 * pi * (wprime_expanded + omega .* x) + phi);

%% The Waveform
% Create the waveform by summing the modulated sinusoids,
% applying a 1/sqrt(f) amplitude weighting.
y = sum((a .* s) ./ sqrt(f), 1);

% Normalize the waveform and scale to the desired SPL.
y = y / max(abs(y));
y = y * (a0 * 10^(l / 20));


end
