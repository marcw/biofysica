classdef RL_StimulusParameters < handle
    properties (Access = private)
        %generator parameters
        generator_sampleRate = 48000; % Hz
        generator_flipSignal = false; % flip resulting signal
        generator_method     = 'HMC'; % 'FFT' or 'HMC' (=HarmonicComplex)

        %noise parameters for 'NOI'
        noise_type         = 'pink';        % 'pink' or 'white'
        noise_rms_dB       = -20;           % in dB full scale
        
        %ripple parameters for left ripple and right ripple
        ripple_velocity   = [1, 1];           % Hz
        ripple_density    = [1, 1];             % per octave
        ripple_modDepth   = [1, 1];             % 0 <= moddepth <= 1 
        ripple_phase      = [0, 0];             % 0 <= phase <= 2pi
        ripple_type       = {'ascending', 'descending'};   % 'ascending' or 'descending'        
        
        %connection parameters
        connect_type    = {'RIP', 'RIP'};    % 'RIP' (ripple) or 'NOI' (noise)
        connect_overlap   = 0.01;            % in seconds; connecting overlap of signal1 and signal2   
        
        %band filter parameters
        band_active       = false;
        band_f_min        = 2000;           % Hz 100..band_f_max
        band_f_max        = 8000;           % Hz band_f_min..8000
        band_noiseWeight  = 0.5;              % weight 0..1
        band_signalWeight = 1;              % weight 0..1

        %pre-emphasis filter parameters 
        % Infinite Impulse Response filter (based on AB Harmony))
        emph_active       = false;
        emph_coeffNum     = [0.7688   -1.5376    0.7688];
        emph_coeffDenom   = [1.0000   -1.5299    0.5453];

        %cutter parameters
        cutter_preSplitDur  = 3;           % in seconds; Duration of first signal; see parameter 'connect_channels'
        cutter_postSplitDur = 5;           % in seconds; Duration of second signal; see parameter 'connect_channels'

        %player parameters
        player_active     = true;          % Hz
        
        %plotter parameters
        plotter_active             = true;
        plotter_rmsWindow          = 0.01;      % in seconds; window length for calculating average rms amplitudes
        plotter_spectrograms       = [7];    % collection of plotter channels 1 to 7:  1=NOI, 2=RIP1, 3=RIP2 4=CON, 5=BND, 6=EMPH, 7=CUT        
        plotter_amplitudes         = [7];        % idem                
        plotter_channelNames       = {'noise', 'ripple1', 'ripple2', 'connect', 'band filter', 'emphasis', 'duration cut'}; %       
        plotter_yScale_spectrum    = {'log',   'log'    , 'log'    , 'log'    , 'log'        , 'log'     , 'log'         }; % 'log' or 'lin'
        plotter_yScale_amplitude   = {'log',   'log'    , 'log'    , 'log'    , 'log'        , 'lin'     , 'log'         }; % 'log' or 'lin'
        plotter_ylim_spectrum_log  = [0.1, 10]; % in kHz   
        plotter_ylim_spectrum_lin  = [  0, 10]; % in kHz         
        plotter_ylim_amplitude_log = [-inf, 0]; % -inf = automatic 
        plotter_ylim_amplitude_lin = [0,  inf]; %  inf = automatic         
        
        %save parameters
        save_active       = false;
        save_directory    = 'C:\DATA\RL\RIPPLE_SND'; % Directory must exist
        save_graphicType = 'png'; % 'pdf' or 'png'        
    end

    properties
       asStruct 
       generator
       noise
       ripples
       connect
       band
       emphasis
       cutter
       player
       plotter
       save
    end
    
    methods
        function result = get.asStruct(obj)
           result.generator = obj.generator;
           result.noise     = obj.noise;
           result.ripples   = obj.ripples;
           result.connect   = obj.connect;
           result.band      = obj.band;
           result.emphasis  = obj.emphasis;
           result.player    = obj.player;
           result.plotter   = obj.plotter;
           result.save      = obj.save;
        end

        function result = get.generator(obj)
            result.method     = obj.generator_method;
            result.sampleRate = obj.generator_sampleRate;
            result.flipSignal = obj.generator_flipSignal;
        end

        function result = get.noise(obj)
            result.duration = max(obj.cutter_preSplitDur, obj.cutter_postSplitDur) + obj.connect.overlap/2; % sec;
            result.type     = obj.noise_type;
            result.rms_dB   = obj.noise_rms_dB;
        end

        function result = get.ripples(obj)
            for i = 1:2
                result(i).velocity = obj.ripple_velocity(i); %#ok<AGROW> 
                result(i).density  = obj.ripple_density(i); %#ok<AGROW> 
                result(i).modDepth = obj.ripple_modDepth(i); %#ok<AGROW> 

                % calculate phase
                overlapPhaseShift  = -pi * obj.ripple_velocity(i) * obj.connect.overlap;
                result(i).phase    = obj.ripple_phase(i) + overlapPhaseShift; %#ok<AGROW> 
            end                        
            switch obj.ripple_type{1}
                case 'ascending'
                    result(1).type = 'descending';
                case 'descending'
                    result(1).type = 'ascending';
            end
            result(2).type     = obj.ripple_type{2};
        end      

        function result = get.connect(obj)
            result.overlap = obj.connect_overlap;
            result.type = obj.connect_type;                          
        end        

        function result = get.band(obj)            
            result.active = obj.band_active;
            if (obj.band_f_min > obj.band_f_max)
                error('f_min should be smaller than f_max');
            end             
            result.f_min = obj.band_f_min;
            result.f_max = obj.band_f_max;
            result.noiseWeight = obj.band_noiseWeight;
            result.signalWeight = obj.band_signalWeight;
        end

        function result = get.emphasis(obj)
            result.active = obj.emph_active;
            result.coeffNum      = obj.emph_coeffNum;
            result.coeffDenom      = obj.emph_coeffDenom;
        end

        function result = get.cutter(obj)
            result.preSplitDur  = obj.cutter_preSplitDur;       % sec 
            result.postSplitDur = obj.cutter_postSplitDur;      % sec
        end

        function result = get.player(obj)
            result.active = obj.player_active;
        end

        function result = get.plotter(obj)
            result.active = obj.plotter_active;
            result.spectrograms = obj.plotter_spectrograms;
            result.amplitudes   = obj.plotter_amplitudes;
            result.channelNames = obj.plotter_channelNames;
            result.yScale_spectrum   = obj.plotter_yScale_spectrum;
            result.ylim_spectrum_log = obj.plotter_ylim_spectrum_log;
            result.ylim_spectrum_lin = obj.plotter_ylim_spectrum_lin;
            result.yScale_amplitude  = obj.plotter_yScale_amplitude;
            result.ylim_amplitude_log = obj.plotter_ylim_amplitude_log;
            result.ylim_amplitude_lin = obj.plotter_ylim_amplitude_lin;            
            result.rmsWindow = obj.plotter_rmsWindow;
        end

        function result = get.save(obj)
            result.active      = obj.save_active;
            result.directory   = obj.save_directory;
            result.graphicType = obj.save_graphicType;
        end
    end
end