classdef RL_StimulusGenerator < CodingStrategy

% RL_StimulusGenerator is a subclass of CodingStrategy    
% CodingStrategy is an implementation of the Strategy Design Patern
% ProcUnits are added to the codingStrategy and connected
% Each procUnit creates or manipulates one or more signals.

    properties
        method
        sampleRate
        flipSignal
    end

    methods
        function obj = RL_StimulusGenerator(par) 
            obj.method     = par.generator.method;
            obj.sampleRate = par.generator.sampleRate;
            obj.flipSignal = par.generator.flipSignal;      
            obj.createProcUnits(par);
            obj.connectUnits;
        end

        function createProcUnits(obj, par)
            % Create instances of ProcUnits and add them to obj (=Strategy instance)
            RL_GenerateNoiseUnit( obj, 'NOI' , 1, 4, par.noise);
            RL_RippleSoundUnit(   obj, 'RIP' , 2, 2, par.ripples); % input 1=NOI 
            RL_ConnectSoundsUnit( obj, 'CON' , 4, 2, par.connect); % input 1=RIP_1, 2=RIP_2,  3=NOI_1, 4 = NOI_2
            RL_BandFilterUnit(    obj, 'BND' , 2, 1, par.band);    % input 1=NOI, 2=CON 
            RL_PreEmphasisUnit(   obj, 'EMPH', 1, 1, par.emphasis);% input 1=BND
            RL_CutterUnit(        obj, 'CUT' , 1, 1, par.cutter);% input 1=BND
            RL_PlotterUnit(       obj, 'PLT' , 7, 0, par.plotter); % input 1=NOI, 2=RIP1, 3=RIP2 4=CON, 5=BND, 6=EMPH, 7=CUT
            RL_PlayerUnit(        obj, 'PLAY', 1, 0, par.player);  % input 1=CUT
            RL_SaveSoundUnit(     obj, 'SAVE', 1, 0, par);
        end
    
        function connectUnits(obj)
            %Main processing flow: NOISE >> RIPPLE >> CONNECT >> BAND >> EMPHASIS >> CUT >>SAVE            
            % connect ProcUnits
            obj.connectProcUnits('NOI' , 'OUTPUT_1', 'RIP' , 'INPUT_1');
            obj.connectProcUnits('NOI' , 'OUTPUT_2', 'RIP' , 'INPUT_2');
            obj.connectProcUnits('RIP' , 'OUTPUT_1', 'CON' , 'INPUT_1');
            obj.connectProcUnits('RIP' , 'OUTPUT_2', 'CON' , 'INPUT_2');
            obj.connectProcUnits('NOI' , 'OUTPUT_3', 'CON' , 'INPUT_3');
            obj.connectProcUnits('NOI' , 'OUTPUT_4', 'CON' , 'INPUT_4');
            obj.connectProcUnits('CON' , 'OUTPUT_1', 'BND' , 'INPUT_1');
            obj.connectProcUnits('CON' , 'OUTPUT_2', 'BND' , 'INPUT_2');
            obj.connectProcUnits('BND' , 'OUTPUT_1', 'PLAY', 'INPUT_1');
            obj.connectProcUnits('BND' , 'OUTPUT_1', 'EMPH', 'INPUT_1');
            obj.connectProcUnits('EMPH', 'OUTPUT_1', 'CUT' , 'INPUT_1');
            obj.connectProcUnits('CUT' , 'OUTPUT_1', 'SAVE', 'INPUT_1');
            obj.connectProcUnits('NOI' , 'OUTPUT_1', 'PLT' , 'INPUT_1');
            obj.connectProcUnits('RIP' , 'OUTPUT_1', 'PLT' , 'INPUT_2');
            obj.connectProcUnits('RIP' , 'OUTPUT_2', 'PLT' , 'INPUT_3');
            obj.connectProcUnits('CON' , 'OUTPUT_2', 'PLT' , 'INPUT_4');
            obj.connectProcUnits('BND' , 'OUTPUT_1', 'PLT' , 'INPUT_5');
            obj.connectProcUnits('EMPH', 'OUTPUT_1', 'PLT' , 'INPUT_6');
            obj.connectProcUnits('CUT' , 'OUTPUT_1', 'PLT' , 'INPUT_7');
        end
    end
end