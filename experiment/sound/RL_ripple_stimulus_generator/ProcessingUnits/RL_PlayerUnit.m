classdef RL_PlayerUnit < RL_ProcUnit
    methods
        function obj = RL_PlayerUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            active     = obj.parameters.active;
            sampleRate = obj.parent.sampleRate;
            if active
                signal = obj.getData('INPUT_1');
                sound(signal, sampleRate);
            end
        end
    end
end