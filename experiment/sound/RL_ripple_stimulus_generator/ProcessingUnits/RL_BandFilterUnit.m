classdef RL_BandFilterUnit < RL_ProcUnit

% The processing unit applies a passband filter to the incoming audio signal 
% and adds noise to the signal above and below the passband.

    methods
        function obj = RL_BandFilterUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            active = obj.parameters.active;
            noise = obj.getData('INPUT_1');
            signal = obj.getData('INPUT_2');            
            if active
                outputData = obj.applyFilters(noise, signal);
            else
                outputData = signal; 
            end
            obj.setData('OUTPUT_1', outputData);
        end

    end

    methods (Access =private)

        function result = applyFilters(obj, noise, signal)            
            noiseWeight = obj.parameters.noiseWeight;
            signalWeight = obj.parameters.signalWeight;           
            noiseBand = obj.applyBandFilter(noise, 'bandstopiir');
            signalBand = obj.applyBandFilter(signal, 'bandpassiir');            
            result = noiseWeight * noiseBand + signalWeight * signalBand;
        end

        function result = applyBandFilter(obj, sound, filterType)
            f_min = obj.parameters.f_min;
            f_max = obj.parameters.f_max;
            Fs    = obj.parent.sampleRate;
            % Bandpass filter 
            bpFilter = designfilt(filterType, 'FilterOrder', 12, ...
                                  'HalfPowerFrequency1', f_min, 'HalfPowerFrequency2', f_max, ...
                                  'SampleRate', Fs);
            result = filter(bpFilter, sound);
        end

    end    
end