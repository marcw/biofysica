classdef RL_PlotterUnit < RL_ProcUnit

% This processing unit can create spectrogram plots and amplitude plots
% for all its imputs

    methods
        function obj = RL_PlotterUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            spectrograms = obj.parameters.spectrograms;
            amplitudes   = obj.parameters.amplitudes;
            channelNames = obj.parameters.channelNames;
            nChannels    = length(channelNames);
            active       = obj.parameters.active;
            if active
                for i = 1:nChannels
                    sound = obj.getData(sprintf('INPUT_%d',i));                                     
                    if ismember(i, spectrograms)                                            
                        figure(100 + i);                                           
                        obj.plotSpectrogram(sound, channelNames, i);
                    end
                    if ismember(i, amplitudes)                    
                        figure(200 + i);                                           
                        obj.plotAmplitude(sound, channelNames, i);
                    end
                end
            end
        end

        function plotSpectrogram(obj, sound, channelNames, i)
            yscale   = obj.parameters.yScale_spectrum{i};
            ylim_spectrum_log = obj.parameters.ylim_spectrum_log;
            ylim_spectrum_lin = obj.parameters.ylim_spectrum_lin;
            sampleRate = obj.parent.sampleRate;            
            % window parameters
            window_length = 1024;
            overlap = round(0.9 * window_length); % 90% overlap
            nfft = 2048; % FFT length, >= window_length
            
            % spectrogram
            spectrogram(sound, window_length, overlap, nfft, sampleRate, 'yaxis');            
            set(gca, 'YScale', yscale);             
            switch yscale
                case 'log'
                    ylim(ylim_spectrum_log);
                    title(sprintf('Spectrogram of signal from %s (yscale=log)', channelNames{i})); 
                case 'lin'
                    ylim(ylim_spectrum_lin); % in kHz                   
                    title(sprintf('Spectrogram of signal from %s (yscale=lin)', channelNames{i}));                    
            end            
            ax.YScale = yscale;            
        end

        function plotAmplitude(obj, data, channelNames, i)
            yscale   = obj.parameters.yScale_amplitude{i};
            ylim_amplitude_log = obj.parameters.ylim_amplitude_log;            
            ylim_amplitude_lin = obj.parameters.ylim_amplitude_lin;            
            sampleRate = obj.parent.sampleRate;
            rmsWindowSize = round(obj.parameters.rmsWindow * sampleRate);
            switch yscale
                case 'log'
                    RL_runningrmsPlot_dB(sampleRate, data, rmsWindowSize, channelNames{i});
                    ylim(ylim_amplitude_log);
                    title(sprintf('RMS of signal from %s (yscale=log)', channelNames{i}));                    
                case 'lin'
                    RL_runningrmsPlot(   sampleRate, data, rmsWindowSize, channelNames{i});
                    ylim(ylim_amplitude_lin);
                    title(sprintf('RMS of signal from %s (yscale=lin)', channelNames{i}));                    
            end
        end
    end
end