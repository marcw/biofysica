classdef RL_PreEmphasisUnit < RL_ProcUnit

% The processing unit use an Infinite Impulse Response filter in order to
% lower the low frequencies in order to create an emphasis for frequencies
% that are important for understanding speach.

    methods 
        function obj = RL_PreEmphasisUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            filter_is_active = obj.parameters.active;
            inputData = obj.getData('INPUT_1');
            if filter_is_active
                outputData = obj.applyFilter(inputData);
            else                
                outputData = inputData;
            end
            obj.setData('OUTPUT_1', outputData);
        end        
    end    

    methods (Access = private)
        function result = applyFilter(obj, data)
            coeffNum = obj.parameters.coeffNum;
            coeffDenom = obj.parameters.coeffDenom;
            result = filter(coeffNum, coeffDenom, data);
        end
    end
end