classdef RL_CutterUnit < RL_ProcUnit

% The processing unit use an Infinite Impulse Response filter in order to
% lower the low frequencies in order to create an emphasis for frequencies
% that are important for understanding speach.

    methods 
        function obj = RL_CutterUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            inputData = obj.getData('INPUT_1');
            outputData = obj.applyCutter(inputData);
            obj.setData('OUTPUT_1', outputData);
        end        
    end    

    methods (Access = private)
        function result = applyCutter(obj, data)
            n_split     = length(data)/2;
            n_preSplit  = obj.parameters.preSplitDur  * obj.parent.sampleRate;
            n_postSplit = obj.parameters.postSplitDur * obj.parent.sampleRate;
            n_left_cut  = n_split - n_preSplit + 1;
            n_right_cut = n_split + n_postSplit;
            signal = data(n_left_cut: n_right_cut);
            if ~obj.parent.flipSignal
                result = signal;
            else
                result = fliplr(signal);
            end
        end
    end
end