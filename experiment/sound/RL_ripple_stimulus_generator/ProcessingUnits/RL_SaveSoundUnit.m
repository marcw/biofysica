classdef RL_SaveSoundUnit < ProcUnit
    properties
        parameters
    end
    methods
        function obj = RL_SaveSoundUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@ProcUnit(parent, ID, nInputs, nOutputs);
            obj.parameters = parameters;
        end

        function run(obj)
            sound = obj.getData('INPUT_1');
            obj.saveSound(sound);
        end

        function saveSound(obj, sound)
            parameterStruct = obj.parameters.asStruct;
            sampleRate = obj.parameters.generator.sampleRate;
            directory  = obj.parameters.save.directory;
            doSave     = obj.parameters.save.active;
            graphicType = obj.parameters.save.graphicType;
            % create audio filename
            fname          = 'Ripple_Sound_%s';
            current_time   = datetime('now');
            datetimestr    = datestr(current_time, 'yyyy-mm-dd__HH-MM-SS');
            fn             = sprintf(fname,datetimestr);
            wavfile = [fn, '.wav'];
            matfile = [fn, '.mat'];
            pdffile = [fn, '.pdf'];
            pngfile = [fn, '.png'];
            audioFilename  = fullfile(directory, wavfile);
            paramFilename  = fullfile(directory, matfile);
            pdfFilename    = fullfile(directory, pdffile);
            pngFilename    = fullfile(directory, pngfile);
            % write audio to file
            if doSave    
                % save audio as .wav
                audiowrite(audioFilename, sound, sampleRate);
                % save parameters as .mat                
                save(paramFilename, "parameterStruct");
                % save spectrogram as pdf or png
                obj.makeSpectrogram(sound);
                switch graphicType 
                    case 'png'
                        exportgraphics(gcf, pngFilename, 'Resolution', 300);
                        close(gcf);
                    case 'pdf'
                        exportgraphics(gcf, pdfFilename);
                end
                close(gcf);
            end

        end

        function makeSpectrogram(obj, sound)
            %yscale   = obj.parameters.yscale;
            sampleRate = obj.parent.sampleRate;            
            % window parameters
            window_length = 1024;
            overlap = round(0.9 * window_length); % 90% overlap
            nfft = 2048; % FFT length, >= window_length            
            % spectrogram
            figure(999);
            spectrogram(sound, window_length, overlap, nfft, sampleRate, 'yaxis'); 
            ax = gca;
            ax.YScale = 'log';
            title('Spectrogram');
        end        

    end    
end 