classdef RL_ConnectSoundsUnit < RL_ProcUnit

    methods
        function obj = RL_ConnectSoundsUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            signalsIn(1,:) = obj.getData('INPUT_1');
            signalsIn(2,:) = obj.getData('INPUT_2');
            signalsIn(3,:) = obj.getData('INPUT_3');
            signalsIn(4,:) = obj.getData('INPUT_4');

            [noiseOut, signalOut] = obj.connectSignals(signalsIn);

            obj.setData('OUTPUT_1', noiseOut);
            obj.setData('OUTPUT_2', signalOut);
        end
    end

    methods (Access = private)   
        function [noise, signal] = connectSignals(obj, signalsIn)
            channels     = obj.parameters.type;
            sampleRate   = obj.parent.sampleRate;
            overlap      = obj.parameters.overlap;
            %flip         = obj.parameters.flip;
            n = length(signalsIn(1,:));
            signals = zeros(2, n);
            for i = 1:2
                switch channels{i}
                    case 'RIP'
                        signals(i,:) = signalsIn(i,:);
                    case 'NOI'
                        signals(i,:)  = signalsIn(3,:);                        
                end
            end
            noise  = RL_connectSignalsWithRamp(fliplr(signalsIn(4,:)), signalsIn(4,:), sampleRate, overlap);
            signal = RL_connectSignalsWithRamp(fliplr(signals(  1,:)), signals(  2,:), sampleRate, overlap);
        end
    end
end