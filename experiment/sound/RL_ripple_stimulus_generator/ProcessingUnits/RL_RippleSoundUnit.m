classdef RL_RippleSoundUnit < RL_ProcUnit %#ok<*AGROW> 
    methods
        function obj = RL_RippleSoundUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end        

        function run(obj)
            signal         = obj.getData('INPUT_1');

            rippleSound1 = obj.generateRippleSound(signal, 1);
            rippleSound2 = obj.generateRippleSound(signal, 2);
            
            obj.setData('OUTPUT_1', rippleSound1);
            obj.setData('OUTPUT_2', rippleSound2);
        end 

    end

    methods (Access = private)
        function result = generateRippleSound(obj, signal, i)
            method          = obj.parent.method;
            sampleRate      = obj.parent.sampleRate;
            density         = obj.parameters(i).density; 
            velocity        = obj.parameters(i).velocity; 
            modulationDepth = obj.parameters(i).modDepth;
            phase           = obj.parameters(i).phase;
            rippleType      = obj.parameters(i).type;
            duration        = length(signal)/sampleRate;            
            % create rippleSound
            switch method
                case 'FFT'
                    rippleSound = RL_generateRippleFFTtype(signal, sampleRate, density, velocity, rippleType, modulationDepth, phase);
                case 'HMC'
                    rippleSound = RL_generateRippleHarmonicComplextype(duration, sampleRate, density, velocity, rippleType, modulationDepth, phase);
            end
            % rescale rms rippleSound to rms signal
            result = rippleSound * (rms(signal)/rms(rippleSound));
        end
    end    
end