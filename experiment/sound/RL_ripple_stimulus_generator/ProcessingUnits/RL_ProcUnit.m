classdef RL_ProcUnit < ProcUnit
    properties
        parameters
    end
    methods
        function obj = RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@ProcUnit(parent, ID, nInputs, nOutputs);
            obj.parameters = parameters;
        end
    end
end