classdef RL_GenerateNoiseUnit < RL_ProcUnit
    methods
        function obj = RL_GenerateNoiseUnit(parent, ID, nInputs, nOutputs, parameters)
            obj = obj@RL_ProcUnit(parent, ID, nInputs, nOutputs, parameters);
        end

        function run(obj)
            outputNoise1 = obj.generateNoise;
            outputNoise2 = obj.generateNoise;
            outputNoise3 = obj.generateNoise;
            outputNoise4 = obj.generateNoise;
            % set output
            obj.setData('OUTPUT_1', outputNoise1);
            obj.setData('OUTPUT_2', outputNoise2);
            obj.setData('OUTPUT_3', outputNoise3);
            obj.setData('OUTPUT_4', outputNoise4);
        end        
    end

    methods (Access = private)
        function result = generateNoise(obj)
            sampleRate = obj.parent.sampleRate;
            duration   = obj.parameters.duration;
            type       = obj.parameters.type;
            output_rms = db2mag(obj.parameters.rms_dB); 
            n = 2*round(duration * sampleRate/2);  % must be an even number
            % create pink or white noise
            switch type
                case 'pink'
                    inputNoise = pinknoise(1,n);
                case 'white'
                    inputNoise = randn(1,n);
            end

            % set the correct loudness
            input_rms = rms(inputNoise);
            result = output_rms * inputNoise / input_rms;
        end
    end
end