clc;clear;
close all;

% By RL
% Generates two part combination signals based on noise and rippled noise 
% Plots and/or saves the generated signals
% Based on Strategy Design Patern (Framework)

%% Create stimulus parameters
par = RL_StimulusParameters; % go to RL_StimulusParameters for editing the program parameters

%% Create stimulus generator
myGenerator = RL_StimulusGenerator(par); 

%% run strategy
myGenerator.run();



