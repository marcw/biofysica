% RL_generateRippleHarmonicComplextype generates a ripple harmonic complex stimulus
% with specific parameters for duration, sample rate, ripple density, velocity, 
% modulation depth, and type of ripple (ascending or descending).
%
% INPUTS:
%   duration        - Duration of the stimulus in seconds
%   sampleRate      - Sampling rate in Hz
%   density         - Ripple density (ripples per octave)
%   velocity        - Ripple velocity (ripples per second)
%   rippleType      - Type of ripple ('ascending' or 'descending')
%   modulationDepth - Depth of modulation (scaling factor for the ripple amplitude)
%   phi             - Initial phase offset for modulation in radians
%
% OUTPUT:
%   result          - Generated ripple harmonic complex stimulus as a time-domain signal
%
% The function calculates a time-domain signal by modulating a set of sine waves
% across a logarithmically spaced frequency range (100 Hz to 8200 Hz). The modulation
% is applied based on the specified ripple type and parameters, resulting in either
% an ascending or descending ripple effect.

function result = RL_generateRippleHarmonicComplextype(duration, sampleRate, density, velocity, rippleType, modulationDepth, phi)

    ripples_per_octave = density;
    ripples_per_sec    = velocity;

    % Calculate the number of samples and time vector    
    n_td          = round(duration * sampleRate);   % number of points in time domain
    n_fd          = 256;                            % Number of points in frequency domain
    t             = (1:n_td)/sampleRate;            % Time vector
    f_low         = 100;                            % Lower frequency bound
    f_high        = 8200;                           % Upper frequency bound
    f             = logspace(log10(f_low), log10(f_high), n_fd); % Frequency vector
    randomPhase   = (rand(1, n_fd) * 2 * pi)';         % Random phase for all frequencies


    % Create a meshgrid for frequency and time to properly handle the dimensions
    [T, F] = meshgrid(t, f); % T is time, F is frequency

    % conver frequencies to octaves 
    octaves  = log2(F/100); % Octaves above 100 Hz

    % Create carrier waves with random phases
    carriers = sin(2 * pi * F .* T + randomPhase);

    % Calculate phase for T and octaves meshgrids
    T_phase = 2 * pi * ripples_per_sec    * T;
    F_phase = 2 * pi * ripples_per_octave * octaves;
    
    % Create modulation functions for frequency domain
    % Determine ripple type
    switch rippleType
        case 'ascending'
            modulationMatrix = sin(T_phase - F_phase + phi);
        case 'descending'
            modulationMatrix = sin(T_phase + F_phase + phi);
        otherwise
            error('Invalid rippleType. Choose either ''ascending'' or ''descending''.');
    end        
    amplitudeMatrix = 1 + modulationDepth * modulationMatrix;

    % Apply modulation to carriers
    modulatedCarriers = amplitudeMatrix .* carriers;

    % Sum modulated carriers across frequencies (rows)
    rippledStimulus = sum(modulatedCarriers, 1); 
    
    result = rippledStimulus; % Return the generated ripple harmonic complex stimulus
end
