function exampleDMR

close all
clearvars
% This script synthesizes, plots, and plays ripple sounds 
%
% It creates three sets of ripple sounds:
%   1. Static ripple sounds with slight parameter variations.
%   2. Moving ripple sounds with different drift rates.
%   3. Dynamic moving ripple sounds where some parameters evolve smoothly.
%
% See also Escabı́, M. A., & Schreiner, C. E. (2002). Nonlinear
% Spectrotemporal Sound Analysis by Neurons in the Auditory Midbrain.
% Journal of Neuroscience, 22(10), 4114–4131.
% https://doi.org/10.1523/JNEUROSCI.22-10-04114.2002 if you want to create
% a dynamic moving ripple with properties like speech

% The time-varying stimulus parameters were generated in the MATLAB
% programming environment. First, the parameters were generated
% as a random sequence of normally distributed samples (randn function in
% MATLAB) using a sampling rate of 3 Hz for Fm(t) and 6 Hz for  (t).
% These sequences had maximum frequency contents of 1.5 and 3 Hz,
% respectively (because the maximum signal frequency is half of the
% sampling frequency). To generate the acoustic sound waveforms at a
% sampling rate of 44.1 kHz (Eq. 4) it was necessary to resample both of the
% parameter signals to an equivalent sampling rate. Therefore, we upsampled
% both signals to 44.1 kHz using a cubic interpolation procedure
% (interp1 function in MATLAB; “cubic” option; upsampling factor,
% 	Escabı´ and Schreiner • Spectrotemporal Sound Analysis in the Auditory Midbrain J. Neurosci., May 15, 2002, 22(10):4114–4131 4115
% 	14,700 for modulation rate and 7350 for ripple density). Next we needed
% 		to convert the parameter amplitudes from a normal to a uniform distribution
% 		so that the probability of occurrence of each parameter is statistically
% 		unbiased within the selected intervals. This normalization was
% 		performed with the error function:
% 		erf  x    2/   0
% 		x
% 		e t2dt.
% 		This function converts normally distributed amplitudes to uniformly
% 		distributed amplitudes over the interval  1 to  1 and a subsequent
% 		linear rescaling of the amplitudes to the selected interval. This operation
% 		had only a subtle effect on the spectrum of these signals and is necessary
% 		to guarantee that the signal parameters are statistically unbiased (flat
% 		distribution) within their predefined range.

%% Default parameters
rng(0);                % set random seed
dur   = 10;             % duration in seconds
n     = 100;          % number of sinusoids (use a lower value for faster figures)
omega = 1;             % ripple density (Hz)
w     = 8;             % ripple drift (Hz)
delta = 0.9;           % normalized ripple depth (0 to 1)
f0    = 250;           % lowest frequency (Hz)
fm1   = 8000;          % highest frequency (Hz)
phi   = 0.0;           % ripple starting phase (radians)
l     = 70;            % level in dB (relative to reference amplitude)
a0    = 1e-5;          % reference amplitude
sr    = 44100;         % sample rate (Hz)

% %% 1. Static Ripple Sounds
% disp('Making static ripple sounds');
% figure('Name','Static Ripple Sounds');
% uw = 0;
% for i = 1:3
% 
% 	% Modify parameters based on subplot index
% 	if i == 1
% 		uomega = 1.5;
% 	else
% 		uomega = omega;
% 	end
% 	if i == 2
% 		udelta = 0.5;
% 	else 
% 		udelta = delta;
% 	end
% 	fprintf('Sound with omega=%.2f, w=%.2f, and delta=%.2f\n', uomega, uw, udelta);
% 	[y, a] = ripplesound(dur, n, uomega, uw, udelta, phi, f0, fm1, l, a0, sr);
% 
% 	% Play sound and pause until finished (dur + extra time)
% 	sound(y, sr);
% 	pause(dur + 0.5);
% 
% 	% Plot envelope (subplot order 1,2,3)
% 	subplot(1,3,i);
% 	plotenv(a, i==1);
% 	title(sprintf('Static %d', i));
% 	nicegraph;
% end
% % Uncomment the following line to save the figure
% % saveas(gcf, 'staticripples.svg');
% 
% %% 2. Moving Ripple Sounds
% disp('Making moving ripple sounds');
% figure('Name','Moving Ripple Sounds');
% uws = [4, 8, -4];  % different drift values for each subplot
% for i = 1:3
% 	uomega = omega;
% 	uw     = uws(i);
% 	udelta = delta;
% 	fprintf('Sound with omega=%.2f, w=%.2f, and delta=%.2f\n', uomega, uw, udelta);
% 	[y, a] = ripplesound(dur, n, uomega, uw, udelta, phi, f0, fm1, l, a0, sr);
% 
% 	sound(y, sr);
% 	pause(dur + 0.5);
% 
% 	subplot(1,3,i);
% 	plotenv(a, i==1);
% 	title(sprintf('Moving %d', i));
% 	nicegraph;
% end
% % saveas(gcf, 'movingripples.svg');



%% 3. Dynamic Moving Ripple Sounds
disp('Making dynamic moving ripple sounds');
figure('Name','Dynamic Ripple Sounds');
for i = 1:3
	if i == 1
		% Varying delta via a smooth random walk
		udelta = smoothwalk(rand(1,10), dur, sr);
		uomega = omega;
		uw     = w;
	elseif i == 2
		% Varying omega: 5 values at 1 then 5 values at 1.5
		udelta = delta;
		uomega = smoothwalk([ones(1,5), ones(1,5)*1.5], dur, sr);
		uw     = w;
	elseif i == 3
		% Varying w over time with four control points
		udelta = delta;
		uomega = omega;
		uw     = smoothwalk([-8, 0, 4, 8], dur, sr);
	end
	% Print parameter size information (similar to Python’s print(shape))
	if i == 1
		fprintf('Delta size: [%s]\n', num2str(size(udelta)));
	elseif i == 2
		fprintf('Omega size: [%s]\n', num2str(size(uomega)));
	elseif i == 3
		fprintf('w size: [%s]\n', num2str(size(uw)));
	end

	[y, a] = ripplesound(dur, n, uomega, uw, udelta, phi, f0, fm1, l, a0, sr);
	soundsc(y, sr);
	pause(dur + 0.5);

	subplot(1,3,i);
	plotenv(a, i==1);
	title(sprintf('Dynamic %d', i));
	nicegraph;

	
end
% saveas(gcf, 'dynamicripples.svg');
end

%% --- Function Definitions ---

function plotenv(a, addLabels)
% plotenv plots the envelope matrix.
%
% Inputs:
%   a         - The envelope matrix (n x m).
%   addLabels - If true, axis labels are added.

imagesc(a, [0 2]);  % display with color limits set to [0,2]
axis xy;            % ensure the vertical axis is oriented correctly
if ~addLabels
	set(gca, 'XTick', [], 'YTick', []);
else
	xlabel('Time');
	ylabel('Frequency (log2 scale)');
end
% colormap jet;
colorbar;
end