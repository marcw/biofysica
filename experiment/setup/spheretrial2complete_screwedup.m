function spheretrial2complete(dname)
% SPHERETRIAL2COMPLETE Combines SPHERE trial files into a single file.
%
% Usage:
%   spheretrial2complete(dname)
%
% Or go to directory containing sphere files:
%   spheretrial2complete

%
% Input:
%   dname - Directory name containing SPHERE trial files.
%
% See also SPHEREMAT2HOOPCSV, SPHEREMAT2HOOPDAT

%% Initialization
if nargin < 1 || isempty(dname)
    d = what;
    dname = d.path;
else
    validateattributes(dname, {'char', 'string'}, {'nonempty'}, mfilename, 'dname', 1);
end

if ~isfolder(dname)
    error('Directory does not exist: %s', dname);
end

cd(dname);
fileInfo = dir('*.sphere');
if isempty(fileInfo)
    error('No .sphere files found in the directory: %s', dname);
end

%% Find unique blocks
fileNames = {fileInfo.name}';
uniqueBlocks = unique(substr(fileNames, 1, strlength(fileNames)-12));

%% Load data per block
nBlocks = numel(uniqueBlocks);
for blockIdx = 1:nBlocks
    blockName = uniqueBlocks{blockIdx};
    files = dir([blockName, '*.sphere']);
    [data, trial] = loadDataFromFiles(files);
    
    % Save combined data for the block
    saveBlockData(data, trial, blockName, dname);
end
end

function [data, trial] = loadDataFromFiles(files)
% Loads data from a list of files and aggregates it.
data = [];
trial = [];

for fIdx = 1:numel(files)
    fprintf('Loading %s\n', files(fIdx).name);
    loadedData = load(files(fIdx).name, '-mat');
    
    if isfield(loadedData, 'data')
        data = [data, loadedData.data]; %#ok<AGROW>
    end
    
    if isfield(loadedData, 'dur')
        trialsingle.duration = loadedData.dur;
        trial = [trial, trialsingle]; %#ok<AGROW>
    end
end
end

function saveBlockData(data, trial, blockName, dname)
% Saves the aggregated data and trial information to a file.
fname = sprintf('%s.sphere', blockName);
fullFileName = fullfile(dname, fname);
save(fullFileName, 'data', 'trial', '-v7.3');
fprintf('Saved combined data to %s\n', fullFileName);
end

function result = substr(strings, startIndex, length)
% Substring extraction for cell arrays of strings.
result = cellfun(@(s) s(startIndex:min(startIndex+length-1, strlength(s))), strings, 'UniformOutput', false);
end
