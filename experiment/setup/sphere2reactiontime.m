function sphere2reactiontime(dname)
% SPHERE2REACTIONTIME
%
% Usage:
%   sphere2reactiontime(dname)
%
% Or go to directory containing sphere files:
%   sphere2reactiontime
%
% Converts trial data stored in sphere mat files in one csv tabular file.
%
% The csv file contains:
% - nLED: number of LEDs (without fixation LED)
% - nSound: number of sounds
% - durLed: duration of LEDs (ms)
% - ISI: interstimulus interval (ms)
%
% This csv file does not contain:
% - Type: red or green LED, sound file
% - Intensity
% - On and offset
% - Sound duration
% - Location of LEDs and SNDs
% - duration LED1 and LED2
% this is assumed to be fixed in the BlindPhosphene studies.
%
%
%
% Input:
%   dname - Directory name containing SPHERE trial files.
%
% See also SPHEREMAT2HOOPCSV, SPHEREMAT2HOOPDAT

%% Initialization
if nargin < 1 || isempty(dname)
	d = what;
	dname = d.path;
else
	validateattributes(dname, {'char', 'string'}, {'nonempty'}, mfilename, 'dname', 1);
end

if ~isfolder(dname)
	error('Directory does not exist: %s', dname);
end

cd(dname);
fileInfo = dir('*.sphere');
if isempty(fileInfo)
	error('No .sphere files found in the directory: %s', dname);
end

%% Find unique blocks
fileNames		= {fileInfo.name}';
uniqueBlocks	= unique(substr(fileNames, 1, strlength(fileNames)-12));

%% Load data per block
nBlocks = numel(uniqueBlocks);
for blockIdx = 1:nBlocks
	blockName	= uniqueBlocks{blockIdx};
	files		= dir([blockName, '*.sphere']);
	T			= loadDataFromFiles(files);

	% Save combined data for the block
	saveBlockData(T, blockName, dname);
end
end

function T = loadDataFromFiles(files)
% Loads data from a list of files and aggregates it.
nfiles		= numel(files);
dur			= NaN(nfiles,1);
nSound		= dur;
nLED		= dur;
ISI			= dur;
RT			= dur;
nSeen = dur;

for fIdx = 1:nfiles
	fprintf('Loading %s\n', files(fIdx).name);
	loadedData = load(files(fIdx).name, '-mat');
	

	btn			= loadedData.data.raw(:,4);
	Fs			= loadedData.cfg.medusaFs;
	btn			= find([0;diff(btn)]>0.5); % sample
	try
	btn			= btn(1)/Fs*1000; % ms
	RT(fIdx)	= btn;
	catch
		% do nothing
	end
	
	%%
	
	nstim = loadedData.trialsingle.nstim;
	ledcnt = 0;
	sndcnt = 0;
	
	selled = false(nstim,1);
	on  = NaN(nstim,1);
	off  = NaN(nstim,1);
	
	for stmIdx = 1:nstim
		modality = loadedData.trialsingle.stim(stmIdx).modality;
		try
			on(stmIdx)		= loadedData.trialsingle.stim(stmIdx).ondelay;
			off(stmIdx)		= loadedData.trialsingle.stim(stmIdx).offdelay;
		catch
			% do nothing
		end

		if strcmpi(modality,'LED')
			selled(stmIdx)=true;
			ledcnt = ledcnt+1;
			d(stmIdx) = off(stmIdx)-on(stmIdx);
		end

		if strcmpi(modality,'sound')
			sndcnt=sndcnt+1;
		end
	
		
	end
	
	nLED(fIdx)		= ledcnt-1; % remove fixation led
	nSound(fIdx)	= sndcnt;
	
	try
		sel = selled & cumsum(selled)>1;
		on = on(sel);
		off = off(sel);
		ISI(fIdx) = on(2)-off(1);
		dur(fIdx) = off(1)-on(1);
	catch
		ISI(fIdx) = NaN;
		dur(fIdx) = NaN;
	end
	
end

% keyboard
T = table(dur,nLED,nSound,ISI,RT,nSeen);
% T

end

function saveBlockData(T, blockName, dname)
% Saves the aggregated data and trial information to a file.
fname = sprintf('%s.csv', blockName);
fullFileName = fullfile(dname, fname);
writetable(T,fullFileName)
% save(fullFileName, 'data', 'trial', '-v7.3');
fprintf('Saved combined data to %s\n', fullFileName);
end

function result = substr(strings, startIndex, length)
% Substring extraction for cell arrays of strings.
result = cellfun(@(s) s(startIndex:min(startIndex+length-1, strlength(s))), strings, 'UniformOutput', false);
end
