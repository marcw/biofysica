%************************************************************************
% 
%                   vocoder_RL::bandFilterClass
%
%************************************************************************

% The bandFiltersClass calculates the filter spectra for a number of bands.
% It makes use of the pyramidShaperClass to shape the filters.


classdef bandFiltersClass < handle

% n frequencies are evenly distributed on a logarithmic scale

    properties (Access = private)
        MIN_NR_OF_BANDS = 1;
        MAX_NR_OF_BANDS = 500;
    end
    
    properties (Access =public)
       bandParameters       
       bottomFreqs
       centreFreqs
       topFreqs
    end
    
    methods (Access = public)
    
        function obj = bandFiltersClass(nBands, fLow, fHigh)
           obj.checkBands(nBands);           
           obj.bandParameters.size = nBands;
           obj.bandParameters.domain = 1:nBands;
           obj.bandParameters.lowestFreq_Hz = fLow;
           obj.bandParameters.highestFreq_Hz = fHigh;           
           [obj.bottomFreqs, obj.centreFreqs, obj.topFreqs] = obj.calculateBandsFreqs;
        end %constructor     
        
    end % public methods

    methods (Access = private)       
    
        function checkBands(obj, nBands)
            if nBands < obj.MIN_NR_OF_BANDS
                Error = MException('rl_vocoderCISim:inputParError', 'nBands low than MIN_NR_OF_BANDS');
                throw(Error);
            end
            if nBands > obj.MAX_NR_OF_BANDS
                Error = MException('rl_vocoderCISim:inputParError', 'nBands exceeds MAX_NR_OF_BANDS');
                throw(Error);
            end        
        end % checkBands
    
        function [bottomFreqs, centreFreqs, topFreqs] = calculateBandsFreqs(obj)
    
           log_lowestFreq  = log10(obj.bandParameters.lowestFreq_Hz);
           log_highestFreq = log10(obj.bandParameters.highestFreq_Hz);
           log_bandWidth = (log_highestFreq - log_lowestFreq)/(obj.bandParameters.size);

           % initialize arrays
           log_bottomFreqs = NaN(1,obj.bandParameters.size);
           log_centreFreqs = NaN(1,obj.bandParameters.size);
           log_topFreqs    = NaN(1,obj.bandParameters.size);          
        
           for iBand = obj.bandParameters.domain                              
               log_bottomFreqs(iBand) = log_lowestFreq + (iBand - 1.0) * log_bandWidth;
               log_centreFreqs(iBand) = log_lowestFreq + (iBand - 0.5) * log_bandWidth;
               log_topFreqs(iBand)    = log_lowestFreq + (iBand      ) * log_bandWidth;       
           end
        
           bottomFreqs = 10.^log_bottomFreqs;  
           centreFreqs = 10.^log_centreFreqs;  
           topFreqs    = 10.^log_topFreqs; 
        end % calculateBandsFreqs

    end % private methods

end %  classdef