classdef pyramidShaperClass < bandShaperClass
    properties (Access = private)
        decay = 40; % in db per octave
    end

    methods (Access = public)

        function obj = pyramidShaperClass
            obj@bandShaperClass;
        end % constructor

        function result = calcShape(obj, bandFrequency, spectralDomain)
            %***************shorts*******************
            freqIndexes = 1:length(spectralDomain);
            shape       = zeros(size(spectralDomain));
            freqStep    = spectralDomain(2) - spectralDomain(1);
            %****************************************
            for iFreq = freqIndexes
                octaves = calcOctaves(spectralDomain(iFreq), bandFrequency);
                if (spectralDomain(iFreq) <= bandFrequency)
                    shape(iFreq) = dBtoLevel(obj.decay * octaves); % db per octave time octaves == dB
                else
                    shape(iFreq) = dBtoLevel(-obj.decay * octaves);
                end %if                             
            end %for
            % rescale for equal spectral weight 
            result = (bandFrequency/freqStep) * shape/sum(shape); 
        end % calcShape
       
    end % public methods

end %classdef

% utility functions
function result = calcOctaves(frequency, centreFreq) 
    result = log2(frequency/centreFreq);         
end

function level = dBtoLevel(dB)
    level = 10^(dB/20);
end


