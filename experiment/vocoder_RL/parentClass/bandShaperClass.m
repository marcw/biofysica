%************************************************************************
% 
%                   vocoder_RL::bandShaperClass 
%
%************************************************************************

% The bandShaperClass is the abstract parent class for specific band shaping
% classes like the pyramidShaperClass.
% Its main purpose is to specify the calcShape method.

classdef bandShaperClass < handle
    properties (Access = protected)
    end
    
    methods (Access = public)
        function obj = bandShaperClass
        end %constructor
    end
    
    methods (Abstract)
        result = calcShape(obj, bandFrequency, spectralDomain);
    end
end

