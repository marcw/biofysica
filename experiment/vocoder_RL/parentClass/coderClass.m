%************************************************************************
% 
%                   vocoder_RL::coderClass 
%
%************************************************************************

% The coderClass is the parent class of encoderClass and decoderClass.
% Its main purpose is to specify the association with the bandfilterClass 
% and the fftParameterClass, and to define the parameter vocoderFreq.


classdef coderClass < handle
    
    properties (Access = protected)        
        fftParameters
        bandFilters  
    end

    methods
        function obj = coderClass(bandFilters, fftParameters)            
            obj.bandFilters      = bandFilters;
            obj.fftParameters    = fftParameters;
        end %constructor     
    end
end