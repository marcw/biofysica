%************************************************************************
% 
%                   vocoder_RL::decoderClass 
%
%************************************************************************

% The decoderClass decodes the envelope signals back into a sound signal.
% It makes use of the bandFilterClass.


classdef decoderClass < coderClass
    properties (Access = private)
        bandShaper
    end % properties

    methods (Access = public)

        function obj = decoderClass(bandShaper, bandFilters, fftParameters) 
            obj@coderClass(bandFilters, fftParameters);
            obj.bandShaper = bandShaper;
        end % constructor

        function vocodedSound = decode(obj, envelopeSignals)

            %****************shorts*******************            
            bandDomain     = obj.bandFilters.bandParameters.domain;
            nBlocks        = obj.fftParameters.blocks.size;
            nFreqs         = obj.fftParameters.freqs.size;            
            blockDomain    = obj.fftParameters.blocks.domain;
            freqDomain     = obj.fftParameters.freqs.domain;            
            spectralDomain = obj.fftParameters.freqs.values;
            nFFTbins       = obj.fftParameters.nFFTbins;
            hop            = obj.fftParameters.hop;            
            %******************************************

            filterSpectra = obj.calcFilterSpectra(spectralDomain);            

            vocodedSpectrumBlocks     = zeros(nFreqs,nBlocks);
            vocodedSpectrumBandBlocks = zeros(nFreqs, nBlocks);
            
            for iBlock = blockDomain
               for iBand = bandDomain                           
                 vocodedSpectrumBandBlocks(freqDomain, iBlock) = envelopeSignals(iBand, iBlock) * filterSpectra(iBand, freqDomain);
                 vocodedSpectrumBlocks(    freqDomain, iBlock) = vocodedSpectrumBlocks(freqDomain, iBlock) + vocodedSpectrumBandBlocks(freqDomain, iBlock);               
               end               
            end

            randomizedVocodedSpectrumBlocks = rwl_randomizePhase(vocodedSpectrumBlocks); % add scrambled phase info
            %vocodedSound = istft(randomizedVocodedSpectrumBlocks, nFFTbins, nFFTbins, hop)';            
            vocodedSound = istft(randomizedVocodedSpectrumBlocks, nFFTbins)';            
        end % decode

    end % public methods    

    methods (Access = private)

        function result = calcFilterSpectra(obj, spectrumDomain)
            %****************shorts*******************            
            bandDomain = obj.bandFilters.bandParameters.domain;
            nBands     = obj.bandFilters.bandParameters.size;
            nFreqs     = length(spectrumDomain);            
            %******************************************            
            filterSpectra = zeros(nBands, nFreqs);
            for iBand = bandDomain
                bandFrequency = obj.bandFilters.centreFreqs(iBand);                
                filterSpectra(iBand,:) = obj.bandShaper.calcShape(bandFrequency, spectrumDomain);                
            end
            result = filterSpectra;
        end % calcFilterSpectra

    end % private methods

end % classdef

    
