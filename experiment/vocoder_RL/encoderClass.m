%************************************************************************
% 
%                   vocoder_RL::encoderClass 
%
%************************************************************************

% The encoderClass encodes a sound signals into envelope signals:
% Step 1: calculate short-time fourier transform of sound, 
%         which results in time blocks with spectra
% Step 2: calculate for all time blocks and bands the weight of the
%         spectrum in a band, which results in envelope signals (in time) 
%         for all bands  

classdef encoderClass < coderClass

    properties
    end
    
    methods (Access = public)

        function obj = encoderClass(bandFilters, fftParameters)
            obj@coderClass(bandFilters, fftParameters);
        end %constructor

        function envelopeSignals = encode(obj, sound) 
            %****************shorts*******************            
            nFFTbins       = obj.fftParameters.nFFTbins;
            hop            = obj.fftParameters.hop;
            %******************************************

            %blockSpectra = abs(stft(sound', nFFTbins, nFFTbins, hop));  %stft: Short-time fourier transform                          
            blockSpectra = abs(stft(sound', nFFTbins));  
            envelopeSignals = obj.calcEnvelopeSignals(blockSpectra);
        end %encode

        function envelopeSignals = calcEnvelopeSignals(obj, blockSpectra)
            [~, nBlocks] = size(blockSpectra);            
            obj.fftParameters.setBlocksSize(nBlocks);
            %****************shorts********************            
            nBands      = obj.bandFilters.bandParameters.size;
            bandDomain  = obj.bandFilters.bandParameters.domain;
            nBlocks     = obj.fftParameters.blocks.size;
            blockDomain = obj.fftParameters.blocks.domain; 
            binStep     = obj.fftParameters.freqs.step;
            %******************************************            
            
            envelopeSignals = zeros(nBands, nBlocks);            
            for iBand = bandDomain
                %****************shorts********************            
                topFreq    = obj.bandFilters.topFreqs(iBand);
                bottomFreq = obj.bandFilters.bottomFreqs(iBand);
                %******************************************                        
                NrOfbinsInBand = (topFreq - bottomFreq)/binStep; %bandWidth in bins
                for iBlock = blockDomain                    
                    spectralWeight = obj.calcBandSpectralWeight(blockSpectra(:,iBlock), topFreq, bottomFreq);                    
                    envelopeSignals(iBand, iBlock) = spectralWeight/NrOfbinsInBand; % normalize to average spectralWeight per bin in the band.
                end
            end            
        end % envelopeSignals

    end % public methods

    methods (Access = private)        
           
        function bandWeight = calcBandSpectralWeight(obj, spectrum, topFreq, bottomFreq)            
            %****************shorts********************
            frequencies = obj.fftParameters.freqs.values';            
            maxFreq = max(frequencies);
            nFreq   = length(frequencies);
            %******************************************
            % resample spectrum from 128 --> 10001 points for more
            % precision in applying a bandpass filter.
            nX = 2001;
            xDomain = 1:nX;
            xSpectrum = maxFreq * (xDomain-1)/(nX-1); % xSpectrum is array from 0 to maxFreq            
            spectrumY = interp1(frequencies,  vertcat(0, spectrum), xSpectrum');
            %******************************************            
            bandSpectrumY = zeros(nX, 1);
            % apply bandpass filter 
            for i = xDomain           
                factor = ((bottomFreq < xSpectrum(i)) && (xSpectrum(i) < topFreq)); % factor is 1 inside the band and 0 outside the band
                bandSpectrumY(i) = factor * spectrumY(i);
            end
            %******************************************
            % sum bandSpectrum and renormalize
            bandWeight = (nFreq/nX) * sum(bandSpectrumY);            
        end % calcBandSpectralWeight

    end % private methods
   
end % classdef