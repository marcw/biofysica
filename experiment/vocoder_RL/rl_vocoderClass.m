%**************************************************************************
%                                                                         
%                    vocoder_RL::rl_vocoderClass V2.5                          
%                                                                           
%**************************************************************************
%                         by Ruurd Lof 2023
%**************************************************************************
% change log:
% V2.2 refactorization
% V2.3 added encoder- and decoderBandFiltersClass
% V2.4 no filtering in the encoder, but only averaging the band weights of
%      the original sound.
% V2.5 Bandshaper moved to decoderClass

%**************************************************************************

% The vocoderClass encapsulates an encoder and a decoder.
% It also creates two instances of bandFiltersClass
% (one for the encoder and one for the decoder) 
% and an instance of fftParametersClass which are needed for both the
% encoder and the decoder.

% The vocoding process is a two step process
% SoundIn --> Encode(SoundIn) --> enveloppeSignals --> 
% Decode(enveloppeSignals) --> SoundOut (=vocoded sound)

% Band are The space between fLow and fHigh will be equally divided in n
% bands on a logarithmic scale.

% The algorithm uses pyramid shaped filters that overlap.
% The enveloppe signals are calculated differently (no prefiltering)

% The vocoder frequency is the internally used sample frequency of
% the sound.

%**************************************************************************

classdef rl_vocoderClass < handle

    properties (Access = private)
        encoder
        decoder        
    end %properties

    properties (Access = private)
        vocoderFrequency
    end

    methods (Access = public)
        function obj = rl_vocoderClass(nBands, fLow, fHigh, vocoderFrequency) 
            obj.vocoderFrequency = vocoderFrequency;
            bandFilters          = bandFiltersClass(nBands, fLow, fHigh);
            fftParameters        = fftParametersClass(vocoderFrequency);            
            obj.encoder          = encoderClass(bandFilters, fftParameters);
            obj.decoder          = decoderClass(bandFilters, fftParameters);            
        end %constructor

        function soundOut = vocode(obj, soundIn, soundFrequency)
            soundData        = resample(soundIn, obj.vocoderFrequency, soundFrequency);
            envelopesSignals = obj.encoder.encode(soundData);
            decodedSound     = obj.decoder.decode(envelopesSignals); 
            soundOut         = resample(decodedSound, soundFrequency, obj.vocoderFrequency);            
        end

    end % public methods

end % classdef