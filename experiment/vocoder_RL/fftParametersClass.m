%************************************************************************
% 
%                   vocoder_RL::fftParametersClass
%
%************************************************************************

% The fftParametersClass provides the fftParameters for the vocoder.

classdef fftParametersClass < handle
    properties
        vocoderFrequency % Sampling frequency of the sound
        nFFTbins      = 256;   % Size of the FFT in TD   
        binShift      = 0.25;  %shift per bin in FFT time domain (==75% overlap)
        blocks
        freqs        
    end   
    
    methods (Access = public)
        function obj = fftParametersClass(vocoderFrequency)
           obj.vocoderFrequency = vocoderFrequency;
           obj.freqs.size = 1+obj.nFFTbins/2;           
           obj.freqs.step = obj.vocoderFrequency/obj.nFFTbins;
           obj.freqs.domain = 1:obj.freqs.size;
           obj.freqs.values = obj.freqs.step * (obj.freqs.domain-1);           
           obj.blocks.size = 0;
           obj.blocks.domain = [];           
        end % constructor

        function setBlocksSize(obj, value)
            obj.blocks.size      = value;
            obj.blocks.domain    = 1:value;            
        end % setBlocksSize
        
        function result = hop(obj)
           result = obj.nFFTbins*obj.binShift;
        end % hop

    end % public methods

end % classdef
