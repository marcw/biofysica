classdef EyeSeeCamSci_EyeTrace < handle

    properties
        RAS
        HVF
        DP
        rotationMatrices
        nrofpoints
        pupilRotation
    end

    properties (Constant)
        calibrationRange = 1:25;
    end


    methods
        function obj = EyeSeeCamSci_EyeTrace(rot_x_deg, rot_y_deg, rot_z_deg) 
            rot_y = deg2rad(rot_y_deg);
            rot_z = deg2rad(rot_z_deg);            
            n = height(rot_y_deg);
            obj.nrofpoints = n;
            rot_x = zeros(n, 1);                        
            rot_y = obj.startingPointCalibration(rot_y);            
            rot_z = obj.startingPointCalibration(rot_z);            
            rot_XYZ = coordinates_XYZ(rot_x, rot_y, rot_z);
            rot_RAS = transform_device2RAS(rot_XYZ, definition_EyeSeeCamSci_XYZ2RAS);
            obj.rotationMatrices = obj.calcRotMatrices(rot_RAS);
            obj.RAS = obj.calcEyeDirection(obj.rotationMatrices);
            obj.HVF = transform_RAS2HVF(obj.RAS);
            obj.DP  = transform_HVF2DP(obj.HVF);
            obj.pupilRotation = rot_x_deg;
        end
    end

    methods (Access = private)
        function result = startingPointCalibration(obj, rotation)
            average = mean(rotation(obj.calibrationRange));             
            result = rotation - average;
        end

        function result = calcRotMatrices(obj, rotations)
            unitMatrix = eye(3);            
            matrices = repmat({unitMatrix}, obj.nrofpoints, 1);                  
            for i = 1:obj.nrofpoints  
                Mx = Rx(rotations.right(i));
                Mz = Rz(rotations.superior(i));
                matrices{i} = Mx * Mz;       
            end % for loop
            result = matrices;
        end

        function result = calcEyeDirection(obj, rotMatrices)
            startingDirection = [0; 1; 0];
            direction = coordinates_RAS(startingDirection);
            for i = 2: obj.nrofpoints
                newDirection = rotMatrices{i} * startingDirection;
                direction.add(newDirection);
            end
            result = direction;
        end
    end
end