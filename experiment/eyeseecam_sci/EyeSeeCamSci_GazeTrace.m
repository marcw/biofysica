classdef EyeSeeCamSci_GazeTrace
   properties
       RAS
       HVF
       DP
       nrofpoints
   end

   methods 
       function obj = EyeSeeCamSci_GazeTrace(headRotMatrices, eyeRotMatrices)
           obj.nrofpoints = length(headRotMatrices);
           obj.RAS = obj.calcGazeDirections(headRotMatrices, eyeRotMatrices);
           obj.HVF = transform_RAS2HVF(obj.RAS);
           obj.DP  = transform_HVF2DP(obj.HVF);
       end
   end

   methods
       function result = calcGazeDirections(obj, headRotMatrices, eyeRotMatrices)
            startingDirection = [0; 1; 0];
            direction = coordinates_RAS(startingDirection);
            for i = 2: obj.nrofpoints
                newDirection = headRotMatrices{i} * eyeRotMatrices{i} * startingDirection;
                direction.add(newDirection);
            end
            result = direction;        
       end
   end
end