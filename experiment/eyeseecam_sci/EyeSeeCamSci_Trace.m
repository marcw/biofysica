classdef EyeSeeCamSci_Trace < handle
    properties
        head
        eye
        gaze
    end

    properties (Access = private)

    end
    methods
        function obj = EyeSeeCamSci_Trace(timeRange, head_Vx_deg, head_Vy_deg, head_Vz_deg, eye_X_deg, eye_Y_deg, eye_Z_deg)            
            obj.head = EyeSeeCamSci_HeadTrace(timeRange, head_Vx_deg, head_Vy_deg, head_Vz_deg);
            obj.eye  = EyeSeeCamSci_EyeTrace(eye_X_deg, eye_Y_deg, eye_Z_deg);
            obj.gaze = EyeSeeCamSci_GazeTrace(obj.head.rotationMatrices, obj.eye.rotationMatrices);
        end
    end
end