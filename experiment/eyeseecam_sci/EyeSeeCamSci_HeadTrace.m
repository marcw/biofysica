classdef EyeSeeCamSci_HeadTrace < handle

    properties
        HVF
        DP
        RAS        
        rotationMatrices
        nrofpoints
        timeRange
    end

    methods
        function obj = EyeSeeCamSci_HeadTrace(timeRange, Vx_deg, Vy_deg, Vz_deg)
            n = height(timeRange);            
            obj.nrofpoints = n;   
            obj.timeRange  = timeRange;
            % convert to radians
            Vx             = deg2rad(Vx_deg);
            Vy             = deg2rad(Vy_deg);
            Vz             = deg2rad(Vz_deg);

            rotSpeed_XYZ   = coordinates_XYZ(Vx, Vy, Vz);
            rotSpeed_RAS   = transform_device2RAS(rotSpeed_XYZ, definition_EyeSeeCamSci_XYZ2RAS);            
            obj.rotationMatrices = obj.calcRotMatrices(rotSpeed_RAS);

            obj.RAS        = obj.calcHeadDirection(obj.rotationMatrices);
            obj.HVF        = transform_RAS2HVF(obj.RAS);
            obj.DP         = transform_HVF2DP(obj.HVF);
        end
    end

    methods (Access = private)

        function result = calcRotMatrices(obj, rotSpeed) 
           
            % initialize the parameters that change in the loop    
            unitMatrix = eye(3); % define 3D identity matrix
            rotMatrices = repmat({unitMatrix}, 3, 1);  
                
            for i = 2:obj.nrofpoints
        
                delta_t = obj.timeRange(i) - obj.timeRange(i-1); 
                
                deltaAngle_right    = delta_t * rotSpeed.right(i);
                deltaAngle_anterior = delta_t * rotSpeed.anterior(i);
                deltaAngle_superior = delta_t * rotSpeed.superior(i);
            
                % calculate the rotation matrices between time = t and t+delta_t;
                rotMatrix_right    = R1(deltaAngle_right);
                rotMatrix_anterior = R2(deltaAngle_anterior);
                rotMatrix_superior = R3(deltaAngle_superior);
            
                % calculate total rotation matrix between time = t and t+delta_t;
                % N.B. order is not important since angles are small        
                deltaRotMatrix = rotMatrix_right * rotMatrix_anterior * rotMatrix_superior;
            
                % Calculate the total rotation away from the startOrientation at time = t      
                % N.B. the multiplication order here is important:         
                rotMatrices{i} = rotMatrices{i-1} * deltaRotMatrix;
            
            end % for loop
            result = rotMatrices;
        end

        function result = calcHeadDirection(obj, rotMatrices)
            startingDirection = [0; 1; 0];
            direction = coordinates_RAS(startingDirection);
            for i = 2: obj.nrofpoints
                newDirection = rotMatrices{i} * startingDirection;
                direction.add(newDirection);
            end
            result = direction;
        end
    end
end