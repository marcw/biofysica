function slims2dat
% SLIMS2DAT
%
% Resamples and reshapes PupilLabs structure data to a matrix. Does this
% for all mat-files in the current folder.
%
% USE:
% - change current folder to relevant data folder (single subject, single
% session)
% - type in SLIMS2DAT in command window.
%
% See also: SLIMS_SACCADE_PARADIGM, RESHAPE, RESAMPLE, SACDET

% TODO:
% - convert 'parameters' to 'stimuli','configuration','data'
% - remove poor data (or incorporate into sacdet)
% - targets in correct coordinate system

%% Initialization
% if nargin<1
% 	dname = '/Users/marcw/Dropbox/Manuscript/stage/2122/Jesse Janssen/data/001-22-08-03'; % change this
% 	cd(dname);
%
% end
close all;

%% Labbook
disp('-- load labbook --');
labbook		= fullfile('..','..','..','Labbook.xlsx'); % relative path to labbook (mat/subject/session)
T			= readtable(labbook); % read the spreadsheet, and put this in a table, containing things like subject_id and paradigm


%% Determine filenames in current folder
% this should be one session of one subject
disp('-- experimental block names --');
d		= dir('*.mat');
fnames	= {d.name};
disp(char(fnames)); % display experimental block filenames
nfiles	= numel(fnames); % number of files

%% Preprocess

for ii = 1:nfiles
	% for ii = 1
	fname = fnames{ii};
	disp(fname);
	
	id		= str2double(fname(4:7)); % subject id
	datum	=  datestr(['20' fname(14:21)],'dd-mmm-yyyy'); % date in text format
	block	= str2double(fname(9:12)); % block
	sel			= T.subject_id==id & T.block_id==block & T.date_dd_mm_yyyy_==datum;
	paradigm	= char(T.paradigm(sel));
	switch paradigm
		case 'saccade'
			
			if strcmp(table2cell(T(sel,4)),'Calibration')
				preprocess1(fname)
				preprocess0(fname)
			else
				preprocess1(fname)
			end
			
		case 'doublestep'
			preprocess2(fname)
	end
	
end

function preprocess1(fname)

%% Load
% load(fname,'stimuli','configuration','data');
load(fname,'data','stimuli');

pldata			= data.pupildata;
evdata			= data.eventdata;
tar				= stimuli.target;

%% get time stamps and align
tpl				= lsl_correct_lsl_timestamps(pldata);
tev				= lsl_correct_lsl_timestamps(evdata);

% tpl			= pldata.Timestamps;
% tev			= evdata.Timestamps;



d = [median(diff(tev)) diff(tev)];

sel = d<1; % select trial duration smaller than 1 s
if sum(sel)
	idx = find(sel);
	d1 = tev(idx-1)-tev(idx-2);
	d2 = tev(idx+1)-tev(idx);
	
	
	
	warning(['extra event, at trial: ' num2str(idx)]);
	
	figure(200)
	clf
	subplot(221)
	plot(tev,'.-')
	hold on
	subplot(222)
	plot(d,'.-')
	hold on

	tev = tev(~sel);
	
	
	d = [median(diff(tev)) diff(tev)];
	
	figure(200)
	subplot(221)
	plot(tev,'.-')
	
	subplot(222)
	plot(d,'.-')
	
	
	subplot(223)
	plotpost(d');
	verline(d1,'r');
	verline(d2,'b');	
end


%%
ntrials			= length(tev)/2;


%%
fs				= 200;

%% at the moment the stamps for the fixation and the target are all in one vector
% remove the time offset (1st sample)
% t0				= tpl(1);
t0				= tev(1);
tpl				= tpl - t0;
events			= tev - t0;
tareve			= events(2:2:end); % event start for the 2nd dot = target in saccade paradigm

% What is each column???
%% Saccade detection for reaction time calculation
% should go once fixed
HVF				= pldata.Data(13:15,:)'; % horizontal, vertical and frontal
pl_confidence	= pldata.Data(1,:); % confidence levels

p				= sqrt(HVF(:,1).^2+HVF(:,2).^2);
v				= gradient(p,1/200);

sel				= pl_confidence>0.8;
v(~sel)			= NaN;


crit			= 3*nanstd(v);
sel				= v>crit;
selon			= [0;diff(sel)]; % onsets (+1)
onset			= find(selon==1);


ntar			= numel(tareve);
RT				= NaN(ntar,1);
for ii = 1:ntar
	sel = tpl>tareve(ii) & tpl<(tareve(ii)+1);
	% 	sum(sel)
	idx = find(sel);
	sel = onset>idx(1) & onset<idx(end);
	if sum(sel)
		rt = onset(sel)-idx(1);
		rt = rt/fs*1000; % ms
		RT(ii) = rt(1);
	end
end
sel				= isnan(RT);
RT				= RT(~sel); %#ok<*NASGU>


figure(300)
clf
plotpost(RT,'xlim',[0 800]);


keyboard
%%

%% Resample to fixed 200 Hz
% Or should we say that the sampling rate was fixed?
% Potential problem: if LSL includes jitter, then this noise will be added
% to the data. Problem becomes clear when determining velocity, i.e.
% diff(x)./diff(t).
HVF				= resample(HVF,tpl,fs);


%% Experimental block
% let's define a trial as 0.4 s before target onset, and 1.5 s after onset
pretarget	= 400; % ms
posttarget	= 1500; % ms

idx			= repmat((-pretarget/1000*fs):(posttarget/1000*fs),ntrials,1); % which data per trial
sel = idx(1,:)<=0;
disp([sum(sel) sum(~sel)])
idx			= round(idx+tareve'*fs); % index per time re target onset (note: events are not resampled, as that is not needed)
% convert HVF to matrix NTRIALS x NSAMPLES
[H,V,F]		= deal(HVF(:,1),HVF(:,2),HVF(:,3));
H			= H(idx);
V			= V(idx);
F			= F(idx);

%%


% figure(301)
% clf
% 
% pvel = max(smv);
% pos = abs(sqrt(H.^2+V.^2));
% pamp = max(pos');
% 
% plot(pamp,pvel,'.');
% nicegraph;

%% There is something wrong HERE
nsample		= (pretarget+posttarget)/1000*fs;
t			= 1:(nsample+1);
t			= t/fs-pretarget/1000;
[vel,smv,acc,sma]          = getvel(H',V',fs,0.01);

sel = t<=0;
disp([sum(sel) sum(~sel)])

nsac = size(H,1);
figure(1)
clf
for ii = 1:nsac
	subplot(311)
	plot(t,H(ii,:));
	% hold on
	title(fname);
	subplot(312)
	plot(t,V(ii,:));
	% hold on
	
	subplot(313)
	plot(t,vel(:,ii));
	hold on
	plot(t,smv(:,ii));
	hold off
	% hold on
	pause
end
%%
keyboard

%% Convert to dat and csv file
% create trial structure
[ntrials,~]		= size(H);
nstim					= 3;

trial(ntrials).nstim	= nstim;

for ii = 1:ntrials
	trial(ii).nstim = nstim;
	for jj = 1:nstim
		switch jj
			case 1
				trial(ii).stim(jj).modality		= 'LED';
				trial(ii).stim(jj).azimuth		= 0;%  6) azimuth location
				trial(ii).stim(jj).elevation	= 0;%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 0;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 200;%  200 ms into the trial, target at 400 ms, so gap at 200 ms
				trial(ii).stim(jj).intensity	= 100;%  6) azimuth location
			case 2
				trial(ii).stim(jj).modality		= 'LED';
				trial(ii).stim(jj).azimuth		= tar(ii,1);%  6) azimuth location
				trial(ii).stim(jj).elevation	= tar(ii,2);%  6) azimuth location
				trial(ii).stim(jj).ondelay		= pretarget;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= posttarget;%  6) azimuth location
				trial(ii).stim(jj).intensity	= 100;%  6) azimuth location
			case 3
				trial(ii).stim(jj).modality		= 'data acquisition';
				trial(ii).stim(jj).azimuth		= [];%  6) azimuth location
				trial(ii).stim(jj).elevation	= [];%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 0;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= pretarget+posttarget;%  6) azimuth location
				trial(ii).stim(jj).intensity	= [];%  6) azimuth location
		end
	end
end

% conversion
slims2hoop(H,V,F,fname,trial);

function preprocess2(fname)

%% Load
% load(fname,'stimuli','configuration','data');
load(fname,'data','configuration','stimuli');

pldata			= data.pupildata; % what parameters are in here?
evdata			= data.eventdata;

tar1				= stimuli.target;
tar2				= stimuli.norm_2target;

%%
x				= (tar2(:,1) - configuration.screenpix(:,1)/2)/configuration.screenpix(:,1);
y				= (tar2(:,2) - configuration.screenpix(:,2)/2)/configuration.screenpix(:,2);
z				= configuration.distoscreen;
tar2(:,1)		= atand(x./z); % degrees
tar2(:,2)		= atand(y./sqrt(x.^2+z.^2));


%% get time stamps and align
tpl				= lsl_correct_lsl_timestamps(pldata);
tev				= lsl_correct_lsl_timestamps(evdata);

% tpl			= pldata.Timestamps;
% tev			= evdata.Timestamps;



d = [median(diff(tev)) diff(tev)];

% sel = d<1; % select trial duration smaller than 1 s
% if sum(sel)
% 	idx = find(sel)
% 	
% % 	d1 = [tev(idx-1)-tev(idx-2)];
% % 	d2 = [tev(idx+1)-tev(idx) ];
% 	
% 	
% 	
% 	warning(['extra event, at trial: ' num2str(idx)]);
% 	
% 	figure(200)
% 	clf
% 	subplot(221)
% 	plot(tev,'.-')
% 	hold on
% 	subplot(222)
% 	plot(d,'.-')
% 	hold on
% 
% 	tev = tev(~sel);
% 	
% 	
% 	d = [median(diff(tev)) diff(tev)];
% 	
% 	figure(200)
% 	subplot(221)
% 	plot(tev,'.-')
% 	
% 	subplot(222)
% 	plot(d,'.-')
% 	
% 	
% 	subplot(223)
% 	plotpost(d');
% 	verline(d1,'r');
% 	verline(d2,'b');	
% end


%%
ntrials			= length(tev)/3;
fs				= 200;


%% at the moment the stamps for the fixation and the target are all in one vector
% remove the time offset (1st sample)
% t0				= tpl(1);
t0				= tev(1);
tpl				= tpl - t0;
events			= tev - t0;
fixeve			= events(1:3:end); % event start for the 1st dot = central fixation in doublestep paradigm
tar1eve			= events(2:3:end); % event start for the 2nd dot = target in doublestep paradigm
tar2eve         = events(3:3:end); % event start for the 2nd dot = target in doublestep paradigm

% What is each column???
HVF				= pldata.Data(13:15,:)'; % horizontal, vertical and frontal?
pl_confidence	= pldata.Data(1,:); % confidence levels


p = sqrt(HVF(:,1).^2+HVF(:,2).^2);
v = gradient(p,1/200);

sel = pl_confidence>0.8;
v(~sel) = NaN;


crit = 3*nanstd(v);
sel = v>crit;
selon                                       = [0;diff(sel)]; % onsets (+1)
onset = find(selon==1);





%%


%% Resample to fixed 200 Hz
HVF				= resample(HVF,tpl,fs);
pl_confidence		= resample(pl_confidence,tpl,fs);


%% Experimental block
idx			= repmat((-0.4*fs):(1.6*fs),ntrials,1); % which data per trial
idx			= round(idx+tar1eve'*fs); % index per time re target onset (note: events are not resampled)

% convert HVF to matrix NTRIALS x NSAMPLES
[H,V,F]		= deal(HVF(:,1),HVF(:,2),HVF(:,3));
H			= H(idx);
V			= V(idx);
F			= F(idx);

ti			= 1000*linspace(-0.4,1,length(H));


%% Convert to dat and csv file
% create trial structure
[ntrials,~]		= size(H);
nstim					= 4;

trial(ntrials).nstim	= nstim;

for ii = 1:ntrials
	trial(ii).nstim = nstim;
	for jj = 1:nstim
		switch jj
			case 1
				trial(ii).stim(jj).modality		= 'LED';
				trial(ii).stim(jj).azimuth		= 0;%  6) azimuth location
				trial(ii).stim(jj).elevation	= 0;%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 0;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 200;%  6) azimuth location
				trial(ii).stim(jj).intensity	= 100;%  6) azimuth location
			case 2
				trial(ii).stim(jj).modality		= 'LED';
				trial(ii).stim(jj).azimuth		= tar1(ii,1);%  6) azimuth location
				trial(ii).stim(jj).elevation	= tar1(ii,2);%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 400;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 450;%  6) azimuth location
				trial(ii).stim(jj).intensity	= 100;%  6) azimuth location
			case 3
				trial(ii).stim(jj).modality		= 'LED';
				trial(ii).stim(jj).azimuth		= tar2(ii,1);%  6) azimuth location
				trial(ii).stim(jj).elevation	= tar2(ii,2);%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 500;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 550;%  6) azimuth location
				trial(ii).stim(jj).intensity	= 100;%  6) azimuth location
			case 4
				trial(ii).stim(jj).modality		= 'data acquisition';
				trial(ii).stim(jj).azimuth		= [];%  6) azimuth location
				trial(ii).stim(jj).elevation	= [];%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 0;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 2000;%  6) azimuth location
				trial(ii).stim(jj).intensity	= [];%  6) azimuth location
		end
	end
end

% conversion
slims2hoop(H,V,F,fname,trial);

function preprocess0(fname)

%% Load
% load(fname,'stimuli','configuration','data');
load(fname,'data','configuration','stimuli');

pldata			= data.pupildata; % what parameters are in here?
evdata			= data.eventdata;
tar				= stimuli.target;

%% get time stamps and align
tpl				= lsl_correct_lsl_timestamps(pldata);
tev				= lsl_correct_lsl_timestamps(evdata);

% tpl			= pldata.Timestamps;
% tev			= evdata.Timestamps;



d = [median(diff(tev)) diff(tev)];

sel = d<1; % select trial duration smaller than 1 s
if sum(sel)
	idx = find(sel);
	d1 = tev(idx-1)-tev(idx-2);
	d2 = tev(idx+1)-tev(idx);
	
	
	
	warning(['extra event, at trial: ' num2str(idx)]);
	
	figure(200)
	clf
	subplot(221)
	plot(tev,'.-')
	hold on
	subplot(222)
	plot(d,'.-')
	hold on

	tev = tev(~sel);
	
	
	d = [median(diff(tev)) diff(tev)];
	
	figure(200)
	subplot(221)
	plot(tev,'.-')
	
	subplot(222)
	plot(d,'.-')
	
	
	subplot(223)
	plotpost(d');
	verline(d1,'r');
	verline(d2,'b');	
end


%%
ntrials			= length(tev)/2;
fs				= 200;

%% at the moment the stamps for the fixation and the target are all in one vector
% remove the time offset (1st sample)
% t0				= tpl(1);
t0				= tev(1);
tpl				= tpl - t0;
events			= tev - t0;
tareve			= events(2:2:end); % event start for the 2nd dot = target in saccade paradigm
fixeve			= events(1:2:end); % event start for the 1st dot = central fixation in saccade paradigm

% What is each column???
HVF				= pldata.Data(13:15,:)'; % horizontal, vertical and frontal?
pl_confidence	= pldata.Data(1,:); % confidence levels



p = sqrt(HVF(:,1).^2+HVF(:,2).^2);
v = gradient(p,1/200);

sel = pl_confidence>0.8;
v(~sel) = NaN;


crit = 3*nanstd(v);
sel = v>crit;
selon                                       = [0;diff(sel)]; % onsets (+1)
onset = find(selon==1);



%%
ntar = numel(tareve);

RT = NaN(ntar,1);
for ii = 1:ntar
	sel = tpl>tareve(ii) & tpl<(tareve(ii)+1);
	% 	sum(sel)
	idx = find(sel);
	sel = onset>idx(1) & onset<idx(end);
	if sum(sel)
		rt = onset(sel)-idx(1);
		rt = rt/fs*1000; % ms
		RT(ii) = rt(1);
	end
end
sel = isnan(RT);
RT = RT(~sel);

%%


%% Resample to fixed 200 Hz
HVF				= resample(HVF,tpl,fs);
pl_confidence		= resample(pl_confidence,tpl,fs);



%% Calibration block
idx			= repmat(-0.3*fs:0,ntrials,1);
ev = [fixeve(2:end) tareve(end)+1];
idx			= round(idx+ev'*fs);


[H,V,F]		= deal(HVF(:,1),HVF(:,2),HVF(:,3));
H			= H(idx);
V			= V(idx);
F			= F(idx);
ti			= 1000*linspace(-0.3,0,length(H));


figure(1)
subplot(323)
plot(H');

subplot(324)
plot(V');

subplot(325)
plot(tar(:,1),mean(H,2),'o');
nicegraph;
ylim([-1 1]);
xlim([-30 30]);

subplot(326)
plot(tar(:,2),mean(V,2),'o');
nicegraph;
ylim([-1 1]);
xlim([-30 30]);

%% Convert to dat and csv file
% create trial structure
[ntrials,~]		= size(H);
nstim					= 2;

trial(ntrials).nstim	= nstim;

for ii = 1:ntrials
	trial(ii).nstim = nstim;
	for jj = 1:nstim
		switch jj
			case 1
				trial(ii).stim(jj).modality		= 'LED';
				trial(ii).stim(jj).azimuth		= tar(ii,1);%  6) azimuth location
				trial(ii).stim(jj).elevation	= tar(ii,2);%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 0;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 300;%  6) azimuth location
				trial(ii).stim(jj).intensity	= 100;%  6) azimuth location
				
			case 2
				trial(ii).stim(jj).modality		= 'data acquisition';
				trial(ii).stim(jj).azimuth		= [];%  6) azimuth location
				trial(ii).stim(jj).elevation	= [];%  6) azimuth location
				trial(ii).stim(jj).ondelay		= 0;%  6) azimuth location
				trial(ii).stim(jj).offdelay		= 300;%  6) azimuth location
				trial(ii).stim(jj).intensity	= [];%  6) azimuth location
		end
	end
end

% conversion
slims2hoop(H,V,F,fname,trial,'-cal');

function slims2hoop(H,V,F,fname,trial,cal)

%%
[~,name,~]	= fileparts(fname);


%%
session = name([1:7 13:21]);
subject = name(4:7);

fpath = fullfile('..','..','..','preprocessed',subject,session);

if ~exist(fpath,'dir')
	mkdir(fpath)
end

%%
if nargin>5
	name = [name cal];
end
ext             = '.dat';
datfile         = fcheckext(name,ext);
datfile			= fullfile(fpath,datfile);
ext             = '.csv';
csvfile         = fcheckext(name,ext);
csvfile			= fullfile(fpath,csvfile);

dlm				= ';';

%% Load data

[ntrials,nsamples]	= size(H);

%% Create dat-file
fid = fopen(datfile,'w','l');
for ii = 1:ntrials
	[x,y,z] = deal(H(ii,:)', V(ii,:)', F(ii,:)');
	d = [x y z zeros(length(x),5)]; % the configuration of the SPHERE coils is different from HOOP coils
	fwrite(fid,d,'float');
end
fclose(fid);


%% Create csv file


%% Header
Repeats			= 1;
ntrials_norep	= ntrials/Repeats;
Random			= 0;
nchan			= 8;
ITI				= [0 0];
Fs				= 200;
% first line, general configuration
first			= [0 ntrials_norep Repeats ntrials ITI Random nchan];
dlmwrite(csvfile,first,dlm)

% next lines, channel configuration
M				= NaN(nchan,6);
for ii			= 1:nchan
	M(ii,:)			= [0 ii ii round(Fs) round(Fs) nsamples];
end
dlmwrite(csvfile,M,'-append','delimiter',dlm)

%% Write trial information
fid = fopen(csvfile,'a+');
for trlIdx = 1:ntrials
	nstim = trial(trlIdx).nstim;
	for stmIdx = 1:nstim
		fprintf(fid,'%d%c',trlIdx,dlm); % 1) trial number
		fprintf(fid,'%d%c',stmIdx,dlm); % 2) stimulus number in trial
		fprintf(fid,'%d%c',round(mean(ITI)),dlm); % 3) Desired Inter Trial Interval
		fprintf(fid,'%d%c',round(mean(ITI)),dlm); % 4) Actual inter trial interval
		switch trial(trlIdx).stim(stmIdx).modality
			case 'LED'
				mod = 'led';
				attr = 0; % default
			case 'sound'
				mod = 'snd1';
				attr = trial(trlIdx).stim(stmIdx).matfile;
				attr = str2double(attr(4:end-4));
			case 'trigger'
				mod = 'trg0';
				attr = 0; % default
			case 'data acquisition'
				mod = 'acq';
				attr = 0; % default
		end
		fprintf(fid,'%s%c',mod,dlm);% 5) Modality of stimulus
		fprintf(fid,'%f%c',trial(trlIdx).stim(stmIdx).azimuth,dlm);%  6) azimuth location
		fprintf(fid,'%f%c',trial(trlIdx).stim(stmIdx).elevation,dlm); % 7) elevation location
		fprintf(fid,'%d%c',trial(trlIdx).stim(stmIdx).ondelay,dlm); % 8) onset
		fprintf(fid,'%d%c',trial(trlIdx).stim(stmIdx).offdelay,dlm); % 9) offset
		fprintf(fid,'%d%c',trial(trlIdx).stim(stmIdx).intensity,dlm);% 10) intensity
		fprintf(fid,'%d%c',attr,dlm); % 11)
		fprintf(fid,'%d%c',0,dlm); % 12)
		fprintf(fid,'%d \n',1); % 13)
		
	end
end

fclose(fid);





function [veltrace,smvtrace,acctrace,smatrace]          = getvel(htrace,vtrace,Fsample,sd)
% Obtain radial velocity from horizontal HOR and vertical VER traces.
%
% [VEL, SVEL] = GETVEL(HOR,VER,SMOOTHFACTOR,FSAMPLE)
%
% Obtain radial velocity from horizontal HOR and vertical VER traces.
%
% See also GSMOOTH, PA_SACDET
%
% MarcW 2007

Rx                                      = htrace;
Ry                                      = vtrace;
R                                       = NaN*Rx;
veltrace                                = R;
acctrace                                = R;
smvtrace                                = R;
smatrace                                = R;
for i                                   = 1:size(htrace,2)
%     Rx(:,i)                             = Rx(:,i)-Rx(1,i);
%     Ry(:,i)                             = Ry(:,i)-Ry(1,i);
    Rx(:,i)                             = gradient(Rx(:,i),1);
    Ry(:,i)                             = gradient(Ry(:,i),1);
    R(:,i)                              = hypot(Rx(:,i),Ry(:,i));
    R(:,i)                              = cumsum(R(:,i));
    


    veltrace(:,i)                       = gradient(R(:,i),1./Fsample);
    smvtrace(:,i)                       = pa_gsmooth(veltrace(:,i),Fsample,sd);
% 	mu = mean(smvtrace(1:100,i));
% 	smvtrace(:,i) = smvtrace(:,i)-mu;
% 	 veltrace(:,i) =  veltrace(:,i)-mu;
% 	 veltrace(:,i) = smvtrace(:,i);
% 	    veltrace(:,i)                       =smvtrace(:,i);

    acctrace(:,i)                       = gradient(smvtrace(:,i),1./Fsample);
    smatrace(:,i)                       = pa_gsmooth(acctrace(:,i),Fsample,sd);
end
