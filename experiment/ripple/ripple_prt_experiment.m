function ripple_prt_experiment

% clearvars;
close all;
clc;
delete(timerfindall);

%% file details
datadir			= '/Users/ssharma/Documents/DATA/IRP/STR';
experimenter	= 'SS';
subjectid		= '0099';
sessionid		= '0000';
formatout		= 'mm-dd-yy';
method			= 'LSL_non_gp';
datetoday		= datestr(now,formatout);
fname			= fullfile([experimenter '-' subjectid '-' sessionid '-' datetoday  '-' method '.mat']);


%% pupil labs lsl
rc			= pupil_remote_control('131.174.140.162',50020);
rc.start_recording();

%% main body
InitializePsychSound;
audiodevices = PsychPortAudio('GetDevices');

deviceid	= 4;
fs			= audiodevices(deviceid).DefaultSampleRate; % sample rate defined by audio device

[vel,dens,depth,nreps,tonset,riplen,ntrials]  = stimpararmeters;

%% Do Experiment
global button_press_LSL event_push event_trig
lsl_settings.evType = 'Digital Events @ lslder06';
lslDevices          = getLslDevices(lsl_settings);
[lslSession]        = labStreamingSession(lslDevices);

lslSession.start;

disp('start now')

global stamp_onset
stamp_onset = zeros(1,3);
ii=1;
while ii < ntrials
	
	event_trig = NaN;
	event_push = NaN;
	button_press_LSL = false;
	fprintf('>>>>> TRIAL %d of %d <<<<<',ii,ntrials)
	[snd,soundFs]   = genripple(vel(ii),dens(ii),depth(ii),riplen(ii),tonset(ii));
	[playTime(ii),pahandle] = playsound(snd,fs,deviceid);
	while ~button_press_LSL
		%fprintf('No button pressed so far... \n');
		button_press_LSL;
		% pause(.21);
	end
	stoptrial(pahandle);
	
	RT(ii) = event_push - (event_trig+tonset(ii)/1000);
	event_p(ii) = event_push;
	event_t(ii) = event_trig;
	
	if (RT(ii)<0.1) %repeat the trial for anticipatory response
		ii=ii-1;
	end
	pause(rndval(300,800,[1 1])/1000); %pause between trials
	ii=ii+1;
	WaitSecs(3);
	
	fprintf('Reaction time (total): %f ms \n',RT);
end

%ii=ii-1;
lslSession.stop

%%

% pldata	= plstr.read();
% evdata	= evstr.read();
% ses.stop();
rc.stop_recording();

delete(ses);
delete(plstr);
delete(evstr);
delete(info_pl);
delete(info_ev);
delete(rc);
sca;
%% FIGURES
save(fname,'RT','vel','dens','depth','nreps','ntrials','playTime','tonset');
figure(1)
plotpost(RT);
nicegraph

promptness=1./RT;

min_vel=min(vel);
max_vel=max(vel);
min_dens=min(dens);
max_dens=max(dens);

vel_grid=linspace(min_vel, max_vel, 100);
dens_grid=linspace(min_dens, max_dens, 100);

[V, D]=meshgrid(vel_grid.', dens_grid.');

prompt_grid=griddata(vel, dens, promptness, V, D);

figure(2)
imagesc(vel_grid, dens_grid, prompt_grid);
mycolormap = customcolormap(linspace(0,1,11), {'#a60026','#d83023','#f66e44','#faac5d','#ffdf93','#ffffbd','#def4f9','#abd9e9','#73add2','#4873b5','#313691'});
colormap(mycolormap);
colorbar
clim([0 5]);
set(gca,'YDir','normal')
% xticks(1:17);
% xticklabels({'-64','-32','-16','-8','-4','-2','-1','-0.5','0','0.5','1','2','4','8','16','32','64'});
% yticks(1:8)
% yticklabels({'0','1/4','1/2','3/4','1','2','4','8'});

%% Stimulus parameters
function [vel,dens,depth,nreps,tonset,riplen,ntrials]  = stimpararmeters
vel     = [-0.5 -1 -2 -4 -8 -16 -32 -64 0 0.5 1 2 4 8 16 32 64]; % Hz
dens    = [0, 0.25, 0.5, 0.75, 1, 2, 4, 8]; % cycles/octave, yielding a total of (17 velocities x 8 densities =) 136

vel     = [0 0.5 1 2 4 8 16 32 64]; % Hz
dens    = [0, 0.25, 0.5, 0.75, 1, 2; % cycles/octave, yielding a total of (17 velocities x 8 densities =) 136
	
depth   = 50; % for now
[vel,dens,depth]  =  meshgrid(vel,dens,depth);% does not work.

vel     = vel(:);
dens    = dens(:);
depth   = depth(:);


%% repetitions

nreps   = 10;
stim    = [vel dens depth];
stim    = repmat(stim,[nreps 1]);
ntrials = length(stim);
%% pseudorandomize
idx		= randperm(ntrials);
stim    = stim(idx,:);
vel     = stim(:,1);
dens    = stim(:,2);
depth   = stim(:,3);
%% time onset and ripple length
tonset  = rndval(2000,2500,[4 1]); % random onset between 400 and 800 ms for the dynamic ripple to start
riplen  = 3000; % 3 second long of a ripple
tonset  = repmat(tonset,[ntrials/length(tonset) 1]);
tonset  = tonset(idx);
riplen  = riplen*ones(1,ntrials);


%% sound playback
function [playTime,pahandle] = playsound(snd,fs,deviceid)
% playTime
% add pulse for the LSL
pulse      = zeros(1,length(snd));
pulse(200:500) = 5;
sndMatrix = [snd ;snd ;pulse];
sndMatrix = sndMatrix;

[ch, len]   = size(sndMatrix);

pahandle = PsychPortAudio ('Open',...
	deviceid,...     % [, deviceid]
	1,...     % [, mode]
	3,...     % [, reqlatencyclass]
	fs,...    % [, freq]
	[3 1],...     % [, channels]
	[],...      % [, buffersize]
	[],...      % [, suggestedLatency]
	[],...      % [, sel   ectchannels]
	[]);        % [, specialFlags=0]

PsychPortAudio('FillBuffer',pahandle,sndMatrix);
playTime = PsychPortAudio('Start',pahandle,[],[],1);
WaitSecs(0.1);
	
%% get response
function [keyn,rt] = getresponse

% Response
keyIsDown = 0;
while ~keyIsDown
	[ keyIsDown, timeSecs, keyCode ] = KbCheck(-1);
end

keyn	= find(keyCode); %KbName
%  left = 80
% right = 79
rt		= timeSecs;

function stoptrial(pahandle)
PsychPortAudio('Stop', pahandle);
PsychPortAudio('Close')
		


function pl_listener(src, event) %#ok<DEFNU,INUSL>
global button_press_LSL event_push
disp('Button Press!');
button_press_LSL = 1;
event_push = event.Timestamps(1);

						
function pl_listener_trig(src, event) %#ok<DEFNU,INUSL>
global event_trig
disp('Trigger In!');
event_trig = event.Timestamps(1);

						
function lslDev = getLslDevices(settings)
% This function gets the LSL devices and sets two channels, one for
% trigger, another for push-button

% Trigger Signal
if isfield(settings,{'evType'})
	evType = settings.evType;
else
	evType = 'Digital Events @ lslder06'; % Select according to the LSL Device name
end
evName = sprintf('Digital Events %d', 0);
lslDev.eventRecorder.lslString_trig = sprintf('type=''%s'' and name=''%s''', evType, evName);

% Push button
if isfield(settings,{'evType'})
	evType = settings.evType;
else
	evType = 'Digital Events @ lslder06'; % Select according to the LSL Device name
end
evName = sprintf('Digital Events %d', 1);
lslDev.eventRecorder.lslString_pb = sprintf('type=''%s'' and name=''%s''', evType, evName);


function [ses] = labStreamingSession(lslDevices)

% Function to create two streams using LSL
%           button_press_LSL = 0;
ses = lsl_session();

info_trig = lsl_resolver(lslDevices.eventRecorder.lslString_trig);
list_trig = info_trig.infos();
if isempty(list_trig)
	error('no streams found');
end
evstream_trig = lsl_istream(info_trig{1});
ses.add_stream(evstream_trig);


info_pb = lsl_resolver(lslDevices.eventRecorder.lslString_pb);
list_pb = info_pb.infos();
if isempty(list_pb)
	error('no streams found');
end
evstream_pb = lsl_istream(info_pb{1});
ses.add_stream(evstream_pb);

% Add listener to the pushbutton stream
addlistener(evstream_pb,'DataAvailable',@pl_listener);
addlistener(evstream_trig,'DataAvailable',@pl_listener_trig);

% Horrible coding using global variables but so far it works....
function pl_listener(src, event) %#ok<DEFNU,INUSL>
global button_press_LSL event_push
disp('Button Press!');
button_press_LSL = 1;
event_push = event.Timestamps(1);


function pl_listener_trig(src, event) %#ok<DEFNU,INUSL>
global event_trig
disp('Trigger In!');
event_trig = event.Timestamps(1);


function stoptrial(pahandle)
PsychPortAudio('Stop', pahandle);
PsychPortAudio('Close');
