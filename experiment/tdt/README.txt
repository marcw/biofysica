02/12/2022

The TDT hardware is connected via an ActiveX control in matlab: 'actxcontrol'

In the near future Matlab will remove this function, however it can be replaced by 'actxserver'
which will have exactly the same functionality.