load('snandan_sig155.mat');
servo=sr_servo;
servo.enable;

niter = 15;
off_delay = 1;
%!!!!
%ver = -ver;

duration = length(ver)*0.1;

servo.enable();
r=input('press enter to start, q to quit','s');
if ~strcmp(r, 'q')
    fprintf('running %d stimulus iterations of %.1f seconds each...\n',niter,duration);
    
    for ii = 1:niter
        fprintf('iteration #%d.',ii);
        
        servo.write_profile(ver,hor,chair);
        fprintf('.');
        
        servo.start();
        fprintf('.');
        
        % wait for the profile to complete and
        % give the servos some extra time to settle
        % at the target position
        pause(duration+off_delay);
        servo.stop();
        fprintf('.\n');
        
        [ver,~,~]=servo.read_profile_sv();
        [pver,~,~]=servo.read_profile_pv();
        
        % plot the profiles
        times=0.1*(0:1999);
        figure(1);
        clf;
        plot(times,ver,'-',times,pver,'.');
        grid('on');
        
    end
    fprintf('done.\n');
    
end
servo.disable();
delete(servo);

