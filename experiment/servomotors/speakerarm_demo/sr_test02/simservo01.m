load('snandan_sig747.mat');
servo=sr_dummy;
servo.enable;

duration = length(ver)*0.1;
off_delay = 1;

servo.enable();

niter = 2;
fprintf('running %d stimulus iterations...\n',niter);

for ii = 1:niter
    fprintf('iteration #%d.',ii);
    
    servo.write_profile(ver,hor,chair);
    fprintf('.');

    servo.start();
    fprintf('.');

    pause(duration+off_delay);
    servo.stop();
    fprintf('.\n');
    
    [ver,~,~]=servo.read_profile_sv();
    [pver,~,~]=servo.read_profile_pv();

    % plot the profiles
    times=0.1*(0:1999);
    figure(1);
    clf;
    plot(times,ver,'-',times,pver,'.');
    grid('on');
    
end
fprintf('done.\n');


servo.disable();
delete(servo);

