classdef sr_servo2 < sr_servo

    properties (Constant)
        p_gain1 = 0.03;
        p_gain2 = 0.03;
        p_gain3 = 0.05;
    end

    methods
        function this = sr_servo2
            this@sr_servo();
            this.get_p_gains;
        end

        function set_p_gains(this)
            P = zeros(3,1);
            P(1)=this.plc.IEC_read(this.varmap.Gain_P_Axis1);
            P(2)=this.plc.IEC_read(this.varmap.Gain_P_Axis2);
            P(3)=this.plc.IEC_read(this.varmap.Gain_P_Axis3);
        end

end
