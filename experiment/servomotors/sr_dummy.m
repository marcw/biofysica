classdef sr_dummy < panaservo
    properties
       enabled
       started
       Profile_1A
       Profile_2A
       Profile_3A
       PV_Position_1A
       PV_Position_2A
       PV_Position_3A
    end
    
    methods (Access=protected)
        
        function set_enable(this, value)
            this.enabled = value;
        end
        
    end
    
    methods
        function this = sr_dummy(s)
            memorytable = 'speakerrobot_PLC_global_variables.xlsx';
            this.varmap = m2c_memory_map(memorytable);
            this.enabled = false;
            this.started = false;
        end
        
        function r = is_enabled(this)
            r = this.enabled;
        end
        
        function start(this)
            this.started = true;
            this.simulate();
        end
        
        function stop(this)
            this.started = false;
        end
        
        function print_status(~)

        end
        
        function simulate(this)
            % this is an identity operation for now
            [a, b, c]=this.read_profile_sv();
            this.PV_Position_1A = 10 * a; %cumsum(a);
            this.PV_Position_2A = b; %cumsum(b);
            this.PV_Position_3A = c; %cumsum(c);
        end
        
        function idata = convert_profile(~, data, limit_r_vel, limit_r_pos)
            maxlen = 2000;
            
            data = reshape(data, [], 1); % make it a column vector
            len = length(data);
            if len > maxlen
                warning('profile too long, truncated to %d samples', maxlen);
                data = data(1:maxlen);
            end
            npad = maxlen-len;
            % convert to int32 data in 1/10 degrees, padded with zeros
            data = int32(round(10*data));
            r_pos = 10*range(data);  % r_pos in degrees
            if  nargin >=4
                if (r_pos(1) < limit_r_pos(1)) || (r_pos(2) > limit_r_pos(2))
                    warning('profile position out of range');
                end
            end
            % FIXME?
            % ??? pad with one leading zero before d/dt? to keep buffer length == 2000
            % padding is fine when running profile from only one buffer.
            idata = diff(data);
            idata = [0; idata];
            
            if nargin >= 3
                r_vel = max(abs(range(idata)));  % r_vel in degrees/second
                if (r_vel > limit_r_vel)
                    warning('profile speed out of range');
                end
            end
            idata = int16([idata; zeros(npad, 1)]);
        end
        
        function result = write_profile(this, main, speaker, chair)
            p1 = this.convert_profile(main);
            p2 = this.convert_profile(chair);
            p3 = this.convert_profile(speaker);
          
            this.Profile_1A = zeropad(p1,2000);
            this.Profile_2A = zeropad(p2,2000);
            this.Profile_3A = zeropad(p3,2000);
            result = 0;
        end
        
        function [main, speaker, chair] = read_profile_sv(this)
            main=this.Profile_1A;
            chair=this.Profile_2A;
            speaker=this.Profile_3A;
            main=cumsum(double(main))/10.0;
            chair=cumsum(double(chair))/10.0;
            speaker=cumsum(double(speaker))/10.0;
        end
        
        function [main, speaker, chair] = read_profile_pv(this)
            main=this.PV_Position_1A;
            chair=this.PV_Position_2A;
            speaker=this.PV_Position_3A;
            main=double(main)/10;
            chair=double(chair)/10;
            speaker=double(speaker)/10;
        end
        
    end  % methods
    
end  % classdef
