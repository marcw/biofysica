classdef (Abstract) panaservo < handle
%PANASERVO Abstract class provinding access to a Panasonic PLC controlling servo motors
%  The PANASERVO class givess access to a PLC controlling servo motors.
%  Typically there are member functions to upload a movement profile (SV=set
%  value), download the process values after the movement finishes(PV),
%  enable servo, disable servo, start profile, stop profile.
%  The specific implementation will be in a child class of PANASERVO, e.g.
%  VS_SERVO for the Vestibular Stimulator, or SR_SERVO for the Speaker
%  Robot. Both setups are in the Biophysics Labs of the Donders Centre for
%  Neuroscience at Radboud University in Nijmegen. The PLC programs are
%  custom made by Panasonic Industry in Best (NL).

% 2018-01-02 GW: fixed maxint problem in convert_profile() function.
%            Now converting position values to int32 instead of int16.
% 

properties
    % VARMAP - this is the memory map of the PLC
    % The memory map is of a class M2C_MEMORY_MAP and will be read from an
    % .xlsx file
    % See also M2C_MEMORY_MAP
    varmap
    
    % PLC - this is the M2C_PLC object
    % See also M2C_PLC
    plc
end

methods (Abstract, Access=protected)
    % SET_ENABLE - enable or disable the servo drive
    % This method is protected to avoid user acces. For safety reasons, 
    % enabling the servo drive should never be allowed in subclasses but
    % only be possible by using touch panel in the setup.
    set_enable(this, value)
end


methods (Abstract)
    % IS_ENABLED - return of the servo drive is enabled.
    r = is_enabled(this)
    % START - start the profile
    start(this)
    % STOP - stop the profile
    stop(this) 
    % PRINT_STATUS - tell us the value of some status variables
    print_status(this) 
    % WRITE_PROFILE - write profiles to the PLC
    % WRITE_PROFILE(ax1, ax2, ax3) writes the profiles for three axes to
    % the PLC.
    result = write_profile(this, axis1, axis2)  
    [ax1, ax2] = read_profile_sv(this) % read the profile (set values) from the PLC
    [ax1, ax2] = read_profile_pv(this) % read the profile (process value) from the PLC

end

methods
    function this = panaservo(hostaddr, memorytable)
        %PANASERVO setup a MEWTOCOL over IP connection to PLC and use the
        %memory map specified in the .XLSX file 'memorytable'
        if nargin ~= 0
            this.plc = m2c_plc(hostaddr);
            this.varmap = m2c_memory_map(memorytable);
        end
    end

    function delete(this)
        % DELETE clean up
        delete(this.varmap);
        delete(this.plc);
    end

    function enable(this)
        %ENABLE Prompt the user to enable the drive using the touch panel.
        %For safety it is not allowed to enable the drive directly from a MATLAB
        %program.
        if ~this.is_enabled()
            warndlg(sprintf('Enable servo drives, then\npress OK to continue'),...
                'Warning','modal');
        end
    end
    
    function disable(this)
        %DISABLE Disable the servo drive
        this.set_enable(0);
    end
        
    function keepalive(this)
        %KEEPALIVE Send a keepalive message to the PLC to avoid dropping the TCP connection
        this.plc.keepalive();
    end
    
    function idata = convert_profile(~, data, limit_r_vel, limit_r_pos)
        %CONVERT_PROFILE Convert a profile buffer to PLC readable values
        % IDATA=CONVERT_PROFILE(DATA, LIMIT_R_VEL, LIMIT_R_POS) converts
        % DATA in degreesto IDATA readable by the PLC.
        % DATA is an array of maximum 2000 samples with floating point 
        % position values. The sample rate is fixed to 10 S/s, and the
        % resolution is 0.1 degrees.
        % IDATA is either truncated or zero padded to 2000 samples
        % LIMIT_R_VEL is a velocity limit that is checked. If the
        % velocity in DATA is beyond this limit, a warning is issued.
        % LIMIT_R_POS is a 2x1 array with min and max position limits.
        % If the position in DATA is beyond these limits, a warning is issued.
        % 
        % This should probably be a 'protected' method, as it is called by
        % WRITE_PROFILE in derived classes.
        % This method could also be 'abstract' as it is overwritten in
        % VS_SERVO as well as SR_SERVO, or it should be removed in the
        % derived classes.
        maxlen = 2000;

        data = reshape(data, [], 1); % make it a column vector
        len = length(data);
        if len > maxlen
           warning('profile too long, truncated to %d samples', maxlen);
           data = data(1:maxlen);
        end
        npad = maxlen-len;
        % convert to int32 data in 1/10 degrees, padded with zeros
        data = int32(round(10*data));
        r_pos = 10*range(data);  % r_pos in degrees
        if  nargin >=4
           if (r_pos(1) < limit_r_pos(1)) || (r_pos(2) > limit_r_pos(2))
              warning('profile position out of range');
           end
        end
% FIXME?
% ??? pad with one leading zero before d/dt? to keep buffer length == 2000
% padding is fine when running profile from only one buffer.

        idata = diff(data);
        idata = [0; idata];

        if nargin >= 3
            r_vel = max(abs(range(idata)));  % r_vel in degrees/second
            if (r_vel > limit_r_vel)
               warning('profile speed out of range');
            end
        end
        idata = int16([idata; zeros(npad, 1)]);
    end
    
    function result = clear_profile(this)
        %CLEAR_PROFILE Clear the profile buffer in the PLC
       result = this.write_profile([],[]);
    end


end  % methods

end  % classdef
