classdef  lsl_lslder_restful < lsl_lslder_base

    properties
        hostname    % Hostname of the LSL stream
        port = 8080;% the network port to connect to
        uri         % URI for the RESTful interface
        socket      % ZeroMQ socket for communication
        connect_timeout = 10; % The timeout for connection to a TCP endpoint
    end

    methods
        function this = lsl_lslder_restful(hostname, port)
            % lsl_lslder_restful Constructor for creating a command interface to an LSL (Lab Streaming Layer) digital event recorder.
            %   this = lsl_lslder_restful(istream) creates a RESTful command interface for the specified input stream.
            %   The interface is responsible for sending commands and receiving responses from an LSL Digital Event Recorder.
            %
            % Example:
            %
            % lslder=lsl_lslder(hostname);
            % lslder.reset();
            % for ii=0:7
            %   lslder.enable(ii,0);
            %   lslder.debounce(ii,20);
            %   lslder.marker(ii,0,sprintf('BUTTON %d PRESSED!',ii));
            %   lslder.enable(ii,1);
            %   lslder.marker(ii,1,sprintf('BUTTON %d RELEASED!',ii));
            % end
            %
            if nargin > 1
               this.port = port;
            end
            if ~ischar(hostname)
                  error("hostname must be a character string");
            end
            this.hostname = hostname;
            if ~this.test_tcp(this.hostname,this.port)
                this.hostname = sprintf("%s.local", hostname);
                if ~this.test_tcp(this.hostname,this.port)
                    error("cannot connect to %s:%d", hostname, this.port);
                end
            end
            this.uri = sprintf("http://%s:%d/json", this.hostname, this.port);
        end

        function res = sendcmd(this, cmd)
            % sendcmd Send a command and receive a response from the LSL Digital Event Recorder.
            %   sendcmd(this, cmd) sends the specified command to the event
            %   recorder and waits for a response. If the response indicates an error, an exception is thrown.

            jcmd = jsonencode(cmd);
            res = webwrite(this.uri,jcmd);
            % Check if the response indicates an error and throw an exception
            if ~strcmp(res.result, 'ok')
                ME = MException('lsl_lslder_restful:error', '%s at:\n%s', res.what, jsonencode(res.line));
                throw(ME);
            end
        end
           
        function result = test_tcp(this, location, port)
            % TEST_TCP tests if the hostname:port is available for TCP transport.
            %
            % Parameters:
            %   - location: The hostname or IP address
            %   - port: The port number
            %
            % Returns:
            %   - result: A logical value indicating if the hostname:port is available
            
            if isempty(port)
                ME = MException('socket: no port in tcp uri with host %s', location);
                throw(ME);
            end
            
            % test if hostname:port is available
            try
               con=tcpclient(location, port, 'ConnectTimeout', this.connect_timeout);
            catch ME
               warning(ME.message);
               result = false; 
               return
            end
            result = true;
            delete(con);
        end
    end
end