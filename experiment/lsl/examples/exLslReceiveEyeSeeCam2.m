function [escdata, escmetadata, evdata] = exSphereLSL01
   
    %
    % Select EyeSeeCam event stream
    %
    info=lsl_resolver('type=''EyeSeeCam @ dcn-eyeseecam01''');

    l=info.list();
    if isempty(l)
        error('no streams found');
    end
    for i=1:size(l,2)
        fprintf('%i: name: ''%s'' type: ''%s''\n',i,l(i).name, l(i).type);
    end
    
    n=input('enter EyeSeeCam stream number to acquire: ');
    escstr=lsl_istream(info{n});
    escmetadata=lsl_metadata_eyeseecam(escstr);

    %
    % Select Digital Event Recorder event stream
    %
    evinfo=lsl_resolver('type=''Digital Events @ lslder01'' and name=''Digital Events 0''');
    l=evinfo.list();
    if isempty(l)
        error('no streams found');
    end
    for i=1:size(l,2)
        fprintf('%i: name: ''%s'' type: ''%s''\n',i,l(i).name, l(i).type);
    end
    
    n=input('enter digital events stream number to acquire: ');
    evstr=lsl_istream(evinfo{n});

    ses=lsl_session();
    ses.add_stream(escstr);
    ses.add_stream(evstr);
    addlistener(escstr,'DataAvailable',@esc_listener);
    addlistener(evcstr,'DataAvailable',@ev_listener);
    
    input('press enter to start');
    
    ses.start();
    input('press enter to stop');
    
    ses.stop();
    escdata=escstr.read();
    evdata=evstr.read();
    delete(ses);
    delete(escstr);
    delete(evstr);
    delete(info)
    delete(evinfo)
    
end

function esc_listener(src, event)
    disp('esc_listener called');
    event
end

function ev_listener(src, event)
    disp('ev_listener called');
    event
end
