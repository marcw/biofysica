function alldata = exLSLDER_Receive_Markers_ZMQ_REST
% exLSLDER_Receive_Markers_ZMQ_REST example for receiving time stamped
% markers from an LSLDER digital event recorder using the alternative
% ZeroMQ or RESTful APIs
% The event recorder is connected to the PC running this Matlab instance
% trough a LAN, or alternatively via a point-to-point network connection on
% a primary or secondary (USB) network interface.
% You need the biofysica toolbox to run this: https://gitlab.science.ru.nl/marcw/biofysica

% hostname of my event recorder
host='raspi6.local';

% create instance of the event recorder, either using the ZeroMQ or the
% RESTful API. The ZeroMQ API is about 10 times faster than the RESTful
% API.
%lslder=lsl_lslder_restful(host);
lslder=lsl_lslder_zmq(host);

t1=tic;
lslder.reset();  % all settings back to startup values
for ii=0:7
    lslder.enable(ii,1);  % enable registering rising edges on input ii
    lslder.enable(ii,0);  % enable registering falling edges on input ii
    lslder.marker(ii,1,sprintf("%d->RISE",ii)); % name marker for rising edge
    lslder.marker(ii,0,sprintf("%d->FALL",ii)); % name marker for falling edge
    lslder.debounce(ii,20); % debounce in ms, eg. for push buttons
end
elapsed=toc(t1);
fprintf('initialisation took %4.2f seconds.\n',elapsed);  % ZMQ: 1.0 secs, REST: 7.2 secs

input('press enter to start, Ctrl-C to stop');

alldata=cell(1,1);

duration=10; % run for this long
tstart=tic(); 
while toc(tstart) < duration
    r = lslder.poll();  % data available?
    if r.size > 0
       data = lslder.read(); % get all available data
       alldata=process_data(alldata,data); % see below
    else
       do_something_else(); % do other important things
    end
end

fprintf('Done.\n');

end

function do_something_else()
% do something to keep you program alive
pause(0.5); % or procrastinate...

end

function alldata=process_data(alldata,data)
% append data to alldata
   persistent nread
   if isempty(nread)
       nread=0;
   end
   fprintf('got data!\n');
   nread=nread+1;
   alldata{nread}=data;
end
