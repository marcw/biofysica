function [escdata, metadata] = exLslReceiveEyeSeeCam1
    %  info=lsl_resolver('type=''Digital Events @ clockpi'' and name=''Digital Events 0''');
    %  info=lsl_resolver('type=''Pupil Capture @ dcn-eyebrain'' and name=''Pupil Primitive Data - Eye 0''');
    

    %
    % Select Pupil Labs event stream
    %
    info=lsl_resolver('type=''EyeSeeCam @ dcn-pl04''');

    l=info.list();
    if isempty(l)
        error('no streams found');
    end
    for i=1:size(l,2)
        fprintf('%i: name: ''%s'' type: ''%s''\n',i,l(i).name, l(i).type);
    end
    
    n=input('enter EyeSeeCam stream number to acquire: ');
    escstr=lsl_istream(info{n});

    metadata=lsl_metadata_eyeseecam(escstr);
    ses=lsl_session();
    ses.add_stream(escstr);
    addlistener(escstr,'DataAvailable',@esc_listener);
    
    input('press enter to start');
    
    ses.start();
    input('press enter to stop');
    
    ses.stop();
    escdata=escstr.read();
    delete(ses);
    delete(escstr);
    delete(info)
    
end

function esc_listener(src, event)
    disp('esc_listener called');
    event
    
end
