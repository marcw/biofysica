function data = exLSLDER_OtoControl_01
% exLSLDER_Receive_Markers_ZMQ_REST example for receiving time stamped
% markers from an LSLDER digital event recorder using the alternative
% ZeroMQ or RESTful APIs
% The event recorder is connected to the PC running this Matlab instance
% trough a LAN, or alternatively via a point-to-point network connection on
% a primary or secondary (USB) network interface.
% You need the biofysica toolbox to run this: https://gitlab.science.ru.nl/marcw/biofysica

% hostname of my event recorder
host='raspi-gw.local';

% create instance of the event recorder, either using the ZeroMQ or the
% RESTful API. The ZeroMQ API is about 10 times faster than the RESTful
% API.
lslder=lsl_lslder_restful(host);
%lslder=lsl_lslder_zmq(host);

t1=tic;
lslder.reset();  % all settings back to startup values
for ii=0:3
    lslder.enable(ii,1);  % enable registering rising edges on input ii
    lslder.enable(ii,0);  % enable registering falling edges on input ii

end
% lslder.marker(0,1,"L ON"); % name marker for rising edge
% lslder.marker(0,0,"L OFF"); % name marker for falling edge
% lslder.marker(1,1,"R ON"); 
% lslder.marker(1,0,"R OFF");
% lslder.marker(2,1,"BTN PRESS"); 
% lslder.marker(2,0,"BTN RELEASE"); 
lslder.debounce(2,20); % debounce push button (in ms)
% lslder.marker(3,1,"TTL RISE"); 
% lslder.marker(3,0,"TTL FALL"); 
elapsed=toc(t1);
fprintf('initialisation took %4.2f seconds.\n',elapsed);  % ZMQ: 1.0 secs, REST: 7.2 secs

input('press enter to start, Ctrl-C to stop');
lslder.clear();
start = lslder.clock();
celldata=cell(1,1);

duration=180; % run for this long
tstart=tic();
while toc(tstart) < duration
    r = lslder.poll();  % data available?
    if r.size > 0
        data = lslder.read(); % get all available data
        celldata=process_data(celldata,data.events); % see below
    else
        do_something_else(); % do other important things
    end
end

fprintf('Done.\n');
% Here celldata is a cell array with arrays of event structs
data=vertcat(celldata{:}); % convert to one array with event structs
data=aos2soa(data); % convert to one struct with array fields
data.timestamp(:) = data.timestamp(:)-start.timestamp;
data.timestamp=data.timestamp';
data.edge=data.edge';
data.chan=data.chan';
end

function do_something_else()
% do something to keep you program alive
pause(0.5); % or procrastinate...

end

function celldata=process_data(celldata,newdata)
% append data to alldata
persistent nread
if isempty(nread)
    nread=0;
end
fprintf('got data!\n');
nread=nread+1;
newdata
celldata{nread}=newdata;
end


function analyse_latency(data)
% assumes audio on one channel and a TTL pulse on channel 3
caudio=1;
cttl=3;
audio_idx=(data.chan==caudio);
audio_times=(data.timestamp(audio_idx));
audio_data=(data.edge(audio_idx));
ttl_idx=(data.chan==cttl);
ttl_times=(data.timestamp(ttl_idx));
ttl_data=(data.edge(ttl_idx));
clf;
stairs([ttl_times, audio_times],[-ttl_data, audio_data]);
set(gca,'YLim',[-2 2]);

ttl_idx=(data.chan==cttl)&(data.edge==1);
ttl_times=(data.timestamp(ttl_idx));
ttl_data=(data.edge(ttl_idx));
latency=audio_times-ttl_times;
latency_on=latency(1:2:end);
latency_off=latency(2:2:end);





end