function data = exLSLDER_OtoControl_Stats_00
% exLSLDER_Receive_Markers_ZMQ_REST example for receiving time stamped
% markers from an LSLDER digital event recorder using the alternative
% ZeroMQ or RESTful APIs
% The event recorder is connected to the PC running this Matlab instance
% trough a LAN, or alternatively via a point-to-point network connection on
% a primary or secondary (USB) network interface.
% You need the biofysica toolbox to run this: https://gitlab.science.ru.nl/marcw/biofysica

% hostname of my event recorder
host='otoder02.local';
data=[];
% create instance of the event recorder, either using the ZeroMQ or the
% RESTful API. The ZeroMQ API is about 10 times faster than the RESTful
% API.
lslder=lsl_lslder_restful(host);
%lslder=lsl_lslder_zmq(host);

t1=tic;
lslder.reset();  % all settings back to startup values
for ii=0:3
    lslder.enable(ii,1);  % enable registering rising edges on input ii
    lslder.enable(ii,0);  % enable registering falling edges on input ii
    
end
% lslder.marker(0,1,"L ON"); % name marker for rising edge
% lslder.marker(0,0,"L OFF"); % name marker for falling edge
% lslder.marker(1,1,"R ON");
% lslder.marker(1,0,"R OFF");
% lslder.marker(2,1,"BTN PRESS");
% lslder.marker(2,0,"BTN RELEASE");
lslder.debounce(2,20); % debounce push button (in ms)
% lslder.marker(3,1,"TTL RISE");
% lslder.marker(3,0,"TTL FALL");
elapsed=toc(t1);
fprintf('initialisation took %4.2f seconds.\n',elapsed);  % ZMQ: 1.0 secs, REST: 7.2 secs

lslder.clear();
lslder.calibrate;
%st = lslder.sdgetst(4000);
cf = lslder.sdgetcf();
    cf_cell = struct2cell(cf);
    cf_names = fieldnames(cf);
    for ii = 1:length(cf_cell)
        fprintf('config.%s:\t', cf_names{ii});
        for v = cf_cell{ii}
        fprintf('%.3f\t', v);
        end
        fprintf('\n');
    end
% cf.debug = 1;
cf.Vthresh_lo = 5; 
cf.Vthresh_hi = cf.Vthresh_lo;
cf.peak_avg_on_thresh = 0.10;
cf.debounce_samples = 1000;
lslder.sdsetcf(cf);
duration=0; % run for this long
input('press enter to start, Ctrl-C to stop');

tstart=tic();
while toc(tstart) < duration
    st = lslder.sdgetst(5000);
    st_cell = struct2cell(st);
    st_names = fieldnames(st);
    for ii = 1:length(st_cell)
        fprintf('stats.%s:\t', st_names{ii});
        for v = st_cell{ii}
        fprintf('%.3f\t', v);
        end
        fprintf('\n');
    end
    fprintf('===================================\n');
    pause(5000/3600);
end

fprintf('Done.\n');
end

