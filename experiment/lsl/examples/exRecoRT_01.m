function alldata = exRecoRT_01
   % exLSLDER_Receive_Markers_ZMQ_REST example for receiving time stamped
   % markers from an LSLDER digital event recorder using the alternative
   % ZeroMQ or RESTful APIs
   % The event recorder is connected to the PC running this Matlab instance
   % trough a LAN, or alternatively via a point-to-point network connection on
   % a primary or secondary (USB) network interface.
   % You need the biofysica toolbox to run this: https://gitlab.science.ru.nl/marcw/biofysica

   % hostname of my event recorder
   host='RecoRT00.local';

   % create instance of the event recorder, either using the ZeroMQ or the
   % RESTful API. The ZeroMQ API is about 10 times faster than the RESTful
   % API.
   %RecoRT=lsl_lslder_restful(host);
   RecoRT=lsl_lslder_zmq(host);

   t1=tic;
   RecoRT.reset();  % all settings back to startup values
   for ii=0:3
      RecoRT.enable(ii,1);  % enable registering rising edges on input ii
      RecoRT.enable(ii,0);  % enable registering falling edges on input ii

   end
   RecoRT.marker(0,1,"L ON"); % name marker for rising edge
   RecoRT.marker(0,0,"L OFF"); % name marker for falling edge
   RecoRT.marker(1,1,"R ON");
   RecoRT.marker(1,0,"R OFF");
   RecoRT.marker(2,1,"BTN PRESS");
   RecoRT.marker(2,0,"BTN RELEASE");
   RecoRT.debounce(2,20); % debounce push button (in ms)
   RecoRT.marker(3,1,"TTL RISE");
   RecoRT.marker(3,0,"TTL FALL");
   RecoRT.debounce(3,10); % debounce push button (in ms);
   elapsed=toc(t1);
   fprintf('initialisation took %4.2f seconds.\n',elapsed);  % ZMQ: 1.0 secs, REST: 7.2 secs

   input('press enter to start, Ctrl-C to stop');
   RecoRT.clear();
   st = RecoRT.sdgetst(4000); %#ok<NASGU>

   % Change sound detector inputs detection threshold. Default = 6
   cf = RecoRT.sdgetcf(); 
   new_thresh = 8;
   cf.Vthresh_lo = new_thresh;
   cf.Vthresh_hi = new_thresh;
   RecoRT.sdsetcf(cf);
   start = RecoRT.clock(); %#ok<NASGU>
   alldata=[];

   duration=60; % run for this long
   tstart=tic();
   while toc(tstart) < duration
      r = RecoRT.poll();  % data available?
      if r > 0
         data = RecoRT.read(); % get all available data
         struct2table(data)
         alldata=process_data(alldata,data); % see below
      else
         do_something_else(); % do other important things
      end
   end

   fprintf('Done.\n');
   % Here celldata is a cell array with arrays of event structs
   % data=vertcat(celldata{:}); % convert to one array with event s  tructs
   % data=aos2soa(data); % convert to one struct with array fields
   % data.timestamp(:) = data.timestamp(:)-start;
end

function do_something_else()
   % do something to keep you program alive
   pause(0.5); % or procrastinate...

end

function alldata=process_data(alldata,newdata)
   % append data to alldata
   fprintf('got new data!\n');
   if isempty(alldata)
      alldata=newdata;
   else
      alldata=[alldata;newdata];
   end
end
