function evdata = exLslReceiveData_SendStringMarkers

% Collect events from the program SendStringMarkers from the liblsl distro.

    %  info=lsl_resolver('type=''Digital Events @ clockpi'' and name=''Digital Events 0''');
    %  info=lsl_resolver('type=''Pupil Capture @ dcn-eyebrain'' and name=''Pupil Primitive Data - Eye 0''');
    
    %
    % Select digital events stream
    %
    info=lsl_resolver('type=''Markers''');
    l=info.list();
    if isempty(l)
        error('no streams found');
    end
    
    for i=1:size(l,2)
        fprintf('%d: name: ''%s'' type: ''%s''\n',i,l(i).name,l(i).type);
    end
    
    n=input('enter digital event stream number to acquire: ');
    evstr=lsl_istream(info{n});
    
    
    ses=lsl_session();
    ses.add_stream(evstr);
    addlistener(evstr,'DataAvailable',@ev_listener);
    
    input('press enter to start');
    
    ses.start();
    input('press enter to stop');
    %pause(30);
    ses.stop();
    evdata=evstr.read();
    delete(ses);
    delete(evstr);
    delete(info)
    
end

function ev_listener(src, event)
    disp('ev_listener called');
    event
end

function pl_listener(src, event)
    disp('pl_listener called');
    event
end
