classdef (Abstract) lsl_lslder_base < handle

    methods (Abstract)
        sendcmd(this, cmd)
    end
    
    methods     
        function reset(this)
            % reset Send a reset command to the LSL Digital Event Recorder.
            %   reset(this) sends a reset command to the Digital Event Recorder to reset the stream settings to poweron defaults.
            cmd = struct('cmd', 'reset');
            this.sendcmd(cmd);
        end
            
        function marker(this, channel, edge, label)
            % marker Define a marker on a channel-edge on the LSL Digital Event Recorder.
            %   marker(this, channel, edge, label) sends a marker command to the Digital Event Recorder.
            %   The command is used to define a marker label for a channel-edge combination in the data stream.
            cmd = struct('cmd', 'marker', 'chan', channel, 'edge', edge, 'label', label);
            this.sendcmd(cmd);
        end

        function enable(this, channel, edge, onoff)
            % enable Enable or disable data acquisition for a specific channel and edge.
            %   enable(this, channel, edge, onoff) enables or disables data acquisition for a
            %   specific channel and edge. If 'onoff' is not provided, the default is to enable (1).
            if nargin < 4
                onoff = 1; % If 'onoff' is not provided, set it to 1 (enable).
            end
            cmd = struct('cmd', 'enable', 'chan', channel, 'edge', edge, 'enable', onoff);
            this.sendcmd(cmd);
        end

        function disable(this, channel, edge)
            % disable Disable data acquisition for a specific channel and edge.
            %   disable(this, channel, edge) disables data acquisition for a specific channel and edge.
            this.enable(channel, edge, 0); % Equivalent to enabling with 'onoff' set to 0 (off).
        end

        function debounce(this, channel, millisecs)
            % debounce Set debounce time for a specific channel.
            %   debounce(this, channel, millisecs) sets the debounce time (in milliseconds)
            %   for a specific channel, allowing for a delay before data acquisition is triggered again after an event has occurred.
            cmd = struct('cmd', 'debounce', 'chan', channel, 'time', millisecs);
            this.sendcmd(cmd);
        end
        
        function numavail = poll(this)
            % poll Poll the device for available data.
            %   numavail = poll() returns the number of available events ready to
            %   be read 
            cmd = struct('cmd', 'poll');
            res = this.sendcmd(cmd);
            numavail = res.size;
        end
        
        function events = read(this)            
            % read Read all available data from the device
            %   data = read() returns all data read from the device.
            %   events are in data.events which is an nx1 struct with
            %   fields: marker (the marker string) and timestamp (a
            %   floating point timestamp in seconds acquired from the event
            %   recorder's internal clock)
            cmd = struct('cmd', 'read');
            res = this.sendcmd(cmd);
            if ~isempty(res.events)
                events = res.events;
            else
                events = [];
            end
        end
        
        function event = get(this)
            % get Read one sample from the device
            %   data = get() returns on sample from the device
            cmd = struct('cmd', 'get');
            res = this.sendcmd(cmd);
            if ~isempty(res.events)
                event = res.events;
            else
                event = [];
            end

        end
        
        function clear(this)
            % clear Clear the data buffer in the device
            cmd = struct('cmd', 'clear');
            this.sendcmd(cmd);
        end

        function ts = clock(this)
            % clock Get the current time from the device.
            %   ts = clock() returns a timestamp, e.g. for marking the start
            %   of a series of measurements
            cmd = struct('cmd', 'clock');
            res = this.sendcmd(cmd);
            ts = res.timestamp;
        end

        function st = sdgetst(this, nsamp)
            % sdgetst Get the statistics from the esp32c3 sound detector
            %   st = sdgetst(nsamp) returns a structure with statistics for the
            %   sound detector in the OtoControl 2 event recorder. The statistics
            %   are computed over 'nsamp' samples (default: 5000).
            if nargin < 2
               nsamp = 5000;
            end
            cmd = struct('cmd', 'sdgetst', 'nsamp', nsamp);
            res = this.sendcmd(cmd);
            st = res.stats;
        end

        function cf = sdgetcf(this)
            % sdgetcf Read the configuration from the esp32c3 sound detector
            %   cf = sdgetcf() returns a structure with the configuration settins
            %   of the sound detector in the OtoControl 2 event recorder.
            %   The settings can be modified and written back using the
            %   sdsetcfg() function.
            cmd = struct('cmd', 'sdgetcf');
            res = this.sendcmd(cmd);
            cf = res.config;
        end


        function res = sdsetcf(this,cf)
            % sdsetcf Write the configuration to the esp32c3 sound detector
            %   r = sdsetcf(cf) writes the configuration cf to 
            %   the sound detector in the OtoControl 2 event recorder.
            %   The settings can be modified and written back using the
            %   sdsetcfg() function.
            cmd = struct('cmd', 'sdsetcf', 'config', cf);
            res = this.sendcmd(cmd);
        end

        function sdbootpm(this)
            % bootpm Put the esp32c3 into boot programming mode
            %   bootpm() puts the esp32c3 into boot programming mode and
            %   allows us to upload new code over the internal serial 
            %   connection from the raspberry pi in the event recorder.
            cmd = struct('cmd', 'bootpm');
            this.sendcmd(cmd);
        end

        function calibrate(this)
            cf = this.sdgetcf();
            st = this.sdgetst();
            cf.offset = st.offset;
            this.sdsetcf(cf);
        end
    end
    
end
