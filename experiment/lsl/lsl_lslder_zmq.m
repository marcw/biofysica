classdef  lsl_lslder_zmq < lsl_lslder_base

    properties
        istream     % LSL stream object
        metadata    % Metadata associated with the LSL stream
        hostname    % Hostname of the LSL stream
        socket      % ZeroMQ socket for communication
    end

    methods
        function this = lsl_lslder_zmq(lsl_istream_or_hostname)
            % lsl_lslderzmq Constructor for creating a command interface to a LSL (Lab Streaming Layer) digital event recorder.
            %   this = lsl_lslderzmq(istream) creates a ZeroMQ command interface for the specified input stream.
            %   The interface is responsible for sending commands and receiving responses from an LSL Digital Event Recorder.
            %
            % Example:
            %
            % some_stream=lsl_istream(some_lsl_streaminfo);
            % lslder=lsl_lslder(some_stream);
            % lslder.reset();
            % for ii=0:7
            %   lslder.enable(ii,0);
            %   lslder.debounce(ii,20);
            %   lslder.marker(ii,0,sprintf('BUTTON %d PRESSED!',ii));
            %   lslder.enable(ii,1);
            %   lslder.marker(ii,1,sprintf('BUTTON %d RELEASED!',ii));
            % end
            %
            if ischar(lsl_istream_or_hostname)
                % assume input argument is a hostname
                this.hostname = lsl_istream_or_hostname;
            else
                %% TODO: Check if this raises an error in absense of lsl_*
                %% library functions

                % extract hostname from LSL descriptor

                % Store the LSL stream and its metadata
                this.istream = lsl_istream_or_hostname;
                this.metadata = lsl_metadata(this.istream);

                % Extract the hostname from the LSL metadata
                this.hostname = this.metadata.as_struct.info(1).hostname(1).Text;
            end

            % Create a ZeroMQ context with one I/O thread
            ctx = zmq.context_t(1);

            % Create a ZeroMQ REQ (Request) socket
            this.socket = zmq.socket_t(ctx, zmq.socket_type.req);

            % Configure socket settings
            this.socket.set_linger(0);          % No waiting for unsent messages on socket closure
            this.socket.set_send_timeout(5000);  % Set a timeout for sending data
            this.socket.set_recv_timeout(5000);  % Set a timeout for receiving data

            % Attempt to connect to the primary URI
            uri1 = sprintf("tcp://%s:5555", this.hostname);
            uri2 = sprintf("tcp://%s.local:5555", this.hostname);
            try
                this.socket.connect(uri1);
                return
            catch
                % If the connection to the primary URI fails, try the fallback URI
                warning('Could not set up ZeroMQ connection to %s\nNow trying fallback URI %s', uri1, uri2);
            end
            this.socket.connect(uri2);

            % Display a success message
            warning('Connection to %s succeeded!', uri2);
        end

        function delete(this)
            % delete Destructor for cleaning up and deleting the LSL Digital Event Recorder.
            %   delete(this) deletes the requester and its associated resources, including the ZeroMQ socket.
            delete(this.socket); % Delete the ZeroMQ socket
        end

        function res = sendcmd(this, cmd)
            % sendcmd Send a command and receive a response from the LSL Digital Event Recorder.
            %   sendcmd(this, cmd) sends the specified command to the event
            %   recorder and waits for a response. If the response indicates an error, an exception is thrown.

            % Send the command using ZeroMQ
            this.socket.jsend(cmd);

            % Receive and parse the response
            res = this.socket.jrecv();

            % Check if the response indicates an error and throw an exception
            if ~strcmp(res.result, 'ok')
                ME = MException('lsl_lslder:error', '%s at:\n%s', res.what, jsonencode(res.line));
                throw(ME);
            end
        end
    end
end