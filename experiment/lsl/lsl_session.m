classdef lsl_session < handle
    % LSL_SESSION - session class that handles incoming data from
    % lsl_istream objects in the background.
    %
    % See also: ADD_STREAM, START, STOP
    %
    % LSL_SESSION uses a timer to periodically check attached lsl_istream
    % objects for available data. 
    
    properties
        tmr
        lsl_lib
        streams={}
        nstreams=0
    end
    
    methods (Access=public)
        function this=lsl_session()
            % Initialize the lsl_session
            this.lsl_lib=lsl_loadlib();
            this.setup_timer();

           %% Only works in a function called from command prompt
            % Add a cleanup function, so we can mop up in case something
            % goes wrong. The idea is explained in this thread:
            % https://nl.mathworks.com/matlabcentral/answers/421555-class-object-won-t-delete-if-it-has-a-timer-property
            %clnup = onCleanup(@() this.cleanup_on_terminate);
            %rand_name = ['cleanup_object_' char(floor(rand(1, 10) * 26) + 97)];
            %assignin('caller', rand_name, clnup);

        end
        
        function delete(this)
             delete(this.tmr);
        end        
        
        function cleanup_on_terminate(this)
            if isvalid(this) && isvalid(this.tmr)  
                warning('lsl_session: Abnormal termination. Trying to clean up and recover...');
                if strcmp(this.tmr.get('Running'), 'on')
                    stop(this.tmr);
                end
                delete(this.tmr);
                ii=1;
                while ii<=this.nstreams
                   delete(this.streams{ii});
                   ii=ii+1;
                end
            end
        end

        function add_stream(this,stream)
            % add an lsl_istream object to this session
            this.nstreams=this.nstreams+1;
            this.streams{this.nstreams}=stream;
        end
        
        function collect(this)
            % let all streams collect data
            % when data is available the stream will trigger a notification
            % event 'DataAvailable' to its listeners.
            ii=1;
            while ii<=this.nstreams
                this.streams{ii}.collect();
                ii=ii+1;
            end
        end
        
        function setup_timer(this)
            this.tmr = timer('ExecutionMode','fixedRate', ... % Run continuously
                'TimerFcn',@lsl_timer_callback); % Run callback function at every timer event
            this.tmr.period=1.0;
            
            function lsl_timer_callback(~,~)
                this.collect();
            end
            
        end
        
        function start(this,period)  % you can actually stop and restart the nested timer function at any time
            % start() starts data collection from the attached lsl_istream
            % objects.
            % start(interval_seconds) starts data collection with the
            % specified interval.
            if nargin>1
                this.tmr.period=period;
            end
            this.flush_streams();
            this.open_streams();
            start(this.tmr);
            pause(0.1); 
        end
        
        function stop(this)
            stop(this.tmr);
            this.collect();  % get remaining data
            this.close_streams();
        end
           
        function open_streams(this)
            ii=1;
            while ii<=this.nstreams
                this.streams{ii}.open_stream();
                ii=ii+1;
            end
        end

        function close_streams(this)
            ii=1;
            while ii<=this.nstreams
                this.streams{ii}.close_stream();
                ii=ii+1;
            end
        end

        function flush_streams(this)
            ii=1;
            while ii<=this.nstreams
                this.streams{ii}.flush_stream();
                ii=ii+1;
            end
        end

    end
    
end
