function zmq_send_multi
ctx = zmq.context_t(1);
socket = zmq.socket_t(ctx,zmq.socket_type.req);
socket.connect("tcp://fozzie.science.ru.nl:5555");

socket.send_multipart('hello!','world','okay');
r=socket.recvStr();
disp(r);

input('Press <Enter> to leave function');
delete(socket);
delete(ctx);
