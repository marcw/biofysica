%function zmq_send
ctx = zmq.context_t(1);
socket = zmq.socket_t(ctx,zmq.socket_type.req);
socket.set_linger(0);
socket.set_send_timeout(5000);
socket.set_recv_timeout(5000);

socket.connect("tcp://raspi-gw.local:5555");

sa = struct('cmd','marker','chan',1,'edge',1,'enable',1,'label','theLabel');

socket.jsend(sa);
[r11,r12,r13,r14]=socket.jrecv()



input('Press <Enter> to leave function');
delete(socket);
delete(ctx);
