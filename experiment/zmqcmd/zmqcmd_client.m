classdef zmqcmd_client < handle
    %OLD STUFF HERE
    properties
        default_port = 5555;
        default_hostname = 'localhost';
        context;
        socket;
        hostname;
        hostip;
        tmax = Inf;
        
    end
    
    methods
        function this = zmqcmd_client(hostname,port)
            % ZMQCMD_CLIENT class constructor
            %
            % obj = zmqcmd_client('localhost');
            % creates a connection to the zmqcmd_server listening
            % on port 5555 at remote host 'localhost'.
            % the port number can optionally be specified as a 2nd argument
            % obj = zmqcmd_client('localhost', 5555);
            
            import org.zeromq.ZMQ;
            if nargin < 2
                port = this.default_port;
            end
            if nargin < 1
                hostname = this.default_hostname;
            end
            this.hostname=hostname;
            this.hostip=gethostbyname(this.hostname);
            if isempty(this.hostip)
                ME = MException('zmqcmd_client:hostname_lookup_failure',...
                    'cannot resolve IP address for host %s ', hostname);
                throw(ME);
            end
            % zmq workaround to prevent infinite wait:
            % probe if hostname:port is available
            con=pnet('tcpconnect',this.hostip,port);
            if con < 0
                ME = MException('zmqcmd_client:connect_error',...
                    'cannot connect to tcp://%s:%d, check hostname, port number in server',...
                    this.hostip, port);
                throw(ME);
            end
            pnet(con,'close');
            
            this.context=ZMQ.context(1);
            this.socket=this.context.socket(ZMQ.REQ);
            uri=sprintf('tcp://%s:%d',this.hostip,port);
            this.socket.connect(uri);
            this.socket.getReceiveTimeOut();

           % wait_for_connection(60);
            
            function wait_for_connection(seconds)
                subsuri=sprintf('tcp://%s:%d',this.hostip,port+1);  % assume port+1
                subs=this.context.socket(ZMQ.REQ);
                subs.connect(subsuri);
                timeout=100;
                subs.setReceiveTimeOut(timeout);
                topic='INIT';
                subs.subscribe(topic);
                % try to get an INIT response back before proceeding
                for ii=1:seconds*1000/timeout
                    str=sprintf('%s %d',topic, ii);
                    this.send(str);
                    result=subs.recvStr();
                    result=erase(char(result),char(0));
                    if strcmp(result,str)
                        return
                    end
                end
                fprintf('received ''%s''\n',result);
                error('no response from proxy %s\n', subsuri);
            end
        end
        
        function [result,telapsed]=send(this, msg, varargin)
            % SEND send a message string msg to a zmqcmd_server
            % result = obj.SEND(msg [, extra_args...]) sends  message string
            % msg to zmqcmd_server. The extra arguments can be text or numeric.
            % all numeric data is sent as a row-major (lexicographically)
            % ordered vector. This is the common
            % ordering for programming languages like C, C++,
            % Python (NumPy) and Pascal.
            tstart=tic;
            this.zmq_write_multi(this.socket, msg, varargin{:});
            telapsed=toc(tstart);
            if telapsed > this.tmax
                warning('zmqcmd_client.send: round trip time exceeded max, %2.1f>%2.1f ms', 1e3*telapsed, 1e3*this.tmax);
            end
            result='';
        end
      
        function [result,telapsed]=jsend(this, msg, varargin)
            % JSEND send a message string msg to a zmqcmd_server
            % result = obj.JSEND(msg [, extra_args...]) sends  message string
            % msg to zmqcmd_server. The extra arguments can be text or numeric.
            % all numeric data is sent as a JSON encoded string.
            n=length(varargin);

            r=cell(1,n);
            for ii=1:n
                r{ii}=jsonencode(varargin{ii}); 
            end
            tstart=tic;
            
            this.zmq_write_multi(this.socket, msg, r{:});
            telapsed=toc(tstart);
            if telapsed > this.tmax
                warning('zmqcmd_client.send: round trip time exceeded max, %2.1f>%2.1f ms', 1e3*telapsed, 1e3*this.tmax);
            end
            result='';
        end
        
        function [result,telapsed]=recv(this)
            % RECV receive a message string from zmqcmd_server
            % result = obj.RECV() receives a message from the zmqcmd_server
            % at the other endpoint
            tstart=tic;
            result=this.zmq_recv_multi(this.socket);
            telapsed=toc(tstart);
            if telapsed > this.tmax
                warning('zmqcmd_client.recv: round trip time exceeded max, %2.1f>%2.1f ms', 1e3*telapsed, 1e3*this.tmax);
            end
        end
    end
    
    
    methods (Static)
        function varargout = zmq_recv_multi(socket)
            import org.zeromq.*;
            import import java.nio.charset.Charset;
            charset = Charset.defaultCharset();
            
            msg=ZMsg.recvMsg(socket);
            
            nframes = msg.size();
            varargout=cell(1,nframes);
            for ii=1:nframes
               frame=msg.pollFirst();
               varargout{ii}=frame.getString(charset);
            end
            %frame.hasMore()
            %msg.recvMsg(socket);
        end
        
        function result = zmq_recv_single(socket)
            import org.zeromq.*;
            
            result=socket.recvStr();
            result=erase(char(result),char(0));
        end

        function zmq_write_multi(socket, cmd, varargin)
            import org.zeromq.*;
            
            % send command string followed by any data frames in varagin
            msg=ZMsg;
            msg.addString(sprintf('%s\0',cmd));
            ii=1;
            while ii <= numel(varargin)
                % transform the input data into a row-major
                % (lexicographically) ordered vector. This is the common
                % ordering for programming languages like C, C++,
                % Python (NumPy) and Pascal.
                if ischar(varargin{ii}) || isstring(varargin{ii})
                    frame=ZFrame(sprintf('%s\0',varargin{ii}));
                else
                    assert(isnumeric(varargin{ii}),'zmq_write_multi: expect numeric data');
                    data=reshape(transpose(varargin{ii}),1,[]);
                    bytes=typecast(data,'uint8');
                    frame=ZFrame(bytes);
                end
                msg.add(frame);
                ii=ii+1;
            end
            msg.send(socket);
        end
    end
end
