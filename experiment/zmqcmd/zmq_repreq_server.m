function zmq_repreq_server

ctx = zmq.context_t(1);
socket = zmq.socket_t(ctx,zmq.socket_type.rep);
socket.set_linger(0);
socket.set_send_timeout(5000);
socket.set_recv_timeout(5000);
socket.bind("tcp://*:5555");

try
    while true
        fprintf("waiting for input from remote endpoint...\n")
        ret = socket.jrecv();

        fprintf("got: ");
        ret
        pause(1); % do work here

        if ~isempty(ret)
            socket.jsend(ret);
        end
    end

catch ME
    fprintf("Excepion caught: %s\n", ME.message)
end

input('Press <Enter> to leave function');
delete(socket);
delete(ctx);