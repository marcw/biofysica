function zmq_repreq_client
ctx = zmq.context_t(1);
socket = zmq.socket_t(ctx,zmq.socket_type.req);
socket.set_linger(0);
socket.set_send_timeout(5000);
socket.set_recv_timeout(5000);

try
    while true
        socket.connect("tcp://dcn-eyeseecam2.local:5555");
        sa = struct('cmd','pulse','arg',1,'darg',1e-3);
        sb = struct('cmd','beep','arg',1e3,'darg',2e-4);
        
        socket.jsend(sa);
        [r11,r12,r13,r14]=socket.jrecv()
        
        s2=cell(1,3);
        s2{1}='hello!';
        s2{2}=sa;
        s2{3}=1e4;
        s2
        socket.jsend(sb);
        r22=socket.jrecv()
    end
catch ME
    fprintf("Exception caught: %s\n", ME.message);
end

input('Press <Enter> to leave function');
delete(socket);
delete(ctx);
