function zmq_recv_multi
ctx = zmq.context_t(1);
socket = zmq.socket_t(ctx,zmq.socket_type.rep);
socket.bind("tcp://*:5555");

c=cell(0);
c=socket.recv_multipart();
r=socket.send('OK!');

input('Press <Enter> to leave function');
delete(socket);
delete(ctx);
