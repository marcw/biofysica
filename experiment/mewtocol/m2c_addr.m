function [ addr_out, bit_out ] = m2c_addr(addr_in, varargin)
% M2C_ADDR convert a variable format address argument to a numeric address
% [ADDR_OUT, BIT_OUT ] = m2c_addr(ADDR_IN [, BIT_IN]) 
% 
% Note: part of this code needs reviewing and verification, see comments
% See also MEWTOCOL, M2C_RC,
   if nargin == 1

      if isstr(addr_in)
         x = hex2dec(addr_in);
      else
         % TODO, check this
         % warning('Gunter is quite sure that this is not okay...');
         % addr_out = addr_in; 
         x = addr_in;
      end
      bit_out = int16(mod(x,16));
      addr_out = int16((x-bit_out)./16); 

   elseif nargin == 2

      if isstr(addr_in)
         addr_out = hex2dec(addr_in);
      else
         addr_out = addr_in;
      end
      bit_in = varargin{1};
      if isstr(bit_in)
         bit_out = hex2dec(bit_in);
      else
         bit_out = bit_in;
      end

   else
      error('Expected one or two arguments');
   end

end
