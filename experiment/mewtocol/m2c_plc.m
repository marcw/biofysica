classdef m2c_plc < handle
% M2C_PLC Abstract class providing memory access to Panasonic PLCs using the MEWTOCOL protocol.
%
% m2c_plc implements the low level functions that read and write to the
% PLC's internal memory, relays, etc. 
% For ease of programming there are only two functions, IEC_read and
% IEC_write. Each takes an IEC_struct from an m2c_memory_map object to
% determine how the read or write data to/from the PLC.
% Communication is done using MEWTOCOL protocol via a TCP socket to
% port 9094 of the PLC
% A timer is started when an m2c_plc object is created. The timer will
% periodically send a small data packet to the PLC to prevent the
% connection being dropped while idling.
%
% See also: M2C_MEMORY_MAP, PANASERVO, MEWTOCOL, IEC_READ, IEC_WRITE
properties (Access=protected)

   handle;
   wd_timestamp;
   wd_timer;
   wd_keepalive_interval;
end

methods

    function this = m2c_plc(ipAddress)
        this.handle = m2c_new_tcp(ipAddress, 9094);
        this.wd_timestamp = tic;
        this.wd_keepalive_interval = 30;
        this.wd_timer = timer( ...
            'TimerFcn', @wd_callback, ...
            'Period', 5, ...
            'ExecutionMode', 'fixedRate' ...
        );
        start(this.wd_timer);
        
        function wd_callback(~,~)
            telapsed = toc(this.wd_timestamp);
            if telapsed > this.wd_keepalive_interval
                this.wd_timestamp = tic;
                this.keepalive();
            end
        end
    end

    function delete(this)
        stop(this.wd_timer);
        delete(this.wd_timer);
        m2c_close(this.handle);
    end

    function keepalive(this)
        % disp('sending keepalive message');
        % send an abort message. This is the shortest possible 
        % command. It shouldn't hurt, because we only send keepalive
        % messages when the network connection is idle
        m2c_send_frame(this.handle,'%01#AB**');
        % if the abort message should cause problems, use the ones
        % below
        % m2c_send_frame(this.handle,'%01#RCP1X0000**');
        % m2c_recv_frame(this.handle);
    end
    
    function update_watchdog(this)
        this.wd_timestamp = tic;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function result = IEC_read(this, IEC_struct, offset, nelements)
        % IEC_read
        % TODO TODO
        this.update_watchdog();

        if nargin > 2
            IEC_struct.offset=offset;
        end
        if nargin > 3
            IEC_struct.length=nelements;
        end
        if nargin > 4
          error('too many input arguments');
        end
        switch IEC_struct.location

          case 'I'
             result=this.IEC_readI(IEC_struct);
          case 'Q'
             result=this.IEC_readQ(IEC_struct);
          case 'M'
             result=this.IEC_readM(IEC_struct);
        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function result = IEC_write(this, IEC_struct, data, offset_, nelements_)
        % IEC_write 
        % TODO TODO
        this.update_watchdog();

        if nargin < 4
            offset = 0;
            nelements = length(data);
        elseif nargin < 5  % offset specified  
            offset = offset_;
            data = data(1+offset:end);
            nelements = length(data);
        elseif nargin < 6 % offset and nelements specified
            offset = offset_;
            nelements = nelements_;
            data = data(1+offset:1+offset+nelements-1);
            IEC_struct.length=length(data);
        else
            error('too many input arguments');
        end
        IEC_struct.offset=0; % we already stripped the data
        IEC_struct.length=nelements;

        switch IEC_struct.location

          % case 'I', makes no sense
          %   result=this.IEC_writeI(IEC_struct, data);
          case 'Q'
             result=this.IEC_writeQ(IEC_struct, data);
          case 'M'
             result=this.IEC_writeM(IEC_struct, data);
        end

    end

end

methods (Access = protected)
    
    function result = IEC_readI(this, IEC_struct)
        switch IEC_struct.type_id
          case 'X'
             result=this.IEC_readIX(IEC_struct);
          case 'W'
          case 'D'
       end
    end

    function result = IEC_readIX(this, IEC_struct)
        result = m2c_RC(this.handle, 1, 'X', IEC_struct.file, IEC_struct.element) == '1';
    end

    function result = IEC_readQ(this, IEC_struct)
        switch IEC_struct.type_id
          case 'X'
             result=this.IEC_readQX(IEC_struct);
          case 'W'
          case 'D'
       end
    end

    function result = IEC_readQX(this, IEC_struct)
        result = m2c_RC(this.handle, 1, 'Y', IEC_struct.file, IEC_struct.element) == '1';
    end

    function result = IEC_readM(this, IEC_struct)
        switch IEC_struct.type_id
          case 'X'
             result=this.IEC_readMX(IEC_struct);
          case 'W'
             result=this.IEC_readMW(IEC_struct);
          case 'D'
             result=this.IEC_readMD(IEC_struct);
       end
    end

    function result = IEC_readMX(this, IEC_struct)
        result = m2c_RC(this.handle, 1, 'R', IEC_struct.element, IEC_struct.bit) == '1';
    end

    function result = IEC_readMW(this, IEC_struct)
        start_addr = IEC_struct.element+IEC_struct.offset;
        end_addr = start_addr + IEC_struct.length - 1;
        result = m2c_toint(m2c_RD(this.handle, 1, 'D', start_addr, end_addr));
    end

    function result = IEC_readMD(this, IEC_struct)
        start_addr = IEC_struct.element+IEC_struct.offset;
        end_addr = start_addr + 2*IEC_struct.length - 1; % plc has 2 data bytes per address, hence times two
        hexresult = m2c_RD(this.handle, 1, 'D', start_addr, end_addr);
        switch IEC_struct.class
            case 'single'
                result = m2c_tosingle(hexresult);
            case 'uint32'
                result = m2c_touint32(hexresult);
        end
    end

    function result = IEC_writeQ(this, IEC_struct, data)
        switch IEC_struct.type_id
          case 'X'
             result=this.writeQX(IEC_struct, data);
          case 'W'
          case 'D'
       end
    end

    function result = IEC_writeQX(this, IEC_struct, value)
        result = m2c_WC(this.handle, 1, 'Y', IEC_struct.file, IEC_struct.element, value);
    end

    function result = IEC_writeM(this, IEC_struct, data)
        switch IEC_struct.type_id
          case 'X'
             result=this.IEC_writeMX(IEC_struct, data);
          case 'W'
             result=this.IEC_writeMW(IEC_struct, data);
          case 'D'
             result=this.IEC_writeMD(IEC_struct, data);
       end
    end

    function result = IEC_writeMX(this, IEC_struct, value)
        result = m2c_WC(this.handle, 1, 'R', IEC_struct.element, IEC_struct.bit, value);
    end

    function result = IEC_writeMW(this, IEC_struct, data)
        start_addr = IEC_struct.element+IEC_struct.offset;
        result = m2c_toint(m2c_WD(this.handle, 1, 'D', start_addr, cast(data, IEC_struct.class)));
    end

    function result = IEC_writeMD(this, IEC_struct, data)
        start_addr = IEC_struct.element+IEC_struct.offset;
        result = m2c_toint(m2c_WD(this.handle, 1, 'D', start_addr, cast(data, IEC_struct.class)));
    end
end

methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function bResult = Read_X(this, addr, bit)
       this.update_watchdog();
       bResult = m2c_RC(this.handle, 1, 'X', addr, bit);
    end

    function bResult = Read_R(this, addr, bit)
       this.update_watchdog();
       bResult = m2c_RC(this.handle, 1, 'R', addr, bit);
    end

    function bResult = Read_Y(this, addr, bit)
       this.update_watchdog();
       bResult = m2c_RC(this.handle, 1, 'Y', addr, bit);
    end

    % !!!
    % function rResult = Read_Real(this, addr)
    % end

    % !!!
    % function Read_WX(this, addr)
    % end

    % !!!
    % function Read_WR(this, addr)
    % end

    % function Read_DWR(this, addr)
    % end

    % function Read_MultiWR(this, addr)
    % end

    % !!!
    % function Read_WY(this, addr)
    % end

    % !!!
    % function Read_Multi_WR_Bit(this, addr)
    % end

    % function ReadPLC_PV_DataTable(this,addr)
    % end

    % function ReadPLC_SV_DataTable(this,addr)
    % end

    % function Write_R(this, addr);
    % end

    % function Write_Real(this, addr);
    % end

end

end
