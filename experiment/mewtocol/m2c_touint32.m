function data=m2c_touint32(hexstr)
% M2C_TOUINT32 Convert hexadecimal string into an 'uint32'
% DATA = m2c_touint32(HEXSTR) convert HEXSTR to an uint32. HEXSTR has 
% to be a multiple of 8 characters long and will be converted to an array
% of uint32 values. Typically HEXSTR is the response data part of the
% result of a call to M2C_RD, M2C_RC, M2C_RR or one of the other MEWTOCOL
% read commands.
%
% See also MEWTOCOL.
   s = reshape(hexstr, 8, [])';
   x=uint32(hex2dec(s));
   data=swapbytes(x);
end
