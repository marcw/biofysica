function [ frame, length, more ] = m2c_recv_frame(handle)
% M2C_RECV_FRAME Receive on frame of a response message
% [ FRAME, LENGTH, MORE ] = m2_recv_frame(HANDLE)
% receive one MEWTOCOL-COM frame from the TCP socket HANDLE and return the
% FRAME data, the LENGTH, and if there are MORE frames ro be received.
%
% See also MEWTOCOL, M2C_RECV, PNET
   idx=[];
   while isempty(idx)
      peek = pnet(handle, 'read', 1024, 'char', 'view', 'noblock');
      CR = char(13);
      idx = find(peek==CR, 1);
      length = idx-1;
   end
   frame = pnet(handle, 'read', idx);

   if ((frame(length) == '&') & (length ~= 6))   %#ok<*AND2> % length 6 is a data request message frame '%|NR|BCC|&|CR'
      more = 1;
   else
      more=0;
   end
   frame(idx) = [];
   global m2c_debuglevel
   if exist('m2c_debuglevel','var') & (m2c_debuglevel > 0)
      fprintf('< "%s"\n', frame);
   end
end


  