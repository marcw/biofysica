function handle = m2c_new_tcp(hostname, port)
% M2C_NEW_TCP Setup a new TCP connection and adjust some parameters.
% HANDLE = m2c_new_tcp(HOSTNAME, PORT) opens a new TCP connection to PORT
% on HOSTNAME and returns the HANDLE to the socket.
%
% See also MEWTOCOL, M2C_CLOSE, PNET.
   handle = pnet('tcpconnect', hostname, port);
   pnet(handle,'setreadtimeout', 5);
   pnet(handle,'setwritetimeout', 5);
end

