function data=m2c_toint(hexstr)
% M2C_TOINT Convert hexadecimal string into an 'int16'
% DATA = m2c_toint(HEXSTR) convert HEXSTR to an int16. HEXSTR has 
% to be a multiple of 4 characters long and will be converted to an array
% of int16 values. Typically HEXSTR is the response data part of the
% result of a call to M2C_RD, M2C_RC, M2C_RR or one of the other MEWTOCOL
% read commands.
%
% See also MEWTOCOL.
   s = reshape(hexstr, 4, [])';
   x=uint16(hex2dec(s));
   y=swapbytes(x);
   data=typecast(y,'int16');
end
