function m2c_send_frame(handle, frame)
% M2C_SEND_FRAME write a formatted command message to the PLC.
% m2c_send_frame(HANDLE, FRAME) writes FRAME to the PLC via the
% active TCP socket on HANDLE.
%
% See also MEWTOCOL, M2C_RECV, PNET.
   global m2c_debuglevel
   if exist('m2c_debuglevel','var') & (m2c_debuglevel > 0) %#ok<*AND2>
       fprintf('> "%s"\n', frame);
   end
   pnet(handle, 'printf', '%s\r', frame);
   if exist('m2c_debuglevel','var') & (m2c_debuglevel > 0)
       fprintf('> done.\n');
   end
end
