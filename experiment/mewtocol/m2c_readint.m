function data = m2c_readint(handle, station_nr, start_addr, nr_regs)
% M2C_READINT Read an int16 value from the PLC
% This is for testing purposes only. For production code use the code in
% the M2C_PLC class that uses memory map descriptions in an XLSX file.
%
% See also MEWTOCOL, M2C_PLC, M2C_MEMORY_MAP
   hex_data = m2c_RD(handle, station_nr, 'D', start_addr, start_addr+nr_regs-1);
   data = m2c_toint(hex_data);
end



