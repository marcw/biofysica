function data=m2c_touint(hexstr)
% M2C_TOUINT Convert hexadecimal string into an 'uint16'
% DATA = m2c_touint(HEXSTR) convert HEXSTR to an uint16. HEXSTR has 
% to be a multiple of 4 characters long and will be converted to an array
% of uint16 values. Typically HEXSTR is the response data part of the
% result of a call to M2C_RD, M2C_RC, M2C_RR or one of the other MEWTOCOL
% read commands.
%
% See also MEWTOCOL.

% old school procedure
%    s = reshape(hexstr, 4, []);
%    s = s([3 4 1 2], :);
%    s=s';
%    x=hex2dec(s);   
%    data=uint16(x);
   s = reshape(hexstr, 4, [])';
   x=uint16(hex2dec(s));
   y=swapbytes(x);
   data=typecast(y,'uint16');
end
