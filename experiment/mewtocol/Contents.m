% Commands for writing and reading to Panasonic PLC's using the MEWTOCOL protocol.
% Version 1.0 20-Feb-2023
%
% Files
%   m2c_addr       - convert a variable format address argument to a numeric address
%   m2c_close      - Close TCP connection to the PLC
%   m2c_debug      - Set the debug level for the MEWTOCOL functions and classes
%   m2c_error      - Print an error message
%   m2c_memory_map - Provides a memory map for Panasonic PLCs
%   m2c_new_tcp    - Setup a new TCP connection and adjust some parameters.
%   m2c_plc        - Abstract class providing memory access to Panasonic PLCs using the MEWTOCOL protocol.
%   m2c_RC         - Read contact
%   m2c_RD         - Read Registers
%   m2c_readint    - Read an int16 value from the PLC
%   m2c_recv       - Receive a respones message from a PLC
%   m2c_recv_frame - Receive on frame of a response message
%   m2c_RR         - Read the contents of the system registers
%   m2c_send       - Send a command with data to a PLC
%   m2c_send_frame - write a formatted command message to the PLC.
%   m2c_tohex      - Convert numerical values into a byte swapped hexadecimal character string.
%   m2c_toint      - Convert hexadecimal string into an 'int16'
%   m2c_toint32    - Convert hexadecimal string into an 'int32'
%   m2c_tosingle   - Convert hexadecimal string into a 'single'.
%   m2c_touint     - Convert hexadecimal string into an 'uint16'
%   m2c_touint32   - Convert hexadecimal string into an 'uint32'
%   m2c_WC         - Write Contact
%   m2c_WD         - Write Registers
%
% See also PANASERVO, VS_SERVO, SR_SERVO


