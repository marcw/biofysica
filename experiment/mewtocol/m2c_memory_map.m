classdef m2c_memory_map < handle
    % M2C_MEMORY_MAP Provides a memory map for Panasonic PLCs
    % M2C_MEMORY_MAP Provides an interface for easy handling of variables in
    % the PLC memory. The memory map is read from an XLSX table with variables
    % name, type and address information. Variable descriptions are accessible
    % through an easy to use API. The XLSX table is generated from code
    % using Panasonic's FPWIN Pro programming software.
    %
    % % Example usage:
    % varmap=m2c_memory_map('speakerrobot_PLC_global_variables.xlsx');
    % varmap.lookup('Profile_1A')
    %
    % ans =
    %
    %   struct with fields:
    %
    %       location: 'M'
    %        type_id: 'W'
    %           file: 5
    %        element: 10000
    %            bit: NaN
    %         offset: 0
    %         length: 1
    %     identifier: 'Profile_1A'
    %         string: '%MW5.10000'
    %           type: 'ARRAY [0..1999] OF INT'
    %          class: 'int16'
    %
    % % Or use shorthand for easy access, e.g. with some plc that
    % % is an instance of M2C_PLC or PANASERVO
    %
    % some.plc.IEC_write(varmap.mch_Enable_Servo_3, value==1);
    %
    % See also: LOOKUP, M2C_PLC, PANASERVO, MEWTOCOL.

    properties (Access=protected)
        memory_map;
    end

   
    methods
        function this = m2c_memory_map(filename)
            % m2c_memory_map Initializes the object using the table in an XLSX file 
            % m2c_memory_map(FILENAME) reads memory information from the
            % XLSX file FILENAME.
            if verLessThan('matlab', '9.1.0')
                error('m2c_memory_map needs matlab version >= 2016b');
            end
            this.memory_map = readtable(filename);
            % remove rows missing identifier or memory address
            this.memory_map = ...
                rmmissing( this.memory_map, 'DataVariables', { 'Identifier', 'FP_Address' });
            % use 'Identifier' in table as row names
            this.memory_map.Properties.RowNames = this.memory_map.Identifier;
        end
   
        function IEC_struct = lookup(this, Identifier)
            % lookup Lookup a variable in the memory map
            % IEC_struct = lookup(IDENTIFIER) looks for IDENTIFIER and
            % returns the IEC address information in IEC_struct. An
            % IEC_struct has the following fields:
            % location   - location identifier, e.g. 'I','Q','M'
            % type_id    - one of the type identifiers, e.g. 'D','W','X'
            % file       - file number
            % element    - element index in file
            % bit        - bit number or NaN
            % offset     - 0 deprecated, do not use
            % length     - 1 deprecated, do not use
            % identifier - the variable name
            % string     - the IEC address string, e.g. '%MW5.100'
            % type       - the PLC data type, e.g. 'BOOL', 'WORD',
            %              'ARRAY [0..3] OF INT', 'DINT', 'REAL'
            % class      - the Matlab primitive data type matching the 
            %              primitive of the PLC data type
            try
                IEC_address = char(this.memory_map{Identifier, 'IEC_Address'});
            catch ME
                IEC_struct = [];
                error('Identifier not found: %s', Identifier);
            end
            IEC_type = char(this.memory_map{Identifier, 'Type'});
            IEC_struct = this.address2struct(Identifier, IEC_address, IEC_type);
        end

        function IEC_struct = address2struct(this, Identifier, IEC_address, IEC_type)
            [fields,n]=sscanf(IEC_address, '%c%c%c%d.%d.%d');
            if (n==5)
              fields=[fields;NaN];
            end
            ts=this.find_matlab_type(IEC_type);

            IEC_struct=struct('location', char(fields(2)), 'type_id', char(fields(3)), ...
                'file', fields(4), 'element', fields(5), 'bit', fields(6), ...
                'offset', 0, 'length', 1, 'identifier', Identifier, ...
                'string', IEC_address, 'type', IEC_type, 'class', ts);
        end

        function ts = find_matlab_type(this,IEC_type)
            switch IEC_type
            case 'BOOL'
                ts='boolean';
            case 'BOOL_OVERLAPPING_DUT'
                ts='uint16';
            case 'INT'
                ts='int16';
            case 'UINT'
                ts='uint16';
            case 'WORD'
                ts='uint16';
            case 'DINT'
                ts='uint32';
            case 'REAL'
                ts='single';
   
            otherwise
                if strncmp(IEC_type, 'ARRAY', 5)
                  ofpos = findstr(IEC_type, 'OF ');
                  tppos = ofpos+3;
                  IEC_type = IEC_type(tppos:end);
                  ts=this.find_matlab_type(IEC_type);
                else
                  ts='unknown';
                end
            end
        end

        function varargout = subsref(this, s)
            % subsref easy variable access
            switch s(1).type
            case '.'
               if length(s) == 1
                  % Implement this.PropertyName
                  s=this.lookup(s(1).subs);
                  varargout={s};
                  ...
               else
                  varargout = {builtin('subsref',this,s)};
               end
         %  case '()'
         %     if length(s) == 1
         %        % Implement this(indices)
         %        ...
         %     elseif length(s) == 2 && strcmp(s(2).type,'.')
         %        % Implement this(ind).PropertyName
         %        ...
         %     elseif length(s) == 3 && strcmp(s(2).type,'.') && strcmp(s(3).type,'()')
         %        % Implement this(indices).PropertyName(indices)
         %        ...
         %     else
         %        % Use built-in for any other expression
         %        varargout = {builtin('subsref',this,s)};
         %     end
         %  case '{}'
         %     if length(s) == 1
         %        % Implement this{indices}
         %        ...
         %     elseif length(s) == 2 && strcmp(s(2).type,'.')
         %        % Implement this{indices}.PropertyName
         %        ...
         %     else
         %        % Use built-in for any other expression
         %        varargout = {builtin('subsref',this,s)};
         %     end
            otherwise
               error('Not a valid indexing expression')
            end

        end
    end
   
end
