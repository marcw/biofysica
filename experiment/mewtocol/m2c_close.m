function m2c_close(handle)
% M2C_CLOSE Close TCP connection to the PLC
% m2c_close(HANDLE) closes the connection to the TCP socket in handle
%
% See also MEWTOCOL, M2C_NEW_TCP, PNET
   pnet(handle, 'close');
end
