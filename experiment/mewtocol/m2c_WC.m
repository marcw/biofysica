function result = m2c_WC(handle, station_nr, mem_area_code, addr, bit, value)
% M2C_WC Write Contact
% Writes data into external output relays, internal relays and link relays
% RESULT = m2c_WC(HANDLE, STATION_NR, MEM_AREA_CODE, ADDR, BIT, VALUE)
% write VALUE into the TCP socket in HANDLE to the PLC with STATION_NR, the
% MEM_AREA_CODE and the BIT number at ADDR. 
%
% Limitation: only one bit at a time can be written
%
% See also MEWTOCOL, M2C_RC.
   if (value)
       b='1';
   else
       b='0';
   end
   nbits = 1;
   msg = sprintf('P%.1d%c%.3d%.1X%c', nbits, mem_area_code(1), addr, bit, b);
   m2c_send(handle, station_nr, 'WC', msg);
   result = m2c_recv(handle);
end
