function result = m2c_WD(handle, station_nr, mem_area_code, start_addr, data)
% M2C_WD Write Registers
% Write data into data, link data, file and index registers
% RESULT = m2c_WD(HANDLE, STATION_NR, MEM_AREA_CODE, START_ADDR, DATA)
% write DATA into the TCP socket in HANDLE to the PLC with STATION_NR, the
% MEM_AREA_CODE and START_ADDR. The END_ADDR is determined
% depending on the type of and the mnumber of elements of DATA
%
% See also MEWTOCOL, M2C_RD.
    s = m2c_tohex(data);
    end_addr = start_addr + (length(s)/4) -1; % data are counted in 2 byte words, i.e. 4 hex digits

    msg = sprintf('%c%.5d%.5d%s', mem_area_code(1), start_addr, end_addr, s);
    m2c_send(handle, station_nr, 'WD', msg);
    result = m2c_recv(handle);
end
