function data = m2c_RD(handle, station_nr, mem_area_code, start_addr, end_addr)
% M2C_RD Read Registers
% Read the contents stored in data registers, link data registers, file
% registers and index registers
% DATA = m2c_RD(HANDLE, STATION_NR, MEM_AREA_CODE, START_ADDR, END_ADDR)
% uses the TCP socket in HANDLE to read DATA from STATION_NR from 
% START_ADDR to END_ADDR.
%
% See also MEWTOCOL, PNET
   msg = sprintf('%c%.5d%.5d', mem_area_code(1), start_addr, end_addr);
   %disp(msg);
   m2c_send(handle, station_nr, 'RD', msg);
   %disp('after m2c_send');
   data = m2c_recv(handle);
end
