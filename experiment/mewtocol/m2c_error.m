function m2c_error(text_data, station_number, command_response_code)
% M2C_ERROR Print an error message
% m2c_error(TEXT_DATA, STATION_NUMBER, COMMAND_RESPONSE_CODE) writes a
% warning message.
%
% Notice: this needs some work
%
% See also MEWTOCOL
    warning('mewtocol error: %s', command_response_code);
end
