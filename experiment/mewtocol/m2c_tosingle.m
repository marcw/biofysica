function data=m2c_tosingle(hexstr)
% M2C_TOSINGLE Convert hexadecimal string into a 'single'.
% DATA = m2c_tosingle(HEXSTR) convert HEXSTR to a 'single'. HEXSTR has 
% to be a multiple of 8 characters long and will be converted to an array
% of 'single' values. Typically HEXSTR is the response data part of the
% result of a call to M2C_RD or one of the other MEWTOCOL
% read commands.
%
% See also MEWTOCOL
   s = reshape(hexstr, 8, [])';
   x=uint32(hex2dec(s));
   y=swapbytes(x);
   data=typecast(y,'single');
end
