function result = m2c_tohex(data)
% M2C_TOHEX Convert numerical values into a byte swapped hexadecimal character string.
% HEXSTR = m2c_tohex(DATA) converts the content of the numerical values in
% DATA to a hexadecimal string HEXSTR. HEXSTR is typically used as the DATA
% part sent in M2C_WC, M2C_WD or one of the other MEWTOCOL write commands.
% The current implementation is incomplete and supports 'int16', 'uint16', 'uint32' and
% 'single' type data.
%
% See also MEWTOCOL.
   if isa(data,'int16')
       data = typecast(data,'uint16');
       bits = swapbytes(data);
       s = dec2hex(bits,4);
   elseif isa(data,'uint16')
       bits = swapbytes(data);
       s = dec2hex(bits,4);
   elseif isa(data,'uint32')
       bits = swapbytes(data);
       s = dec2hex(bits,8);
   elseif isa(data,'single')
       udata = typecast(data,'uint32');
       bits = swapbytes(udata);
       s = dec2hex(bits,8);   
   else
       error('m2c_tohex: unsupported data type');
   end
   result = reshape(s', 1, []);
end
