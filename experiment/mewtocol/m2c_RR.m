function data = m2c_RR(handle, station_nr, start_addr, end_addr)
% M2C_RR Read the contents of the system registers
% Reads the contents stored in the system registers of the PLC
% DATA = m2c_RR(HANDLE, STATION_NR, START_ADDR, END_ADDR) uses the TCP
% socket in HANDLE to read data from STATION_NR from START_ADDR to
% END_ADDR.
%
% See also MEWTOCOL.
   msg = sprintf('0%.3d%.3d', start_addr, end_addr);
   %disp(msg);
   m2c_send(handle, station_nr, 'RR', msg);
   %disp('after m2c_send');
   data = m2c_recv(handle);
end
