function m2c_debug(level)
% M2C_DEBUG Set the debug level for the MEWTOCOL functions and classes
% m2c_debug(LEVEL) sets the debug level to LEVEL
% If LEVEL > 0 the functions m2c_send_frame and m2c_recv_frame output some
% low level protocol information
%
% See also MEWTOCOL, M2C_SEND_FRAME, M2C_RECV_FRAME
   global m2c_debuglevel;
   m2c_debuglevel = level;
end
