function data = m2c_RC(handle, station_nr, mem_area_code, address, varargin)
% M2C_RC Read contact
% Read the contents stored in external input relays, external output
% relays, internal relays, link relays and timer or counter contacts.
% DATA = m2c_RC(HANDLE, STATION_NR, MEM_AREA_CODE, ADDRESS[, BIT])
% uses the TCP socket in HANDLE to read DATA from STATION_NR from 
% ADDRESS. BIT is an optional argument.
%
% Limitation: despite the functionality of the MEWTOCOL 'RC' command, 
% this function reads only one bit at a time using the unit code 'P'
%
% See also MEWTOCOL, PNET, M2C_ADDR
   [addr,bit] = m2c_addr(address, varargin{:});
   nbits=1;
   msg = sprintf('P%.1d%c%.3d%.1X', nbits, mem_area_code(1), addr, bit);
   m2c_send(handle, station_nr, 'RC', msg);
   data = m2c_recv(handle);
end
