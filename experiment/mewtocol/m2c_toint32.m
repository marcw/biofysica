function data=m2c_toint32(hexstr)
% M2C_TOINT32 Convert hexadecimal string into an 'int32'
% DATA = m2c_toint32(HEXSTR) convert HEXSTR to an int32. HEXSTR has 
% to be a multiple of 8 characters long and will be converted to an array
% of int32 values. Typically HEXSTR is the response data part of the
% result of a call to M2C_RD or one of the other MEWTOCOL
% read commands.
%
% See also MEWTOCOL.

   % old school procedure
   % s = reshape(hexstr, 8, []);
   % s = s([7 8 5 6 3 4 1 2], :);
   % s = s';
   % x=hex2dec(s);
   s = reshape(hexstr, 8, [])';
   x=uint32(hex2dec(s));
   y=swapbytes(x);
   data=typecast(y,'int32');
end
