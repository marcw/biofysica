% Set up matlab path for the labstreaminglayer library

% remove complete liblsl from matlabpath, we add system dependent path in architecture
% specific init files
exclude_d=[biofysica_root '/liblsl'];
exclude_p=genpath(exclude_d);
rmpath(exclude_p);

% don't have this on the matlabpath
exclude_d=[biofysica_root '/experiment/lsl-helpers-plugins'];
exclude_p=genpath(exclude_d);
rmpath(exclude_p);

ARCH=computer('arch');

% Set up matlab path for the architecture dependent part of
% the labstreaminglayer library
pathdirs = { ...
   [ biofysica_root '/liblsl/' ARCH ]
   };

for i = 1:size(pathdirs,1)
   d = pathdirs{i};
   if exist(d,'dir')
      p=genpath(d);
      addpath(p);

      exclude_d=[d '/init.d'];
      exclude_p=genpath(exclude_d);
      rmpath(exclude_p);

      exclude_d=[d '/.svn'];
      exclude_p=genpath(exclude_d);
      rmpath(exclude_p);

      exclude_d=[d '/.git'];
      exclude_p=genpath(exclude_d);
      rmpath(exclude_p);

      % disp(['added directory ', d, ' to the MATLAB path']);
   else
      disp(['directory ', d, ' does not exist, not added to the MATLAB path']);
   end
end
clear pathdirs d p exclude_d i exclude_p ARCH


