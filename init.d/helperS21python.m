function helperS21python
global BIOFYSICA_PY_SYS
global BIOFYSICA_PY_EXE
global biofpy
pe=pyenv;
v=pe.Version;
e=pe.Executable;
loaded = (pe.Status == "Loaded");
BIOFYSICA_PY_EXE=e;
BIOFYSICA_PY_SYS=py.eval('__import__(''sys'')',struct);
PYPATH=[biofysica_root '/utilities/python'];
BIOFYSICA_PY_SYS.path.append(PYPATH);
biofpy=py.eval('__import__(''biofpy'')',struct);
end