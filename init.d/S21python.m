function S21python
global BIOFYSICA_PY_SYS
global BIOFYSICA_PY_EXE
global biofpy
try
    pe=pyenv;
    v=pe.Version;
    e=pe.Executable;
    py.int(0); % do someting to try and get it loaded
    loaded = (pe.Status == "Loaded");
catch ME
    disp(ME.message);
    v='0.0';
    e='(null)';
    loaded=false;
end
if loaded && str2num(v)>=3
   helperS21python()
else
    if loaded
        fprintf('biofysica toolbox: need python version >= 3, you have %s\n',v);
    else
        fprintf('biofysica toolbox: could not load a python %s interpreter ''%s''\n',v,e);
    end
    fprintf('                   functions depending on python will not be available\n');
end

end
