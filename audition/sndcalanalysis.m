% SNDCALANALYSIS
%
%

%% Initialization
close all;
clearvars;
cd('/Users/marcw/Dropbox/Manuscript/Sanne Eemers/RDC/matlab');
d = fullfile('..','..','DAC','sndcalibration');
cd(d);

%%
Fs		= 48828.125;
fc		= [125 250 500 1000 2000 4000 8000];
nfft	= 2^16;
xl		= [100 12000];

%% Calibration
d = dir('*-calibration.mat');
fname = d.name;
load(fname);
snd			= double(snddata{1});
snd			= highpass(snd,'Fc',40,'Fs',Fs,'order',1000);
snd			= lowpass(snd,'Fc',10000,'Fs',Fs,'order',1000);
snd			= detrend(snd);
nsamples	= numel(snd);
t			= 0:nsamples-1;
t = t/Fs;

figure(1)
clf
subplot(211)
plot(t,snd);
hold on
xlabel('time (s)');
ylabel('amplitude (V)');
title('calibration');
nicegraph;
axis normal;

subplot(212)
[f,a] = getpower(snd,Fs,'nfft',nfft);

semilogx(f,a);
hold on
xlabel('frequency (Hz)');
ylabel('amplitude (dB)');
sel = t>0.5 & t<4.5;
caldB = 20*log10(rms(snd(sel)));
[~,idx] = min(abs(f-1000));
title(['Lrms = ' num2str(round(caldB,1)) 'dBV, Lspec = ' num2str(round(max(a),1)) ' dBV']);
nicegraph;
axis normal;



%% Sweep analysis
fname = 'sndrec-aco-avatone-23-03-27-sweep.mat';

load(fname);
snd			= double(snddata{1});
nsamples	= numel(snd);
t			= 0:nsamples-1;

figure(2)
clf
subplot(221)
plot(t,snd);
nicegraph;
axis normal;

subplot(222)
[fo,ao] = getpower(snd,Fs,'nfft',nfft);
plot(fo,ao);
xlim(xl);
nicegraph;
axis normal;

snd			= double(snddata{3});
nsamples	= numel(snd);
t			= 0:nsamples-1;

subplot(223)
plot(t,snd);
nicegraph;
axis normal;

subplot(224)
[fi,ai] = getpower(snd,Fs,'nfft',nfft);
plot(fi,ai);
xlim(xl);
nicegraph;
axis normal;


figure(4)
clf
semilogx(fo,smooth(ao)-smooth(ai),'k-','LineWidth',2);
xlim(xl);
set(gca,'XTick',fc);
nicegraph;
xlabel('frequency (Hz)');
ylabel('gain (dB)');
axis normal;
title('sweep');



%% Tone
d = dir('*-tone*.mat');
fnames = {d.name};
nfiles = numel(fnames);

figure(5)
clf
fc = [125 250 500 1000 2000 4000 8000];
A = NaN(nfiles,1);
for ii = 1:nfiles
	fname = fnames{ii};
	load(fname);
	snd			= double(snddata{1});
	snd			= highpass(snd,'Fc',50,'Fs',Fs,'order',100);
	nsamples	= numel(snd);
	t			= 0:nsamples-1;
	t			= t/Fs;
	subplot(221)
	plot(t,snd);
	hold on
	axis normal;
	xlim([0.2 0.3]);
	subplot(222)
	[fo,ao] = getpower(snd,Fs,'nfft',nfft);
	semilogx(fo,ao);
	hold on
	axis normal;
	ylim([-40 20]);
	[~,idx] = min(abs(fo-fc(ii)));
	plot(fo(idx),ao(idx),'o');
	
		snd			= double(snddata{3});
	snd			= highpass(snd,'Fc',50,'Fs',Fs,'order',100);
	nsamples	= numel(snd);
	t			= 0:nsamples-1;
	t			= t/Fs;
	subplot(223)
	plot(t,snd);
	hold on
	axis normal;
	xlim([0.2 0.3]);
	subplot(224)
	[fi,ai] = getpower(snd,Fs,'nfft',nfft);
	semilogx(fi,ai);
	hold on
	axis normal;
	ylim([-40 20]);
	[~,idx] = min(abs(fo-fc(ii)));
	plot(fi(idx),ai(idx),'o');
	
	A(ii) = ao(idx)-ai(idx);
end


figure(6)
clf
semilogx(fc,A,'ko-','MarkerFaceColor','w','MarkerSize',12,'LineWidth',2);
nicegraph;
set(gca,'XTick',fc);
xlim([fc(1)/2 fc(end)*2]);
ylim([-25 15]);
xlabel('frequency (Hz)');
ylabel('gain (dB)');
axis normal;
title('tones');

figure(4)
hold on
semilogx(fc,A,'ro-','MarkerFaceColor','w','MarkerSize',12,'LineWidth',2);

%% Speech

fname = 'sndrec-aco-avatone-23-03-27-WAV.mat';

load(fname);
snd			= double(snddata{1});
snd			= highpass(snd,'Fc',60,'Fs',Fs,'order',1000);
snd			= lowpass(snd,'Fc',10000,'Fs',Fs,'order',1000);
snd			= detrend(snd);
nsamples	= numel(snd);
t			= 0:nsamples-1;

[u,l] = envelope(snd,1000,'rms');
sd = std(snd);
sel = u>0.1*sd;

figure(7)
clf
subplot(221)
plot(t,snd,'k-');
nicegraph;
axis normal;
hold on
plot(t,u,'r-','LineWidth',2);
horline(0.1*sd);
idx1 = find(sel,1,'first');
idx2 = find(sel,1,'last');

LspeechV = 20*log10(rms(snd(idx1:idx2)));

c = 94-caldB;

LspeechdB = LspeechV+c;
title(['Lspeech = ' num2str(round(LspeechdB,1)) ' dB SPL']);

subplot(222)
[fo,ao] = getpower(snd,Fs,'nfft',nfft);
plot(fo,ao);
xlim(xl);
nicegraph;
axis normal;

snd			= double(snddata{3});
snd			= highpass(snd,'Fc',60,'Fs',Fs,'order',1000);
snd			= lowpass(snd,'Fc',10000,'Fs',Fs,'order',1000);
snd			= detrend(snd);
nsamples	= numel(snd);
t			= 0:nsamples-1;

subplot(223)
plot(t,snd);
nicegraph;
axis normal;

subplot(224)
[fi,ai] = getpower(snd,Fs,'nfft',nfft);
plot(fi,ai);
xlim(xl);
nicegraph;
axis normal;


%%
figure(4)
title('sweep & tone');
savegraph('transfercharacteristic','eps');
% savegraph('transfercharacteristic','eps');

