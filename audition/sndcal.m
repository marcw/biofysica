close all;
clearvars;

% Sweep
flag = 'sweep';

% Noise
% Tones
% Stimuli
% Calibration
flag = 'calibration';
flag = 'tone';
flag = 'WAV';
%create rz6 and tl objects
rz6 = biox_eeg_nirs;                             %create rz6 object
tl = biox_rz6_tasklist;                         %create task list   

if strcmp(flag,'WAV')
	%%
	snddir = 'C:\Users\Svetlana\Documents\MATLAB\OLSA\Sounds\Sounds_NL';
	d = dir([snddir '\*.wav']);
	fnames = {d.name};
	fname = fnames{1};
	fname = fullfile(snddir,fname);
	[Y, FS] = audioread(fname);
	Y = Y/rms(Y);
	whos Y
	%%
	rz6.write_wavdata(Y, 1);   %write wav data to RZ6
	pause(.5)
end
% %set global parameters
% signalbyte = bin2dec('0100 0000');              %signal @ B6 
% rz6.write_signalbyte(signalbyte);               %write signal byte
divSND       = 1;     
chanlist = [1 2 3 4];
%store one per 1 cycle  
%daqSND       = bin2dec('00 0000 0100');      %select daq chans 3

%declare task parameters and assign values
attenuation = [5 5];                              %attenuation 20dB
lpf = 12000;                                     %noise low pass filter
hpf = 200;                                      %noise high pass filter
tasktype = 'SoundA';         %start a sound for channel A
tonefreq = [125 250 500 1000 2000 4000 8000];
%add tasks to list
%timing block
tl.add_task(0.000,'Att', attenuation(1),attenuation(2));
tl.add_task(0.000, 'Daq', 'Start', chanlist, divSND);
%timing block
switch flag
	case 'noise'
		tl.add_task(0.100,tasktype,'noise', hpf, lpf);
		
	case 'sweep'
		tl.add_task(0.100,tasktype,'sweep', 100, 7,1);
	case 'tone'
		soundtype = 'Tone';        %start a tone sound
		tl.add_task(0.100,tasktype,soundtype,tonefreq(7));
	case 'WAV'
		soundtype = 'WAV';        %start a wav sound
		reset = 'Reset';          %start the sound from the beginning
		tl.add_task(0.100, tasktype, soundtype, reset);
		
	case 'calibration'
end
if ~strcmp(flag,'calibration')
		tl.add_task(5.000, tasktype, 'stop');

end
tl.add_task(5.100, 'Daq', 'Stop', chanlist);
tl.add_task(5.200,'Ready');

%add tasks to list
rz6.write_tasklist(tl);


%wait for the task to be ready
while (rz6.read_trialready() == 0)
  % do nothing  
end

snddata  = rz6.read_acqdata(chanlist);  %read acq data from RZ6
 
delete(tl);
delete(rz6);


save(['sndrec-aco-avatone-23-03-27-' flag '.mat'],'snddata');
return
%%
Fs = 48828.125;
Ain = snddata{1};
Aout = snddata{3};
Bin = snddata{2};
Bout = snddata{4};
t = 1:length(Ain);
t = t/Fs;
figure(1)
clf
ax(1) = subplot(121)
plot(t,Ain);
hold on
plot(t,Bin);
nicegraph;

xlabel('time (s)');
ylabel('Amplitude (V)');

ax(2) = subplot(122)
plot(t,Aout);
hold on
plot(t,Bout);
nicegraph;
xlabel('time (s)');
ylabel('Amplitude (V)');

linkaxes(ax,'x');