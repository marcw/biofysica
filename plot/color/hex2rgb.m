function rgb = hex2rgb(hex)
    % HEX2RGB Convert hexadecimal color values to RGB values.
    %
    %   RGB = HEX2RGB(HEX)
    %   
    %   Converts a hexadecimal color string (e.g., '#A3586D') into an RGB 
    %   triplet.
    %
    %   Input:
    %       HEX - A string representing color in hexadecimal format.
    %
    %   Output:
    %       RGB - A 1x3 vector containing red, green, and blue values scaled
    %             between 0 and 1.
    %
    %   Example:
    %       rgb = hex2rgb('#A3586D');
    %
    %   See also RGB2HEX, STATCOLOR

    % Default value if no input provided
    if nargin < 1
        hex = '#A3586D';
    end

    % Remove '#' character if present
    if strcmpi(hex(1), '#')
        hex(1) = [];
    end

    % Convert hex to RGB
    rgb = reshape(sscanf(hex, '%2x'), 3, []).' / 255;
end

