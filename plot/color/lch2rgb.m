function RGB = lch2rgb(LCH)
%PA_LCH2RGB Convert LCH colors to RGB colors.
%
% RGB = LCH2RGB(LCH)
%
% References:
% - http://easyrgb.com/index.php?X=MATH
% - https://en.wikipedia.org/wiki/Lab_color_space
% - http://www.biostat.jhsph.edu/bit/compintro/bruce/RGBland.pdf
%

% Handle default input
if nargin < 1
    ncol = 64;
    l = linspace(0, 1, ncol);
    H = repmat(300, 1, ncol);
    C = zeros(1, ncol);
    L = 90 - l * 30;
    LCH = [L; C; H]';
    LCH = LCH(1:15, :);
end

% Convert CIE L*CH to CIE L*ab
L = LCH(:, 1);
C = LCH(:, 2);
H = LCH(:, 3);
a = cos(H * pi / 180) .* C;
b = sin(H * pi / 180) .* C;

% CIE-L*ab to XYZ
Y = (L + 16) / 116;
X = Y + a / 500;
Z = Y - b / 200;

XYZ = [X Y Z];
sel = XYZ > 6 / 29;
XYZ(sel) = XYZ(sel).^3;
XYZ(~sel) = (XYZ(~sel) - 4 / 29) * 3 * (6 / 29)^2;

% Reference white point adjustment
ref = [95.05, 100.000, 108.90] / 100;
XYZ = bsxfun(@times, XYZ, ref);

% XYZ to RGB conversion using transformation matrix
T = [3.2406, -1.5372, -0.4986; ...
    -0.9689, 1.8758, 0.0415; ...
    0.0557, -0.2040, 1.0570];
RGB = XYZ * T';

% Gamma correction for sRGB
threshold = 0.0031308; % specific threshold for colorspace
sel = RGB > threshold;
RGB(sel) = 1.055 * (RGB(sel).^(1 / 2.4)) - 0.055;
RGB(~sel) = 12.92 * RGB(~sel);

% Clip RGB values to [0, 1]
RGB = max(min(RGB, 1), 0);

end
