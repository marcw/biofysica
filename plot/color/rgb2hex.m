function hexColors = rgb2hex(rgbValues)
    % RGB2HEX Convert RGB color values to hexadecimal color strings.
    %
    %   HEXCOLORS = RGB2HEX(RGBVALUES)
    %
    %   Converts an Nx3 matrix of RGB values into an N-cell array of
    %   hexadecimal color strings.
    %
    %   Input:
    %       RGBVALUES - An Nx3 matrix where each row represents Red, Green, and Blue color values.
    %                   Expected to be in [0, 255] range.
    %
    %   Output:
    %       HEXCOLORS - An N-cell array containing the hexadecimal color representations.
    %
    %   Example:
    %       hexColors = rgb2hex([255, 0, 0; 0, 255, 0; 0, 0, 255]);
    %
    %   See also HEX2RGB

    % Ensure RGB values are in the right range
    if any(rgbValues(:) < 0) || any(rgbValues(:) > 255)
        error('RGB values should be in the [0, 255] range.');
    end

    % Convert RGB to HEX
    hexColors = arrayfun(@(row) sprintf('#%02X%02X%02X', rgbValues(row, :)), ...
                         1:size(rgbValues, 1), 'UniformOutput', false);
end
