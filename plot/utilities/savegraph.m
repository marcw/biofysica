function savegraph(file,type)
% SAVEGRAPH(FILE,TYPE)
%
% Functions for opening and saving graphics that operate the same for
% Windows and Macintosh operating systems.
%
% See also OPENGRAPH

%% Initialization
if nargin<1
	file = 'saveGraphOutput';
end
if nargin<2
	type = 'eps';
end

%% Maximum size
fig = gcf;

%-- PB: 2024/11/15: issue with macOS Sonoma	--%
% set(fig,'units','normalized','outerposition',[0 0 1 1])
% fig.PaperPositionMode = 'auto';

%% Save
file	= fcheckext(file,type);
formats = {'png','jpeg','jpg','tiff','bmp','pdf','eps','svg'};
if any(strcmp(type,formats))
	sptype = ['-d' type];
	switch type
		case 'jpg'
			sptype = '-djpeg';
		case 'bmp'
			sptype = '-dbmp16m'; %24 bit BMP file format
		case 'eps'
			sptype = '-depsc';
		case 'pdf'
			sptype = '-dpdf';
		case 'svg'
			sptype = 'svg';
	end
	switch type
		case 'eps'
			if ispc && verLessThan('matlab', '8.1.0') % Windows OS for older Matlab versions
				print(sptype,'-r300','-painter',file); % to really save in Vector format
			else % Mac OS
				print(sptype,'-r300','-painters',file); % to really save in Vector format
			end
		case 'pdf'
			if ispc && verLessThan('matlab', '8.1.0') % Windows OS for older Matlab versions
				print(sptype,'-r300','-painter','-bestfit',file); % to really save in Vector format
			else % Mac OS
% 			sptype = 'pdf';
				print(sptype,'-r300','-painters','-fillpage',file); % to really save in Vector format
% 				saveas(fig,file,sptype);

			end
		case 'svg'
			saveas(gcf,file,sptype);
		otherwise
			print(sptype,'-r300',file);
	end
end