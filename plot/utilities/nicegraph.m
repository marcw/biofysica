function nicegraph(varargin)
% NICEGRAPH(FIG, 'PropertyName', PropertyValue, ...)
% Enhances the aesthetics of a MATLAB figure.
%
% Input:
% - FIG: handle to the figure (optional)
% - 'SquareAxis': true/false, set axis to square (default: true)
%
% NICEGRAPH(FIG, 'FontSize', SIZE, 'LineWidth', WIDTH) allows for customization
% of the graph's font size and line width.
%
% Example:
%   nicegraph(gcf, 'SquareAxis', false);
%
% See also SAVEGRAPH

% Parse input arguments
p = inputParser;


% Check if the first argument is a figure handle
if nargin > 0 && ishandle(varargin{1}) && strcmp(get(varargin{1}, 'Type'), 'figure')
    fig = varargin{1};
    varargin(1) = [];  % Remove first element so it's not parsed again below
else
    fig = gcf;  % Use current figure as the default
end

% addOptional(p, 'fig', gcf, @(x) ishandle(x) && strcmp(get(x, 'Type'), 'figure'));
addParameter(p, 'SquareAxis', true, @islogical);
addParameter(p, 'YMinorTicks', 'auto', @(x) isnumeric(x) || strcmp(x, 'auto'));
addParameter(p, 'FontSize', 20, @isnumeric);
addParameter(p, 'LineWidth', 1, @isnumeric);
parse(p, varargin{:});

% Retrieve parsed parameters
fontSize = p.Results.FontSize;
lineWidth = p.Results.LineWidth;

% Apply customizations to the figure's current axes

figure(fig);

% Set properties for the axes
set(gca, ...
    'Box' 	, 'off'       , ...
    'TickDir'		, 'out'       , ...
    'TickLength'	, [.02 .02]   , ...
    'XMinorTick'	, 'off'        , ...
    'YMinorTick'	, 'off'        , ...
    'YGrid'			, 'off'       , ...
    'XGrid'			, 'off'       , ...
    'XColor'		, [.3 .3 .3]  , ...
    'YColor'		, [.3 .3 .3]  , ...
    'FontSize'		, fontSize, ...
    'YDir'         , 'normal'    , ...
    'LineWidth', lineWidth);

% Set square axis if specified
if p.Results.SquareAxis
    axis square;
end



end

