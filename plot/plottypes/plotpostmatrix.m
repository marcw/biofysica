function plotpostmatrix(data,varargin)
% Create a sample 3D array with random data.
% In this example, the array has 10 rows, 5 columns, and 3 slices.
% You can replace this with your own 3D array.
% data = rand(10, 5, 3);

%% Initialization
if nargin<1
	% Create a sample 3D array with random data.
	% In this example, the array has 10 rows, 5 columns, and 3 slices.
	% You can replace this with your own 3D array.
	data = rand(1000, 5, 3);
end


if exist('cbrewer','file')
	% cmap = flipud(cbrewer('div','RdYlBu',64));
	cmap = flipud(cbrewer('div','RdBu',64));
	
else
	cmap = jet(64);
end

credMass	= keyval('credMass', varargin,0.95);
% showMode	= keyval('showMode',varargin,false);
% showCurve	= keyval('showCurve', varargin,false);
% compVal		= keyval('compVal', varargin);
% ROPE		= keyval('ROPE', varargin);
% yaxt		= keyval('yaxt', varargin);
ylab		= keyval('ylab', varargin,'Y value');
xlab		= keyval('xlab', varargin,'X value');
% xl			= keyval('xlim', varargin,minmax([compVal; paramSampleVec]'));
% main		= keyval('main', varargin);
cmap			= keyval('Color', varargin,cmap);
% breaks		= keyval('breaks', varargin);
% nbreaks		= keyval('nbreaks', varargin,18);


%%

% Flatten the data for histogram plotting
flat_data = data(:);

% Generate histogram data without actually plotting it
[~, edges] = histcounts(flat_data);

% Get the size of the 3D array
[~, cols, rows] = size(data);

% Get max count per factor
mx_counts = NaN(cols,rows);
hdi = NaN(2,cols,rows);
for cc = 1:cols
	for rr	= 1:rows
		sq_data = squeeze(data(:,cc,rr));
		counts				= histcounts(sq_data,edges);
		mx_counts(cc,rr)	= max(counts);
		hdi(:,cc,rr)		= hdimcmc(sq_data,0.99);
		
	end
end
% Normalize counts to max 1 for plotting purposes
mx_counts	= max(mx_counts(:));
% Normalize edges to contain most of the data
lower = hdi(1,:,:);
lower = lower(:);
upper = hdi(2,:,:);
upper = upper(:);

min_edge = min(lower);
max_edge = max(upper);

min_edge			= keyval('min_edge', varargin,min_edge);
max_edge			= keyval('max_edge', varargin,max_edge);

%%
mu			= mean(flat_data); % average population data = 0
mu			= keyval('mu', varargin,mu);

hdi			= hdimcmc(flat_data,0.95); % highest density interval
mx_edges	= max(abs(hdi-mu));
sel			= edges<=max_edge & edges>=min_edge;
edges		= edges(sel);
% Get color data
mu_data		= squeeze(mean(data));
min_data	= -mx_edges+mu;
max_data	= mx_edges+mu;

min_data			= keyval('min_data', varargin,min_data);
max_data			= keyval('max_data', varargin,max_data);

ncol		= size(cmap,1);
s			= round(1+(ncol-1)*(mu_data-min_data)/(max_data-min_data));
rgb_image	= ind2rgb(s,cmap);

% x_width = mx_edges*2;
x_width = 1.1*(max_edge-min_edge);
y_width = 1.1;
% Loop through each slice to plot histograms
for cc = 1:cols
	for rr = 1:rows
		% Initialize variables to hold the x and y offsets for each histogram
		x_offset = (cc-1)*x_width;
		y_offset = (rr-1)*y_width;
		
		% Extract the slice of data at the 3rd dimension k
		slice_data = squeeze(data(:, cc, rr));
		
		% Flatten the data for histogram plotting
		flat_data = slice_data(:);
		
		
		% Generate histogram data without actually plotting it
		counts = histcounts(flat_data,edges);
		% Normalize to max 1
		counts = counts/mx_counts;
		
		% Compute bin centers and widths
		bin_centers = (edges(1:end-1) + edges(2:end)) / 2;
		bin_width = edges(2) - edges(1);
		
		% Adjust bin centers for x-offset
		bin_centers = bin_centers + x_offset;
		
		% Draw image square
		x = [x_offset - x_width / 2, x_offset + x_width / 2, ...
			x_offset + x_width / 2, x_offset - x_width / 2, ...
			x_offset - x_width / 2]+mu;
		y = [0, 0, y_width, y_width, 0]+y_offset;  % Add y_offset to the base
% 		whos rgb_image
% 		[cols cc]
% 		[rows rr]
		
		try
		rgb = squeeze(rgb_image(cc,rr,:));
		catch
					rgb = squeeze(rgb_image(rr,cc,:));
		end

		patch(x, y, 'k', 'FaceAlpha', 0.6,'FaceColor',rgb,'EdgeColor','none');
		
		hold on;
		
		% Draw bars manually using patch function
		for i = 1:length(bin_centers)
			if counts(i)
				x = [bin_centers(i) - bin_width / 2, bin_centers(i) + bin_width / 2, ...
					bin_centers(i) + bin_width / 2, bin_centers(i) - bin_width / 2, ...
					bin_centers(i) - bin_width / 2];
				y = [0, 0, counts(i), counts(i), 0] + y_offset;  % Add y_offset to the base
				patch(x, y, 'k','FaceColor',rgb,'EdgeColor','k');
				% 					patch(x, y, 'k','FaceColor',rgb,'EdgeColor',rgb);
			end
		end
		
		HDI	= hdimcmc(flat_data,credMass);
		plot(HDI+x_offset,[0 0]+y_offset,'k-','LineWidth',3);
	end
end
xlim([-x_width/2+mu (cols-1)*x_width+x_width/2+mu])
ylim([0 rows*y_width])
nicegraph;

xt = ((1:cols)-1)*x_width+mu;
yt = ((1:rows)-0.5)*y_width;
xtlab		= keyval('XTickLabel', varargin,1:cols);
ytlab		= keyval('YTickLabel', varargin,1:rows);

set(gca,'XTick',xt,'XTickLabel',xtlab);
set(gca,'YTick',yt,'YTickLabel',ytlab);
set(gca,'XMinorTick'  , 'off'      , ...
	'YMinorTick'  , 'off');
verline(((1:cols)-1)*x_width+mu,'k-')
% Add axis labels and title
xlabel(xlab);
ylabel(ylab);
colorbar;
colormap(cmap);

end

