function [hpatch,hline] = errorpatch(X,Y,E,col,opacity)
% ERRORPATCH Plots the graph of vector X vs. vector Y with an error patch specified by the vector E.
%
% INPUTS:
%   X - Data for x-axis
%   Y - Data for y-axis
%   E - Error data
%   col - Color specification for plot and patch
%   opacity - Alpha value for error patch
%
% OUTPUTS:
%   HPATCH - Handle to patchseries
%   HLINE - Handle to lineseries

if nargin < 4
    col = 'k';
end

if nargin < 5
    opacity = 0.4;
end

% Ensure vectors are row vectors
X = ensureRowVector(X, 'X');
Y = ensureRowVector(Y, 'Y');
E = ensureRowVectorOrMatrix(E, 'E');

% Check for dimension compatibility
if length(Y) ~= length(X)
    error('Y and X should be the same size');
end

if size(E,2) ~= size(X,2)
    error('E and X should be the same size');
end

% Remove NaN values
[ X, Y, E ] = removeNaNs(X, Y, E);

% Create patch
x = [X, fliplr(X)];
if size(E,1) > 1
    y = [E(1,:), fliplr(E(2,:))];
else
    y = [Y+E, fliplr(Y-E)];
end

% Plotting
hpatch = patch(x, y, col, 'EdgeColor', 'none');
alpha(hpatch, opacity);
hold on;
hline = plot(X, Y, 'Color', col, 'LineWidth', 2);


end

function vec = ensureRowVector(vec, name)
    if size(vec,1) > 1
        vec = vec(:)';
        if size(vec,1) > 1
            error('%s should be a row vector', name);
        end
    end
end

function vec = ensureRowVectorOrMatrix(vec, name)
    if size(vec,1) > 2
        vec = vec(:)';
        if size(vec,1) > 2
            error('%s should be a row vector or a 2-row matrix', name);
        end
    end
end

function [ X, Y, E ] = removeNaNs(X, Y, E)
    if size(E,1) > 1
        sel = isnan(X) | isnan(Y) | isnan(E(1,:)) | isnan(E(2,:));
        E = E(:, ~sel);
    else
        sel = isnan(X) | isnan(Y) | isnan(E);
        E = E(~sel);
    end
    X = X(~sel);
    Y = Y(~sel);
end
