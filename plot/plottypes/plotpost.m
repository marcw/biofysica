function [postSummary, histInfo, h] = plotpost(paramSampleVec, varargin)
% PLOTPOST Plot and summarize the MCMC parameter sample vector.
%
%   [postSummary, histInfo, h] = PLOTPOST(paramSampleVec) plots a histogram
%   or density curve based on the MCMC parameter sample vector 'paramSampleVec'.
%
% Inputs:
%   - paramSampleVec: Numeric vector, containing the parameter samples.
%
% Optional Name-Value Pair Arguments:
%   - 'credMass': Scalar, credibility mass to calculate HDI. Default is 0.95.
%   - 'showMode': Logical, whether to display mode in the plot. Default is false.
%   - 'showCurve': Logical, whether to display density curve. Default is false.
%   - 'compVal': Scalar, comparison value to be plotted. Default is [].
%   - 'ROPE': Vector, containing lower and upper bounds of ROPE. Default is [].
%   - 'yaxt': Vector, specifying the ticks on the y-axis. Default is [].
%   - 'ylab': String, y-axis label. Default is [].
%   - 'xlab': String, x-axis label. Default is 'Parameter'.
%   - 'xlim': Vector, limits for the x-axis. Default is calculated based on data.
%   - 'main': String, title of the plot. Default is [].
%   - 'Color': Vector, specifying the RGB values for bar or line color. Default is [.6 .6 1].
%   - 'breaks': Vector, positions for the histogram breaks. Default is [].
%   - 'nbreaks': Scalar, number of breaks if 'breaks' is not specified. Default is 18.
%
% Outputs:
%   - postSummary: Structure, containing summary statistics like mean, median, mode, HDI, etc.
%   - histInfo: Structure, containing histogram information such as bin counts, density, and breaks.
%   - h: Handle to the graphical elements in the plot.
%
% Examples:
%   [postSummary, histInfo, h] = PLOTPOST(randn(1, 1000), 'credMass', 0.9);
%   [postSummary, histInfo, h] = PLOTPOST(randn(1, 1000), 'showCurve', true);
%
% Reference:
%   - Original in R: Kruschke, J. K. (2011). Doing Bayesian Data Analysis:
%     A Tutorial with R and BUGS. Academic Press / Elsevier.

% Modified to Matlab by: Marc M. van Wanrooij


%% Initialization
% Define default parameters
p			= inputParser;
addRequired(p, 'paramSampleVec', @isnumeric);
addParameter(p, 'credMass', 0.95, @isnumeric);
addParameter(p, 'showMode', false, @islogical);
addParameter(p, 'showCurve', false, @islogical);
addParameter(p, 'compVal', [], @isnumeric);
addParameter(p, 'ROPE', [], @isnumeric);
addParameter(p, 'yaxt', []);
addParameter(p, 'ylab', '');
addParameter(p, 'xlab', 'Parameter');
addParameter(p, 'xlim', []);
addParameter(p, 'main', '');
addParameter(p, 'Color', [43,140,190]/255);
addParameter(p, 'breaks', []);
addParameter(p, 'nbreaks', 18, @isnumeric);
parse(p, paramSampleVec, varargin{:});

paramSampleVec = paramSampleVec(:);

% Extract parsed parameters
credMass	= p.Results.credMass;
showMode	= p.Results.showMode;
showCurve	= p.Results.showCurve;
compVal		= p.Results.compVal;
ROPE		= p.Results.ROPE;
yaxt		= p.Results.yaxt;
ylab		= p.Results.ylab;
xlab		= p.Results.xlab;
xlim_opt	= p.Results.xlim;

if isempty(xlim_opt)
	xlim_opt = minmax([compVal; paramSampleVec]');
end
main		= p.Results.main;
col			= p.Results.Color;
breaks		= p.Results.breaks;
nbreaks		= p.Results.nbreaks;

%% Do The Work
% Your existing computation code, slightly reorganized
postSummary			= computePostSummary(paramSampleVec, credMass,ROPE,compVal);
[histInfo, breaks]	= computeHistInfo(postSummary,paramSampleVec, breaks, nbreaks);

% Your existing plot code, slightly reorganized
h					= plotHistAndCurve(postSummary, histInfo, breaks, showCurve, col);

% Your existing label and display code, slightly reorganized
annotatePlot(postSummary, compVal, ROPE, credMass, xlab, ylab, yaxt, main, xlim_opt, histInfo,showMode);
end

% function postSummary = computePostSummary(paramSampleVec, credMass,ROPE,compVal)
% % Compute summary statistics for the posterior distribution.
% postSummary.mean	= nanmean(paramSampleVec);
% postSummary.median	= nanmedian(paramSampleVec);
% postSummary.mode	= kdeMode(paramSampleVec);
% HDI					= hdimcmc(paramSampleVec,credMass);
% postSummary.hdiMass = credMass;
% postSummary.hdiLow	= HDI(1);
% postSummary.hdiHigh	= HDI(2);
% % Determine the ROPE
% if ~isempty(ROPE)
% 	pcInROPE				= sum(paramSampleVec>ROPE(1) & paramSampleVec<ROPE(2))/length(paramSampleVec);
% 	postSummary.ROPElow		= ROPE(1);
% 	postSummary.ROPEhigh	= ROPE(2);
% 	postSummary.pcInROPE	= pcInROPE;
% end
% % Compare to the comparison value.
% if ~isempty(compVal)
% 	postSummary.compVal		= compVal;
% 	postSummary.pcGTcompVal = sum(paramSampleVec>compVal)/length(paramSampleVec);
% end
% end

function [histInfo, breaks] = computeHistInfo(postSummary,paramSampleVec, breaks, nbreaks)
% Compute histogram information.
if isempty(breaks)
	by=(postSummary.hdiHigh-postSummary.hdiLow)/nbreaks;
	breaks = unique([min(paramSampleVec):by:max(paramSampleVec) max(paramSampleVec)]);
end

N					= histc(paramSampleVec,breaks);
db					= mean(diff(breaks));
histInfo.N			= N;
histInfo.density	= N./db/sum(N);
histInfo.breaks		= breaks;
end

% function [histInfo, updatedBreaks] = computeHistInfo(postSummary, paramSampleVec, inputBreaks, numBreaks)
%     % Validate inputs
%     if ~isstruct(postSummary) || ~isfield(postSummary, 'hdiHigh') || ~isfield(postSummary, 'hdiLow')
%         error('Invalid postSummary input.');
%     end
%     
%     if ~isnumeric(paramSampleVec)
%         error('paramSampleVec must be a numeric array.');
%     end
%     
%     if ~isempty(inputBreaks) && ~isnumeric(inputBreaks)
%         error('inputBreaks must be a numeric array.');
%     end
%     
%     if ~isnumeric(numBreaks)
%         error('numBreaks must be a numeric value.');
%     end
% 
%     % Calculate breaks if not provided
%     if isempty(inputBreaks)
%         binWidth = (postSummary.hdiHigh - postSummary.hdiLow) / numBreaks;
%         updatedBreaks = unique([min(paramSampleVec):binWidth:max(paramSampleVec), max(paramSampleVec)]);
%     else
%         updatedBreaks = inputBreaks;
%     end
% 
%     % Compute histogram data
%     binCounts = histcounts(paramSampleVec, updatedBreaks);
%     binWidth = mean(diff(updatedBreaks));
% 
%     % Populate output structure
%     histInfo.binCounts	= binCounts;
%     histInfo.density	= binCounts ./ (binWidth * sum(binCounts));
%     histInfo.breaks		= updatedBreaks;
% end


function h = plotHistAndCurve(postSummary, histInfo, breaks, showCurve, col)
% Plot histogram and curve.
if ~showCurve
	h = bar(breaks,histInfo.density,'histc');
	delete(findobj('marker','*')); % Matlab bug?
	set(h,'FaceAlpha', 0.6,'FaceColor',col,'EdgeColor',col);
end
if showCurve
	if strcmp(showCurve,'patch')
		hpatch = patch(postSummary.mcmcDensity.x,postSummary.mcmcDensity.y,col);
		alpha(hpatch,0.3);
	end
	h = plot(postSummary.mcmcDensity.x, postSummary.mcmcDensity.y,'b-');
	set(h,'Color',col,'LineWidth',2);
	
end
hold on
end

function annotatePlot(postSummary, compVal, ROPE, credMass, xlab, ylab, yaxt, main, xlim_opt, histInfo,showMode)
% Annotate the plot with additional information.
xlabel(xlab);
ylabel(ylab);
box off;
xlim(xlim_opt);
title(main);
set(gca,'YTick',yaxt);

cenTendHt	= 1.1*nanmax(histInfo.density);
cvHt		= 0.7*nanmax(histInfo.density);
ROPEtextHt	= 0.55*nanmax(histInfo.density);


% Display the comparison value.
if ~isempty(compVal)
	cvCol					= [0 .3 0];
	pcgtCompVal				= round(100*sum(paramSampleVec>compVal)/length(paramSampleVec)); % percentage greater than
	pcltCompVal				= 100 - pcgtCompVal; % percentage lower than
	plot([compVal compVal],[0.96*cvHt 0],'k-','Color',cvCol,'LineWidth',2);
	str						= [num2str(pcltCompVal) '% < ' num2str(compVal,3) ' < ' num2str(pcgtCompVal) '%'];
	text(compVal,cvHt,str,'HorizontalAlignment','center','Color',cvCol);
	postSummary.compVal		= compVal;
	postSummary.pcGTcompVal = sum(paramSampleVec>compVal)/length(paramSampleVec);
end
% Display the ROPE.
if ~isempty(ROPE)
	ropeCol					= [.5 0 0];
	pcInROPE				= sum(paramSampleVec>ROPE(1) & paramSampleVec<ROPE(2))/length(paramSampleVec);
	plot(ROPE([1 1]),[0 0.96*ROPEtextHt],':','LineWidth',2,'Color',ropeCol);
	plot(ROPE([2 2]),[0 0.96*ROPEtextHt],':','LineWidth',2,'Color',ropeCol);
	
	str						= [num2str(round(100*pcInROPE)) '% in ROPE'];
	text(mean(ROPE),ROPEtextHt,str,'Color',ropeCol,'HorizontalAlignment','center');
	
	postSummary.ROPElow		= ROPE(1);
	postSummary.ROPEhigh	= ROPE(2);
	postSummary.pcInROPE	= pcInROPE;
end
% Additional stuff: To make Matlab figure look more like R figure
ax					= axis;
yl					= ax([3 4]);
ydiff				= yl(2)-yl(1);
yl					= [yl(1)-ydiff/5 yl(2)+ydiff/5];
ylim(yl);
set(gca,'TickDir','out');
% axis square;

% Display mean or mode:
if ~showMode
	m			= round(postSummary.mean, 3, 'significant');
	str			= ['mean = ' num2str(m)];
	text(postSummary.mean,cenTendHt,str,'HorizontalAlignment','center','FontSize',15);
elseif strcmp(showMode,'median')
	m			= round(postSummary.median, 3, 'significant');
	str			= ['median = ' num2str(m)];
	text(postSummary.median,cenTendHt,str,'HorizontalAlignment','center','FontSize',15);
else
	m			= round(postSummary.mode, 3, 'significant');
	str			= ['mode = ' num2str(m)];
	text(postSummary.mode, cenTendHt, str,'HorizontalAlignment','center','FontSize',15);
end

% Display the HDI.
plot([postSummary.hdiLow postSummary.hdiHigh],[0 0],'k-','LineWidth',4);
str			= [num2str(100*credMass,2) '%HDI'];
text(mean([postSummary.hdiLow postSummary.hdiHigh]),-ydiff/10,str,'HorizontalAlignment','center','FontSize',12);

m			= round(postSummary.hdiLow, 3, 'significant');
str			= num2str(m);
text(postSummary.hdiLow,-ydiff/10,str,'HorizontalAlignment','right','FontSize',15,'VerticalAlignment','middle');
m			= round(postSummary.hdiHigh, 3, 'significant');
str			= num2str(m);
text(postSummary.hdiHigh,-ydiff/10,str,'HorizontalAlignment','left','FontSize',15,'VerticalAlignment','middle');

end
