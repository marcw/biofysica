function violinplot(x, y, varargin)
% VIOLINPLOT(X, Y)
% Plots a violin plot of continuous metric data Y against a nominal, categorical variable X.

% Key-value inputs:
% 'Color' - a 3xN color matrix

% Input parsing
p				= inputParser;
addRequired(p, 'x');
[ux, ~, ix]		= unique(x);
uix				= unique(ix);
nx				= numel(ux);
addRequired(p, 'y');
if exist('cbrewer', 'file')
    col			= cbrewer('qual', 'Dark2', nx);
else
    col			= jet(nx);
end
addOptional(p, 'Color', col);
addOptional(p, 'xlim', [min(x)-1 max(x)+1]);
addOptional(p, 'ylim', [min(y(:))-1 max(y(:))+1]);

parse(p, x, y, varargin{:});
col				= p.Results.Color;

% Plotting violin plots
M = NaN(nx, 1);
for ii = 1:nx
    sel			= ix == uix(ii);
    a			= y(:, sel);
    hdi			= hdimcmc(a);
    mu			= mean(y(:, sel));
    M(ii)		= mu;
    
    [F, XI]		= ksdensity(y(:, sel));
    F			= 0.4 * F ./ max(F);
    f			= F + ii;
    xi			= XI;
    
    hpatch		= patch(f', xi', col(ii, :));
    hold on;
    
    set(hpatch, 'EdgeColor', 'none');
    
    % Use default ink (can consider other styles commented out below)
    plot([ii ii], hdi, '-', 'LineWidth', 3, 'Color', 'k');
    plot(ii, mu, 'o', 'MarkerFaceColor', 'w', 'MarkerEdgeColor', 'k', 'MarkerSize', 10);
    
    % Other potential ink styles
    % alpha(hpatch, 0.5);
    % plot([ii ii], hdi, '-', 'LineWidth', 3, 'Color', col(ii, :));
    % plot(ii, mu, 'o', 'MarkerFaceColor', col(ii, :), 'MarkerEdgeColor', col(ii, :), 'MarkerSize', 15);
    % plot([ii ii], hdi, '-', 'LineWidth', 3, 'Color', 'w');
    % plot(ii, mu, 'o', 'MarkerFaceColor', 'w', 'MarkerEdgeColor', 'w', 'MarkerSize', 10);
end

% Setting the axis properties
set(gca, 'XTick', 1:nx, 'XTickLabel', ux);
xlabel('independent parameter');
ylabel('dependent parameter');
xlim(p.Results.xlim);
ylim(p.Results.ylim);

end
