function theme = getFuturamaTheme()

		theme = struct();
		theme.colors  = struct(...
			'background', '#1e8449', ...
			'text',    '#e74c3c', ...
			'panel',   '#abebc6', ...
			'border',  '#abb2b9', ...
			'lighter', '#f4d03f', ...
			'light',   '#1e8449', ...
			'medium',  '#ec7063', ...
			'dark',    '#e74c3c');
		
		theme.fontSize		= 24;
		theme.lineWidth		= 1;
		% 		https://fonts.google.com/specimen/Dongle
		theme.font			= "Dongle";
		theme.cmap			= color_gradient(theme.colors.light, theme.colors.dark, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 26;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.background;
		theme.faceColor		= theme.colors.dark;
		theme.faceAlpha		= 0.3;
		theme.axisSquare	= true;
		theme.xColor		=  theme.colors.lighter;
		theme.yColor		=  theme.colors.lighter;
		theme.colorsPanel	= theme.colors.panel;
end
