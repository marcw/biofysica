function theme = getSpidermanTheme()
		theme = struct();
		theme.colors  = struct( ...
			'background', 'b', ...
			'text',    '#101010', ...
			'panel',   '#de0619', ...
			'border',  '#101010', ...
			'light',   '#e27B78', ...
			'medium',  '#e55751', ...
			'dark',    '#601a18', ...
			'white',   '#fefefe', ...
			'blue',    '#333399' ... 
			);
		
		
		theme.fontSize		= 18;
		theme.lineWidth		= 1;
		% 		https://fonts.google.com/specimen/
		theme.font			= "Bangers";
		theme.cmap			= color_gradient(theme.colors.medium, theme.colors.blue, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 20;
		theme.faceAlpha		= 0.3;
		theme.axisSquare	= true;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.panel;
		theme.faceColor		= theme.colors.dark;
		theme.xColor		= theme.colors.border;
		theme.yColor		= theme.colors.border;
		theme.colorsPanel	= theme.colors.light;
end

