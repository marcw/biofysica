% DEMOAPPLYTHEME demonstrates how to apply a theme to a figure containing 
% relationship plots and an image plot.
function demoApplyTheme

%% Initialize the figure
% If a figure with the identifier 1 exists, it'll be cleared, else it'll create a new figure.
fig = figure(1);
clf;

%% Plot Data
% This creates the scatter plot and regression line for a relationship dataset.
createRelationshipPlot();

% This visualizes a matrix as a color-coded image in a subplot.
createImagePlot();

%% Theme Application
% Selects a theme for styling the plots. 
% Options: 'barbie', 'alien', 'futurama', 'gameofthrones'. Change the name for a different theme.
% theme_name = 'barbie';
% theme_name = 'alien';
% theme_name = 'futurama';
% theme_name = 'gameofthrones';
% theme_name = 'simpsons';
% theme_name = 'spiderman';
theme_name = 'oppenheimer';

applyTheme(theme_name, fig);

end

%% HELPER FUNCTIONS

% CREATE RELATIONSHIP PLOT:
% This function creates a scatter plot with a regression line.
% The relationship shown is between 'stimulus' and 'response'.
function createRelationshipPlot()
% Initiates a subplot in a 1-row, 2-column configuration, on the left side.
subplot(121);

x = linspace(-90,90,100);
y = 0.8*x + 15*randn(size(x));

% Plot scattered data.
plot(x, y, 'o');
hold on;

% % Calculate regression line and plot it.
% b = regstats(y,x);
% xi = [min(x) max(x)];
% yi = b.beta(1) + b.beta(2) * xi;
% plot(xi, yi, '-');
% Fit a linear regression model
[p, S] = polyfit(x, y, 1); % 'p' contains the coefficients and 'S' provides additional statistics

% Evaluate the linear regression over the data range
yfit = polyval(p, x);
plot(x,yfit,'-');
% Compute the confidence bounds
% [~, ~, ~, ~, stats] = regress(y', [ones(size(x')) x']); % Getting residuals and regression stats
[~, delta] = polyconf(p, x, S, 'predopt', 'curve', 'alpha', 0.05, 'simopt', 'on');

% Add labels and a title to the plot for context.
xlabel('stimulus');
ylabel('response');
title('relationship');

% Add moving average error patch (assuming functionality for this is available).
% [avg, ~] = movavg(5 * sqrt(b.r.^2), 10);
% keyboard
E = [yfit-delta;yfit+delta];
errorpatch(x, yfit, E);
xlim([-100 100]);
ylim([-100 100]);

end

% CREATE IMAGE PLOT:
% This function renders a matrix as a color-coded image.
function createImagePlot()
% Initiates a subplot in a 1-row, 2-column configuration, on the right side.
subplot(122);

% Define a matrix.
C = [0 2 4 6; 8 10 12 14; 16 18 20 22];

% Display the matrix as a color-coded image.
imagesc(C);
% contourf(C)
colorbar;

% Add labels and a title to the image plot for context.
xlabel('stimulus');
ylabel('response');
title('relationship');
end
