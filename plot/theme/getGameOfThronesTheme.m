function theme = getGameOfThronesTheme()
		theme = struct();
		theme.colors  =  struct(...
			'background', '#1f0700', ...
			'text',       '#D7B257', ...
			'panel',      '#F2F3B8', ...
			'border',     '#BD6D33', ...
			'lighter',    '#F2F3B8', ...
			'light',      '#DFCB69', ...
			'medium',     '#BD6D33', ...
			'dark',       '#8C4522' ...
			);
		theme.fontSize		= 24;
		theme.lineWidth		= 1;
		% 		https://fonts.google.com/specimen/Cinzel
		theme.font			= "Cinzel Decorative";
		theme.cmap			= color_gradient(theme.colors.light, theme.colors.dark, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 26;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.background;
		theme.faceColor		= theme.colors.dark;
		theme.faceAlpha		= 0.3;
		theme.axisSquare	= true;
		theme.xColor		=  theme.colors.border;
		theme.yColor		=  theme.colors.border;
		theme.colorsPanel	= theme.colors.panel;
		
end
