function theme = getAlienTheme()

		theme = struct();
		theme.colors = struct(...
			'background', '#000000', ...
			'text',    '#F0F0F0', ...
			'panel',   '#0B0C07', ...
			'border',  '#81CF47', ...
			'lighter', '#CAF6A7', ...
			'light',   '#ACF570', ...
			'medium',  '#76D52F', ...
			'dark',    '#1D6001');
		theme.fontSize		= 18;
		theme.lineWidth		= 1;
		% 		https://fonts.google.com/specimen/Archivo+Black
		theme.font			= "HomepageBaukasten"; %"Archivo Black"; %
		theme.cmap			= color_gradient(theme.colors.light, theme.colors.dark, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 24;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.background;
		theme.faceColor		= theme.colors.dark;
		theme.faceAlpha		= 0.3;
		theme.axisSquare	= true;
		theme.xColor		=  theme.colors.border;
		theme.yColor		=  theme.colors.border;
		theme.colorsPanel	= theme.colors.panel;
		
end
