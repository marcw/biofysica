function theme = getDefaultTheme()
    theme = struct();
		theme.colors = struct(...
			'background', '#ffffff', ...
			'text',    '#a62675', ...
			'panel',   '#fdf6fa', ...
			'border',  '#d74ea2', ...
			'lighter', '#f5d1e6', ...
			'light',   '#eeb4d7', ...
			'medium',  '#d74ea2', ...
			'dark',    '#bf2986');
		theme.fontSize		= 18;
		theme.lineWidth		= 1;
		theme.font			= "Sansita Swashed";
		theme.cmap			= color_gradient(theme.colors.light, theme.colors.dark, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 24;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.background;
		theme.axisSquare	= true;
		theme.faceColor		= theme.colors.dark;
		theme.faceAlpha		= 0.3;
		theme.xColor		= theme.colors.border;
		theme.yColor		= theme.colors.border;
		theme.colorsPanel	= theme.colors.panel;
end

%   ggplot2::theme(
%     panel.grid.minor = element_blank(),
%     panel.grid.major = element_blank(),
%     text = element_text(color = oppenheimer_theme_colors["text"], family = font_family),
%     title = element_text(size=20),
%     panel.background = element_rect(fill = oppenheimer_theme_colors["panel"]),
%     panel.border = element_rect(fill = NA, color = oppenheimer_theme_colors["border"],linewidth=1.4),
%     axis.title = element_text(size=17),
%     axis.text = element_text(size=15,color = oppenheimer_theme_colors["text"]),
%     axis.ticks = element_line(color = oppenheimer_theme_colors["border"],linewidth=1),
%     legend.background = element_rect(fill = oppenheimer_theme_colors["panel"], color = NA),
%     strip.background = element_rect(fill = oppenheimer_theme_colors["light"], colour = oppenheimer_theme_colors["light"]),
%     strip.text = element_text(colour = oppenheimer_theme_colors["text"]),
%     ...
%   )
% }
%
% 
% Here's how MATLAB plot properties correspond to the ggplot2 theme elements:
% 
% 1. panel.grid.minor & panel.grid.major:
%    - In MATLAB, the equivalent is grid on for major gridlines. MATLAB
%    doesn't have minor gridlines by default, but you can toggle them using
%    ax.XMinorGrid and ax.YMinorGrid for a specific axis object ax.  
% 
% 2. text:
%    - This corresponds to setting properties like ax.FontName,
%    ax.FontSize, and ax.FontWeight for a specific axis ax. 
% 
% 3. title:
%    - In MATLAB, this would be title('Your Title', 'FontSize', 20).
% 
% 4. panel.background:
%    - This can be set using ax.Color for an axis ax.
% 
% 5. panel.border:
%    - For MATLAB, this is a bit more indirect. You might use a combination
%    of ax.Box (to ensure a box is present) and then setting the ax.XColor,
%    ax.YColor, etc. to modify the color.  
% 
% 6. axis.title:
%    - This refers to axis labels in MATLAB. For example, xlabel('Your
%    Label', 'FontSize', 17). 
% 
% 7. axis.text:
%    - Refers to the tick labels. Can be modified with ax.XTickLabel and
%    properties like ax.FontSize. 
% 
% 8. axis.ticks:
%    - This corresponds to the appearance of the ticks themselves in
%    MATLAB. You can modify properties like ax.TickLength, ax.XColor, and
%    ax.YColor.  
% 
% 9. legend.background:
%    - In MATLAB, you might modify the legend box background using a handle
%    to the legend object. For example, if lgd is a legend handle, then
%    lgd.BoxFace.Color sets the background color.  
% 
% 10. strip.background & strip.text:
%    - In MATLAB, "strip" would most closely be analogous to subplot titles
%    or annotations. This might be handled with title on a specific subplot
%    or with annotation.  
% 
% 
% The exact mapping and implementation will vary based on the specifics of
% what you're plotting in MATLAB and how closely you want to replicate the
% ggplot2 appearance.  
%
% More info:
% In R:
% strip.background = element_rect(fill = simpsons_theme_colors["lighter"], colour = simpsons_theme_colors["border"]),
% 
% 
% - fill: Refers to the background color of the element, which in this
% context is the "strip" or the label area of facets. 
%   
% - colour: Refers to the border color of the element.
% 
% In MATLAB terms:
% 
% - fill corresponds to the BackgroundColor property when you're dealing
% with UI components or the Color property when you're dealing with
% graphical objects like patches or axes.  
% 
% - colour corresponds to the EdgeColor property for graphical objects that
% have borders, such as patches or rectangles. 
% 
% So, if we consider a hypothetical MATLAB scenario where "strip" is a
% rectangle (as a very rough analogy), the translation might be: 
% 
% matlab
% rectangle('Position', [...], 'FaceColor', simpsons_theme_colors.lighter,
% 'EdgeColor', simpsons_theme_colors.border); 
% 
% 
% However, MATLAB does not have a direct "strip" concept like ggplot2's
% facet strips. If you're trying to modify the background and border color
% of subplot titles or other annotations, you'll have to use a combination
% of text, rectangle, and other plotting functions to achieve a similar
% look.    
