% APPLYTHEME applies a theme to the provided figure or current figure
% INPUT:
% - theme_name: Name of the theme (e.g., 'barbie')
% - fig: Figure handle (optional)
% Example:
%   applyTheme('barbie', gcf);

% Attribution to the original theme source.
% Original BarbieTheme from:
% @software{JaneBarbie2023,
%   author = {Jané, Matthew B. and Pilling, Luke C. and Kenny, Christopher T.},
%   month = {6},
%   title = {theme_park: popular culture ggplot themes},
%   url = {https://github.com/MatthewBJane/theme_park},
%   version = {0.0.9},
%   year = {2023}
% }


function applyTheme(theme_name, fig)
    % Check if the figure handle is provided; if not, use the current figure.
if nargin < 2
	fig = gcf;
end

    % Retrieve the theme details based on the theme name.
theme = getTheme(theme_name);
    % Check and set the appropriate font for the theme.
checkAndSetFont(theme);
    % Apply the theme settings to axes and color bars within the figure.
applyToAllOfType('axes', theme, fig);
applyToAllOfType('Colorbar', theme, fig);
end


function checkAndSetFont(theme)
%% Check font
   %% Check font
    % Get the list of available fonts on the system.
	fonts = listfonts;
    % Check if the theme's font is available; if not, revert to default font.
if ~any(strcmp(theme.font, fonts))
	theme.font = get(gca, 'FontName');
	warning(['Font not found. Please download at https://fonts.google.com/specimen/' theme.font]);
end

end


function applyToAllOfType(type, theme, fig)
    % Find all objects of the specified type within the given figure.
all_objects = findobj(fig, 'Type', type);
    % Iterate through each object and apply the theme.
for obj = all_objects'
	if strcmp(type, 'axes')
		applyAxis(theme, obj);
	elseif strcmp(type, 'Colorbar')
		applyColorbar(theme, obj);
	end
end
end

function theme = getTheme(theme_name)
% Define a default theme as a base.
defaultTheme = getDefaultTheme();

    % Retrieve the specific theme based on the theme name.

switch theme_name
	case 'barbie'
		theme = getBarbieTheme();
	case 'alien'
		theme = getAlienTheme();
	case 'futurama'
		theme = getFuturamaTheme();
	case 'gameofthrones'
		theme = getGameOfThronesTheme();
	case 'simpsons'
		theme = getSimpsonsTheme();
	case 'spiderman'
		theme = getSpidermanTheme();
	case 'oppenheimer'
		theme = getOppenheimerTheme();
		% ... Add other themes here ...
		
	otherwise
		error('Theme not recognized');
end

% Override default theme with specific theme properties.
    % Merge the default theme properties with the specific theme properties.
theme = mergeStructs(defaultTheme, theme);
end


function applyAxis(theme,ax)
children	= ax.Children;    % Get all plotted data on the axis
nChildren	= numel(children);
for ii = 1:nChildren
	h		= children(ii);
	switch h.Type
		case 'line'
			h.Color				= theme.colors.dark;
			h.MarkerFaceColor	= theme.colors.dark;
			h.LineWidth			= theme.lineWidth;
		case 'image'
			colormap(theme.cmap);
		case 'patch'
			h.FaceColor	= theme.faceColor;
			h.FaceAlpha = theme.faceAlpha;
	end
end

ax.FontName		= theme.font;
ax.Color		= theme.colorsPanel;
ax.XColor		= theme.xColor;
ax.YColor		= theme.yColor;
ax.FontSize		= theme.fontSize;
ax.LineWidth	= theme.lineWidth;
ax.TickDir		= theme.TickDir;
ax.TickLength	= theme.TickLength;
ax.YGrid		= theme.Grid;
ax.XGrid		= theme.XGrid;
ax.YDir			= theme.YDir;
ax.Box			= theme.Box;
ax.XMinorTick	= theme.XMinorTick;
ax.YMinorTick	= theme.YMinorTick;
ax.Title.FontSize	= theme.titleFontSize;
ax.Title.Color		= theme.titleColor;
ax.XLabel.Color		= theme.xLabelColor;
ax.YLabel.Color		= theme.yLabelColor;
ax.Parent.Color	= theme.parentColor;
if theme.axisSquare
	axis(ax,'square');
end
end


function applyColorbar(theme,ax)

ax.FontName			= theme.font;
ax.Color			= theme.colors.panel;
ax.XColor			= theme.xColor;
ax.YColor			= theme.yColor;
ax.FontSize			= theme.fontSize;
ax.LineWidth		= theme.lineWidth;
% ax.TickLength	= theme.TickLength(1);
ax.Box				= theme.Box;
ax.Title.FontSize	= theme.titleFontSize;
ax.Title.Color		= theme.titleColor;
ax.XLabel.Color		= theme.xLabelColor;
ax.YLabel.Color		= theme.yLabelColor;
ax.Parent.Color		= theme.parentColor;

end



function merged = mergeStructs(A, B)
fn = fieldnames(B);
for i = 1:numel(fn)
	A.(fn{i}) = B.(fn{i});
end
merged = A;
end

