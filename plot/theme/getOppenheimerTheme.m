
function theme = getOppenheimerTheme()

		theme = struct();
theme.colors = struct( ...
	'background' , '#000000',...
    'coolflame', '#fdc232', ...
    'flame',     '#fd8532', ...
    'hotflame',  '#fd4b32', ...
    'panel',     '#222222', ...
    'border',    '#000000', ...
    'lightmain', '#eeb4d7', ...
    'text',      '#393939', ...
    'light',     '#6b6b6b', ...
    'medium',    '#323232', ...
    'dark',      '#000000' ...
);

theme.colors.dark = theme.colors.hotflame;
		theme.fontSize  = 18;
		theme.lineWidth = 1;
		theme.font = "IM FELL English";
% 		theme.cmap = color_gradient(theme.colors.light, theme.colors.dark, 64);
		theme.cmap = color_gradient(theme.colors.coolflame, theme.colors.hotflame, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 24;
		theme.titleColor	= theme.colors.hotflame;
		theme.xLabelColor	= theme.colors.hotflame;
		theme.yLabelColor	= theme.colors.hotflame;
		theme.parentColor	= theme.colors.background;
		theme.axisSquare	= true;
		theme.faceColor		= theme.colors.coolflame;
		theme.faceAlpha		= 0.3;
		theme.xColor		=  theme.colors.coolflame;
		theme.yColor		=  theme.colors.coolflame;
		theme.colorsPanel	= theme.colors.panel;
end
