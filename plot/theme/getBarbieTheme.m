
function theme = getBarbieTheme()

		theme = struct();
		theme.colors = struct(...
			'background', '#ffffff', ...
			'text',    '#a62675', ...
			'panel',   '#fdf6fa', ...
			'border',  '#d74ea2', ...
			'lighter', '#f5d1e6', ...
			'light',   '#eeb4d7', ...
			'medium',  '#d74ea2', ...
			'dark',    '#bf2986');
		theme.fontSize  = 18;
		theme.lineWidth = 1;
		theme.font = "Sansita Swashed";
		theme.cmap = color_gradient(theme.colors.light, theme.colors.dark, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 24;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.background;
		theme.axisSquare	= true;
		theme.faceColor		= theme.colors.dark;
		theme.faceAlpha		= 0.3;
		theme.xColor		=  theme.colors.border;
		theme.yColor		=  theme.colors.border;
		theme.colorsPanel	= theme.colors.panel;
end
