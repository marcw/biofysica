function cmap = color_gradient(low,high, ncolors)
high	= hex2rgb(high);
low		= hex2rgb(low);
r		= linspace(low(1),high(1),ncolors);
g		= linspace(low(2),high(2),ncolors);
b		= linspace(low(3),high(3),ncolors);
cmap	= [r; g; b]';
end

