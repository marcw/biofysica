function theme = getSimpsonsTheme()
		theme = struct();
		theme.colors  = struct(...
			'background', '#f7dc6f', ...
			'text',    '#3498db', ...
			'panel',   '#f7dc6f', ...
			'border',  '#3498db', ...
			'lighter', '#fdfefe', ...
			'light',   '#1e8449', ...
			'medium',  '#9c5b01', ...
			'dark',    '#1c2833' ...
			);
		
		theme.fontSize		= 24;
		theme.lineWidth		= 1;
		% 		https://fonts.google.com/specimen/
		theme.font			= "Rock Salt";
		theme.cmap			= color_gradient(theme.colors.medium, theme.colors.dark, 64);
		theme.TickDir		= 'out';
		theme.TickLength	= [.02 .02];
		theme.Grid			= 'off';
		theme.XGrid			= 'off';
		theme.YDir			= 'normal';
		theme.Box			= 'on';
		theme.XMinorTick	= 'off';
		theme.YMinorTick	= 'off';
		theme.titleFontSize = 26;
		theme.titleColor	= theme.colors.text;
		theme.xLabelColor	= theme.colors.text;
		theme.yLabelColor	= theme.colors.text;
		theme.parentColor	= theme.colors.background;
		theme.faceColor		= theme.colors.dark;
		theme.faceAlpha		= 0.3;
		theme.axisSquare	= true;
		theme.xColor		= theme.colors.border;
		theme.yColor		= theme.colors.border;
		theme.colorsPanel	= theme.colors.panel;
end
